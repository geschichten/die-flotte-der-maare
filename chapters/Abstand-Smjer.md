\BefehlVorKapitelDFDM{Smjer-n.png}{Smjer ist Schiffsmechanikeran und hat das Vize-Kommando über die Flotte der Maare.}{Betäubung, Ableismus, Fischleid impliziert.}

Abstand
=======

\Beitext{Smjer}

Die Schattenmuräne hatte noch einmal an Fahrt verloren, seit
das Tauchdeck wieder unter Wasser stand. So gehörte sich das
für Verdrängerfahrt. Auf diese Weise mussten sie die Segel
bloß etwas fieren, um dem Forschungsschiff nicht davonzusegeln. Die
Menge des Dampfes ließ allmählich etwas nach, aber immer noch
konnte bloß Jentel vom Mastkorb aus genug sehen, um Bericht zu
erstatten. As berichtete nichts Besorgniserregendes. Aber der
Ruf, der aus dem Schiffsbauch der Schattenmuräne an Deck drang, war
es schon. Sindra war drauf und dran, Amira den Befehl zu erteilen,
nachzusehen, aber stattdessen stieg Rash eilig an Deck.

"Jannam sagt, Kamira hat um Hilfe gerufen und Jannam schwimmt
los, weil es dringend klang.", sagte Rash. "Das soll ich ausrichten."

Wenn Rash nicht steuerte, war Rash zuständig für die Kommunikation zwischen den Decks. In
neueren Schiffen, die Smjer konstruierte, gab es ein Röhrensystem, durch
das Schall auch in andere Decks transportiert werden konnte, aber
so etwas hatte die Schattenmuräne noch nicht.

"Sag Myrken, sie möge unter Wasser hören.", befahl Sindra. Myrken
war wie Jannam auch eine der Nixen der Schollencrew. "Du
stellst dich wieder ins Mitteldeck und lässt dir weitergeben, was Myrken
sagt und gibst selbst wiederum an Smjer weiter."

"Sehr wohl.", sagte Rash und stieg den Niedergang postwendend wieder
hinab.

"Aye!", sagte auch Smjer und bewegte sich zum Niedergang. Es brauchte
nicht lang, bis Rash etwas zurückmeldete, was er
weitergab: "Missglückt. Tauchboot sinkt. Mehr Hilfe!"

Sirenu hatte weniger Worte, und über die Distanz mussten sie auch noch
sparsam gewählt werden. Zumindest, wenn die Personen nicht zwischendurch
für Luft auftauchen wollten.

Sindra erkannte sofort, dass dies eine Arbeit für Nixen war. Es waren
außer Smjer noch drei an Bord und Sindra befahl, was die einzige
naheliegende Möglichkeit war:

"Schickt Myrken los.", befahl sie laut brüllend. "Smjer, du übernimmst Myrkens
Position. Jentel Abflug vom Mastkorb, Adune folgt Myrken über das
Tauchdeck. Kanta an Smjers jetzige Position. Amira in den Mastkorb."

Smjer bewegte sich bereits ins Unterdeck, während er noch die wesentlichen
Befehle an Rash weitergab. Er erblickte gerade noch, wie Jentel sich geschickt
auf eine Weise halb vom Mastkorb baumeln ließ, auf die as genug Schwung
bekam, sich über Bord zu katapultieren.

Das Wasser war kalt. Smjer war es nicht mehr so gewohnt. Vor allem nicht
mitten auf dem Ozean. Zuletzt war er ufernäher geschwommen, wo es immer
etwas wärmer war. Trotzdem ließ sich Smjer nicht aufhalten, gut Luft zu
holen und sich unter Wasser abzusenken, wo der Schall ihn erreichen würde. Es
gab keine wesentlichen neuen
Informationen, bis der Nixenschwarm das Tauchboot mühevoll in die Schattenmuräne
schob. Smjer fädelte ein Seil durch die Griffe, das zu einem der
Flaschenzüge gehörte, mit denen er eigentlich die Decks wechselte. Kamira war
sehr außer Atem, aber half ihm dabei, ihn zu benutzen. Noch während ihrer
Bemühungen kletterten Ashnekov und Janasz aus dem Tauchboot. Sie
waren klitschnass, packten ebenfalls mit an und atmeten, als hätten sie länger
keine Frischluft mehr bekommen: Nicht so schnell, als hätten sie
keine Luft bekommen, aber dankbar darum, wieder befreit zu sein.

Marah fehlte.

Niemand sagte es direkt. Es war ja an sich auch offensichtlich. Als das
Tauchboot soweit hochgezogen war, dass das Wasser herauslief, was
nicht hineingehörte, schloss sich Smjer den meisten Nixen, Janasz und
Ashnekov an, wieder an Deck zu gelangen. Die Kapitänin wählte Janasz
aus, mit Berichten anzufangen. Smjer verstand, warum sie Janasz wählte -- als
Person, die die Pfeile schoss, hatte Janasz wahrscheinlich das meiste
mitkriegen können, was passiert war --, aber hätte selbst anders
entschieden. Janasz wirkte
aufgelöst und als könnte er nicht so gut priorisieren, was nötige
Information war. Was zu viel Detail und was zu wenig.

"Sie haben Marah.", sagte er.

Viele Blicke richteten sich auf Sindra. Es war kein Geheimnis, dass
Sindra Marah liebte.

"Mehr Details.", verlangte Sindra, als wäre sie die Ruhe selbst. Ihr
war nichts anzumerken.

So sehr die Situation einen kühlen Kopf verlangte, war Sindras Umgang
mit Emotionen schon sehr ungewohnt. Smjer hatte sie am Anfang für
unempathisch gehalten. Nun wusste er es besser. Es zeigte sich bei
ihr eben eher in Worten als in Körperreaktionen.

"Sie wussten, dass wir kommen.", berichtete Janasz. "Sie haben in
der Bilge in der Dunkelheit gewartet. Ashne und ich sind hineingekrochen, ohne
sie zu bemerken, weil sie leise waren. Dann haben wir Licht angemacht und
sie waren viele." Janaszs Gesicht zeigte Panik, wie als würde er
die Situation noch einmal durchleben. Ihm stockte die Stimme.

"Hast du Personen betäubt?", fragte Sindra. Sie klang eine Spur
strenger als sonst dabei.

Das brachte Janasz aus dem Schweigen wieder heraus. Er schüttelte
den Kopf. "Es hätte wenig Sinn ergeben. Ich hätte nie alle
erwischt."

"Und es hätte verraten, wie wir Personen betäuben.", ergänzte Sindra. "Es
ist gut, dass das vielleicht noch geheim ist."

Natürlich dachten sich die Landsleute der Forschungsschiffe, dass
es etwas mit Betäubung zu tun haben musste, was den Schlaf auslöste. Zumindest
manche, die nicht an Geister oder Schlafzauber glaubten. Aber solange
Janasz immer alle Personen betäubte, die zugegen waren, und die
Pfeile wieder einsammelte, mit denen er es tat, wussten sie nicht wie.

Janasz wirkte erleichtert. Aber die Kapitänin forderte ihn sofort
auf, weiter zu berichten. Das tat er dann auch eilig: "Ashne und ich sind ins Tauchboot
zurückgeflohen, aber sie kamen näher und ich glaube, sie wollten
uns angreifen. Sie waren laut und unkoordiniert.", berichtete
Janasz. "Dann ist Marahs Kopf kurz von unten im Tauchboot
aufgetaucht und sie hat uns befohlen, zu versuchen, die Schleuse von
innen zu schließen und, vor
allem, uns, komme, was wolle, festzuhalten."

Smjer konnte sich denken, wie die Geschichte weiterging. Es war einer
der Momente, in denen er sich wünschte, nicht so oft recht zu haben, und
dass es irgendwo ein Wunder gäbe. Sie Marah vielleicht irgendwie
versteckt hätten und sie später eintreffen würde.

"Janasz hat mich festgehalten, während ich versucht habe, die
Schleuse zu schließen.", übernahm Ashnekov. "Aber lange bevor
ich fertig war, -- was auch sinnvoll war, weil die Forschungscrew
dann doch versucht hat, uns festzuhalten -- , haben Marah und Kamira die
Verbindung zwischen Forschungsschiff und Tauchboot gelöst." Ashnekov
holte tief Luft und seufzte eilig. "Marah hat sich durch die Wassermassen
ins Schiff spülen lassen, oder ist hineingeschwemmt worden, da
bin ich nicht sicher. Sie hat durch einen kräftigen
Schlag mit der Fluke verhindern können, dass Forschungscrewmitglieder
durch das Loch von Bord gingen und es dann von innen verschlossen."

Sindra nickte. Ihr Gesicht wirkte konzentriert, als würde sie
nachdenken.

"Ich konnte das Tauchboot so drehen, dass der Rest Luft nicht
entweichen würde.", fügte Kamira hinzu. "Aber bewegen konnte ich
es alleine nicht, weil es nicht dazu gebaut ist, mit soviel
Wasser darin geschoben zu werden."

Sindra nickte erneut. "Wir halten zunächst Abstand.", kommandierte
sie. "Der vorläufige Plan: Wir sind vor ihnen. Wenn wir die Distanz
halten, können wir sie vielleicht dazu bringen, uns folgen zu wollen. Wir
drehen dabei allmählich unseren Kurs auf West. Das ist vielleicht
ein Kompromiss, auf dem sie uns weiter folgen würden, auch wenn
es sie nicht näher an Grenlannd heranbringt."

Rash hatte wieder das Steuer übernommen, das in der Zwischenzeit
Sindra geführt hatte, und bestätigte den Befehl.

"Und langfristig?", fragte Smjer.

Sindra antwortete nicht nur ihm, sondern laut: "Langfristig haben wir
wie stets das Ziel, dass das Forschungsschiff es nicht nach Grenlannd
schafft. Unsere Angriffsstrategie ist aber nicht mehr möglich. Macht
euch Gedanken und nennt mir alle
Ideen, was wir alternativ machen können, um dieses Ziel zu erreichen."

"Was ist mit Marah?", fragte Kamira.

"Auch Ideen, wie wir Marah retten können, bitte an mich.", kommandierte
Sindra.

In Smjer entfalteten sich grobe Ideen für
die erste der beiden Fragestellungen, wie sie
sich gegen das Forschungsschiff mit einer neuen Strategie wenden
könnten, als ein verzweifelter und wütender Schrei über das Deck
schallte. Smjer seufzte innerlich, als Ushenka über das Deck direkt
auf Sindra zustapfte. Er hatte auch dieses Mal eine naheliegende
Vermutung, was kommen würde, und hoffte, dass es anders käme, auch
wenn er Ushenka in gewissen Dimensionen sogar verstand. Sie hatte
eine innige Beziehung mit Marah und anders als Sindra war sie
weder von Emotionen distanziert, noch drückten sie sich in einer
zurückhaltenden Weise aus.

Ushenka versuchte, beeindruckend zu sein, aber
gegen die riesige Kapitänin mit ihrem gelassenen, stabilen Stand war
so schnell keine Person beeindruckend.

"Du wirst meine Marah retten! Hast du verstanden?", schrie sie Sindra
an.

"Ich werde mein Möglichstes tun.", antwortete Sindra.

Es überraschte Smjer beinahe, dass sie jetzt, ganz am Anfang des
Streits, schon einen eigentlich unmissverständlichem Tonfall
wählte, der hätte klar machen sollen, dass das Gespräch an der Stelle beendet
war. Aber Smjer hätte es gewundert, wenn Ushenka sich davon beeinflussen
ließe, was Sindra für das Ende eines Gespräches hielt. Zumal die
Aussage durch ihre Wortwahl, kannte jemand Sindra weniger
gut, wahrscheinlich auch eher nicht wörtlich sondern abwiegelnd
aufgefasst werden würde.

"Das!", schrie Ushenka und deutete Arme wedelnd
um sich. "Das ist nicht das Möglichste! Ich will
mein Kind wiederhaben! Ich will, dass jetzt und nicht
später alles in Bewegung gesetzt wird, um sie zu retten! Wir werden das
Forschungsschiff plattwalzen und irgendwelches Ehrengedöns
fallen lassen! Verstanden? Es geht hier um Marah!"

"Ich weiß, worum es geht.", sagte die Kapitänin leise und sachlich. Mehr
nicht. Sie stand einfach Ushenka gegenüber und blickte ruhig
auf sie herab, vielleicht sogar etwas bedrohlich.

"Wie kann mein Kind so eine kaltblütige Kapitänin
lieben, der sein Leben nichts, gar nichts wert ist!", fügte
Ushenka hinzu. Sie drehte sich um und stampfte wieder
über das Deck, unschlüssig, wütend.

"Wir werden natürlich den Vorschlag in Betracht ziehen, unsere
Regeln außer Acht zu lassen.", antwortete Sindra verspätet auf Ushenkas
Vorschlag. "Aber selbst, wenn wir uns entschieden, dass wir Leben
nehmen würden, -- was wir vermutlich geschlossen und für jede
Situation ablehnen werden --, wären wir nicht für einen Angriff
oder etwaiges Plattwalzen gerüstet."

Wie Smjer Sindra kannte, war sie blitzschnell bereits sehr viele
Szenarien im Kopf durchgegangen und hatte sie bezüglich Durchführbarkeit
und der Moralvorstellungen der Flotte bewertet. Etwas, woran er sich auch
hatte gewöhnen müssen, und was für Ushenka nicht
transparent sein konnte: Dass Sindra tatsächlich auch abseits üblicher
Wege schnell alles durchdachte.

"Wir haben ein Assassinan!", schrie Ushenka von wo sie inzwischen
stand. "Du kannst mir viel erzählen, aber keine Lügen!"

"Amira ist kein Assassinan mehr!", verkündete die Kapitänin, immer
noch sachlich, aber sehr deutlich. "Würdest du wirklich den Mord
von zwei Dritteln einer Forschungscrew -- so viel bräuchten
wir etwa, um an Bord des Forschungsschiffes nicht ausgeliefert
zu sein -- in die Hände einer Person
geben, die ein Leben lang darunter leiden wird, mit der Tat leben
zu müssen, um eine Person zu befreien, die damit dann auch leben
muss, das etwas für sie getan wurde, wo sie sich mit Leib und
Geist jederzeit zwischenschmeißen würde?"

"Sie ist mein Kind!", schrie Ushenka, sodass Smjer vom Zuhören
die Kehle brannte. "Ich würde alles für sie tun! Alles!"

"Nicht alles offenbar.", widersprach Sindra ruhig, und ohne
weitere Erklärung, als bräuchte es keine. "Das Gespräch
führt uns nicht weiter. Du störst leider wichtige Abläufe an
Deck. Halt dich unter Deck auf, bis
du Vorschläge hast, von denen du dir eher vorstellen kannst, dass
wir sie umsetzen würden."

"Du schickst mich unter Deck?", fragte Ushenka.

"Du bist Befehle noch nicht gewohnt.", antwortete Sindra, beinahe
sanft. "Wir sind in einer Notlage. Du hast mir Folge zu leisten, im
Normalfall ohne zu hinterfragen."

"Darf ich sie begleiten?", fragte Kanta.

Sindra nickte.

Kanta legte Ushenka einen Arm um die Schulter und führte sie den
Niedergang hinab. Noch bevor sie ganz außer Sicht waren, hörte
Smjer Ushenka schluchzen und sah ihren Körper schlaff
einknicken.

Er hatte durchaus eigentlich Mitleid mit ihr, aber er spürte es kaum
noch. Es fühlte sich für ihn so an, als wäre durch Ushenka
entsetzlich viel Zeit verloren gegangen, aber eigentlich war
die Unterbrechung nicht lang gewesen. Nur emotional. Emotionalität
war nicht unbedingt etwas Schlechtes, aber fühlte sich für ihn immer
an, als benötigte sie viel Zeit.

"Meine bisher vielversprechendste Idee ist, sie in die Santenstrudel
zu locken.", schlug Smjer schließlich vor und unterbrach
damit die Stille.

Die Santenstrudel waren irritierende
Strömungen im Meer, das an die Südwüste angrenzte. Die Steppe, wo
die Schollencrew auf sie gewartet hatte. Die meisten
Nixen kannten die Strömung gut. Diese Zwergencrew war aber wahrscheinlich
weniger darauf vorbereitet, weil die Santenstrudel nicht auf ihrem Weg nach Grenlannd
gelegen hätten.

Die Chancen standen schlecht, dass sie sie bis dorthin locken könnten, aber
Sindra gab zunächst entsprechende Befehle. Es würde ihnen Zeit verschaffen, egal
wie kurz auch nur ein Teil des Plans funktionierte. Sindra machte das bereits für die ganze
Crew transparent.

Dann besprachen sie die Lage in einer größeren Runde. Smjer wusste nicht
genau, wie Sindra es anstellte, dass die Crew dabei so diszipliniert
und aufmerksam war. Es herrschte Hochanspannung. Selbst die
Ziege schien gespannt zu lauschen. Einzig Janasz weinte
und sagte hin und wieder etwas wie "Ich habe sie nicht retten können!" oder
"Was sie wohl mit ihr machen?". Die Kapitänin schickte ihn nicht weg.

Aus den Vorschlägen leitete sie recht schnell ab, dass sie Unterstützung
brauchen würden und die Lage sehr schlimm war. Die Zwergencrew hatte
gewusst, wie die Crew der Maare angreifen würde. Das hieß, es wusste wohl mindestens Zarin
Katjenka Bescheid. Das Geheimnis war viel wert. Selbst wenn die Zarin
von der Flotte der Maare als eines der weniger beängstigenden Gegneranen
eingestuft wurde, konnte das Zarenreich durchaus Finanzierung gebrauchen
und das Geheimnis teuer verkaufen. Entscheidungen dieser Art hatte
die Zarin bereits häufiger gefällt.

Es blieb nun einmal so: Die Landvölker, die irgendwo spürbar in der Weltpolitik
mitmischten, waren gefährlich und verhielten sich respektlos.

Sie sandten den Briefwels, der noch an Bord war, Richtung Schattenforelle. Von
dort war am schnellsten Hilfe zu erwarten. Sie baten darum, gleich weitere
Schiffe aus der Flotte zu informieren.

Kamira war gerade vom Tauchdeck zurückgekehrt, wo er den
Wels mit Post in den Ozean entlassen hatte, als Jentel meldete, dass das
ihnen folgende Forschungsschiff stark an Fahrt verlor und dass alles, was
as erkennen konnte, auf eine bewusste Entscheidung hindeutete.

"Sollen wir ebenfalls Fahrt reduzieren?", fragte Rash.

"Sie würden dadurch bemerken, dass wir nicht versuchen, davonzusegeln, sondern
sie locken möchten.", widersprach Smjer.

Sindra nickte schwer. "Reduziert die Fahrt, aber nur soweit, wie das durch
Ungeschick passieren könnte.", schrie sie. "Stellt euch an, wie vor
drei Jahren noch!"

Smjer konnte sich ein Grinsen nicht verkneifen und auch andere
lachten kurz.

Sie hatten ohnehin wenig Raum für gespieltes Ungeschick. Der Abstand wurde
zwar zunehmend langsamer größer, aber gerade als sie überlegten, dass
sie das Forschungsschiff ja länger noch sehen würden als umgekehrt, weil
Jentel sagenhafte Nachtsichtfähigkeit hatte, meldete letzteran, dass
das Forschungsschiff wendete.

"Wenden! Aufholen!", kommandierte Sindra. "Anpeilen, dass wir südlich
von ihnen landen."

Plan B. Sie würden versuchen, ihnen den Weg nach Grenlannd wenigstens
schwer zu machen, sie zu irritieren, die ja im Gegensatz zu
ihnen nicht einmal genau wussten, wo
Grenlannd lag, und irgendwann würde die Schattenforelle zu ihnen
stoßen. Sie könnten mit zwei schnelleren Schiffen das eine langsamere vielleicht
einschüchtern und belagern, bis sie Marah rausrückten und die
Heimfahrt anträten. Es war realistisch genug, um auf Erfolg zu
hoffen, und der beste Weg, den sie hatten.

Aber es kam alles ganz anders. Eigentlich nicht sehr anders, nur
mit vertauschten Rollen: Das Forschungsschiff trat unerwartet früh
den Heimweg an. Vielleicht, weil sie tatsächlich Angst vor diesem
grünen Schiff hatten, das in der Lage gewesen wäre, das Forschungsschiff
zum Sinken zu bringen, indem sie das Loch wieder öffneten.

Es peilte einen bekannten Fischgrund an, einen, in dem verschiedene
weniger hochseetaugliche Angelschiffe des Zarenreiches Netze ins
Wasser tauchten. Ein Ort, den Nixen eigentlich besonders mieden. Vielleicht
wusste die Forschungscrew das, aber vielleicht war es auch einfach nur
die Hilfe, die sie von dort zu erwarten hatte, die sie dorthinlockte. Sindra
wagte sich sehr dicht in den gemiedenen Bereich heran. Mit einem
Schiff wie der Schattenmuräne war dies durchaus nicht so riskant, wie
etwa ein Schwimmausflug dorthin. Aber die Forschungscrew schaffte es,
die Angelschiffe auf ihre Seite zu ziehen. Sie bildeten einen
Halbkreis, den sie versuchten, um die Schattenmuräne zu schließen. Die
Schattenmuräne war zu schnell dafür, aber konnte auf diese Art nicht
mehr dem Forschungsschiff nahe sein.

---

Es war eine schlimme Niederlage, die sie alle spürten, als sie wieder
allein und fern von Küsten im süd-ost-maerdhischen Meer beidrehten und
auf Post oder wenigstens Briefwelse warteten, die sie dann ihrerseits
mit Hilferufen losschicken konnten. Janasz weinte nicht mehr, übergoss
sich stattdessen inzwischen mit Selbstvorwürfen, es wäre alles
seine Schuld.

"Inwiefern?", fragte Sindra sachlich.

Das war so ein Moment der Anspannung, in dem vielen sonst sehr gelassenen
Personen der Geduldsfaden riss und sie das erstbeste Crewmitglied
fertig machten, das gerade nervte.

"Ich hatte ein ungutes Gefühl, bevor wir dieses Mal losfuhren.", begründete
Janasz.

"Das ist normal.", sagte Sindra. "Wir haben an einem Ort angegriffen, an
dem wir sonst nicht angreifen, mit einer anderen Crew und ein für uns
ungewohntes Schiff." Sie lächelte sogar ein bisschen, als sie
das sagte. "Gibt es etwas, was du weißt, was wir wissen sollten?"

Janasz zögerte und schüttelte dann den Kopf. "Nein."

"Dann ist es nicht deine Schuld.", beruhigte die Kapitänin. Sie
seufzte und wandte sich an die Crew: "Das Forschungsschiff wird inzwischen im Hafen von Mizugrad
eingelaufen sein. Zumindest ist das das sicherste und wahrscheinlichste
Ziel.", sagte sie laut. "An Land haben wir keine Macht. Ich habe gerade
keinen Plan und glaube, dies ist eine Situation, die wir mit
der ganzen Crew der Maare bereden sollten."

"Uns kommt entgegen, dass Herbst mit Herbststürmen kommt und
das Landsvolk sich ohnehin weniger zutraut, den Ozean zu überqueren.", erinnerte
Smjer. "Wir können uns ungefährlicher alle gemeinsam treffen und
werden nicht auf viele Forschungsschiffe acht geben müssen."

Dieses war ja nur so überstürzt losgefahren, weil Information
an Land geflossen waren, dass die süd-ost-maerdhische See
derzeit unbewacht sein könnte.

Sindra nickte. "Da es aktuell nichts gibt, was wir tun können, schlage
ich vor, dass wir versuchen, uns auszuschlafen.", sagte sie.

"Als ob ich schlafen könnte.", grummelte Ushenka, die inzwischen
wieder an Deck war.

Die Kapitänin ging nicht darauf ein. Sie selbst trank noch einen Tee
an Deck, um Abstandgewinnen zu ermöglichen, wie sie sagte. Smjer und
Rash setzten sich zu ihr. Beides Leute, mit denen er gut schweigen
konnte, und das taten sie im Wesentlichen auch.

---

Smjer hatte ohnehin unruhig geschlafen, sodass er sich zunächst
nicht sicher war, ob ihn wirklich Sindras Brüllen aus dem Schlaf
gerissen hatte, oder ein Traum.

"Alle zurück auf ihre Posten!", brüllte Rash, den Befehl ins Unterdeck
weitergebend.

Es war also kein Traum. Das war kurzer Schlaf gewesen. Als Smjer
an Deck gelangte, waren die Segel bereits voll gesetzt.

"Kurs Raum!", befahl Sindra.

Das war ein Befehl, der nicht besagte, in welche Richtung sie segeln
würden, sondern wie sie sich zum Wind ausrichteten, nämlich so, dass der
Wind von schräg hinten kam. Es war der schnellste Kurs. Sindra wollte
Fahrt aufnehmen, um eine Bedrohnung abzuhängen, und sie wollte dazu
das Tauchdeck leerfahren. Dafür war eine gewisse Mindestgeschwindigkeit
nötig.

"Es wird nicht reichen, bei der Flaute.", brummte Smjer.

Sindra nickte. "Kaum.", sagte sie leise. "Ideen?"

Smjer blickte sich um. Wenn Sindra darauf setzte, das Tauchdeck
leerzulenzen, dann mussten etwaige verfolgende Schiffe für Landschiffsbau
ungewöhnlich schnell sein. Und ihm stockte einen Augenblick der Atem, als
er das Schiff sah: Es war die Schattenscholle. Kein Schiff aus dem
Landschiffsbau, sondern das verschollene Schiff der
Flotte.

Dieses Landsvolk wusste nicht, wie
Segel auf so einem Schiff richtig gestellt wurden. Bei viel Wind hätten sie
es wahrscheinlich mit geschlossenem und Luft befülltem Tauchdeck leicht
aus Versehen gekentert. Aber es war wenig Wind. Zu wenig Wind, um
ihr eigenes leerzusegeln. Und auf diese Art war die Schattenmuräne
langsamer, hatte zu viel Wasserwiderstand. Auf dem Deck standen
mit Messern und anderen Schneid- und Spießwaffen gerüstete Zwerge in
Bereitschaft, soweit Smjer das erkennen konnte, aber noch trennte
sie eine brauchbar große Distanz.

"Übergib mir das Kommando.", sagte Smjer sachlich.

"Smjer übernimmt das Kommando!", brüllte Sindra ohne zu zögern
über das Deck. "Seine Befehle sind meine Befehle!"

Er hörte Rashs Echo im Unterdeck. Ein kurzes Gefühl der Bewunderung
durchströmte ihn. Sie hatten es einmal abgesprochen, wann
Smjer Sindra das Kommando abnehmen würde. Sie hatte auch damals
nicht gezögert, es zu akzeptieren, aber sich alles genau
erklären lassen. So genau, wie er bereit gewesen war, darüber
zu reden. Nun hatte er eher damit gerechnet, aber sie
hatte auch jetzt nicht mit der Wimper gezuckt.

"Rash, nimm die Ziege unter Deck!", befahl Smjer.

Rash wäre die einzige Person, die in der Lage sein würde, Aga
beizubringen, dass das nun eine notwendige und
unumgängliche Sache wäre. Alle anderen Crewmitglieder
außer Aga würden seinen Befehlen rasch Folge leisten
können, daher war dies der zeitkritischste und erste, den
er erteilte. Und Rash wäre gleichzeitig in der Lage,
Befehle weiterzugeben.

Während Rash sich um die Ziege kümmerte, so liebevoll, dass
es Smjer berührte, zählte er Personen. "Wir tauchen
ab.", informierte er die Crew. "Und wir sind mindestens eine
Person zu viel. Es wird sehr eng. Kanta?"

Kanta trat vor, schloss die Augen, öffnete sie wieder
und bebte vor Angst.

"Ich male mir für dich die besten Chancen aus, dass du
von den uns Folgenden aufgelesen wirst und dann vertreten
kannst, dass du nicht zu uns gehörst.", informierte er.

Kanta war relativ groß, aber vielleicht nicht groß
genug. Vielleicht müsste er noch eine
Person fragen. Es war eine furchtbare Situation. Aber er
war geschult zu tun, was musste.

"Ich gehöre zu euch.", sagte Kanta fast tonlos.

Smjer ging nicht darauf ein. "Du weißt, wie Forschung
funktioniert und kannst glaubhaft behaupten, dass du
von uns bei einem Überfall einer anderen Forschungscrew
gefangen genommen worden wärest.", fuhr
er fort. "Wir können dich in Marahs Bootsschale aussetzen
und du hast die besten Überlebenschancen von allen
Fußpersonen an Bord damit. Du kannst außerdem gut lügen."

Kanta war eine mutige Person. Smjer konnte ihr fast dabei
zusehen, wie in ihr seine Idee Form annahm. Aber dann
zerbrach die frische Überzeugung wieder und wandelte
sich in Entsetzen um: "Ich bin weniger
wichtig als die Ziege?"

"Die Ziege hat nie einer Person etwas getan und wird es auch
nicht.", schrie Smjer.

Kanta zuckte zusammen. Überzeugt schien sie nicht. Und Smjer wünschte
sich, er hätte doch nicht sie gefragt, weil er gerade Wut auf sie hatte
und Wut sollte kein Antrieb sein, aus dem eine Person um so
einen Einsatz für die Crew gebeten würde.

"Ich habe das Kommando nicht mehr. Darf ich einen Vorschlag
unterbreiten?", mischte sich Sindra ein.

Smjer rollte fast die Augen, aber stimmte schließlich zu. Sindra war
nicht übereilt mit Entscheidungen, nie gewesen. Er kannte sie
bloß noch nicht in einer Position, in der sie nicht Befehlsmacht hatte.

"Ich würde gern gehen.", sagte sie. "Ich weiß, das klingt selbstaufopfernd, aber
das ist es nicht: Ich kann die Jolle sogar auch segeln und steuern, das habe ich
rudimentär gelernt. Und ich bin groß."

Deshalb würde es mehr Platz geben. Der Platz würde reichen. Er hätte vor
der Aufnahme der Schollencrew ohne Probleme für alle an Bord gereicht, aber
für zwei Crews war die Schattenmuräne eigentlich nicht ausgelegt. Es
ergab leider Sinn. Smjer nickte. "Aye."

---

Obwohl er das Manöver nur während der Landcrew-freien Einweihungsfahrten geübt hatte, tat
er es fast wie im Schlaf. Stand neben sich. Es verlief trotzdem sehr
präzise und so, wie es sollte:

Der fußlastige Teil der Crew wurde ins Nixendeck gepfercht, wo sie eng
aufeinander hockten. Die Segel wurden eingeholt und auf bestimmte
Weise über das Deck gebunden. Dann löste er den Mechanismus aus, der
das Bootsmaterial zum Leben erweckte. Es schloss sich und zog sich
zusammen, wie eine Schnecke, die sich ins Haus verkroch, und vielleicht
war das Bootsmaterial auch fern verwandt mit Schnecken. Die Dichte des
Materials erhöhte sich, bis die Ladung schwer genug war, die Schattenmuräne
trotz des darin eingeschlossenen Luftraums unter Wasser zu ziehen. Alle
Nixen blieben außerhalb des Schiffsrumpfs und schoben die Schattenmuräne unter Wasser
an. Davon. Wasserdampf hatten sie auch dabei aufsteigen lassen, sodass
die Crew, die sich nun auf der Schattenscholle befand und ihnen folgte, nicht
wusste, wohin sie verschwunden sein könnten und sich das alles, wie
immer, am besten mit Magie erklären ließe.

Und mitten aus dem Dampf heraus segelte Sindra in der für sie viel zu
kleinen, kippeligen Jolle und gab der Schattenscholle ein neues
Ziel. Smjer sah den tief liegenden Rumpf der kleinen Jolle von unten. Wäre er nicht
unter Wasser gewesen und hätte die Luft ohnehin angehalten, hätte
er es spätestens jetzt gemacht, als er den letzten Zeichen
seiner Kapitänin nachblickte, die ihn über vier Jahre hinweg mit vollstem
Respekt befehligt, die schlimmsten Zeiten mit ihm
durchgestanden und auf ihre Art Mut und Hoffnung verbreitet
hatte.
