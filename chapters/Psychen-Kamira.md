\BefehlVorKapitelDFDM{Kamira-n.png}{Kamira ist an Bord für Konfliktmanagement, psychischen Beistand und Barrierabbau zuständig. Er hat am Anfang den Nixen-Anteil der Crew zusammengesetzt und Smjer gefragt, ob er das Vize-Kommando übernehmen würde.}{Rassismus.}

Psychen
=======

\Beitext{Kamira}

Das Nixendeck hatte im Heck einen Raum, der für Psychohygiene
gedacht war und dafür einen Rückzugsort bot. Psychohygiene war
nicht bei allen ein beliebtes Wort dafür. Manche nannten es eher
Seelenpflege oder Therapie, mentalen Beistand oder vereinfacht
Konfliktgespräche. Kamira selbst hatte
da keinen Vorzug, welchen Namen andere
dem gaben. Er fand es viel mehr auch interessant. Der
gewählte Name für seine Arbeit verriet etwas über die Bedürfnisse
aber auch die Scham der Crewmitglieder, die zu ihm kamen.

Es war eine spannende Arbeit. Sie brachte Herausforderungen
und sehr verschiedene Anforderungen mit sich. Und es war
auf einem engen Schiff mit inzwischen elf
Personen eine sehr nötige, damit das Miteinander funktionieren
konnte. Elf waren sie mit Aga, aber die Ziege war wenig überraschend
noch nie zu ihm gekommen.

Dafür sollte heute etwas anderes Überraschendes passieren: Amira. Amira
war noch nicht lange an Bord, aber es war an Kamira nicht
vorbeigegangen, dass sie viel aufzuarbeiten hatte, und auch, dass
im Moment alles viel auf einmal für sie war. Er hatte ihr
möglichst unaufdringlich sein Aufgabenspektrum
an Bord erklärt und ihr dabei versucht, Mut zu machen, zu
ihm zu kommen, wenn sie soweit wäre und glaubte, dass er
helfen könnte. Aber er hatte ihr auch versichert, dass
es dazu keinerlei Verpflichtungen
gab und sie nie unter Druck gesetzt werden würde, das Angebot
in Anspruch zu nehmen. Sie hatte -- kaum verwunderlich -- überfordert
gewirkt, aber gefasst reagiert und gesagt, dass sie darüber
nachdenken würde. Diese Reaktion auf seine Vorstellung war häufig. Wenn Personen mit
dem Konzept einer Therapie noch nie in Berührung gekommen waren,
brauchten sie oft lange, bis sie sich dafür entschieden, wenn
sie es taten. Viele legten den Gedanken erst einmal zur
Seite, stellten fest, dass das Konzept für sie nicht passte, oder
kamen wieder darauf zurück, wenn sie sich eingelebt
hatten. Umso überraschter war Kamira als Amira nun, gerade etwa eine
Woche an Bord, schon bei ihm klopfte.

"Was kann ich für dich tun?", fragte Kamira vorsichtig, als sie
unsicher in der offenen Tür stand. Sie rührte sich nicht
direkt, also sagte er noch: "Wenn du magst und dich damit
wohl fühlst, komm rein und schließ die Tür hinter dir."

Amira folgte dem Vorschlag und blickte sich unsicher im Raum
um. Er war dunkel gehalten, entsprach überwiegend dem, was
Nixen für gewöhnlich als gemütlich empfanden, aber ein
paar Einrichtungsgegenstände versuchten auch den Bedürfnissen
anderer Völker gerecht zu werden: Der Boden war fast überall
mit weichem Material ausgelegt, mit Gummerlatech -- einem
dehnbaren, festen, wasserbeständigen Stoff, der im Meervolk etwa
so verbreitet war, wie Wollstoffe unter Landvölkern --, das sich an den Wänden zu
Hügeln hochtürmte. Nixen lehnten sich daran gern an, besonders
an dem weichen, dicken Gummerlatech, das hier verlegt
war. Die Rückwand, die das Heck des Schiffs bildete, bestand
aus semitransparentem Material, sodass das Meer dahinter erahnt
werden konnte. Es fiel sachte gelblich-grünliches Licht ins
Zimmer. Es gab aber auch einen Tisch, zwei Stühle und eine
Kiste. Kamira saß ungern auf dem Stuhl, aber diese Art zu
sitzen führte manchmal dazu, dass Landsleute sich wohler
fühlten, also tat er es in manchen der Sitzungen doch.

"Es gibt sehr viele verschiedene Bedürfnisse, sich zum
Reden niederzulassen. Manche reden auch lieber im Stehen.", leitete
Kamira daher ein. "Wenn du zum Reden hier bist, mach es dir
so gemütlich, wie du dich am wohlsten fühlst und ich passe mich dir
an. Sag mir auch gern, wie du am liebsten hättest, dass ich mich
platziere."

Amira grinste plötzlich. "Die Kapitänin sitzt bestimmt im
Schneidersitz oder wenigstens angewinkelten Beinen auf dem Boden."

Das stimmte. Kamira beschloss, dass das keine zu private
Information war und bestätigte es mit einem Nicken, das Lächeln erwidernd.

"Ich bin insgesamt sehr unsicher, was ich genau möchte.", sagte
Amira. "Ich habe Angst, dir Zeit zu stehlen. Ich habe Angst, dass
du denkst, dass ich hier bin, um über meine Vergangenheit als
Assassinan zu reden. Aber ich möchte vielleicht über etwas anderes
reden und es kommt mir sehr banal vor."

"Das klingt, als hätte es dich einiges an Mut gekostet, bei
mir zu klopfen.", folgerte Kamira. Der Gedanke war ihm nicht
erst jetzt gekommen.

Amira nickte. Sie ließ sich schließlich in einen Schneidersitz ihm gegenüber
nieder, nicht allzu dicht und eher mittig im Raum, wo der Boden
am wenigsten bequem war. Es wirkte nicht so, als hätte sie
versucht, sich selbst wohl zu fühlen. Aber vielleicht war es
trotzdem gerade das Optimum, weil sich mehr Gemütlichkeit
zu genehmigen, ihr zu unangenehm gewesen wäre. Das war zumindest
bei vielen der Fall, die das erste Mal zu Kamira kamen.

"Du bist hier mit allen Anliegen richtig.", versicherte Kamira. "Es
geht hier immer um dich, nicht um mich. Es geht nicht darum, dass
ich bestimmte Erlebnisse von dir durcharbeiten möchte, sondern
meine Aufgabe ist, dir Werkzeuge für die Probleme zu geben, von
denen du entscheidest, dass sie gerade dran sind, damit du es
zum größten Teil selber kannst."

"So etwa hat Sindra mir das auch erklärt.", sagte Amira.

Ihre Anspannung ließ trotzdem nicht nach. Kamira war nicht so
gut darin, das zu erfühlen, wie beispielsweise Rash, aber
er bekam es doch mit. "Für einen Vertrauensaufbau ist es
vielleicht auch gar nicht verkehrt, wenn wir über Probleme
reden, die dir weniger gefährlich oder leichter vorkommen.", fuhr
Kamira fort. "Oder eben welchen, die konkret gerade anliegen. Und
wenn dein Problem ist, dass du dich erst einmal in die
Situation hineinfühlen musst, mit mir in diesem Raum zu
sein und wir heute gar nicht reden, ist das auch in Ordnung."

Ein schmales Lächeln trat in Amiras Gesicht. "Danke." Und
verblasste wieder.

Sie schwieg, bis das Schweigen so lang wurde, dass es
Kamiras Erfahrung nach den meisten unbehaglich wurde. Er
befürchtete, dass sie sich unwohl fühlen könnte und deshalb
gehen würde, noch bevor sie geredet hätte, obwohl sie eigentlich
wollte. Nun hatte
sie den Mut gefasst, zu klopfen. Wie wahrscheinlich wäre, dass
sie ihn wieder fassen würde, wenn sie sich beim ersten
Mal nichts zu sagen getraut hätte?

Kamira konnte nicht einschätzen, wie wahrscheinlich dieses
Szenario war.

Als Amira anfing, sich nervös über die Kleidung zu streichen, beugte
sich Kamira nach hinten zur Kiste, um daraus eine kleinere
Kiste mit einer Auswahl an Spielzeug zu entnehmen. Die Sammlung bestand aus
Dingen zum Drehen, Reiben oder Drücken, die die Finger
beschäftigten. Amira blickte es einen Moment skeptisch an, aber
die Skepsis verflog so rasch, wie Kamira es selten beobachtet
hatte. Amira ließ sich auf das Spielzeug ein, probierte es
aus und entschied sich schließlich für eine kreisrunde,
kleine Scheibe auf Blasentangbasis, bei der es viele Ausbuchtungen
gab, die sich mit den Fingern einigermaßen lautlos in die
eine oder die andere Richtung drücken ließen. Sie beschäftigte
die Finger damit, bis sie nach einer kurzen Zeit schon gewöhnt
daran waren und die Aktivität fortführten, ohne dass sie
hinsah. "Es funktioniert!", sagte sie, runzelte dann aber die
Stirn. "Also, kommt darauf an, welche Funktion es haben soll. Sollte
etwas mit mir passieren, wie, dass ich hypnotisiert bin
oder so etwas? Ein anderer Teil meines Bewusstseins spricht?"

Kamira grinste und schüttelte den Kopf. "Es hat verschiedene
Funktionen bei verschiedenen Personen.", antwortete er. "Für
manche nimmt es einfach Nervosität. Bei manchen kontrolliert
es andere Gefühle. Manche können besser
reden, wenn ihre Finger beschäftigt sind. Manchen sind
ihre Hände in Gesprächen plötzlich sehr bewusst und sie stören, bis
sie eine Aufgabe haben. Manche Gehirne sind einfach unfassbar
dauerunterfordert und brauchen eine gezielte Dauerstimulation. Manche
Gehirne sind sehr kreativ, und würden sie nicht mit einer kleinen
Aufgabe beschäftigt werden, könnte der Rest der Gehirnaktivität
nicht bei einer Sache bleiben und würde sich stattdessen
verselbstständigen." Kamira hielt inne und reflektierte, ob letzteres
mit ihm gerade passierte. Aber das war es nicht. Er hatte
den Eindruck, dass es Amira helfen würde, wenn sie zum einen
sehr genau wüsste, welchen Zweck etwas hatte, und zum anderen, sie
darüber endlich ins Gespräch kamen.

"Ich weiß nicht, wer ich bin.", sagte Amira. Wie aus dem nichts. So
als hätte sie es nicht beabsichtigt, erschreckte sie sich selbst
ein wenig. "Das habe ich so nicht sagen wollen. Es hat damit zu
tun, was ich sagen wollte, aber das ist mir rausgerutscht."

"Möchtest du, dass ich es wieder vergesse?", fragte Kamira. Da
bestand eigentlich wenig Hoffnung. Kamira hatte ein sehr gutes
Gedächtnis. Aber er konnte ein relativ brauchbares Äquivalent
dazu: Er konnte sich so verhalten, als wären ihm bestimmte
Informationen nicht bekannt. Er würde ihr dies transparent
kommunizieren, sollte sie die Frage bejahen.

Amira schüttelte den Kopf. "Ich würde trotzdem gern zunächst
über Regeln reden."

Das war interessant. "Welche Regeln?" Nicht nur das Thema fand
Kamira interessant gewählt, sondern auch, dass sie den Mut hatte, selbst
eine Priorität auszusprechen.

"Ich wusste, bevor ich hierher kam, nicht, was Privatsphäre ist.", erklärte
Amira. "Ich bekomme die ganze Zeit mit, was alle hier an Bord so
machen, wo sie jeweils sind. Ich bekomme Inhalte mit, die nicht
für meine Ohren bestimmt sind. Glaube ich. Und dann wiederum weiß
ich auch nicht, was eigentlich normal ist. Was eigentlich
alle mitkriegen. Verstehst du?"

Kamira nickte. "Das erschließt sich mir.", sagte er. "Es
wird viele soziale Regeln geben, beziehungsweise
Bedürfnisse, für die dir ein Gefühl fehlt." Kamira
nahm sich selbst ein anderes Spielzeug, ein prinzipiell
ähnliches, das aber etwas größer war, und ließ die Finger
darüber gleiten. "Falls dich das beruhigt: Es gibt kulturelle
Unterschiede zwischen den meisten von uns. Wir alle wissen
nicht ganz genau, was für die jeweils anderen Personen
normal ist.", sagte er. "Was nicht so klingen soll wie: Dein
Problem haben doch alle hier. Ich glaube dir, dass dein
Problem noch einmal anders ist. Die Umgebung macht es
nur deshalb vielleicht leichter, daran zu arbeiten."

"Ich möchte gern lernen, mich so zu verhalten, dass andere
sich mit mir wohl fühlen.", fasste Amira ihr Anliegen
zusammen. "Kannst du mir dabei helfen?"

"Ja, das kann ich.", versicherte Kamira. "Zunächst
einmal gibt es verschiedene Ansätze, dieses Anliegen
anzugehen, und das wird dich vielleicht überraschen: Du
kannst von der einen Seite darangehen und Verhaltensregeln
lernen. Ich würde da gern, wenn du magst, von dir an
das Problem aber nicht nur von einer Regelseite aus
herangehen, sondern auch über das Verständnis: Darüber, wie
sich für andere Privatsphäre anfühlt und dass du sie auch
haben darfst." Kamira wartete ein Nicken ab, bevor
er ergänzte: "Du kannst aber gleichzeitig auch von
der anderen Seite herangehen. Du kannst, wenn du dich
damit wohl fühlst, deine Schwierigkeiten der Crew gegenüber
kommunizieren und dir wünschen, dass du darauf angesprochen
wirst, wenn dein Verhalten Personen unangenehm ist. In
dieser Crew wirst du da überwiegend auf Verständnis stoßen."

Amira runzelte die Stirn. "Werde ich
dann nicht noch eher zu einer Last? Indem ich auch noch
andere auffordere, mir zu helfen, an meinen Fehlern zu
arbeiten?"

"Ein Wunsch und eine Aufforderung sind noch einmal zwei
verschiedene Dinge, doch ich verstehe, was du meinst.", antwortete
Kamira. "Es geht, wenn du anderen von deinen Schwierigkeiten
erzählst, aber nicht darum, dass du weniger bereit wärest, an dir
zu arbeiten, und auch nicht darum, dich zu entschuldigen. Sondern
es geht darum, anderen zu vermitteln, woran sie mit dir
sind. Nämlich an einer Person, die etwas zum aktuellen
Zeitpunkt nicht kann, aber lernen möchte, und deren
Hürden, dies zu lernen, höher sind, als die der meisten
Personen."

Amira nickte langsam. "Dass andere dann wissen, woran sie
sind, klingt fair ihnen gegenüber. Der Rest klingt für
mich nach Rechtfertigung."

Kamira schüttelte den Kopf. "Das verstehe ich, aber
es gehört zur Einordnung dazu.", sagte er. "Sie sind mit dir
nicht an einer Person, die Hintergründe erklärt, um es
leichter zu haben, sondern an einer Person, die ihre
Situation transparent macht und lernen will. Du
gibst ihnen damit die Möglichkeit und Freiheit, dir
zu helfen, wenn sie möchten. Das könnten sie sonst nicht, weil
sie nicht wissen, was du brauchen könntest."

Amira wechselte das Bein, das unten lag. Ihr Blick wanderte
noch einmal durch den Raum, als würde sie etwas suchen, aber
nicht finden. Sie nickte abwesend.

"Möchtest du doch anders sitzen?", fragte Kamira. "Die
Polsterung an den Wänden ist weich."

Amira ließ sich noch einige Momente Zeit. Aber dann stand sie
doch auf, bewegte die Spielzeugkiste so, dass sie auch nach ihrem Umzug
immer noch zwischen ihnen stand, setzte sich auf eine weichere Stelle
an den Hügel und lehnte sich dagegen. Ihre Distanz hatte
sich dabei kaum geändert.

Kamira wandte seinen Körper ihr zu und
lächelte. "Nun sitzt du fast wie die meisten Nixen, die
mich besuchen. Fühlst du dich so besser?"

Amira nickte und lächelte. Kamira sah es ihr förmlich an. Eine
bequeme Sitzhaltung machte für Gespräche über Psyche einfach so viel
aus. "Ich glaube, ich bin es nicht gewohnt, meine Bedürfnisse zu
äußern und einen Sinn darin zu sehen."

"Oder dir auch nur zu erlauben, welche zu haben.", ergänzte
Kamira.

Amira nickte. "Auf der anderen Seite habe ich mich umgesetzt
und bin jetzt hier."

Kamiras Lächeln auf diese Feststellung hin war sehr breit. "In
der Tat.", sagte er. "Es mag seltsam für dich klingen, aber
das ist eine enorme Leistung. Ich weiß, dass es sehr viel
Mut kostet."

"Ich habe in den letzten Tagen so viel gemacht, was sich für
mich vorher so angefühlt hat, als würde ich es nicht können, als
wären es Unmöglichkeiten.", sagte Amira. "Und seltsamerweise
war hier zu klopfen eines der schwierigsten Dinge. Schwieriger
noch, als der Moment, in dem ich", Amira stockte, machte eine
Pause. Und führte dann zu Ende, womöglich anders als
geplant: "aufgehört habe, Assassinan zu sein."

"Wie fühlst du dich jetzt?", fragte Kamira.

"Überfordert mit allem.", sagte Amira. "Ich möchte mich
selbst sortieren, bevor ich mit dir darüber rede."

Kamira nickte. "Komm zu mir, wann immer du dich danach
fühlst, mit genau dem, was du bringen möchtest. Ich
glaube, ich habe durch den Vorschlag, dass du
deine Schwierigkeiten kommunizieren kannst, etwas
abgelenkt.", sagte er. "Ich mag den Exkurs noch kurz
zu Ende führen: Es ist ein Vorschlag für dich, damit
du darüber nachdenken kannst, ob es sich richtig für dich
anfühlt. Es ist kein Weg, der für alle der richtige
Weg ist, und ich habe das nicht für dich zu entscheiden. Du
zeigst damit einen privaten, verletzlichen
Teil von dir. Mach das nur, wenn du dich damit wohl
und sicher fühlst."

"Ob ich mich je wieder sicher fühlen werde?", fragte Amira
nachdenklich.

Kamira beschloss, es als rethorische Frage aufzufassen. Amira
wäre nicht hier, wenn sie sich nicht auch sicher genug
dafür fühlen würde. Und doch
verstand er sie. Sich sicher genug fühlen, um etwas zu
tun, und sich sicher fühlen, waren eben zwei sehr verschiedene
Dinge. Vom Anfang eines Prozesses, die eigene Psyche
auseinanderzunehmen, bis wieder ein Zustand erreicht war, der
sich brauchbar stabil anfühlte, vergingen nicht selten
Jahre. "Vielleicht passt zu der Frage sogar, wenn
wir nun über das Konzept von Privatsphäre reden. Ein
Konzept, dass auf einem engen Schiff ohnehin ein komplexeres
Thema ist, als anderswo. Magst du?"

Amira nickte. "Es fängt damit an, dass ich hier vor der
Tür stand, aber es ist ja das Nixendeck. Und ich bin keine
Nixe. Das Nixendeck ist für mich eigentlich nicht erlaubt."

"Dieser Raum und der Niedergang zum Unterdeck sind davon
ausgenommen.", erklärte Kamira. "Viele an Bord bezeichnen
den Schlaf- und Gemeinschaftsraum der Nixen als Nixendeck. Aber
du hast Recht, das ist missverständlich."

---

Kamira beschwerte sich nie über seine Arbeit, auch nicht darüber, dass
sie manchmal viel war. Trotzdem fühlte er sich erleichtert, als
Smjer seinen Termin für heute absagte. Smjer hatte wöchentlich
drei Termine, sagte aber meistens zwei davon ab. Sie hatten es so
vereinbart, weil Smjer sich nicht selbst dazu organisiert bekam, einen
Termin spontan zu vereinbaren, wenn er einen brauchte. Sie
verschoben auch entsprechend viele Termine, wenn eine andere
Person gerade Bedarf hatte. Es war ein psychologischer Trick, aber
Tricks waren ja nicht verboten. Im Gegenteil.

Heute hätte Kamira mit Smjer gerechnet. Smjer hatte ihm in ihrer
letzten Sitzung erzählt, dass er sich um die Crew der Schattenscholle
sorgte. Marah war noch nicht wieder zurück. Smjer war angespannt. Aber
zum Anfang seines Termins war er in ein Kartenspiel mit Rash
vertieft gewesen, das ihn abgelenkt hatte. Er hatte es kurz unterbrochen, um
mit Kamira abzusprechen, ob Ablenkung nicht auch einfach eine
gute Sache war. Ablenkung war auch nicht verboten. Im Gegenteil.

Und schließlich hatte am späten Abend Sindra noch einen Termin bei
ihm. Die Kapitänin war wenig überraschend viel beschäftigt. Sie
hatte diesen Termin mit ihm vereinbart, damit sie nicht auf die
Idee käme, irgendetwas anderes wäre zu dem Zeitpunkt wichtiger. Kamira
konnte nicht leugnen, sich auf das Gespräch mit Sindra sehr zu
freuen. Sie sprachen viel zu wenig miteinander, dabei hatten sie
über die vier Jahre auf der Schattenmuräne, die sie nun miteinander
verbracht hatten, eine gute Freundschaft entwickelt.

Sie brachte Tee mit, setzte sich gelassen auf den Boden, die
Beine angezogen und zu einer Seite gekippt. Sie beobachtete
den Dampf des Tees, als sie für sie beide Becher eingoss.

"Die Porzellantassen wurden neulich eingeweiht.", teilte
sie mit.

Kamira lächelte. Er hatte sich damals, als Ashnekov und Janasz die
Kiste für die Verladung ins Tauchboot abgeliefert hatten, schon etwas gewundert,
warum sie so klapperte. Sie hatte zwischen den Lebensmittelkisten
und -säcken gestanden. Die Tassen und die Kanne
waren auf diese Art aus Versehen an Bord gekommen und sie hatten sie
in die Kapitänskajüte geräumt. Vielleicht hätten sie sie irgendwann
zurückgegeben, wenn die Überfälle nicht zunehmend gefährlicher
geworden wären. "Wie ist es dazu gekommen?"

"Amira hat mir Tee gekocht, bevor sie mich bedroht hat.", berichtete
die Kapitänin.

Kamira nahm einen der Becher entgegen und lehnte sich mit einem
Ellenbogen in die weichen Deckenmatten an der Wand. Sindra
war die einzige Person, die Kamira kannte, die sich durch
nichts daran hindern gelassen hatte, es sich gemütlich zu
machen. Dann wiederum hatte sie auch erklärt, dass es etwa
80% ihrer Gehirnkapazität auslastete, wenn sie es nicht täte, die
dann nicht für Gespräche aller Art zur Verfügung stünden. So
hatte sie sich ausgedrückt.

"Es geht mal wieder um Emotionen.", leitete Sindra ein. "Es
kommt außerdem etwas hinzu, was ich noch nie angesprochen
habe: Ich habe Angst, dass meine mangelden Emotionen auf
meine Herkunft zurückgeführt werden."

Über ihre Herkunft hatte Sindra nie etwas erzählt. Kamira hatte
nie weiter nachgebohrt, weil sie es mied, darüber zu reden. "Ist
es in Ordnung für dich, wenn ich einmal aufräume?", fragte
Kamira.

"Ich bitte darum.", antwortete Sindra.

"Du hast keinen Mangel an Emotionen. Du drückst sie zum
einen anders aus und zum anderen hast du eine andere
Verteilung an Emotionen.", hielt Kamira fest. Sie hatten
schon oft darüber geredet und über die Zeit wusste er,
was Sindra wiederholt hören wollte, oder musste. "Du drückst
deine Emotionen nicht typisch aus. Deine natürliche Reaktion
auf eine plötzliche Änderung ist nicht Panik, sondern
eine Extra-Portion Gelassenheit. Wo andere eine intensive Wut
spüren, hast du das Bedürfnis, konkret mitzuteilen, was
sich ändern muss und warum. Zuneigung spiegelt sich bei
dir meistens nicht darin wieder, dass du Personen in den
Arm nehmen willst oder ihnen nahe sein willst, sondern darin, dass
du ihnen Wünsche erfüllst."

"Ich erfülle allen gern Wünsche. Wenn ich kann.", wandt Sindra
ein.

"Du liebst alle, würde ich behaupten.", erklärte Kamira schlicht.

Sindra wirkte kurz nachdenklich und nickte dann. "Aye.", sagte
sie. "Da hast du wohl geschickt recht."

"Du hast weniger Trauer, Angst, Panik und Ekel als andere. Du
hast vielleicht weniger Wut, aber es kann auch sein, dass deine
Schwelle zur Wut eine andere ist, oder, wie gesagt, dein Ausdruck
von Wut nicht Lautsein ist.", fuhr Kamira fort. "Das heißt aber
nicht, dass du weniger emotional wärest. Was spürst du, wenn du
dir den Dampf über dem Tee anschaust?"

Sindras Blick wanderte unvermittelt zur Oberfläche des Tees
in ihrem Becher. Sie blies sachte darüber. Und es hatte
sehr sicher nicht nur den Zweck, dass der Tee dadurch kühler
würde. "Ich weiß es ehrlich gesagt nicht.", murmelte sie. "Aber
ein Gefühl ist es schon."

Kamira lächelte und nickte. "Es gibt so viele verschiedene
Facetten von Gefühlen. Sprachen kennen entsprechend verschiedene
Spektren an Vokabeln für die Gefühle, je nachdem, welche
Gefühle in der zugehörigen Kultur stärker verbalisiert wurden. Ein Name für ein Gefühl
in der einen Sprache bedeutet selten exakt das Gleiche wie
in einer anderen Sprache für das selbe Gefühl. Oder die übersetzte
Vokabel beschreibt eben nur ein
ähnliches Gefühl.", breitete Kamira aus. "Es gibt zum
Beispiel verschiedene Gefühlsbezeichnungen in Sprachen abhängig
davon, wie viel Licht oder Regen ein Volk gewohnt ist. Zwerge
zum Beispiel, bei denen Tagebau eine tragende Rolle spielte,
kennen ein Wort für die Stille unter dem Berg."

Sindra blickte sachte lächelnd von ihrem Tee auf. "Du
leitest zum Thema Herkunft über."

"Das war eigentlich nicht beabsichtigt, aber du hast
recht." Kamira runzelte die Stirn. Das war ungeschickt
gewesen. "Ich wollte eigentlich darauf hinaus, dass Sprachen
jeweils Worte für Gefühle entwickeln, über die sich die Völker
austauschen wollen. Aber Personen, die anders fühlen, gibt
es in jeder Kultur. Personen, die auf eine Weise fühlen, die
sich dann nicht in der entsprechenden Sprache verbalisieren
lässt. Manchmal ist es dann möglich, auf andere Sprachen
auszuweichen, aber manchmal geht es auch gar nicht, weil
manche Arten und Weisen zu fühlen eher selten sind. So
ähnlich, wie die meisten Sprachen visuell sind, aber es
Personen gibt, die es eben nicht sind."

Sindra nickte. "Es ist mir durchaus aufgefallen, dass Sprachen
verschiedene Gefühlsspektren verschieden gut beschreiben, aber
ja. Das Gefühl, wenn ich Wasserdampf beobachte, dafür kenne
ich in keiner Sprache Ausdrucksweisen, die es treffen."

"Und wenn keine Worte für die entsprechenden Emotionen
existieren, dann hat das wiederum eine Reihe Folgen für die
Psyche. Die Existenz mancher Dinge realisieren wir erst,
wenn es dafür Worte gibt. Und solange wir keine Worte haben, fühlen
wir uns oft falsch, oder Leute nehmen uns nicht ernst", fügte Kamira hinzu.

"Wie bei Geschlecht zum Beispiel?", schlug Sindra vor.

Das war ein gutes Beispiel. Kamira nickte. "Es fällt mir
schwer, mir vorzustellen, wie ich mich in einer Kultur gefunden
hätte, die kein Wort für Solstim hätte."

Für die fünf Geschlechter hatten Nixen in vielen anderen Sprachen
eigene Wörter entwickelt. Dazu hatten sie die jeweilige Sprache und
ihre Entwicklung betrachtet. Aus alten Wörtern von Vorgängern der
Sprache und moderneren Verschleifungen hatten sie Begriffe erfunden, indem sie
sich an der Entwicklung der originalen Begriffe im Sirenschen orientiert hatten.

Solstim war eines dieser fünf Geschlechter.

Ein altes Wort
für Sonne, wie Sol in den Vorgängersprachen
von Kazdulan, war jeweils die Bezeichnung für das
Geschlecht, das am ehesten männlich entsprach, wie es
bei den meisten maerdhischen Menschenvölkern
definiert wurde. Jene hatten keinen längeren Textabriss wie ein
Lexikon, der 'Mann' definierte, sondern lediglich eine
Definition in der Gesellschaft, im
Subtext des Redens und gegenseitigen Verständnisses des jeweiligen
Volkes. Besonders so einige Menschenvölker glaubten, dass
ihr Männlichkeitsbegriff ein unumstößliches Universalkonzept wäre, dass alle
Völker automatisch so nachempfinden können müssten und
es deshalb nichts dazu aufzuschreiben gäbe. Kamira empfand es
deshalb beinahe unangenehm, dass unter den nur fünf Geschlechtern der
Nixen ausgerechnet ein diesem Geschlecht annähernd entsprechendes
dabei war, besagtes Sol.

Solstim war Sol mit etwas
ergänzt, das im weitesten Sinne einen alten Wortstamm
für Wende beinhaltete: Sonnenwende, aber mit etwas weichem
am Ende. Es gab kaum eine kulturelle Entsprechung dieses
Geschlechts bei den meisten Landvölkern, vielleicht
eine Mischung aus dem Männlichkeitsbegriff bei den bekannteren Elbenvölkern und
dem Begriff Lesbe, der in manchen der Landvölker entwickelt
worden war.

Lun, von alten Wörtern für Mond abgeleitet, umfasste
eine ganze Facette an Weiblichkeitsbegriffen verschiedener
Völker, aber weniger performative.

Für letztere stand
Rosalun: Ein Begriff, bei dem Lun noch eine ausdrucksvolle
Blume hinzugefügt wurde. Rosalun entsprach einem Geschlecht, zu
dem starke Expression gehörte. Personen fanden sich in dem
Begriff wieder, für die ihr Geschlecht nicht nur zu ihnen
gehörte, sondern Ausdruck in ihrer ganzen Art, sich
darzustellen, und in ihrer Kommunikation
fand.

Und schließlich blieb Neutre als
ein weit gedehnter Geschlechtsbegriff, der alle umfasste, die
sich in keiner der anderen Gruppen wiederfanden.

Ein wichtiger
Bestandteil bei Geschlecht war die Selbstfindung. Für
Kamira hatte es nicht lange gebraucht, bis er gewusst
hatte, dass sich Solstim für ihn sehr passend und wie
ein Zuhause angefühlt hatte. Manche brauchten ihr
halbes Leben für den Findungsprozess. Und Beispiele wie
Rash zeigten, dass auch diese Kategorien eigentlich zu
starr waren. Rashs Geschlecht setzte sich wie die
Lichtmuster in einem Kaleidoskop ständig neu zusammen.

"Deine Angst, dass deine atypischen Gefühle oder deren atypische
Ausdrucksweise auf deine Herkunft zurückgeführt
werden könnten. Das war, worum es eigentlich gehen sollte.", kam
Kamira wieder zum Punkt.

Sindra nickte. "Genau. Ich hatte die Angst schon länger, aber
ich habe sie nie in Worte gefasst."

"Eine einfache Idee dazu: Du willst nicht über deine Herkunft
reden.", sagte Kamira. "Aber du kennst eben auch den Hintergrund
zu Ausdrucksweisen und Versprachlichung verschiedener
Gefühle in verschiedenen Kulturen. Wir sprachen
darüber nicht zum ersten Mal. Das heißt, es gibt darüber eine
naheliegende Verknüpfung, warum Leute auf die Idee kommen
könnten, es mit deiner Herkunft in Zusammenhang zu bringen."

Sindra hob einen Finger und öffnete den Mund, schon
ein paar Momente, bevor sie sprach. "Das hilft! Ja.", sagte
sie. "Und was mache ich dagegen?"

"Hast du nur Angst, dass andere es darauf zurückführen, oder
hast du auch Angst, sie könnten recht damit haben, dass es
einen Zusammenhang mit deiner Herkunft gibt?", fragte Kamira
mutig, in der Hoffnung, es ginge nicht zu weit.

"Ersteres.", sagte Sindra nachdenklich. "Eigentlich bin
ich ziemlich sicher, dass ich schon damals als" -- Sindra
zögerte und sprach dann weiter, als wäre sie sich
nicht sicher, ob die Worte passten -- "atypisch
empfunden wurde."

"Ehrlich gesagt, hätte ich mit nichts anderem gerechnet.", sagte
Kamira. "Woher soll sonst diese tief sitzende Angst kommen, dass
dein Fühlen falsch wäre, während du hier in der Crew mit deiner
Ausdrucksweise eigentlich akzeptiert bist? Warum sollte ein
Amira dafür reichen, um das alles wieder hoch zu holen und
in diesen furchtbaren Traum zu übersetzen?"

Sindra nickte langsam und wechselte halb das Thema: "In
meinem Traum habe ich mich außerdem gefreut, dass
Rash mit Kanta jetzt eine Person hätte, mit der sich Rash
verstünde. Und das ergibt nicht so viel Sinn. Rash
redet eigentlich mit allen. Was bedeutet das?"

Kamira dachte nach und drehte dabei den Teebecher in der
Hand. Hätte sein Gefühl ihm nicht gesagt, dass Sindra
bald gehen würde, er hätte wohl noch einmal die Spielzeugkiste
hervorgeholt. Bei Sindra ging das. Sie mochte, wenn sich
Personen in ihrer Gegenwart frei verhielten. Sie war unfassbar
flexibel, was anderer Leute Verhaltensweisen und Vorlieben anging. Kamira
hätte ihr mehr von dieser Flexibilität von anderen ihr
gegenüber gewünscht. "Ich
weiß es nicht.", sagte Kamira. "Es gibt viele Möglichkeiten. Es
könnte heißen, dass du von Rash wenig Hintergrund kennst, von
anderen aber schon. Das weiß ich aber nicht, weil ich nicht
weiß, was du von Rash weißt. Es könnte aber auch sein, dass
Rash tatsächlich etwas fehlt, und du es Rash gönnst, ob
Kanta nun die Person ist, die das geben kann, oder nicht."

"Weil ich es selbst nicht geben kann?", überlegte Sindra.

"Das wäre auch verhältnismäßig naheliegend.", bestätigte
Kamira. "Aber es sind auch alles gewagte Hypothesen." Kamira
teilte solche Hypothesen nicht mit allen. Viele neigten
dazu, in eine Hypothese mehr als eine Möglichkeit zu lesen, sie
für eine wahrscheinliche Wahrheit zu nehmen und nicht nur
für eine Idee zum Spielen und später Einordnen. Sindra
konnte so etwas sehr gut.

"Danke. Du hast mir wieder sehr geholfen.", sagte
Sindra.

Das klang nach Abschied, wie Kamira schon vermutet hatte. Aber
traurig stimmte es ihn doch. Er erinnerte sich an ihre Anfangszeit
zurück, als Sindra noch nicht eine ganze Flotte befehligt, sondern
es nur die Schattenmuräne gegeben hatte. Damals waren Forschungsschiffe in
Richtung Grenlannd noch eine Seltenheit gewesen und sie hatten noch gar nicht
gewusst, wie ihnen geschah, hatten teils nicht einmal das Ausrauben
mit dem Auftauchen der Schattenmuräne in Verbindung gebracht, weil
sie das Fehlen der Vorräte erst viel später bemerkt hatten. Sie
hatten zu der Zeit noch die Inhalte der Säcke und Fässer durch Kies, Steine
und getrocknetes und wiedergenässtes, braunes Seegras ersetzt. Und
Ushenka hatte sie mit Geschichten aus dem Gemunkel an Land über
Geister erheitert, die angeblich durch Schiffswände waberten und
Dinge in Nutzloses verwandelten. Über die vielen haarsträubenden Theorien.

Damals hatte Sindra mehr Zeit gehabt. Sie hatten sich alle
noch nicht gut gekannt und Sindra und er hatten sich gegenseitig ausgeforscht: Ihre
Sprachen, ihre Psychen, ihre Körper. Sindra war dahingehend
ähnlich hemmungslos gewesen wie er. Er war auch Schiffsärztan, was
bei einer Hybridcrew eine Herausforderung war. Das Vertrauen, das
Sindra in ihn hatte, hatte ihm gut getan. Irgendwann, -- sollten sie
den ganzen Kram hier überleben, und sollte die Überfällerei je zu ihren
Lebzeiten ein Ende haben --, würde er sie fragen, ob sie mit ihm
etwas erforschen wollte.
