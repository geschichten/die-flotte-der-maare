\BefehlVorKapitelDFDM{Rash-n.png}{Rash schreibt die Nachrichten, die von Bord gehen. Rash hat außerdem eine gute Charakterkenntnis und ist sowohl mit Sindra als auch mit Amira in einer Beziehung.}{Trauma, Shutdown?, Albträume, Gewalt, Selbstverletzendes Verhalten, Gaslighting.}

Trauma und Träume
=================

\Beitext{Rash}

Es war wirklich nicht alles gut. Irgendwo da draußen fuhr noch ein
Segelboot mit einem Assassinan, so der letzte Stand. Also, besagte Person
fuhr wohl nicht mehr dort, aber war vermutlich nicht minder hinter ihnen
her.

Dann waren da noch die möglichen Verhandlungen mit Zarin Katjenka. Sie
waren sich selten so uneins gewesen. Eine Uneinigkeit nicht nur
zwischen den Crewmitgliedern, sondern eine, die auch viele Crewmitglieder
mit sich selber hatten. Aber es gab keinen sinnlosen Zank, eher eine
überlegte Debatte, die gelegentlich unterbrochen wurde, wenn Personen
eine Pause davon brauchten.

Die Zarin hätte zugesagt, -- und vielleicht könnten sie die
Verhandlung wieder dahin bringen, dass sie es noch einmal tun
würde --, für die nächsten sechs Jahre ihr Ansinnen, Grenlannd
zu erforschen, ruhen zu lassen, unter der Bedingung, dass die Maare
für sie eine Seekarte erstellten und etwas über ihre Schiffsbautechniken
preisgaben, sowie ihre Beute, die sie von den
anderen Forschungsschiffen erlangten, teilten. Sie würden dafür mit
Proviant versorgt werden, wenn Knappheit da war, was vor allem zum
Ende des Winters in den letzten vier Jahren ein Problem gewesen
war. Und sie hätten im Hafen von Mizugrad für lau liegen dürfen.

Was die Zarin mit Vorräten anderer Forschungsschiffe wollte, war nicht
ganz klar. Sie hatten es nicht herausfinden können in der kurzen Zeit. Ob
sie einfach mal kulinarische Ausflüge in andere Kulturen machen wollte, oder
ob sie die Maare gebeten hätte, auch anderes als Vorräte zu stehlen, wussten
sie nicht.

Sie waren sich eigentlich relativ einig, dass sie den Vertrag nicht
wirklich eingehen wollten. Zumal es auch fraglich war, wie sehr sie
in Mizugrad nach ihrem Sabotageakt noch willkommen wären. Immerhin
waren es Lecks, die einigermaßen leicht flickbar waren. Bisschen
schwierig vielleicht für ein Volk ohne große Taucherfahrung, trotzdem
machbar.

Aber dann war da das Druckmittel: Die Zarin wusste um ihre
Methode, wie sie Schiffe ausraubten. Und sie konnte das Wissen
teuer verkaufen. Eigentlich war es nun an ihnen, ein Angebot zu
machen, das die Zarin ihnen gewogen stimmen würde.

Rash fiel es schwer darüber nachzudenken.

Rash lag in Kamiras Raum auf dem Rücken, der Oberkörper gut
abgepolstert etwas erhöht und Sindras Kopf auf
dem Bauch. Das endlos lange
Haar der Kapitänin war ordentlich um sie herumdrappiert. Das
hatte Marah getan, weil Sindra das Gefühl mochte und
entspannte, wenn Marah ihr Haar sortierte. Im Moment
reagierte sie gar nicht. Das hatte
sie öfter, seit es Gelegenheit dafür gab, weil sie
weit genug weg von der Küste waren. Es war Herbst. Es wurden
derzeit keine Forschungsschiffe losgeschickt. Sie lagen
vor Anker vor den Inseln mit den Küchengärten der Nixen, wo
sie für den Winter geerntet hatten. Es war der Ort, an dem damals Amira
an Bord gelangt war. Jentel und Amira wachten hier
besonders intensiv, um zu vermeiden, dass eventuelle
weitere Assasinpersonen dasselbe tun könnten. Etwa die eine, die für die Morde auf
der Schattenscholle verantwortlich gewesen war und vielleicht
da draußen noch irgendwo nach ihnen suchte.

Rash fühlte den eigenen Körper bei dem Gedanken an die
Herzperson wärmer werden. Aber Amira war gerade nicht
hier unten. Kamira hatte Marah und Sindra den Raum
für ein paar Stunden überlassen, und Marah hatte Rash
gebeten, sie zu begleiten.

Sie gingen alle verschieden mit ihren Traumata um. Marah
hatte eine Menge geflucht, manchmal geweint, und viel
mit Kamira gesprochen. Kamira kannte den Speziesismus und
Rassismus, der Marah widerfahren war, selbst, und konnte sich gut
um sie kümmern.

Sindras Trauma kannte höchstens Jentel ein wenig, aber
die beiden sprachen nicht so oft miteinander. Sindra
hatte Albträume. Damit konnten sie noch arbeiten. Aber
sie versank auch viel in Schweigen, in Zustände, in denen
sie überhaupt nicht kommunizierte. In so einem war
sie nun seit gefühlt einer Stunde. Rash strich ihr
etwas ratlos über den Kopf. Marah war mit dem Haaresortieren
fertig und legte sich neben sie.

"Warum mögt ihr mich?", fragte Sindra.

Marah fädelte ihre Arme zwischen Sindras Kopf und Rashs Körper
hindurch und umarmte Sindra. Küsste sie auf die Stirn. "Du
bist sehr gut.", sagte sie.

"Bist du.", bestätigte Rash.

Ob Sindra nun bereit wäre, zu reden? Vielleicht hatte
Marah Rash auch nicht ohne Grund gefragt, dabei zu
sein.

"Du fühlst genau richtig.", fügte Rash hinzu. Rash
ahnte, worum es ging.

Sindra schüttelte den Kopf. Ihre Nase drückte sich dabei
in Rashs Bauch.

"Doch.", widersprach Rash.

"Ich hasse die Zarin so, so sehr!", rief Marah aus. "So
inbrünstig! Ich habe dafür gar keinen Ausdruck! Das
war so schlimm, was sie dir angetan hat."

"Dir.", korrigierte Sindra. "Sie hat vor allem dich grausam
behandelt."

"Ja, Sindra, das hat sie.", sagte Marah. Nun wieder
erstaunlich ruhig. "Darüber haben wir schon gesprochen.
Mehrfach. Nun hast du endlich mal den Mund aufgemacht. Nun
bist du dran. Wir kümmern uns jetzt um dich."

"Ich kann nicht gut über mich reden.", sagte Sindra.

"Ich weiß.", sagte Rash. "Du musst auch nicht. Aber ich glaube, du
hast deshalb etwas gesagt, weil du jetzt ein bisschen darüber
reden möchtest und weil du von uns in all den Punkten, in denen
du gerade zweifelst, hören musst, dass du nicht das Problem
bist. Und das ist in Ordnung. Dafür ist Raum da. Wenn du
ihn willst. Aber du musst nicht."

"Ach, ihr.", seufzte Sindra. Aber sagte dann lange nichts
mehr.

Bevor sie wieder in das grausame Schweigen verfallen konnte, -- zumindest
hatte Rash das Gefühl, dass sie darin von grausamen Gedanken und Emotionen
beherrscht wurde --, fügte Rash hinzu: "Ich weiß, was passiert
ist. Ich war teils dabei, den Rest weiß ich von Marah. Ich kann auch versuchen, dich
da durch zu navigieren. Ich kenne dich ein bisschen und kann
mir vorstellen, an welchen Stellen es dich erwischt hat."

Wieder schwieg Sindra lange. Dieses Mal unterbrach Rash
das Schweigen nicht. Irgendetwas an Sindra fühlte sich
leichter an. "Ich komme mir so albern vor. Es ist
so unsinnig.", sagte sie schließlich. "Bitte
hilf mir."

Während Sindras Stimme vielleicht etwas schwach, aber
immer noch ruhig und sachlich klang, zeigte sich Marahs
Mitfühlen umso deutlicher. "Ich liebe dich.", flüsterte
sie, als ihr die Tränen kamen.

Rash hätte ihr auch einen Arm angeboten, aber Sindra
war ziemlich groß und brauchte das gerade dringender.

"Und ich dich?", fragte Sindra.

Es tat so weh. So, so weh. Rash versuchte die innere
Wut herunterzukämpfen. "Selbst die Zarin hat am Ende
gesehen, wie sehr du sie liebst.", sagte Rash. "Als
du sie aufgefangen hast, weggerannt bist mit ihr, als du
die Dinge getan hast, die sinnvoll waren."

"War das nicht berechnet?", fragte Sindra. "Bin ich
nicht eher ein gefühlloses Monster, dass irgendwo
in meinem Kopf als Spielregel festgelegt hat, dass
ich alles daran setzen muss, Marah zu helfen? Und
dann trotzdem allein gescheitert wäre, weil ich nicht
geweint habe? Oder geschrien? Wieso kann ich nicht
schreien?"

"Hast du denn nichts gefühlt?", fragte Rash. "Das
wäre auch in Ordnung, aber abhängig davon erkläre
ich nun verschiedene Dinge."

"Erklär sie beide, wenn du magst.", bat Sindra. Sie
drehte den Kopf auf die Seite, sodass sie Marah
anblickte. "Doch, ich habe mich schrecklich
gefühlt. Und ich habe gefühlt, dass ich dich
sehr liebe. Aber ich musste dann denken, dass das
nicht stimmt, weil ich sonst Dinge wie schreien
können müsste."

Marah legte eine Hand an Sindras Wange. "Dann kannst
du eben nicht schreien.", sagte sie eindringlich. "Ich
zweifele nicht ein bisschen daran, dass du mich
liebst."

"Warum kann ich nicht schreien?", fragte Sindra.

Rash versuchte die Gewaltfantasien gegen die Zarin unter
Kontrolle zu bringen. Dieses Mal musste Rash einige
Male tief durchatmen, bis Rash wieder in der Lage war,
brauchbar ruhig zu sprechen. "Warum du es nicht kannst, weiß
ich nicht.", gab Rash zu. "Aber dass du nicht genügend
lieben oder anderweitig fühlen würdest, ist nicht der
Grund. Du kannst andersherum sehr sicher sein, dass du
es wirklich nicht kannst, wenn du diese zielführende
Sache in so einer Situation nicht
hinbekommst. Und es ist absolut grausam, was
die Zarin da getan hat." Rash war doch lauter
geworden.

Plötzlich musste Rash grinsen. Amira hatte
ihr Geschwister als Feuer bezeichnet, als wütend an
den richtigen Stellen. Und hatte gesagt, dass Rash
dem Geschwister in dem Punkt vielleicht ein bisschen
ähnelte. Und doch wieder ganz anders war. "Das andere
Schlimme ist, dass durch den Fokus auf Schreien als
Ausdruck einer bestimmten Emotion die Zarin diese
beiden Dinge verknüpft hat. Das Schreien mit der
Emotion. Und weil du dich um die Frage geschert hast,
warum du nicht schreien kannst, wofür die Gründe
ganz woanders liegen, hast du keine Kapazitäten mehr
gehabt, zu analysieren, dass die Verknüpfung selbst
schon Unfug ist."

"Großer Unfug.", stimmte Marah zu. "Ich weiß, dass
du ganz schön starke Gefühle hast. Und ja, auch ich
musste bei dir neu lernen, sie zu lesen. Du drückst
sie eben anders aus. Aber du kannst doch auch
diese Logik-Sache: Wer profitiert davon, wenn
wir von konstruierten Situationen wie dieser Erpressung
durch die Zarin mal absehen, dass du Gefühle so
ausdrückst, wie alle anderen?"

"Die ganze Welt.", antwortete Sindra matt. "Wir
sprechen an Bord alle Kazdulan. Wir sprechen auch mal was
anderes, aber wir profitieren davon, dass wir uns auf
eine Bordsprache geeinigt haben. Und ich spreche
die Emotionssprache der Mehrheit nicht."

Rash senkte die Lider und streichelte über Sindras
Haar. Leider hatte Sindra da zumindest teilweise recht. Rash
glaubte nicht, dass die ganze Welt davon profitierte, aber
es würde Personen in Sindras Umgebung leichter fallen, sie
von Anfang an zu verstehen, wenn Sindra diese Ausdrucksweise
beherrschen würde. "Es ist so ein Unsinn.", sagte Rash. "Ich
verstehe dich. Du hast auch irgendwo untergründig recht. Aber
es ist hier bei den Emotionen was anderes als bei
Kazdulan oder einer gemeinsamen Sprache. Denn, auch dafür
hast du Kazdulan. Du kannst auf Kazdulan sagen: Ich bin
gerade sehr wütend. Und wenn Leute dir das dann nicht glauben, weil
du das in einer Art vorgebracht hast, die sachlich für
die anderen klingt, dann ist das deren Ignoranz und nicht
dein Fehler. Dann hast du von deiner Seite alles fürs
Verständnis gegeben. Du bist nicht das Problem."

Sindra drehte den Kopf wieder mit der Nase zu Rashs Bauch
und drückte das Gesicht hinein. Rash strich ihr weiter
über den Kopf. "Ich liebe dich auch, weißt du das
eigentlich?", sagte Rash, und dann kurz darauf: "Vielleicht
nicht ganz so doll drücken, ja?"

Sindra ließ sofort nach. "Es tut mir leid."

"Braucht es nicht. Ich sage, wie immer, rechtzeitig Bescheid.", beruhigte
Rash.

"Du hast nun den Zweig mit Gefühlen erklärt, glaube ich.", lenkte
Sindra das Gespräch zurück auf das Thema. Dafür, dass sie
vorhin nicht hatte darüber reden mögen, lief es jetzt ganz
gut. "Ich habe aber auch bestimmte Emotionen gar nicht. Trauer
zum Beispiel. Und Wut auch sehr abgeschwächt. Was, wenn
ich nicht lieben könnte, wäre das schlimm?"

Rash schüttelte den Kopf. "Es besteht von keiner Seite das Recht
oder ein Grund oder sonst etwas, dich dazu unter Druck zu setzen, zu
lieben, oder dich dafür abzuwerten, wenn du nicht lieben könntest.", sagte
Rash. "Oder auch nur, dich dafür gruselig zu finden."

"Das!", fiel Marah wieder ins Gespräch ein. "Ich weiß noch nicht, worauf
du genau hinauswillst, aber das fand ich so ekelhaft. Dass die
Zarin sich gegruselt hat, oder dass Wörter wie Monster entstehen, weil
eine Person Liebe nicht so zeigt wie erwartet. Das ist so
widerlich schlimm!"

Marah gab noch einige grummelnde Geräusche von sich, die Rash
abwartete, bis Rash fortfuhr: "Angenommen, du hättest deine Ethik, die
besagte, du möchtest, dass es Personen gut geht. Angenommen, du
hättest sie wie Spielregeln erlernt, aber würdest damit keine
Gefühle verbinden, außer vielleicht eine gewisse Ordnungsliebe, sodass
du zuverlässig bei diesen Regeln bleibst.", leitete Rash ein. "Wo
wäre das Problem? Wieso sollte das irgendjemandem das Recht geben, an
dir herumzuexperimentieren, ob du nicht doch irgendwie fühlst oder
dich etwas stört, soweit gehend, dass dir vorgeschlagen
wird, eine Person zu sezieren, die dir wichtig ist? Ob dein
Grund dafür, dass das weh tut, nun Liebe ist oder nicht."

Marah schluckte schwer und Tränen schossen in ihre Augen. Sie
bekam für ein paar Momente keine Luft.

Rash sah sie besorgt an, ließ Sindras Haar los und reichte Marah
die Hand. "Scheiße. Das war unbedacht von mir.", sagte Rash. "Das
hat Übles ausgelöst bei dir. Das hätte mir klar sein sollen."

Marah griff die Hand, aber schüttelte den Kopf. "Ja, aber.", sagte
sie leise. "Es war so schlimm, insgesamt, auf so vielen Ebenen, und
es ist wichtig, dass wir das hier besprechen." Marah holte tief
Luft. "In dem Moment habe ich die Aggression und Abwertung darin, die
gegen mich ging, nicht so sehr gespürt, tatsächlich. Später schon, und
es ist nicht das erste Mal, dass ich mit so einer", Marah zögerte, "Abwertung
ist ein viel zu schwaches Wort dafür. Jedenfalls dass ich dieser
widerlichen Einstellung begegnet wäre, und es ist schlimm. Aber hier wurde es als Druckmittel
für Sindra benutzt, das ist eine andere Ebene von Grausamkeit, die hinzukommt."

Sindra nickte. "Das ist in meinem Kopf immer der Moment, in dem alles
blank in mir wird, wenn ich versuche, die Situation zu reflektieren. Ich
weiß nicht, wie ich in der Situation überhaupt noch sprechen gekonnt
habe. Vielleicht wegen des Bluts. Manchmal erwische ich mich bei dem
Gedanken, dass ich mich vielleicht verletzen sollte, um aus dieser
Starre wieder herauszukommen."

"Soll ich dich sehr fest in den Arm nehmen?", fragte Rash.

"Ja bitte. Ihr beide.", sagte Sindra leise.

Rash tat es. Und lächelte. Das klappte doch ganz brauchbar heute mit
dem Äußern von Bedürfnissen. Marah robbte näher heran und schloss die
kleineren Arme wieder um Sindras Kopf.

"Ich habe dich eigentlich ganz schön bewundert, weil du alles danach
so gut in Worte hast fassen können, was in der Situation schlimm
war.", sagte Marah.

Rash stimmte zu. Das war sehr analytisch gewesen.

"Ich erinnere mich nicht mehr genau.", sagte Sindra. "Aber
gefühlt bringe ich das Problem nie genug auf den Punkt. Ich
wollte überzeugen, die Zarin und mich, davon, dass ich nicht
das Problem bin. Wie ihr sagt. Aber ich konnte das nicht. Also
fange ich an zu glauben, dass ich es bin."

"Du bist nicht das Problem.", wiederholte Rash leise.

"Ich versuche es immer wieder in meinem Kopf zu argumentieren, aber
ich schaffe es nie in wenigen Zeilen, und die Gegenseite gewinnt
immer.", fügte Sindra hinzu. "Im Moment geht es. Darf ich wieder
mit euch reden, wenn ich nicht mehr gegen sie ankomme?"

"Immer.", lud Rash ein. "Ich würde
gern etwas zu vorhin ergänzen, mit den Sprachen."

"Gern.", sagte Sindra.

"Du hast unserer aller Sprachen erlernt.", leitete Rash ein. "Weil
sie zu uns gehören, weil du es respektvoll fandest. Nun gut, du hast
auch ein Sprachtalent und kannst so etwas schneller als andere. Aber
du sagst auch, dass du davon profitierst, Sprachen zu lernen."

"Willst du darauf hinaus, dass andere davon profitieren würden, mich
zu verstehen? Meine Emotionssprache?", fragte Sindra.

"Ja, auf jeden Fall.", stimmte Rash zu. "Genau das. Nicht zuletzt, weil
wir dann Dinge hinterfragen, die wir noch nie hinterfragt haben, die
uns helfen. Durch dich lernen Personen ganz viel über sich selbst."

"Aber würde ich nicht auch davon profitieren, die Emotionssprache
der anderen zu erlernen?", fragte Sindra.

Rash lächelte. "Als ob du es nicht probieren würdest.", sagte
Rash. "Genauso, wie du keiner Person böse bist, die das mit einer
neuen Sprache nicht so schnell oder sogar gar nicht hinbekommt, solltest
du dir selbst nicht böse sein, dass du es nicht kannst. Das ist
in Ordnung so. Du musst nicht alles lernen können."

Sindra nickte. "Danke.", sagte sie. "Ich fühle mich nun besser. Danke, dass
ihr da seid."

"Noch etwas.", sagte Marah. "Du bist nicht mehr bei der Zarin und in
der Welt da draußen mit lauter fremden Personen, die sich damit
Arbeit machen, Personen, die irgendwie nicht in ihr Konzept von
gleich passen, oder die sie ausbeuten können, abzuwerten. Du
bist bei uns, in der Flotte der Maare. Wir haben dich kennengelernt. Wir
haben dich lieb, wie du bist. Du gehörst genau hier her. Wir
wollen keine andere Sindra."
