\BefehlVorKapitelDFDM{Sindra-n.png}{Sindra ist die Kapitänin der Flotte der Maare. Sie ist sehr groß.}{Blutbad, Gemetzel, Ableismus, Trauma, Erwähnung von Mord.}

Sortieren
=========

\Beitext{Sindra}

Sindra hatte Kanta sehr intensiv vernommen, damals, als sie an Bord
gekommen war. Sie fand es interessant, die Situation damals mit dem
Dazustoßen von Amira ungefähr heute zu
vergleichen. Kanta hatte versucht, mit einem Ruderboot
überzusetzen. Jentel hatte sie schon frühzeitig vom Mastkorb aus gesehen und Sindra
informiert. Sie hatten eigentlich entschieden, keine weitere Person an Bord
nehmen zu wollen. Kanta war kein unregistrierter Passagier der Forschungscrew
gewesen, sonst wäre sie nicht an ein Beiboot gekommen und hätte nicht
einfach unter den Augen der Crew wegrudern können. Das war zu
unwahrscheinlich.

Kanta war Teil der Forschungscrew gewesen. Das hatte sie in der Unterredung
auch offen dargelegt.

Jentel hatte versucht, Kanta zunächst zu gruseln, so zu tun, als wäre
as eine gefährliche Seekreatur, die das Beiboot angriffe. Das hatte
Kanta nicht beeindruckt. Dann hatte as versucht, das Ruderboot
zurückzuziehen. So, dass es für Kanta vielleicht wie Magie wirken
mochte. Kanta hatte Sindra hinterher erklärt, dass sie nie an
Magie geglaubt hatte. Anders als große Teile der Forschungscrew.

Und dann war Kanta einfach so rasch gerudert, dass sie aus
der Sichtweite ihres Forschungsschiffs gelangt wäre, und wenn
die Schattenmuräne dann auch ohne sie weggesegelt wäre, wäre sie
allein auf dem Meer gewesen, mit nichts als einem Ruderboot.

Also hatte Jentel das Boot gekentert. Und Kanta hatte diesen
unbeschreiblich schlimmen Fehler gemacht, das Boot loszulassen. Das
war Regel Nummer eins beim Seefahren, niemals Boote loslassen. Es
hätte zu hoher Wahrscheinlichkeit ihren Tod bedeutet, wenn
Jentel nicht in der Nähe geblieben wäre.

Das, was schließlich ausschlaggebend dafür gewesen war, dass
Sindra sie so intensiv verhört hatte, war, dass tatsächlich
ein Versuch unternommen worden war, den Elben zu retten. Sie
hatten Crewmitglieder aus verschiedenen Motiven an Bord der
Schattenmuräne, aber eine Person von
einem Forschungsschiff, das zum Fußvolk gehörte, und
die eine gewisse Unterstützung von entsprechender Seite erfuhr, fühlte
sich für Sindra sicher gefährlicher an, als eine Assassinperson, die
sie nicht angriff, obwohl sie es konnte.

Es hatte sich für sie sicherer und entspannter angefühlt, Amira zu
vernehmen.

Amira war auch mit einem Ruderboot übergesetzt, allerdings mit einem
schlankeren, getarnten. Einem Boot, das aus gewachstem Stoff
und Gestänge bestand, sich klein zusammenfalten ließ und eigentlich alles andere
als hochseetauglich war, aber zum Übersetzen hatte es gereicht. Sie hatte
es in der Vorratskammer unter dem getrockneten Seegras verstaut, das
erst bei einem neuen Überfall zum Einsatz käme. Jentel
hatte von all dem nichts bemerkt, weil
Amira tagsüber an Bord gekommen war. Amira war
allerdings nicht von einem Forschungsschiff übergesetzt, sondern von
der kleinen Insel, in deren Nähe sie manchmal ankerten. Einer Insel, die
weit weg von Land war, häufig überspült wurde, und wo die Nixen der Crew unter
Wasser Esspflanzen und Gewürze anbauten. Die Schiffe des
Schattenschwarms hielten hier manches mal, um zu ernten und die
Gärten zu pflegen. Von diesem Ankerplatz wussten Amiras Auftraggebende
wiederum durch die geheimen Briefe. Nicht durch den, den Amira
mitgebracht hatte. Es hatte mehrere gegeben. Sie hatten Amira dort
mit nichts als ihrem Boot und etwas Nahrung ausgesetzt, sodass
sie keine andere Möglichkeit gehabt hatte, sich zu retten, als an
Bord eines der Schattenschiffe zu gelangen. Und dann wäre der
Plan gewesen, dass sie die Crew des jeweiligen Schattenschiffs
überwältigte oder erpresste, um sie auszuliefern, oder
anderweitig den Mordauftrag ausführte, um wieder an Land
zu kommen. Die Auftraggebenden hatten nicht damit gerechnet, dass
eine Assassinperson als Crewmitglied willkommen geheißen
werden könnte, sodass die Notwendigkeit eines Mordes nicht
bestand.

Sie hatten das Gespräch unterbrochen, um Smjer hinzuzuziehen, nachdem Sindra die
Sicherheit der Lage einigermaßen einschätzen konnte. Zunächst hatte sie
dann mit Smjer zu zweit gesprochen, weil Smjer darum gebeten hatte. Er hatte
sich Sorgen gemacht und von ihr eine Einschätzung gewollt, ohne, dass
eine potenziell gefährliche Person Sindra hätte unauffällig erpressen
können.

Anschließend hatte Sindra ihnen in ihrer Kajüte Zeit für sich
gegeben, damit Amira ihre Geschichte
auch ihm darlegen würde. Sie sollten sie dazu holen, sobald etwas gesagt
würde, was sie noch nicht wusste.

Vielleicht war das nicht die beste Entscheidung, sie wusste es nicht. Sie
brauchte die feuchte Draußenluft im Gesicht und ein bisschen Ruhe,
um ihre Gedanken zu sortieren. Die Situation hatte sie zwar in dem Moment kaum
belastet, aber nun merkte sie doch, dass die Bedrohnung eine Nachwirkung
auf sie hatte. Außerdem konnte sie sich vorstellen, dass
Amira und Smjer ein anderes Gespräch führen würden, als wäre sie dabei. Vielleicht
brachte auch das Neues. Jentel fragte vom Mastkorb, was los wäre. Sie
antwortete, dass sie einfach ein wenig Ruhe bräuchte, und kam erst verspätet
darauf, dass er vielleicht Amiras Erscheinen an Bord meinen könnte. Sie
ergänzte nichts. Um das Einweihen der Crew würde sie sich später Gedanken
machen. Nun tat sie erst einmal ein paar tiefe und ruhige Atemzüge salziger
Vorregenluft.

Die Kajütentür öffnete sich einen schmalen Spalt, und eine Hand winkte sie
heran. Sindra schmunzelte. Sie mochte etwas an der Unnauffälligkeit
Amiras.

"Weißt du Genaueres über die Bedrohung oder die Auftraggebenden?", fragte
Sindra, als sie alle drei beisammen saßen.

"Es sind neue Auftraggebende.", informierte Amira. "Deshalb konnte ich
auch weg und habe es ausgenutzt. Sie haben mich sozusagen
abgekauft, und haben noch nicht so viele Druckmittel gegen mich
gehabt. Und ich weiß entsprechend wenige Details über sie. Ich
hatte zu viel Angst, den Auftrag nicht zu bekommen."

"Nicht so viele Druckmittel klingt nicht wie keine.", bemerkte Smjer. "Das
wäre interessante Information für uns."

"Ich habe mich von den Personen entfernt, die ich liebe. Sie
haben fliehen können. Ich
habe niemanden mehr.", berichtete Amira. "Es wird schwer, mich
darüber unter Druck zu setzen, aber es ist nicht völlig
auszuschließen. Ich glaube aber, dass die neuen Auftraggebenden
gar nicht auf die Idee gekommen sind, dass ihr Druckmittel
nicht reicht. Dass sie mich vermutlich
umbringen würden, wenn ich ohne ausgeführten Auftrag zurückkehrte. Wie
ihr wisst, hatte ich keine Wahl, als an Bord zu kommen."

"Was wäre dein Gewinn dafür gewesen, Sindra zu töten?", fragte Smjer. "Abgesehen
von überleben."

"Freiheit.", antwortete Amira.

"Das ist ein ganz schön großer Gewinn für den Tod der Kapitänin
der Schattenmuräne allein.", wunderte sich Smjer.

Sindra nippte nachdenklich
an ihrem Tee und verbrannte sich fast die Lippen. Er war frisch gekocht und
noch viel zu heiß.

Amira trank ebenfalls einen Schluck und konnte die Hitze
wohl besser ab. Jedenfalls machte sie keine Anzeichen, dass
der Tee für sie zu heiß wäre. "Es ging nicht unbedingt nur um die Kapitänin." Sie
wandte sich Sindra zu. "Es ging darum, dass die Flotte der Maare Forschung boykottiert. Und
es war mein Auftrag, das zu verhindern, indem ich dich
töte, oder wenn nötig auch weitere Personen. Ich habe wenige Hilfsmittel zur
Verfügung bekommen. Vor allem die Inhalte der Briefe und etwas Geld."

"Durftest du alle Briefe selber lesen und hast sie zurückgeben müssen, dabei
aber einen mitgehen lassen?", fragte Sindra.

Amira bestätigte.

"Hast du irgendwelche weiteren Details zu den Auftraggebenden?", fragte
Sindra. "Warum zum Beispiel versuchen sie, mich hinterrücks zu töten, anstatt mit
einem bewaffneten Schiff die Flotte zu versenken?"

"Sie haben Angst vor der Schattenflotte.", sagte Amira. "Das ist alles
was ich weiß."

Es hatte wohl noch
einen anderen Grund, den Amira vielleicht
einfach nicht kannte: Die Schattenflotte
war schnell. Sie waren schon ein paar Kriegsschiffen davongefahren, aber
sie hatten bisher nicht gewusst, ob sie tatsächlich Ziel gewesen waren.

Der Aufwand, den die Landsleute inzwischen
betrieben, um die Schattenflotte aufzuhalten, fing an, Sindra
sehr zu beunruhigen. Lange würde eine Katastrophe oder
das Ende der Maare nicht mehr aufzuhalten sein. Die Frage
war nur, wie lange doch noch, und wie das Ende aussehen
würde.

---

Sindra trat zur Mittagszeit an Deck. Jentel hatte vom Mastkorb
gerufen, dass sich ein
neues Forschungsschiff näherte. Das war nicht geplant. Sie
kannten die ungefähren Routen der Forschungsschiffe. Griffen
sie nicht auch sonst bevorzugt abends an? Es war von Vorteil, wenn es
beim Überfall dunkel wurde. Der Gruseleffekt
war abends stärker, und Nixen konnten nachts sehr
gut sehen -- besser als tagsüber --, weil ihre Augen
auf Unterwasser-Sicht ausgelegt waren. Aber als Sindra in den Himmel
blickte, erkannte sie, dass es bloß ein besonders heller
Mond war, der erstrahlte, den sie zerst für die Sonne gehalten
hatte. Er blendete, so sehr, dass sie die Hand schützend über die Augen
halten musste.

Das Forschungsschiff war viel zu dicht. Sie konnte es
in allen einzelnen Details erkennen. Das war nicht gut. Sie
befehligte, dass sie wenden sollten, um die Distanz zu
vergrößern. Mit ruhiger Stimme, wie stets.

"Schnell, schnell!", hörte sie Kanta zu Rash zischen. "Nur, weil
die Kapitänin so gelassen ist und keinen Finger rührt, heißt
das nicht, dass das für uns auch gelten darf."

Kanta hatte recht. Sindra rührte sich zu wenig. Sonst war
sie doch aktiver. Nun fühlte sie sich auf eine seltsam
albtraumhafte Weise gelähmt.

Ein kleiner Teil von ihr freute sich, weil Rash mit Kanta
eine liebe Person gefunden hatte, mit der Rash sich
verstand. Ein anderer Teil drängte Sindra dazu, endlich
etwas zu tun. Sie wusste seltsamerweise nicht was. Und
als sie über das Deck schritt, in der Hoffnung, sie würde
es dabei herausfinden, stolperte sie über einen Körper. Ein
Blick zu Boden verriet ihr, dass es Marah war. Und Marah
war tot.

Diese Erkenntnis führte zu ihrem Entschluss, dass sie das
Forschungsschiff dieses Mal nicht angreifen, sondern
Priorität auf Flucht setzen würden. Sie gab entsprechende
Befehle, blickte sich um, ob auch der sonst unter Wasser angreifende
Teil der Crew noch hier wäre, um es mitzubekommen. Kamira war nicht
da. Es war ungewöhnlich still im Unterdeck. Nicht, dass das Nixendeck
je besonders laut gewesen wäre. Aber es war, als würde etwas
sogar das Geräusch ihrer Schritte dämpfen.

Es überraschte sie wenig, auch Kamira tot vorzufinden. Von
Janasz und Ashnekov fehlte jede Spur. Ihr Kopf
arbeitete. Sie wusste seit ihrer Unterredung mit Amira, dass
sie eine Person in der Flotte hatten, die Nachrichten
verschickte, die letztendlich bei Amiras Auftraggebenden
gelandet waren. Sie hatte bisher
nicht damit gerechnet, dass jene Person sie wirklich hatte tot
sehen wollen, obwohl Amira einen entsprechenden Mordauftrag gehabt hatte. Aber
Sindra glaubte eigentlich nicht, dass der Person an Bord
das bewusst gewesen war. Sie hatte eher damit gerechnet, dass es ihr
darum ginge, die Flotte wieder zu verlassen, um Geld, um
irgendeine Form von Profit oder Sicherheit, für die
die Person bereit war, Informationen preiszugeben, ohne
genau zu wissen, was das zur Folge haben würde.

Nun wäre es umso dringender, dass sie herausfände, wer es
wäre. Und es wäre sinnvoller, wenn sie alle
zusammenblieben, damit sie nicht einzeln ermordet werden
könnten. Auf dem Weg die sechs bis acht Decks aufs Oberdeck hinauf scheuchte sie jede
Person, die sie sah, vor sich her. Janasz war mit
kochen beschäftigt gewesen. Aber sie konnte ihn davon
überzeugen, dass es gerade Wichtigeres gab. Er lachte, als
er ihr folgte, ein Lachen, das sich in ihr Gehirn
brannte, weil es so seltsam war.

An Deck waren sie gar nicht so viele. Ihr
fiel es schwer, sie zu zählen. Etwas stimmte nicht. Das
Forschungsschiff war nicht mehr da. Rash sah sie mit
einem seltsamen Lächeln an, das noch merkwürdiger war als
Janaszs Lachen. "Warum weinst du nicht?", fragte
Rash, ohne die Lippen zu bewegen.

Dann fiel Rash vornüber zu Boden. Rashs Körper bewegte sich dabei, als
handelte es sich um einen Sack Proviant. Sindra
fühlte sich nicht nach weinen. Sie bemerkte, dass sie barfuß
in Marahs Blut stand. Deren Kopf abgetrennt vom Körper neben Sindra
lag. Als sie wieder aufblickte, bereit, sich zwischen die
Assassinperson und gegebenenfalls andere Opfer zu werfen, stand
Amira direkt vor ihr. Sie war es, die gesprochen hatte, an
Rashs Stelle, nachdem sie Rash von hinten erstochen
und bloß noch festgehalten hatte.

Nun hielt sie das blutige Messer Sindra entgegen. "Ich
hatte eigentlich für dich arbeiten gewollt.", sagte sie. "Aber
du bist so kalt, dass du nur grundböse sein kannst. Ich
hasse dich und werde die Welt vor deiner kranken Emotionslosigkeit
beschützen."

Sindra musste fast lachen, als sie sich wünschte, wenigstens
wütend zu sein, weil sie nicht weinen oder trauernd sein
konnte, dass Marah tot war. Oder Kamira oder Rash. Dass sie
sich ein Ersatzgefühl für das fehlende Gefühl wünschte, war
albern, dann hätte sie sich auch gleich das Trauergefühl
wünschen können, oder den erwarteten Schock. Sie
hätte Trauer empfinden sollen. Besonders
für Marah. Sie liebte Marah. Liebte sie wirklich? War
es Liebe, wenn Trauer ausblieb? Sie blickte nach unten, als
sie Marahs Stimme hörte: "Ich habe dich immer vor Elben
gewarnt."

Das ergab keinen Sinn. Amira war ein Mensch. Allerdings war
die Abwehrhaltung der Nixen gegenüber Menschen eher noch größer als die
gegenüber Elben und Sindra konnte das gut verstehen. Es
gab kaum ein ausbeutenderes Volk als Menschen. Nicht
alle Menschen, aber die in Zentral-Maerdha und an der Westküste
schon. Marah betonte
lediglich häufiger Elben, weil zur Crew zwar bisher Elben, aber
keine Menschen gehört hatten.

Sie versuchte, sich
körperlich gegen Amira zu stemmen, als diese ihr
Messer in sie stach. Es ergab auch keinen Sinn, dass sie
es nicht warf. Das wäre sicherer gewesen. Sindra verzichtete
darauf, es Amira mitzuteilen.

Als sie bemerkte, dass sie keinen Schmerz fühlte, wachte sie
auf.

---

Vielleicht war es mal wieder Zeit für ein Gespräch mit Kamira.
So sehr ihr im Traum mitgeteilt worden war, dass sie keine
Emotionen hätte, hatte sie doch gerade eine Emotion sehr stark: Angst.

---

Erst einmal gab es allerhand zu tun, wie immer. Weniger spannende
Aufgaben vor allem, die aber trotzdem gemacht werden mussten. Sortieren
und Verplanen der neuen Vorräte, oder der fehlenden, die sie aufgrund
der Lage nicht mehr erreichen konnten, weil Marah derzeit
nicht zum Festland pendelte, Decksarbeiten, Navigation. Für
Navigation war Sindra hauptverantwortlich. Zusammen mit den
anderen Steuerpersonen Rash und Smjer setzte sie sich zusammen und
zeichnete dünne Linien in Karten, um Positionen und Kurse zu bestimmen. Das war aktuell nicht
ganz einfach, weil sie Rash bisher nicht über die Briefe
unterrichtet hatte. Sindra mochte nicht, wenn Dinge nicht
ausgesprochen waren.

Sie musste endlich mit Rash reden. Sie hatte
sich gefragt, wie sie verfahren sollte, auf Basis eines Briefs in
Rashs Handschrift. Ob sie Rash verdächtigen
sollte. Sindra hatte wirklich keine Ahnung, wen sie verdächtigen
sollte, und vor allem wollte sie niemanden verdächtigen. Sie
hatte auch versucht, über die andere Seite an das Problem
heranzugehen: Wen sie keinesfalls verdächtigen würde. Aber sie
schloss so etwas auch bei keinem der Crewmitglieder aus. Sie
wollte es bei Marah. Dringend. Aber sich wünschen, dass eine
Person vertrauenswürdig war, weil sie sie liebte, war
gefährlich. Es ergab bei den Nixen weniger Sinn, dass sie
nicht hinter der Sache standen. Immerhin ging es um einen
Ort, für den sie seit Jahrzehnten, wenn nicht gar seit mindestens
einem Jahrhundert die Initiative ergriffen, ihn zu schützen. Viele
Nixen wuchsen vor den Küsten des Kontinents Grenlannd auf, bevor
sie in der Weltgeschichte herumreisten.

Das Schiff war ein Nixenschiff. Überwiegend. Eigentlich war
es ein Hybridschiff: Es war geplant als Schiff, dass von Nixen und
Landsleuten zusammen gefahren wurde. Vermutlich das erste
geplante Hybridschiff der Geschichte. Aber das Material kam
aus dem Ozean und der größte Anteil der Planung war Nixen zuzuschreiben, unter
anderem Smjer, sowie einigen, die gar nicht an Bord waren.

Und trotzdem: Auch, wenn die Motive der Nixen klarer und überzeugender
waren, weil es um sie ging, hieß das nicht, dass nicht eine
Nixe andere Pläne haben könnte. Aus welchen Gründen auch
immer.

Sindra war klar, dass ihr Traum nicht bedeutete, dass Amira am
meisten zu verdächtigen wäre. Ihr Traum war aus Angst entstanden, und
hatte daher ihre Crew umgebracht, angefangen mit Marah. Und
Amira hatte die Fähigkeiten für so etwas, deshalb hatte
ihr Traum das so ergänzt. Ihr tat es fast leid, dass sie das
so geträumt hatte, ihr Traum da so pauschalisiert eine
einfache Begründung auf Kosten der Assassinperson erfunden
hatte.

Sie war nun drei Tage Teil der Crew. So lange hatte Sindra bereits den
Schriftverkehr mit ihrer Kontaktperson an Land eingestellt, nur
Nachrichten bekommen und keine zurückgeschickt. Und das
war gefährlich. Das war die Bedeutung des Anfangs des Traums: Über
die Kontaktperson erfuhren sie, wann welche Forschungsschiffe welche
Routen nehmen würden. Die Informationen, die sie zurücksendeten, waren
an sich weniger relevant. Sie verrieten nie genau, wo sie waren. Das
war nicht notwendig. Trotzdem waren die Rückantworten wichtig. Es
konnte ja nicht verborgen werden, dass keine Nachrichten mehr
zurückgeschickt wurden. Falls sie eine verräterische Person
an Bord hatten, wusste sie das wahrscheinlich. Und
im Traum war das viel zu dichte Forschungsschiff zur Mittagszeit
das, was ihr Hirn aus der Idee machte, dass mögliche
Auftraggebende in Folge dessen eine neue Methode für ein Attentat versuchen
würden, da war sich Sindra einigermaßen
sicher. So sicher wie ein Traum eben gedeutet werden konnte. Es
gab immer Unsicherheiten dabei.

Sindra erbat bei Smjer, ob er seine Steuerschicht etwas verlängern
könnte, damit sie sich mit ihrem Problem in die Kapitänskajüte zurückziehen
konnte. Wie immer widersprach er nicht. Manchmal fragte sich
Sindra, ob es nicht besser wäre, wenn sich ihr mehr entgegengesetzt
würde. Dann wiederum versicherte ihr der größte Teil der
Crew immer wieder, dass ihre Befehle meist sehr begründet und
durchdacht wären, sie sie nie aus Spaß gab, eine Priorität zuordnete, und
es einfach wenig Sinn ergab, zu widersprechen.

Und natürlich galt auch, dass auf einem Schiff nicht ohne
Grund eine Person die Befehlsgewalt hatte, der Folge zu leisten
war. Vor allem, wenn Dinge schnell gehen mussten.

Wie es bei einem Gemetzel der ganzen Crew der Fall wäre. Darauf wäre
Sindra nicht vorbereitet. Das war vielleicht die Bedeutung ihrer
Lähmung im Traum, oder der Vorwurf, dass sie nichts täte. Aber
ersteres konnte auch einfach klassische Angst vor Handlungsunfähigkeit
sein, die in Träumen oft eine Rolle spielte, und letzteres war
der Abwertung zuzuordnen, die Sindra erfuhr, weil ihre
Emotionen nicht so sichtbar waren wie bei anderen und
ihr manche ganz fehlten. Weil es
andere gruselte, dass sie in den gefährlichsten Situationen
gelassen war. Das, was letztendlich im Traum auch zu dem Mord
an ihr durch Amira geführt hatte. Es gab das Vorurteil, dass
Leute, die nicht laut und deutlich fühlten, die in angespannten
Situationen ruhig blieben und die keine Trauer zeigten, von
Grund auf böse und nicht vertrauenswürdig wären. Das Vorurteil
war so stark, dass Sindra manchmal selbst daran glaubte. Vor
allem glaubte, dass sie vielleicht gar nicht lieben könnte. Es
hieß, wer nicht trauerte, konnte
auch nicht richtig lieben.

<!--
Eine Welle eines starken, sehnsuchtsvollen Gefühls rann durch
Sindras ganzen Körper, weil sie an Marah dachte. Und eine
nur leicht weniger leidenschaftliche, als sie an Rash dachte.
-->

Aga, die Ziege, trabte auf sie zu, als sie die Tür zur Kapitänskajüte
erreichte, und ließ sich streicheln. Aga
hatte eine beruhigende Wirkung auf Sindra. Als wüsste die Ziege,
worum es im Leben eigentlich ging. Sie mähte ein wenig traurig, als
Sindra die Kapitänskajüte betrat. Sie war am liebsten an Deck. Sie
würde gleich vermutlich zu Smjer traben. Smjer und die
Ziege hatten auch eine warme Bindung aufgebaut.

Sindra setzte sich auf den größeren der Stühle in ihrer Kapitänskajüte
und betrachtete den Brief erneut. Sie wurde nicht schlau daraus. Die
Ausdrucksweise, der Stil, passte zu keinem der Crewmitglieder. Niemand
hier glaubte, dass sie nur auf Frauen stünde. Zumindest glaubte sie das. Was war
das überhaupt für eine skurrile Information zum Weitergeben?

Rash wusste, dass sie Salvenit sprach. Das hatte nicht in der Nachricht
gestanden. Und doch hatte Amira sie in der Sprache einfach
angesprochen. Die Assassinperson hatte erklärt, dass sie es
einfach ausprobieren gewollt hatte, weil die Liste der Sprachen, die
Sindra sprach, so lang war.

Es klopfte. Sindra packte den Brief wieder ein und legte ihn in
eine Schublade, bevor sie zur Tür ging und sie öffnete. Es war
Amira.

Aus irgendeinem Grund stimmte Sindra der Anblick der
Assassinperson weich. "Magst du dich setzen?", fragte sie.

Amira nickte. Sie wirkte gerade unsicher. Schon über die kurze
Zeit, dass sie sich kannten, schwankte es ganz schön bei
ihr.

Sindra schloss die Tür hinter sich, sobald Amira eingetreten war, und nahm wieder auf
ihrem Stuhl Platz. Die Füße auf der Sitzfläche, was
zu einem gegenseitigen Austausch eines Lächelns führte. "Was
kann ich für dich tun?"

"Ich habe damit gerechnet, dass du mich irgendwann über mein
Leben ausfragen würdest.", sagte Amira. "Darüber, wie vielen
und was für Personen ich das Leben genommen habe. Mir ist es
unangenehm, solltest du das noch vorhaben, dass das Gespräch noch
vor uns liegt."

Das war ein Gespräch für einen Tee, überlegte Sindra. Sie
hatte sich die Frage gestellt, das konnte sie nicht leugnen. Sie
hatte sich aber auch direkt überlegt, dass sie es nicht erfragen
wollte. Sie überdachte es nun doch noch einmal, da Amira quasi das Angebot
machte.

Sie nickte zur Kenntnis nehmend und zündete das Gasflämmchen
unter dem Wasserbehälter für Tee an. Sie hatten diese Konstruktion
von einem salvenischen Schiff gestohlen, fiel ihr ein. Deshalb hatte
Amira vielleicht damit umgehen können. Oder vielleicht, weil
Amira sie ja schon seit einigen Tagen beobachtet hatte, bevor sie
in Erscheinung getreten war.

Solange das Wasser zum Aufwärmen brauchte, setzte Sindra sich wieder
hin und beschloss, bei ihrer vorherigen Entscheidung zu bleiben. "Ich
mag gern eine ähnliche Frage stellen:", sagte sie. "Wieviele
Personen hast du freiwillig und ohne negative Gefühle dabei getötet? Ohne irgendeinen
Druck von außen, der alle anderen Möglichkeiten schlimmer hat erscheinen lassen. Wie
viele Male warst du dabei nicht in einer Situation, in der nicht eine
Option widerwärtiger gewesen wäre als die andere? Und wie oft ging es dabei
sozusagen nur um dein Leben und nicht auch um die Lebenssituation vieler
anderer?"

"Nie.", gestand Amira ohne Umschweife. Sie sah trotz ihrer Unsicherheit
gelassen dabei aus, vielleicht eine Spur wütend. "Abgesehen
von diesem Auftrag.", ergänzte sie.

Den sie ja nicht ausgeführt hatte.

"In dem Fall werde ich dich nie danach fragen.", versprach
Sindra. "Ich gehe davon aus, dass es Trauma ist. Sofern es nicht
eine Gefährdung für uns bedeutet, geht es mich nichts
an, solange du mich nicht als die Person aussuchst, der du dich freiwillig
und von dir aus gern anvertrauen möchtest."

Das Wasser fing bereits zu kochen an. Diese Vorrichtung war sehr
schnell. Amira wirkte nachdenklich und vielleicht schockiert. Sindra
überließ sie sich selbst, als sie mit dem Wasser Tee aufgoss.

"Ich weiß nicht, was ich sagen soll.", sagte Amira und holte
Sindra aus ihren Gedanken zurück. "Ich bin unbeschreiblich dankbar. Aber
ich verstehe es nicht. Gehst du damit nicht ein großes Risiko ein?"

"Ich glaube nicht.", sagte Sindra. "Es gibt so eine Art Schaulust, glaube
ich. Traumata machen Angst. Leute glauben, ein Anrecht darauf zu
haben, jene zu wissen, damit eine Vertrauensbasis hergestellt werden
könnte. Aber es ist, als würde ich einer Person erst vertrauen, wenn
sie sich vor mir entkleidet und sich nackt präsentiert. Was
überhaupt keinen Sinn ergibt."

"Es geht hier darum, dass ich Personen getötet habe.", erinnerte
Amira. "Das stellt im Gegensatz zu anderen Traumata vielleicht tatsächlich
eine Gefahr dar."

"Sehr richtig.", sagte Sindra. "Das ist etwas anderes. Hier vermischen
sich zwei Elemente. Wichtige Inhalte und Trauma. In der
Metapher ist ersteres dann vielleicht eher, was du in
den Taschen hast. Ich habe mir Gedanken darüber gemacht, wie ich
das eine getrennt vom anderen betrachten kann." Sindra entfernte das Teesieb
aus der Kanne und goss ein, nachdem Amira mit einem Nicken beantwortet
hatte, dass sie auch Tee mochte. "Das Detail, dass du gemordet hast, und
zum Morden hierher geschickt worden bist, kenne ich. Das ist wertvoll
zu wissen. Ich weiß nicht, inwiefern ich
viel mehr für mich relevante Informationen dadurch
bekommen sollte, dass du mir im Detail mehr über deine Geschichte darlegst. Es
sei denn, wir können ein Muster in den Aufträgen finden, die du bisher
hattest. Etwas, was uns in diesem Fall weiterhilft."

<!--Wegen der Kürzung ist das Tee trinken nun etwas sehr kurz. -->

"Es waren, wie gesagt, neue Auftraggebende." Amira seufzte. "Ich
teile gern jedes Detail mit dir."

"Alles, was du möchtest.", sagte Sindra und kehrte dann noch
einmal zur Ursprungsfrage zurück: "Ich
werde dir jedenfalls Fragen nach deinen Traumata nicht ohne
Notwendigkeit stellen. Wenn
dich jemand aus der Crew bedrängen sollte, darüber zu reden, darfst
du dich an mich oder Kamira wenden. Wir geben dann weiter, dass
das nicht in Ordnung ist. Und hier hat sich noch niemand gegen
einen Befehl von mir offen widersetzt."

Amira nickte bloß und hielt sich wieder an der Teetasse fest. Aber
dann schob sie doch ein geflüstertes "Danke" hinterher.

"Möchtest du noch etwas klären oder loswerden?", fragte Sindra.

"Das klingt wie ein dezenter Rauswurf.", sagte Amira unsicher.

"Ja. Also, im Fall, dass dem nicht so ist.", sagte Sindra. "Sonst
habe ich Zeit und Ruhe. Und du darfst jederzeit wiederkommen."

Amira trank ihre Tasse leer und stellte sie ab. "Ich
komme vermutlich wieder, wenn ich darf.", sagte sie. Sie stand
auf und wandte sich zur Tür.

"Herzlich gern.", sagte Sindra. "Magst du Rash zu mir schicken?"

Amira wandte sich hastig zu ihr um. Sie wusste wohl, was das bedeutete. Ungefähr
zumindest. Sie nickte. "Brauchst du mich dabei vielleicht
doch?", fragte sie.

"Wie meinst du das?", fragte Sindra irritiert.

"Als Schutz.", antwortete Amira.

Sindra betrachtete sie mit einem ausgiebigen Blick. Dann nickte
sie schließlich. Nicht, dass sie wirklich Angst vor Rash gehabt
hätte. Vielleicht fand sie es eher interessant, wie Rash reagieren
würde, wenn die Person, die einen der Briefe in Rashs Handschrift
an Bord gebracht hatte, dabei
wäre. Und umgekehrt. Ob Rashs Reaktionen etwas bei Amira auslösen
würden.

Sindra war sich auch, als es wieder an der Tür klopfte, noch nicht
sicher, welche der Entscheidungen die bessere gewesen wäre.
