\BefehlVorKapitelDFDM{Katjenka-n.png}{Katjenka regiert das Süd-Ost-Maerdhische Zarenreich der Zwerge und wird von der Flotte der Maare als eines der kleineren Übel gesehen, wenn es darum geht, mit Landvölkern zu verhandeln.}{Fesseln, Gefangennahme, psychische und physische Folter, Ableismus besonders gegen neuroatypische Personen, Völkermord - thematisiert, Rassismus, Selbstverletzendes Verhalten, Blut, Verletzung, BDSM-Anspielung, Gedanken zu Suizid.}

Verhalten
=========

\Beitext{Katjenka}

Selbst in Fesseln verknotet war diese riesige Gestalt zwischen ihrer
Hofwache beeindruckend. Katjenka betrachtete sie neugierig. Überlegte
einen Moment, ob sie Angst haben sollte. Aber die Festung war gut
bewacht. Ihre Leute richteten ihre Waffen auf die Person und zwangen
sie in die Knie. Sie wehrte sich kaum, im Gegenteil, legte dabei
einiges an Rest-Eleganz in die Bewegungen.

"Wie heißt du?", fragte Katjenka. Sie entschied sich für ihre
Landssprache Kazdulan und hoffte, die Person würde sie verstehen. Sie
überragte Katjenka selbst auf den Knien. Allerdings hatte sie ihr Gesäß
dabei nicht herabgesenkt.

"Sindra.", antwortete die Person. "Pronomen, wenn du über mich
sprichst, sie, ihr, ihr, sie. Ich fühle mich mit der Bezeichnung
Frau wohl."

Katjenka runzelte die Stirn. Wenn auch ihr nicht gleich klar war, was
Sindra ihr mit diesem Hinweis sagen wollte, war sie doch beeindruckt
vom Kazdulan. Es war mit Akzent gesprochen, aber ohne Zögern, als wäre
sie es gewohnt, die Sprache zu sprechen.

Die Schattenscholle war gerade wieder im Hafen eingelaufen, mit guten und
schlechten Nachrichten: Sie hatten die Schattenmuräne verloren. Sie
hatte sich vor den Augen ihrer Crew in Nebel aufgelöst, wie sie berichtete. Aber
aus dem Nebel war dann ein kleines, sehr schnelles Segelboot
hervorgebrochen. Sie hätten es niemals eingeholt, wenn es nicht
irgendwann gekentert wäre. Die Person darin, Sindra, war nicht in
der Lage gewesen, es wieder aufzurichten und einzusteigen. Es war
für eine erheblich leichtere Person gemacht.

"Gebt ihr trockene Kleidung!", befahl Katjenka. "Ich möchte mich
in Ruhe mit ihr unterhalten. Lasst dann auch die Fesseln weg, aber
bewacht sie gründlich. Diese Meerleute sind geschickt im Entwischen."

"Sehr wohl.", sagte eine der Wachen, stellvertretend für die anderen.

Sindra durfte wieder aufstehen, was sie wieder nicht ohne eine für die Größe
erstaunliche Eleganz tat, und wurde aus dem Raum geführt.

Katjenka winkte Junita heran, die im Hintergrund gestanden hatte, unter
anderem um im Zweifel zu dolmetschen. "Kennst du den Akzent?"

Junita schüttelte den Kopf. "Es ist kein Akzent, den ich zuordnen
könnte. Es ist kein skandernscher Akzent, aber am ehesten würde
ich ihn dort einordnen."

"Skandern war dieses relativ wenig bewohnte Gebiet im höchsten Norden
Maerdhas, richtig?", fragte Katjenka, die Stirn runzelnd.

Junita nickte.

"Wohnen dort Riesen?", fragte Katjenka.

"Einige.", antwortete Junita. "Es ist nicht so sehr bekannt, wieviele
dort wohnen. Sie wurden lange gejagt und nur wenige haben den
Völkermord überlebt." Junita blickte Katjenka direkt ins Gesicht, was ungewöhnlich
für sie war, wenn sie nicht allein waren.

"Ist sie ein Riese? Oder ein Mensch?", fragte Katjenka. "Eigentlich
sieht sie nämlich weder wie ein Mensch noch wie ein Riese aus."

"Warum ist das so wichtig?", fragte Junita.

Das war eine interessante Frage. Also, nicht nur die Frage an sich war
interessant. Sondern auch, dass Junita sie stellte. Hier. Katjenka
nickte. "Vielleicht ist es nicht wichtig.", sagte sie. "Ich wollte
vor allem wissen, was uns blüht."

"Wir können nichts darüber aus der Herkunft schließen.", erklärte
Junita. "Zum einen bestehen unsere Kenntnisse über Riesen quasi
aus Vorurteilen, die bewusst für die Rechtfertigung
des Völkermords entwickelt worden sind. Zum anderen hat Sindra
vermutlich so lange Zeit auf See verbracht, dass ungewiss wäre, wie
sich eine andere Prägung auf sie ausgewirkt haben kann."

Katjenka nickte. Junita brachte sie manchmal sehr gut wieder
gedanklich dorthin zurück, wo die Musik spielte. Sie mochte
sie dafür. "Aus den Gerüchten über die Flotte der Maare schließend würde
ich vermuten, wir haben es mit der Kapitänin zu tun?"

Junita nickte. "Das deckt sich mit fast allen Geschichten, die
bei uns ankommen."

Die Geschichten waren nicht sehr zuverlässig und durchsetzt mit
Spuk und Nebel. "Hast du noch einen wichtigen Gedanken?"

Junita schüttelte den Kopf. Auf ein Zeichen der Zarin zog sie
sich zurück in den Hintergrund.

---

Katjenka verlegte sich mit ihren Leuten in die große Empfangshalle
der Kramelin, einem alten Säulen- und Kuppelbau, der schon über
Generationen hinweg der Zarfamilie für die Regierungsgeschäfte und
als Behausung zur Verfügung gestanden hatte. Sie ließ, einem
weiteren Gerücht folgend, das noch weniger bestätigt war, einen
Tisch und zwei Sessel in den hohen Raum tragen, sowie Tee
zubereiten. Es hieß, die Kapitänin und Tee wären zwei nicht
trennbare Dinge. Es hieß auch, die Kapitänin sei grausam auf
eine untergründige Art, die nicht leicht auszumachen wäre. Sie
wäre manipulativ und ein Monster.

Katjenka wollte sich davon nicht beeindrucken lassen, aber
eine unbestimmte Angst konnte sie nur schwer leugnen. Sie
rief sich das Bild dieser großen, nicht einzuordnenden Person noch
einmal in Erinnerung, versuchte sich mental vorzubereiten.

Und schließlich betrat Sindra den Raum. Begleitet von zwei Dutzend
Wachen. Sie hatten Katjenkas Ermahnung wirklich ernst
genommen. Sindra überragte sie um fast eine halbe
Körperlänge. Sie wirkte selbstbewusst und hatte ein Lächeln
auf dem Gesicht, kein breites, nur gerade so viel, dass es
eben eines war.

Sie trug ein Kleid, das ihr viel zu eng und zu kurz war. Es
war immerhin nicht für Zwerge gedacht gewesen. Sie hatten durchaus
für den Fall der Fälle einige Kleider für zum Beispiel Elben
aufbewahrt, der Mode des Zarenreichs für die anderen Körper
nachempfunden. Sie hatten wahrscheinlich für Sindra das größte Kleid
ausgesucht, das sie hatten. Vielleicht hätten sie es eher
mit Hosen probieren sollen. Immerhin war Sindra in welchen
gekommen. Das Kleid war modisch betrachtet ein
etwas veraltetes Modell, hatte ein feines Drahtgeflecht integriert, das
den Rock um sie herum bauschig wirken ließ.

Katjenka war es schleierhaft, wie sich dieses Bild zusammenfügen
konnte, aber es funktionierte. Sindra bewegte sich nun anders, graziler
vielleicht. Wie sie so eine Strähne ihres endlos langen, braunen
Haars am Scheitelansatz begann hinter das Ohr zu legen und von
dort aus hinter den Rücken. Das Haar war schwer und dezent gewellt.

"Ich hoffe, unsere mangelhafte Ausstattung an langen Kleidern
wirft kein allzu schlechtes Licht auf unsere Gastfreundschaft.", begrüßte
Katjenka erneut. Sie hoffte, dass Sindra ihre furchtlos
wirkende Höflichkeit wenigstens etwas beeindrucken würde.

Sindra sah an sich hinab, als hätte sie noch gar nicht realisiert, dass
das Kleid zu kurz war. Sie strich beiläufig eine Falte aus
dem Rock, blickte wieder auf und schüttelte den Kopf. "So ein
Problem hatten wir auch einmal an Bord. Wir sind gewohnt, das
beste aus dem zu machen, was da ist."

Katjenka lud sie ein, ihr gegenüber am Tisch Platz zu nehmen. Eine
der Wachen rückte den größeren der Sessel für sie vom Tisch ab. Sindra
warf einen Blick darauf. "Ist er gefedert?", fragte sie.

Katjenka runzelte die Stirn. Legte sie in Gefangenschaft wirklich
darauf wert, dass der Sessel gefedert wäre? Löste
ihre Art, mit Sindra umzugehen, das Gegenteil von Eindruck
aus? "Ja, denke schon.", antwortete sie trotzdem.

"Ich bin sehr schwer.", informierte Sindra. "Es ist schon einmal
vorgekommen, dass ich Federungen mit meinem Körpergewicht
beschädigt habe. Möchtest du, dass ich das Risiko eingehe?"

War das ein Schmunzeln? Machte sie sich über Katjenka lustig?

Katjenka besann sich darauf, dass es hier nicht um eine Anspruchshaltung
ging, sondern Sindras Hinweis als Angebot für sie gedacht gewesen war. Sie
nickte schließlich, und gewann die Fassung wieder. "Wir haben
Ersatz.", informierte sie. "Setz dich."

Sindra rückte den Sessel selbst in den passenden Abstand zum
Tisch, der Wache, die es andernfalls übernommen hätte, freundlich
aber bestimmt zunickend, und nahm darauf Platz.

In dieser simplen Geste lag so einiges auf einmal verborgen, was
Katjenka Unbehagen bereitete. Kein großes Unbehagen, aber doch spürbares.

Sindra verhielt sich in keiner Weise wie eine Gefangene. Nicht so,
als könnte sie sich überhaupt unterordnen. Katjenka war nicht
gewohnt, mit so einer Person zu reden, vor allem nicht mit
einer Person, die kein Land regierte. Sondern eine Flotte. Sie
sprach mit einer Regentin, die sie noch nicht kannte, und
die eigentlich keine Mittel hatte, aber trotzdem Macht über
sie.

Doch was ihr am meisten zu denken gab, war die Reaktion
ihrer Wache auf das Zunicken gewesen. Sie war auf Sindras wortlose
Gesten angesprungen, als hätte sie Sindra zu gehorchen. Als
wäre sie Sindras Wache und nicht Katjenkas.

Sindra musste in ihre Schranken verwiesen werden.

"Du solltest dankbar sein, dass wir dich gefangen genommen
haben.", sagte die Zarin zu ihr. Zu dieser außerordentlich bemerkenswerten
Person. Nicht zuletzt, weil sie es geschafft hatte, sich
in der bauschigen Rockmode inzwischen im
Schneidersitz auf den Sessel mit seinen zu schmalen Lehnen zu positionieren
und völlig unbeeindruckt auszusehen.

"Hast du das Wort 'wir' betont?", erkundigte sich Sindra.

"Das habe ich.", bestätigte die Zarin.

Das feine Lächeln auf Sindras Gesicht wurde breiter. "Von
allen Nationen, die mich hätten gefangen nehmen können, ist
dies mitnichten die schlechteste Option." Der Wortschatz
sprach dafür, dass sie Kazdulan sehr gut
kannte. Das war interessant und beeindruckend. "Dennoch
erschließt sich mir nicht, warum ich dankbar für ein
Unrecht sein sollte, nur weil schlimmeres Unrecht
geschehen könnte."

"Ihr habt Schiffe bestohlen und Forschungsvorhaben
sabotiert. Wie kommt ihr auf die Idee, dass euch
hier das Unrecht geschieht?", fragte Katjenka, vielleicht
etwas lauter als nötig.

Sindra reagierte darauf, indem sie sich selbst Tee
eingoss, ohne den Schneidersitz zu entknoten, und sich
anschließend mit einer dampfenden Tasse zurücklehnte. "Meines
Wissens seid ihr über unsere Motive und daher unsere
eigene Einordnung, was warum Unrecht ist, informiert.", sagte
sie bloß ruhig. Den Blick auf den Teedampf gesenkt, blies
sie sachte darüber.

Es stimmte. Die Schattenscholle hatte ihnen auf verschiedene
Weisen eine Reihe an Nachrichten zukommen lassen. Als Reaktion
zu ihrem Angebot auf Verhandlung.

Katjenka ärgerte sich. Dieser Person war nicht so leicht
etwas vorzuspielen. Sie hätte es gar nicht erst versuchen
sollen. Vorspielen barg immer ein gewisses Risiko, hinterher, wenn
es fehlschlug, noch weniger ernst genommen zu werden. Obwohl es gar
nicht genau den Punkt traf, was Katjenka in dieser
Konversation fehlte. Respekt und Ernstnehmen war da. "Du
bist gewohnt, dich nicht unterzuordnen." Dann würde Katjenka
es vielleicht mit Ehrlichkeit versuchen.

"Nicht sehr.", gab Sindra freundlich zu. "Es haben sich
bisher vor allem Personen auf Spiele mit mir eingelassen, die
eher submissiv sind, woraus sich für mich die dominante Rolle
ergab. Ich wäre aber einem Spiel mit umgekehrter Rollenverteilung
nicht abgeneigt."

Katjenka schluckte. Der sanfte, vielleicht etwas alberne Gesichtsausdruck
dieser riesigen Person tat etwas mit ihr. Sie verstand nicht, worum
es ging, aber ihr Innerstes reagierte mit einer sehr seltsamen Art
von, vielleicht, Sympathie? Sie war sich nicht sicher, ob sie wissen
wollte, worum es ging. Und fragte trotzdem: "Einem Spiel?"

"Oh!", machte Sindra. Der Gesichtsausdruck von eben verschwand ohne
Rückstände. "Es ging um meine Rolle als Kapitänin?"

Katjenka runzelte die Stirn und nickte.

"Ich war Kapitänin der Maare.", bestätigte Sindra, was
Katjenka ja schon vermutet hatte. "Für solange
sie existiert. Ich habe das Kommando gestern abgegeben. Es ist
mir nicht schwer gefallen. Ich bin es nicht gewohnt, ja, aber
es fällt mir nicht schwer."

"Das kommt ziemlich anders rüber.", sagte Katjenka. Sie versuchte
eine gewisse Strenge in die Stimme zu legen.

Sindra lächelte. "Dann geht es doch um Spiele.", sagte sie. "Ich
kann eine Person anerkennen als eine, die über mich bestimmt, weil die
Handlungsfähigkeit einer Gruppe davon abhängt, dass es eine
Person tut. Ich kann auch ein ungleiches Machtverhältnis, wie
es zwischen uns existiert, als solches anerkennen. Damit
meine ich nicht, dass ich es gutheiße. Aber beides zeigt
sich bei mir nicht in gebückten Körperhaltungen oder irgendetwas
anderem, das Leute oft erwarten zu sehen, wenn sie solche Fragen
bezüglich der Fähigkeit, sich unterzuordnen, stellen. Diese
Reaktionen sind Spiel. Manche spielen
es unterbewusst oder weil ausreichend Druck auf sie ausgeübt
wird, dass sie spielen müssen. Wenn ich muss, merke ich
das sehr bewusst. Möchtest du, dass ich spiele?"

Katjenka sah sich im Raum um. Die Körperhaltungen der ihr
Untergebenen waren identisch und sie empfand sie als
hilfreich. Sie hatte sich all die Fragen durchaus schon
gestellt. War es nötig, war es gut? Aber als ein Spiel
hatte sie es noch nicht bezeichnet. "Ist es wirklich
ein Spiel, oder ist es nicht viel mehr eine Art, Bereitschaft
und Respekt zu kommunizieren?"

"Kommt drauf an. Und es muss kein Widerspruch sein.", sagte
Sindra. "Ich spreche in der Tat die
Sprache, vielleicht Hofsprache, in der
Respekt über Höflichkeitsregeln und Unterordnungsgesten
kommuniziert wird, in keiner Weise fließend. Das heißt
nicht, dass ich dich nicht respektierte." Sindra nahm
sich die Zeit, einen Schluck des Tees zu trinken. "Ich
habe meine eigene, für mich natürliche Weise, Respekt
zu kommunizieren. Ich kann versuchen, sie in deine
zu übersetzen, aber dann wäre es Performance. Ein Spiel."

Katjenka fiel durchaus auf, dass die Wörter 'Hof' und
'Höflichkeit' verwandt waren und dass Sindra dies
bewusst eingesetzt hatte. "Kazdulan
sprichst du ziemlich gut.", merkte Katjenka an. Vielleicht
um abzulenken.

"Das ist eine ganz andere Art von Sprache.", sagte
Sindra schlicht.

"Aber deine Körpersprache ist eigentlich auch recht flexibel.", überlegte
Katjenka. Warum ließ sie sich darauf ein?

"Danke.", sagte Sindra. "Du beobachtest aufmerksam. Das ist
auch eine Sprache, da hast du recht."

"Ich behaupte, die Körpersprache, die Respekt bekundet, ist
eine, die du lernen kannst, aber nicht lernen willst.", kam
Katjenka zum Schluss. Sie hoffte, damit endlich etwas
zu bewegen.

"Es ist eine, die ich wahrscheinlich sogar recht zügig sprechen kann. Vielleicht
brauchbar genug, dass nicht auffiele, dass es ein Schauspiel
wäre, obwohl sie mir fremd ist und nicht behagt.", sagte
Sindra. "Die Frage ist, wenn dir daran gelegen ist, mit
mir Verträge zu schließen, ob du dann möchtest, dass ich dir
einen Respekt vorspiele, den ich selbst nicht als Respekt verstehe. Möchtest
du das?"

Katjenka erinnerte sich, dass Sindra das schon einmal gefragt
hatte. Und dass sie die Frage nicht beantwortet hatte, weil sie
nicht wollte, dass Sindra ihr etwas vorspielte, aber sie das
Gefühl hatte, dass sie ihr auf der Nase herumtanzte, und das
wollte sie erst recht nicht. "Mach dich nicht lustig über
mich.", befahl Katjenka mit ruhiger Stimme.

Sindra setzte die Teeschale ab. "Selbstverständlich nicht."

"Warum glaubst du, dass wir immer noch Verträge eingehen
wollen?", fragte Katjenka. "Wir haben zwei von euch gefangen
genommen. Reicht das nicht, um darzulegen, dass wir in
der Übermacht sind und nicht auf Verträge mit euch
angewiesen?"

"Ihr wart schon immer in der Übermacht.", antwortete Sindra
gelassen. "Also, nicht konkret das süd-ost-maerdhische Zarenreich der Zwerge. Landvölker
im Allgemeinen. Es hat sich nichts geändert."

"Wir kennen euer Geheimnis.", setzte die Zarin nach. "Und bedeutet
es wirklich gar nichts, dass wir die Kapitänin gefangen
genommen haben?"

Sindra schüttelte den Kopf. "Ich bin ersetzbar.", sagte sie. "Es
werden neue Methoden entwickelt werden. Die Lage ist mies, aber
das war sie auch schon immer."

"Das klingt nicht, als wäre die Flotte der Maare besonders
gewillt, nun zu verhandeln." Katjenka stand auf und schritt
durch die Halle. Sie winkte Sindra, ihr zu folgen.

Die ehemalige Kapitänin stand auf und folgte ihr. Die Wachen
folgten ebenfalls, dezent im Hintergrund, wie sich das gehörte. Ein
Teil von Katjenka fragte sich, ob sie Sindra gehorchen würden,
wenn sie nur ein Kommando aussprechen würde. Und dann fühlte
sie für einen kurzen Augenblick Panik, dass vielleicht die Kramelin unterwandert
sein könnte. Das war Unfug. Diese Macht hatte die Flotte der
Maare nicht. Aber wieso war diese riesige Person so gelassen? Würde
sich das gleich ändern?

Sie traten um eine Ecke in einen anderen Bereich der Halle, der
von der Teerunde nicht einsehbar gewesen wäre, wo sie die Nixe
aufgehängt hatte. Sie baumelte an ihrem Schwanz ruhig vor sich
hin.

"Uns ist zu Ohren gekommen: Du hängst an dieser Nixe.", sagte Katjenka
gelassen. Sie wählte bewusst ein Wortspiel. Um zu provozieren, um
diese Person vielleicht endlich dazu zu bringen, zu verstehen, wie
die Lage aussah.

Sindra blickte zur Nixe hinauf. Die Nixe öffnete die Augen und blickte
zurück. Der Blickaustausch war nicht kurz, aber Sindra zeigte
keine besondere Entrüstung, bevor sie sich wieder an Katjenka
richtete. "Ich hänge an dieser Person. Ja."

Hatte sie das Wortspiel überhaupt bemerkt? Aber eigentlich war
es ihr sehr zuzutrauen. Sie sprach die Sprache so fließend, dass
es ihr kaum entgangen sein konnte. Sie war auch sonst sehr
aufmerksam.

Das war Katjenka allerdings auch. Ihr war durchaus aufgefallen, dass
Sindra das Wort 'Nixe' mit dem Wort 'Person' ersetzt hatte.

Sindra wandte sich wieder an die Nixe und gab Geräusche von sich, von
denen sich Katjenka nicht einmal sicher war, ob es sich um eine
Sprache handeln könnte. Oder ob es Gesang oder einfach liebliche
Geräusche sein mochten. Die Nixe fauchte zurück, dass Katjenka
zuckte. Aber Sindra ließ sich davon nicht beeindrucken.

"Hast du zu ihr gesprochen?", fragte Katjenka.

Sindras Blick wirkte unergründlich. "Wir sind uns recht einig, dass
die Aufhängung eher kein gutes Licht auf deine Gastfreundschaft
wirft.", teilte sie mit. "Da das vorhin deine Sorge war, können
wir daran arbeiten?"

"Hat die Nixe Höhenangst?", fragte Katjenka, einen neuen
Provokationsversuch startend.

Sindra blickte wieder zur Nixe hinauf. Diese starrte einige
Momente zurück, schlug dann die Lider nieder und nickte.

"Versteht sie uns?", fragte Katjenka.

Sindra und die Nixe tauschten ein Grinsen aus. Es war so
seltsam. Zuvor hatte die Nixe sich gegen jede Form von
Interaktion mit ihr lautstark und mit Einsatz ihres Körpers
gewehrt. Nun hing sie da und grinste.

"Wir können sie ein bisschen herunterlassen, wenn es dich
glücklich macht.", räumte Katjenka ein.

"Das wäre reizend.", antwortete Sindra. "Angesichts der
Tatsache, dass sie kleiner ist als du, und sich an Land viel
schlechter bewegen kann als wir, habe ich mir allerdings
erhofft, sie könnte mit mindestens dem gleichen Respekt behandelt
werden wie ich."

Wieder erinnerte sich Katjenka daran, dass Sindra die Nixe mit
Person referenziert hatte. "Sieh es als Lebensversicherung.", argumentierte
sie.

"Inwiefern brauchst du in deiner Kramelin mit zwei Dutzend Wachen
eine Lebensversicherung?", fragte Sindra. "An Land. Bezüglich
zwei Personen, die auf See zu Hause sind."

"Ich wüsste gern, wie wichtig sie dir ist.", wechselte Katjenka das
Thema.

"Sehr.", antwortete Sindra mit einem schmalen Lächeln.

"Aber müsstest du dann nicht hier stehen und weinen, oder schreien
und fluchen?", fragte die Zarin. "Wenn ihr sagt, dass dort in Grenlannd
schützenswertes Leben ist, das euch so viel wert ist wie euer
eigenes, also so viel wert, dass unsere Art Forschung zu sehr
schmerzen würde, dann würde ich gern sehen, wie sehr du
an dieser Nixe hängst. Sonst kann ich dir oder euch das
schwerlich abkaufen."

Sindra seufzte und senkte den Blick. "Ich zeige Emotionen nicht
typisch.", sagte sie. "Das heißt nicht, dass ich sie nicht habe."

"Zeig sie!", befahl Katjenka. Sie fühlte sich nicht gut dabei. Aber
auf der anderen Seite fühlte sie sich nicht wohl in Gegenwart
einer Person, die keine Gefühle zeigte. Das war ihr unheimlich
und suspekt. Ja, das war es, was ihr vorhin nicht klar werden
wollte. Es ging nicht um Respekt oder Ernstnehmen. Es ging darum, dass
Sindra gefühlstot war. "Zeig sie, und du kannst deine Nixe
wiederhaben."

Sindra blickte sie an, blinzelte einige Male. Nichts passierte. Und
dann gab die Nixe Töne von sich. Wie Sindra vorhin. Sindra blickte
auf und lauschte, genau, wie die Zarin. Im Gegensatz zu Sindra hatte
die Nixe unverkennbar feuchte Augen.

"Was sagt sie?", fragte Katjenka.

"Ob ich manchmal weine, wenn ich mir physischen Schmerz
zufüge.", murmelte Sindra. "Aber ich habe vermindertes
Schmerzempfinden. Nein, das hilft wenig."

Versuchte die Nixe, Sindra zu beraten, was diese tun könnte, um
den Anschein emotionalen Fühlens zu erwecken, das sie eigentlich gar nicht
hatte? Katjenka wusste nicht genau, was sie dazu antrieb, aber
sie reichte Sindra das Messer, das in der toten Nixe gesteckt
hatte, das die Assassinperson nicht wieder eingesammelt
hatte.

Sindra nahm es entgegen und schnitt sich ohne zu zögern damit
in den Arm. Einen Moment passierte nichts, dann quoll Blut aus
dem Schnitt hervor. Rotes. Normales rotes Blut. Was hatte
Katjenka erwartet?

"Das ist eher kontraproduktiv.", teilte Sindra ruhig mit. Sie
wischte die Klinge grob auf der eigenen Haut ab und reichte
sie zurück. "Ich könnte mich umbringen, was auch wenig
Sinn ergäbe."

"Du hast einfach keine Gefühle.", hielt Katjenka fest. "Warum
sollte ich Verträge mit Personen eingehen wollen, die keine
Gefühle haben? Halten gefühlstote Wesen die dann? Wo
ist da Verlass? Inwiefern seid ihr gefühllosen
Maare vertrauenswürdig?"

"Wieso bringst du Vertrauenswürdigkeit mit Gefühllosigkeit
in Zusammenhang?", fragte Sindra. "Und warum denkst du, ich
wäre gefühllos, all die Dinge in Betracht ziehend, die ich
gesagt habe, und für die ich stehe, die ich gemacht habe, und
für die ich mich einsetze, lediglich darauf basierend, wie
ich Emotionen zeige oder eben nicht zeige?"

"Du hast den Ruf einer Monsterkapitänin.", entfuhr es Katjenka. "Wenn
ich Verträge schließe, dann möchte ich wissen, woran ich bin. Ich
habe genug Hinterhalte erlebt, dass ich mich absichern
möchte." Das hatte sie wirklich. Allerdings hatten die Herrschenden,
die sie hinters Licht geführt hatten, Emotionen gezeigt. Gespielte. Die
Argumentation war nicht ganz schlüssig.

Sindra blickte sie lange schweigend an. Wenn da gerade doch ein
Fetzen Wut gewesen sein mochte, war sie nun wieder nachdenklich
und ruhig. "Das ist ein Moment, in dem ich denke, dass ich mich
vielleicht zu weit über die Reling lehne. Ich habe dich
nicht zu beraten.", leitete sie ein. "Ich mag trotzdem anmerken, dass
es bessere Mittel gibt, mögliche Hinterhalte unwahrscheinlicher
zu machen."

"Und welche?", fragte Katjenka gereizt. Nein, es stand dieser Ex-Kapitänin
wirklich nicht zu.

"Zwei Varianten fallen mir ein.", sagte diese ungerührt. "Druckmittel, die
stark genug sind. Aber sie müssen immer stark genug sein. Das ist
aus meiner Sicht eine in vielen Punkten nachteilhafte Variante." Sindra
hob erst einen und nun zwei Finger hoch. "Gute Konditionen. Die Ziele und
Ethik der Flotte der Maare liegen offen und sind
konsistent. Biete uns etwas an, wovon wir und ihr profitieren, und was
uns in besagter Ethik und besagten Zielen nicht zu Kompromissen
zwingt."

"Ihr habt eines der Ziele sehr klar gemacht.", reagierte die Zarin. "Wir
sollen Grenlannd nie erreichen."

"Aye.", stimmte Sindra lächelnd zu.

Es war nicht zu fassen. "Und wie könnten wir von euch profitieren, wenn
ihr nicht den Weg freizugeben bereit seid?"

"Ab diesem Punkt möchte ich Vorschläge höchstens unterbreiten, nachdem
ihr aufhört, meine Herzperson zu foltern.", sagte Sindra sachlich. Sie
blickte hinab auf die selbst hinzugefügte Verletzung am Arm, aus der
das Blut inzwischen langsamer sickerte. "Das war ein interessantes
Erlebnis.", murmelte sie. "Es hat mich noch weiter beruhigt und
tut es immer noch. Kontraproduktiv einfach."

Die Nixe über ihnen bewegte sich, schüttelte den Kopf und grinste. Katjenka
war sich inzwischen sicher, dass sie jedes Wort verstand. Es machte
sie immer noch fassungslos: Sie hatte eine Nixe an ihrem Fischschwanz aufgehängt, die
jedes Wort verstand, aber es erst in Anwesenheit dieser riesigen Person
zugab. Sie hatte die Kapitänin der Flotte der Maare gefangen. Und
sie kommunizierten mit Blicken, als wären sie frei, als würde
Zarin Katjenka ihnen völlig egal sein. Und die stärkste gezeigte
Emotion war die der Nixe, als Katjenka versucht hatte, bei Sindra
welche zu erzwingen. "Ihr seid wirklich eine Monsterkapitänin.", murmelte
sie.

"Du ihrst mich plötzlich?", bemerkte Sindra schneller als Katjenka
selbst.

"Ich möchte sehen, wie du reagierst, wenn wir deine Nixe sezieren.", sagte
Katjenka. Sie hatte das eigentlich nicht vor. Aber sie sah die Drohung
als eine Art Therapie. Es war nicht gut, Emotionen zu verbergen, und
am meisten nicht für die Kapitänin selbst. Entweder, Sindra
hatte tatsächlich keine, dann würde die Drohung nicht schaden, oder
es würde nur gut für sie sein.

"Glaubst du, dass du mir damit einen Gefallen tust? Mich durch Erpressung dazu zu
bringen, Emotionen zu zeigen?" Sindra hatte also direkt verstanden,
worum es ging. "Emotionen, die ich sehr wohl habe. Aber die dir
nicht richtig ausgedrückt erscheinen. Denkst du wirklich, dass irgendeine
Person von uns dadurch gewinnt, dass du mich in dieses Muster deiner erwarteten
Emotionsexpression presst?"

War das wieder die leichte Wut? Sindra rührte sich nicht vom
Fleck, versteifte sich etwas. Auf einmal fühlte Katjenka
die untergründige Angst vor der Monsterkapitänin und all den
Gerüchten von vorhin deutlich.

"Und nachdem du das reflektiert hast, vielleicht noch die Frage:", fuhr
Sindra fort. "Glaubst du, es
ist eine besonders durchdachte Idee, eine Person, die ungefähr doppelt so
groß ist wie du, die direkt neben dir steht, die dich vielleicht mit einem
gut platzierten Schlag töten könnte, zu einer Emotion zu provozieren, die
sehr wohl auch ein impulsiver Wutausbruch sein könnte, der nicht an morgen denkt?"

"Ich verstehe", sagte Katjenka vorsichtshalber, damit Sindra wusste, dass
sie zuhörte, und hoffentlich nichts in dieser Richtung tun würde. Vielleicht
hielt Sindra den Körper so steif, um es zu vermeiden. Sie war nicht
aggressiv. Eigentlich nicht.

"Wir kennen uns kaum.", sagte Sindra, immer noch mit dieser
Anspannung. "Was versprichst du dir davon, psychologische Spiele
mit mir zu spielen, anstatt zu verhandeln? Ist es ein morbider Spieltrieb? Ist
es Rache? Oder ist es die schlichte Feindlichkeit gegenüber Personen, die
nicht deine Sprache sprechen. Damit meine ich nicht Kazdulan, und
nicht einmal unbedingt Körpersprache, worüber wir vorhin sprachen. Sondern
Sprachen, von denen dir gerade wahrscheinlich nicht einmal bewusst ist,
dass es sich dabei um Sprachen handelt. Macht dir das Angst? Ist es das?"

"Die Welt ist voller Monsterherrschenden!", erscholl eine neue Stimme
durch den Raum, klangvoll und wütend. "Aber Sindra ist keine davon!"

"Bist du in der Lage, Marah aufzufangen?", fragte die Stimme einer
anderen Person auf Salvenit, einer Sprache, die hier fast niemand
sprach. Aber Katjenka hatte sie nicht ohne Grund erlernt. Ein
Grauen stieg in ihr auf. Die Person hatte gezielt in einer
Lautstärke gesprochen, dass sie nicht schrie, aber für
Sindra und Katjenka gut hörbar war.

Katjenka war nicht klar, wie sie mitten im Raum aufgetaucht war, aber sie
trug die Uniform der Assassinen, die sie so fürchtete. Ein Messer flog
durch die Luft und zerschnitt das Seil, an dem die Nixe hing. Sindra
musste in dem kurzen Moment, in dem Katjenka abgelenkt gewesen war, unter
die Nixe getreten sein und geantwortet haben, vielleicht mit einem
Nicken. Als Katjenka nun wieder hinsah, sah sie ein Bild, das emotionaler
war als alles, was sie hatte provozieren wollen. Diese riesenhafte
Gestalt hatte die Arme fest um die Nixe geschlossen, und gleichzeitig
liebevoll und sanft, küsste die Stirn der Nixe, sich
nicht viel Zeit nehmend, aber dafür umso mehr Innigkeit.

Dann brach Chaos aus.
