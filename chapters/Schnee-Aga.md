\BefehlVorKapitelDFDM{Aga-n.png}{Aga ist eine Bergziege. Sie ist von den Maaren bei einem Überfall auf ein Forschungsschiff an Bord gelangt.}{Zumindest etwas offenes Ende.}

Schnee
======

\Beitext{Aga}

Schnee. Diese weiße, kühle Nässe. Die habe
ich vermisst. Und nun liegt sie hier, völlig
unerwartet an Bord. Dass es auch an Bord schneien
kann, damit habe ich nicht gerechnet. Aber schön so. Wirklich
hübsch. Diese kleinen Flocken, die im Wind über
das Deck und in die Segel streichen. Und dann aus
der unteren Wölbung des Stoffbauchs herunterrieseln, wenn keine mehr
hineinpassen. Direkt auf mich und um mich herum. Sie
sind symmetrisch. Die meisten. Ich frage mich, warum?

Ein bisschen wie Berg ist es nun auch. Der
Wind macht das. Der schiebt das Schiff nämlich nicht
nur nach vorn, sondern lehnt es auch ein bisschen
an die Wellen an, sodass das Deck leicht abschüssig
zur Seite ist.

Janasz klettert zweimal am Tag die Finkennetze hinauf. Aber
der Schnee behagt ihm nicht so wie mir. Ich habe mich
gefragt, ob ich das für ihn probieren soll, solange es
schneit. Ich habe mit den Finkennetzen so einen kontinuierlichen
Disput. Sie sagen, sie seien das kletterigste hier
an Bord. Aber sie sind auch nicht aus Stein oder wenigstens
aus Holz, sondern haben etwas Labberiges an sich.

Ich denke, ich sollte nicht hinaufsteigen. Noch nicht
zumindest. Es ist nicht schlimm, wenn jeder Tag ein
Noch-Nicht-Tag ist.

Wir fahren nach Grenlannd in diesem Winter. Dort wird
es nicht schneien, sagt Rash. Ich mag den Schnee. Aber
Grenlannd muss gut sein und ich freue mich trotzdem
darauf. Viele an Bord haben eine ganz
warme Stimmung deswegen. So eine, wo es sich für sie
nach Heimreise anfühlt.

Und auch, wenn ich wohl nie selbst heimreisen werde, fühle
ich das Gefühl, als wäre es mein eigenes. Vielleicht
kann ich dann auch mal hier ankommen.
