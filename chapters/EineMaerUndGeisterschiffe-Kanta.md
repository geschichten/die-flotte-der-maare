\BefehlVorKapitelDFDM{Kanta-n.png}{}{Lebensmüde Gedanken, Leichenschändung, Erwähnung von Sex, missbräuchliche Eltern-Kind-Beziehung, Gaslighting und Gewalt erwähnt, Slutshaming, Ableismus, Machtmissbrauch, binäres Weltbild, GateKeeping im Zusammenhang mit Geschlecht, Erwähnung von Genitalien, wirbellose Tiere (erwähnt), Quallen (erwähnt). Das Kapitel ist viel, viel länger als das davor.}

Eine Mär und Geisterschiffe
===========================

\Beitext{Kanta}

Kanta würde die minzteraner Universitätsbibliothek vermissen. Die kühlen,
vertrauten Räume hatten ihr Zuflucht geboten, als sie es gebraucht hatte. Hier
hatte sie zu träumen gewagt, das erste Mal das Gefühl gehabt, ihr Leben
hätte einen entfernten Sinn oder wäre es wert, gelebt zu werden. Und
hier war all die aufgebaute Hoffnung wieder zerschlagen worden, lag
in Trümmern. Und nun bot sie ihr Trost. Ihre Eltern hatten ihr
zugestanden, sie ein letztes Mal zu betreten, um sich von diesem
Leben verabschieden zu können. So drastisch hatte sie es nicht ausgedrückt, das
hätten sie nicht verstanden. Aber Abschied an sich verstanden sie.

Professor Lange würde nicht hier sein. Es war Nacht. Er war nicht
informiert, dass sie hier war. Wahrscheinlich schlief er. Oder
wälzte sich wach im Bett herum und kam mit sich selbst nicht klar. In
ihr stritt sich ein kleiner Teil gegen den größeren, nachgiebigeren, ob
sie sich letzteres wünschen sollte. Immerhin war er auch Auslöser
dafür, dass ihre Welt in Scherben lag. Aber er war genauso Auslöser
dafür, dass sie etwas gehabt hatte. Er hatte sie unterrichtet. Er
hatte sie Teilhaben lassen an seinen Forschungen und Studien, hatte
ihr gezeigt, wie sie sich hier in der Bibliothek
zurechtfinden konnte. Und er hatte mit ihr
geschlafen, was auch schön gewesen war. Allerdings hatte nie zur
Option gestanden, dass er sie heiraten würde, weil er bereits verheiratet
war. Ein Umstand, der ihr im Besonderen nie gefallen hatte und den
sie widerwillig geschluckt hatte, weil sie durch ihn das erste Mal
überhaupt gefühlt hatte, und das hatte sie nicht einfach aufgeben
können.

Sie hätte sich gern von ihm verabschiedet. Obwohl er sie verraten
und entblößt hatte. Als ihre
Cousine sie mit ihm in der Bibliothek entdeckt und ihn gefragt hatte, ob
er der geheimnisvolle Mann wäre, mit dem sie
verheiratet wäre -- sie hatte ihre Familie belogen, dass
sie verheiratet wäre, um Freiheiten haben zu können --, hatte er
ihr wahrheitsgemäß gesagt, dass das nicht der Fall wäre. Darüber
hinaus hatte er laut darüber nachgedacht, wie unwahrscheinlich das eigentlich
war, dass sie verheiratet wäre, aber mit ihm noch nie über ihre Ehe
gesprochen hätte. Seine Ausführungen hatten für sie nicht einmal
logisch Sinn ergeben. Vielleicht hatte er sich aus etwas
herauswinden wollen. Und als der Schwindel so aufflog: Ob
es sich denn schickte, dass sie
unverheiratet mit ihm jeden Tag in der Bibliothek studierte.

Es war eine Unverschämtheit. Vielleicht war ihm das mehr passiert, nicht
aus einer bestimmten Strategie heraus, sondern weil er oft
nicht ganz bei der Sache war. Trotzdem war es so eine Unverschämtheit, davon
zu reden, was sich schickte, aus seiner ehebrechenden Position heraus.

Kanta wunderte sich, dass sie das am meisten verletzte. Dabei
war es die Information über ihr Lügen und nicht seine verletzenden
Überlegungen, die ihr Leben zerstört hatte. 

Ihre Eltern waren gewalttätig. Sie schlugen sie nicht, nicht mehr
zumindest, und in ihrer Kindheit war physische Züchtigung in ihrem
Elternhaus verglichen mit anderen weniger vorgekommen. Daher hatte
sie lange nicht verstanden, warum sie sich so davor fürchtete, warum
es so eine Folter für sie war, daheim zu sein. Beziehungsweise bei
ihren Eltern. 'Daheim' war kein passendes Wort dafür. Daheim war
sie hier. Die Gewalt hatte vor allem in einer Art der Kommunikation
bestanden, die sie davon überzeugt hatte, dass sie keine Lebensberechtigung
hätte, dass sie minderwertig wäre, nichts könnte und widerlich
wäre, ohne dass diese Ausdrücke je gefallen wären. Es war eher so, dass
sie die wenigste psychische Gewalt erlebte, wenn sie es einsah und
selbst aussprach.

Um dem sich daraus ergebenden, permanenten Minderwertigkeitsgefühl
etwas entgegen zu setzen, hatte sie immer gründlich gelernt und sich am Ende im
Lernen verkrochen. Das war ihr Anlass gewesen, die Bibliothek
zu betreten. Und hier hatte sie bei Professor Lange das erste
Mal gelernt, wie es war, ein Elb zu sein, eine Person, die
als liebenswert wahrgenommen werden konnte. Und als schlau. Er
hatte den Austausch mit ihr geliebt, erst, weil sie zuhörte wie
ein Schwamm, dann, weil ihn faszinierte, wie schnell sie
lernte, und schließlich, weil sie die richtigen Fragen stellen
konnte, die ihn weiterbrachten. Er war Sprachwissenschaftler. Kanta
liebte Sprachen. Sprachmuster, Rhetorik, Ursprünge, Sprachen, die
aus anderen Elementen bestanden, nonverbale Sprachen, die
Klänge, die Gedichtsformen, alles daran. Sie hatte gerade angefangen, sich
für das wenige, was über die Sprache der Nixen bekannt war, zu
faszinieren, als es passiert war. Das Aufdecken ihres zweiten, ihres
eigentlichen Lebens, das ihr nun wieder nicht nur verboten, sondern
auch verwehrt war.

Sie wusste nicht genau, was nun anstand. Sie würde wohl tatsächlich
mit irgendwem verheiratet werden. Es hatte irgendwann auffliegen müssen, das
war ihr klar. Es grenzte fast an ein Wunder, dass sie
ihr Lügennetzwerk so lange hatte aufrecht erhalten können. Natürlich
hatten ihre Eltern ihren Ehemann kennen lernen wollen. Den
sie angeblich auf einer Studienfahrt geheiratet hatte. Aus
technischen Gründen, weil irgendein Teil der Exkursion nur
verheirateten Frauen gestattet gewesen wäre, hatte
sie es ebenso angeblich unterwegs tun müssen und nicht erst, als
sie wieder daheim wäre. Von der Geschichte war lediglich wahr, dass
sie auf der Studienfahrt gewesen war. Dabei hatte sie
sich überlegt, dass es vielleicht ihre beste und letzte
Möglichkeit wäre, die Geschichte über einen Ehemann in
die Welt zu setzen, um dann längerfristig mehr
Freiheiten zu haben. Sie hatte
dafür unzählige Ausreden und Geschichten gebraucht, warum
ihre Eltern wieder und wieder keine Möglichkeit bekamen, ihn
zu treffen. Und der
Ehering war weder einfach zu beschaffen gewesen, noch hatte sie Geld
dafür gehabt, eigentlich. Sie kramte ihn aus der kleinen Tasche, die sie
dafür in ihren Rock eingenäht hatte. In jeden ihrer Röcke. Ein
Ehering für eine Ehe mit sich selber. Und sie wollte sich eigentlich
nicht von sich selbst scheiden lassen.

Sie gedachte, wegzulaufen. Sie wusste noch nicht genau wie, aber sie
hatte eine Idee, wohin. Die Geisterflotte.

"Ich habe damit gerechnet, dass ich dich hier finde.", hallte eine
leise Stimme durch die verlassene Bibliothek.

Nicht Professor Lange, aber eine Stimme, die sie gerade umso lieber
hören wollte. "Ich habe mit dir überhaupt nicht gerechnet!", rief
sie ebenso leise zurück. Sie steckte den Ring zurück in die Tasche.

Arwin schloss die Türen zur Bibliothek hinter sich, während sie ihm
entgegenkam. Sie wäre am liebsten gelaufen, hätte ihn in die Arme
genommen, an sich gedrückt und geweint. Stattdessen eilte sie so
schnell, dass es gerade noch kein Laufschritt war, nahm seine Hand
und drückte sie zärtlich gegen ihr schneller klopfendes Herz. Es
war kein verliebtes Herzklopfen. "Seid ihr überfallen worden?", fragte
sie. Sie verkniff sich, gleich zu fragen, ob es die Geisterflotte
gewesen war.

Arwin nickte. "Wir mussten umkehren. Ich bin an Deck geblieben. Es
war gruselig."

Arwin war Forscher und hatte auf einem Forschungsschiff für eine
Entdeckungsreise angeheuert.

Kanta ließ seine Hand los. Sie gruselte sich reichlich wenig. "Gibt
es Tote oder Verletzte?", fragte sie.

Bei Forschungsunternehmungen
der Universität Minzter, für die die Staaten der Bantine auf
Antrag die finanziellen Mittel zur Verfügung stellten, war es noch
nie zu Todesfällen gekommen. Angegriffen und bestohlen worden, sodass
die Reise abgebrochen werden musste, waren sie allerdings jedes Mal.

Aber es hatte Schiffbrüche gegeben, die
mit der Geisterflotte in Verbindung gebracht wurden. So
manches Handelsschiff war nie irgendwo angekommen. Sowie
gab es Gerüchte, dass einige Forschungsschiffe anderer Völker und
Nationen weniger glimpflich davon gekommen wären.

Es hörte sich
für Kanta weit weg an, und unstimmig. Sie arbeitete dicht
mit Leuten zusammen, die sich mit der Erforschung des neuen Kontinents
mehr oder weniger direkt befassten. Die Kommunikation mit
anderen internationalen Universitäten war nicht unbedingt
immer durchsichtig, sie forschten in mehr oder minder unfreundlicher
Konkurrenz, aber es gab dennoch Informationsaustausch. So
etwas wie Handel: Dieses Material gegen diese Erkenntnis. Es
war unwahrscheinlich, dass
noch keine Geschichte mit festmachbareren Fakten bei ihr
angekommen wäre, welchen, die über 'hätte auch irgendwie
ein Unfall sein können' hinausgingen, wenn es entsprechende
blutige Übergriffe der Geisterflotte wie aus den Gerüchten
gegeben hätte.

Dann wiederum war das
süd-ost-maerdhische Zarenreich der Zwerge, aus dem die meisten
der brutalen Gruselgeschichten stammten, ausreichend weit weg
und die Berichterstattung langsam und mit am undurchsichtigsten, dass
Kanta letzte Zweifel doch nicht abstreifen würde.

"Quasi nein.", sagte Arwin. "Es sei denn, du zählst ein paar blaue Flecken und
dieses Mal eine kleine Platzwunde. Die aber schon halbwegs verarztet war, als
wir den Matrosen fanden."

"Hui, das ist mal eine interessante Geschichte.", entfuhr es Kanta.

"Wir glauben inzwischen, dass sie nur Vorräte klauen wollen. Und es ist völlig
unklar, wie sie das machen.", erklärte Arwin. "Allmählich glaube ich
auch an Magie oder Hexerei oder Geister."

Kanta kicherte. "Du und Geistergeschichten. Es muss wirklich unheimlich
gewesen sein."

"Wir können an Bord nicht zu viele sein, weil wir ja genug Proviant für die
Überfahrt brauchen und jede weitere Person entsprechend mehr Proviant
braucht. Und weil die Schiffe nicht so groß sind. Der Staat möchte
nicht so viel verlieren, deshalb setzt er keine größeren Schiffe als
für so eine Überfahrt nötig für
die Forschungsreisen ein. Zumindest nicht, bis wir wissen, dass es
wahrscheinlich ist, dass wir es schaffen.", berichtete Arwin.

Kanta wusste das alles. Das hatte er schon einmal erzählt, als
er anheuern wollte. Aber sie unterbrach
ihn nicht. Vielleicht wäre ja irgendein neues Detail dabei, das sie
gebrauchen konnte. Immerhin hatte er so einen Überfall dieses
Mal selbst erlebt und berichtete nicht aus zweiter Hand.

Sie hatte schon manches Mal irgendwo in ihren unrealistischen
Träumen die Idee gehabt, auf eines der Schiffe der Geisterflotte
zu gelangen. Das ergab sich fast automatisch aus den Ideen, vielleicht
auch mal auf einem der Forschungsschiffe Minzters anzuheuern. Die
Forschungscrews wurden natürlich fast ausschließlich oder sogar
ausschließlich aus Männern zusammengestellt. Ihr würde das
verwehrt werden. Soweit kannte sie sich in ihrem Umfeld aus.

Aber von hier wegkommen, auf eine Weise, die mit Forschung
und Abenteuer in Zusammenhang stand, war ein Traum, der sie
immer wieder einholte. Oder viel mehr so etwas wie ein Muss.

Der nächste Gedankenschritt war immer gewesen, sich auf
einem Forschungsschiff zu verstecken. Sie würde allerdings
eine ganze Überfahrt nach Grenlannd wahrscheinlich
nicht überleben, wenn sie versteckt bliebe. Und sie hatte nicht
mit einer gnädigen Behandlung zu rechnen, wenn sie erwischt würde. Es
waren schon unregistrierte Passagiere auf Schiffen von und
nach Minzter hingerichtet worden, meist welche, die versucht
hatten, sich oder Ware von Maerdha nach Arelis oder zurück zu schmuggeln. Kanta
wusste nicht, wie anders das bei Reisen in Richtung Grenlannd
gehandhabt würde. Spannenderweise hatte sie davor weniger
Angst, als ihren Eltern nach einem solchen Vorhaben ausgeliefert
zu werden. Für eine ganze Reise nach Grenlannd wäre sie
das Risiko wohl eingegangen, aber dort war noch kein Schiff
angekommen.

Also hatte sie überlegt, ob es vielleicht möglich wäre, sich nur zu
verstecken, bis die Geisterflotte angriff. Ab da war ihr Plan noch
vage, eher noch Fantasie. Sie wusste nicht wie, aber ihre Hoffnung wäre dann, auf
eines der Geisterschiffe überzusetzen. Bevor Arwin gekommen war, hatte
sie angefangen, sich das erste Mal darüber konkrete Gedanken
zu machen, aus den unrealistischen Vorstellungen einen Plan für einen
durchführbaren Versuch wachsen zu lassen.

Die Geisterflotte machte keine Toten, wenn es Überlebende gab. Das klang
irreführend. Es hieß, dass die Geisterflotte, sobald
sie einem Schiff zu nahe käme, das ganze Schiff versenkte und überhaupt
keine Überlebenden ließe, aber sie hatte schon oft Schiffe angegriffen, ohne
dass diese überhaupt gemerkt hatten, dass sie angegriffen worden waren, und
dabei waren vor allem Lebensmittel und vielleicht ein paar andere Gegenstände
wie Karten abhanden gekommen, nie war jemand getötet worden.

Es waren Schiffe samt Crew vollständig gesunken und verschwunden. Ohne
Überlebende, die davon berichten könnten, konnte natürlich niemand nachweisen, dass ein
Angriff der Geisterflotte Ursache dafür gewesen wäre oder eben nicht. Das klassische
Problem. Es wunderte Kanta nicht, dass die Geisterflotte mit dem
unerklärbaren Phänomen der scheinbar kontaktlosen
Überfälle und anderer Gruseligkeiten für Erklärungen herhalten musste, wo
es noch keine bessere gab. So entstanden dann wohl Gerüchte.

Trotzdem, ein klein wenig hatte Kanta durchaus Bedenken, dass
sie sich Gefahren nur ausredete. Vor Minzter waren häufiger Schiffsteile
und manchmal sogar Leichen angeschwemmt worden, die entstellt oder
zumindest nicht wahrscheinlich durch ein Unwetter in diesen Zustand
gebracht worden waren. Aber Kanta vermutete dahinter etwas anderes als
die Geisterflotte. Es passte für sie nicht zusammen.

"Wir haben versucht, alle Stellen, an denen das Schiff irgendwie heimlich
geentert werden könnte, zu überwachen. Wir haben nichts gesehen.", fuhr
Arwin fort. "Wir haben in einiger Entfernung das Geisterschiff gesehen. Es
war in unheimlichen Nebel gehüllt. Der Rumpf wirkte grünlich, als wäre er
voll mit Algen bewachsen. Einige meinen, das Schiff würde aus den Tiefen
der Meere auftauchen und Unglück bringen. Wir haben versucht, die Verfolgung
aufzunehmen und eine Weile mithalten können. Es war ein schnelles Schiff, aber
es war, als wollten sie eine Weile den Abstand konstant lassen. Dann irgendwann
erhob sich das Schiff aus dem Wasser, als würde es wachsen, und rauschte davon. Wir
konnten nur noch hinterher sehen."

"Und die Vorräte waren weg.", ergänzte Kanta.

"Ja richtig. Also, sie haben uns ausreichend übrig gelassen, um
wieder hierher zu kommen, aber mehr eben auch nicht.", bestätigte
Arwin. "Und die zwei Mann, die wir zur Bewachung
der Vorräte dagelassen haben, die uns einen Hinweis geben sollten, woher
der Überfall passiert, woher sie kommen, schliefen friedlich. Wie durch
einen Schlafzauber. Wir haben sie für eine ganze Weile nicht aufwecken
können."

"Sie fallen also aus dem Stehen in den Schlaf.", folgerte Kanta.

"Ja, das ist auch mein Gedanke. Beziehungsweise unser aller. Und
er passt mit dem Bericht der Wachen zusammen.", bestätigte Arwin. "Sie
erinnern sich, dass sie im Stehen wie aus dem Nichts plötzlich
einschlafen und umfallen. Heinreld hat nicht mehr mitgekriegt, wie er dabei
mit der Stirn auf eine Kante gekracht ist. Daher die Platzwunde. Der
Schrank hatte hinterher einen kleinen Schaden, der zur Wunde passt. Die
Wunde war grob versorgt."

Arwin setzte sich an einen der Lesetische, nachdem er ihr einen Stuhl
vom benachbarten bereit gestellt hatte. Sie setzte sich dazu. Er
durchwühlte seine Tasche und legte ein paar Stücke Papier mit
Kohlezeichnungen vor ihr ab. "Ich habe das Geisterschiff versucht
zu zeichnen.", sagte er.

Und er konnte sehr gut zeichnen. Es zeigte das Schiff von der Seite ohne
Nebelschwaden, von der Seite mit Nebelschwaden, und
mit weniger Nebelschwaden von hinten. Besonders das
mittlere hätte so in ein illustriertes Buch mit Horrorgeschichten
gekonnt. "Hübsch!"

"Du hattest mal gesagt, du wollest bei der Geisterflotte anheuern.", erinnerte
sich Arwin.

Kanta wurde schlagartig sehr heiß. Sie blickte auf. Ja, das hatte sie.

"Wie ernst war das gemeint?", fragte er. "Ich habe mich nie getraut,
zu fragen. Ich weiß, dass du Geheimnisse birgst. Ich weiß nicht, wie
schlimm dein Leben aussieht."

"Keine Geheimnisse mehr.", widersprach Kanta. "Sie sind in deiner
Abwesenheit aufgeflogen. Ich kann dir alles erzählen." Nach
kurzem Zögern entschied sie sich, es zusammenzufassen: "Ich
war nie verheiratet. Die Ehe war eine Lüge."

"Ich habe es mir fast gedacht.", murmelte Arwin.

"Ich habe dir einfach vertraut, dass du bei den Geschichten bleiben
würdest, die ich erzähle, und vor dir nicht so genau auf
mein Spiel geachtet.", gestand Kanta.

"Ich habe nie Zweifel an deinen Geschichten geäußert, habe sie
so behandelt, als wären sie wahr.", sagte
Arwin. "Es hat für mich nie eine Rolle gespielt. Und
du musst mir nichts weiteres erzählen."

"Ich würde gern.", sagte Kanta leise. Sie hatte es immer gewollt. Nun, da
alles ohnehin zu spät war, konnte sie es vielleicht. Sich einem Freund
anvertrauen. "Wenn du möchtest." Sie spürte, wie ihre Kehle enger wurde.

Kanta weinte selten. Aber Arwin hatte so eine Art, dass das ging.

"Ich bin gern da, wenn du möchtest.", sagte Arwin, klang einfühlsam
dabei. "Ich weiß nicht, ob, oder glaube nicht, dass ich dir
helfen kann. Ich habe hauptsächlich Forschung im Kopf. Das weißt
du ja. Ich hatte nie irgendwelche schlimmen Probleme. Ich möchte
dich verstehen und für dich da sein, aber ich glaube, ich werde
ganz viel nicht nachvollziehen können."

Das war vielleicht das ehrlichste, das je jemand zu ihr gesagt
hatte. Dachte Kanta. Und dann, dass es vielleicht übertrieben war, so
zu denken. "Zu deiner Frage von vorhin: Ja, ich meinte das sehr ernst. Ich
möchte gern auf die Geisterflotte gelangen." Sie fühlte
ein klein wenig Angst, als sie das sagte. Dass sie vielleicht
belauscht würden. Es war etwas, was kaum jemand verstehen würde, und
wofür sie deshalb mit viel Ablehnung und Schlimmerem rechnete.

"Ich hätte Angst um dich. Sehr große.", sagte Arwin. "Ich habe
lange überlegt, ob ich dich frage. Aber wir suchen tatsächlich
eine Frau, die wir auf die Geisterflotte schmuggeln möchten. Eine
mit viel Bildung. Eine wie dich."

Kantas ganzer Körper kribbelte. Es waren gleich drei Dinge, die
ihn dazu brachten: Die Möglichkeit, die sich hier eröffnete, das
Kompliment an ihre Bildung, und die Stellungnahme, dass sie
die Richtige wäre. Wofür auch immer, im ersten Augenblick
fühlte es sich gerade für sie immer sehr stark an, wenn ihr
eine Person sagte, dass sie geeignet oder richtig wäre. Sie
nickte. Und dann war sie verwirrt. "Warum eine Frau?"

"Wir haben dieses Mal einen Blick auf die Crew des
Schiffes erhaschen können, bevor der Nebel aufstieg. Wir
glauben, dass die Crew nur aus Frauen besteht.", erklärte
Arwin.

Das war mal interessant. Wirklich interessant. Eine Chance
zu bekommen, nicht obwohl, sondern weil sie eine Frau war.

"Es würde lebensgefährlich werden.", ermahnte Arwin sie. "Ich
berichte dir davon schweren Herzens, weil ich dich wirklich sehr
mag. Es ist einfach so wahrscheinlich, dass du das nicht überleben
würdest."

"Es gibt Schlimmeres.", sagte Kanta. "Dies ist das letzte Mal, dass
ich in dieser Bibliothek sein darf. Weil der Heiratsschwindel
aufgeflogen ist. Ich erkläre dir gleich alles.", versprach sie. "Ich
hatte vorgehabt, diese Nacht zu fliehen. Und ich hätte auch nicht
gewusst, ob ich das überlebe." Das entsprach nicht ganz der Wahrheit, aber
fast: Sie hatte angefangen, diese Pläne zu schmieden. Sie hatte es
sich noch nicht fest vorgenommen. "Und zu meinem unfertigen Fluchtplan
gehörte die Idee, mich auf einem Forschungsschiff zu verstecken, um
unentdeckt zur Geisterflotte zu gelangen."

"Warum genau willst du eigentlich zur Geisterflotte?", fragte
Arwin. "Das ist ein seltsam spezifischer Selbstzerstörungs-Fluchtplan."

"Ein paar Gründe, aber vor allem habe ich aus den Geschichten der
Geisterschiffe den Eindruck, dass sie mir Zugang zu einer neuen
Welt verschaffen könnten.", erklärte sie. "Wo legen sie an? Was
für Personen sind darauf? Welches Volk fährt sie? Was ist ihr
Sinn?" Sie strich vorsichtig die Zeichenpapiere glatter, ohne
dabei die Zeichnung selbst zu berühren. "Durch das Wissen, das
wir über die Überfälle haben, glaube ich, dass dahinter idealistische
Leute stehen können, aber ich weiß nicht, was für Ideale sie
haben. Ich frage mich, inwiefern sie die andere Seite kennen, die
sie berauben. Und was sie täten, hätten sie mehr Information."

"Um Informationsaustausch geht es bei dem Auftrag.", sagte
Arwin. "Natürlich ist der Wunsch nicht, dich oder eine andere
Frau einfach auf der Geisterflotte loszuwerden. Der Wunsch ist, dass
du alles über die Flotte herausfindest und irgendwann zurückkommst
und davon berichtest." Arwin blickte sie lächelnd an. "Aber du
bist immer gut darin gewesen, Geheimnisse zu wahren. Wenn du
an Bord der Geisterflotte ein neues Zuhause findest, kannst
du da einfach bleiben. Daran kann dich niemand so leicht
hindern. Du solltest dich bloß nicht mit der Prämisse
bewerben."

"Ich verstehe." Kanta gefiel, dass Arwin ihr gleich diese
Möglichkeit darlegte. Die sie sich ohnehin ausgemalt hatte. Die
Option wurde noch ein bisschen interessanter mit dem Wissen, dass
wahrscheinlich nur Frauen an Bord der Geisterflotte wären. "Wie
ist der Plan für mich, bei der Geisterflotte an Bord zu gelangen?"

---

Sie hatten Glück, dass sie niemand sah, als sie die Bibliothek durch
einen der Hinterausgänge verließen. Kanta hatte noch kein Lügenkonstrukt
gebaut, mit dem ihr eine Möglichkeit zugestanden worden wäre, zum
Hafen zu gelangen. Sie vermutete auch, dass Lügenkonstrukte nicht
mehr so einfach wären wie früher. Eigentlich waren sie nie einfach
gewesen. Aber nun war das Vertrauen in sie, dass sie irgendetwas
Wahres sagen könnte, bei ihren Eltern von vornherein gar nicht
erst vorhanden.

Auf dem Weg zum Hafen besprachen Arwin und sie kurz, was sie
dem Kapitän der Forschungscrew sagen würden. Sie überlegten, dass
die Wahrheit, wenn auch nicht die vollständige, gar keine so
schlechte Motivation darlegte, warum Kanta freiwillig auf
eines der Geisterschiffe gelangen wollte. Sie faszinierte
Forschung und Sprache und sie hatte Grund genug von hier
wegzukommen. Die größere Schwierigkeit, die sich vorhin schon angedeutet
hatte, war zu argumentieren, warum sie auch wieder zurück wollen
würde. Aber das stellte sich einfacher heraus als erwartet:

Der Kapitän und große Teile der Crew hatten Angst vor der
Geisterflotte, glaubten die Geschichten, dass jene auch Schiffe
vollends zerstörten und keine Überlebenden zurückließen, und
konnten sich nicht vorstellen, dass eine Person überhaupt
in Erwägung zog, dort bleiben zu wollen. Sie gingen eher davon
aus, dass Kanta sich eine Weile verstecken und spionieren
wollte, um dann bei sich bietender Gelegenheit wieder von
Bord zu gehen. Allerdings hatte niemand je eines der
Geisterschiffe nah vor Ufer gesehen, also wäre das dann
irgendwo im nirgendwo.

Der Kapitän schätzte das Risiko relativ hoch ein, dass Kanta die Sache nicht
überleben würde. Aber er glaubte ihr trotzdem, dass sie
es eingehen wollte und würde. Seiner Einschätzung nach
waren die Versuche, Grenlannd zu erreichen, für alle Crewmitglieder
lebensgefährlich. Sie alle gingen ein mehr oder weniger großes Risiko ein, ihr Leben auf
See zu lassen, und Kanta eben ein größeres, wobei sie auch
zusätzlich zur Überzeugung, das Richtige zu tun, auch noch
Leidensdruck hatte, der sie drängte.

Er glaubte, dass sein Forschungsschiff
durch eines der Geisterschiffe eines Tages zerlegt werden
könnte. Er glaubte den Gerüchten über die gesunkenen Forschungsschiffe
vor der süd-ost-maerdhischen Küste und hielt für realistisch, dass es ihnen früher oder
später auch so ergehen würde. Aber er lebte für Forschung und
glaubte, dass es eine sinnvolle Strategie wäre, eine Person an
Bord der Geisterflotte zu schmuggeln, um neue Erkenntnisse zu erlangen, wie
sie auf Dauer an jener vorbeigelangen könnten.

Waffen hatten sie trotzdem wenige an Bord. Gründe dafür waren zum
einen ihr Gewicht und Volumen, und die geringen finanziellen
Mittel, die sie dann eher in Navigationsgeräte, Reparaturen und Proviant
investierten. Zum anderen, dass sie die Waffen auch hätten benutzen können müssen. Was
unter Forschenden nur begrenzt gegeben war. Kanta jedenfalls
konnte nicht gut mit einem Schwert, Degen oder auch nur mit
einem Messer umgehen. Letzteres versuchte ein Crewmitglied
ihr zu zeigen.

Der Kapitän meinte, sie wollten direkt wieder ablegen, aber
zu Kantas Enttäuschung meinte er damit nicht morgen oder
so etwas. Sie brauchten neue Vorräte, mussten Verteidigungsschreiben
verfassen, um die geplante Fahrt dann auch wirklich genehmigt
zu bekommen -- es handelte sich nur um Formalitäten, aber
diese brauchten eben Zeit --, Rechnungen
ausstellen und verschicken und dann, als der Papierkram erledigt war, wurden
sorgsam Vorräte ausgewählt, beschaffen und überlegt verstaut, sodass sie vielleicht
nicht so leicht gestohlen werden könnten wie sonst. Ein
paar Crewmitglieder wurden ausgetauscht, aber
Arwin war wieder dabei. Es dauerte zwei Wochen, bis sie endlich
ablegten. Zwei Wochen, in denen Kanta aus einer unergründlichen
Angst bestand, dass ihre Eltern sie doch finden würden oder
irgendetwas Schlimmes passieren würde. Sie
durfte schon an Bord wohnen. Auch ein paar
andere Matrosen blieben an Bord und betrachteten sie
skeptisch. Immerhin nicht wollüstig. Auch davor hatte sie etwas
Angst gehabt, aber der Kapitän hatte ihr versichert, wenn jemand
ihr gegenüber übergriffig werden würde, dürfe sie ihm das
mitteilen, und die Person würde verwarnt und beim zweiten Mal
von Bord fliegen. Zumindest solange sie noch im Hafen lägen. Er
wusste, dass manche Männer so waren, er
hatte Töchter, und er duldete so ein Verhalten an Bord nicht.

Kanta begann die Forschungscrew zu mögen. Sie wurde nicht sonderlich
zurückgemocht. Aber eben wenigstens halbwegs respektiert.

Arwin hatte bis zum Ablegen überwiegend an Land zu tun, aber
an einem freien Abend besuchte er sie, brachte ihr einen Satz
Kleider, weil sie ja nichts hatte mitnehmen können, und machte mit ihr einen
Abendspaziergang am Meer. Es rauschte um ihre Füße auf den weißen
Sand unterhalb der Stadt Minzter mit ihren flachen Sandsteinbauten. Das
Meer war dunkel, aber es war einer der Tage, an denen die Leuchtquallen
sich unter der Wasseroberfläche sammelten. Viele kleinere und größere
blass-helle Punkte waren über das Meer verteilt. Aus dem Grund trug
es den Namen Nachtmeer. Was witzig war, denn in der Crew der
Geisterschiffe wurden auch nicht selten Mahre oder Nachtmahre gesehen.

Bei diesem Spaziergang schüttete sie Arwin ihr Herz aus. Es war
erleichternd, dass da nun eine Person war, die alles wusste.

---

Der Tag der Abfahrt würde sich Kanta für immer in ihre Erinnerungen
einbrennen. Es ging hektisch an Deck zu und war aufregend, wie
die Segel gehisst wurden, aber erst lose waren, weil das Schiff
aus dem Hafen zunächst herausgetreidelt wurde. Und dann waren plötzlich
ihre Eltern da. Wahrscheinlich hatten sie voller überzeugender
Sorge Professor Lange gefragt, wo sie sein könnte, und er
hatte ausgeplaudert, dass sie mit Arwin befreundet war, was
sie schließlich hierher geführt haben mochte. Kanta
wollte unter Deck huschen, aber ihr Vater entdeckte
sie zu schnell.

"Erwarte nicht einen Pfennig von uns, wenn du wiederkommst!", schrie
er.

Es war an sich kein Satz, der sie hätte beängstigen sollen, aber er
tat es. Er versuchte, rufend mit dem Kapitän zu verhandeln, dass
er sie zurückschicken möge, dass sie ihm gehöre. Er warnte den
Kapitän vor ihren Lügen und davor, dass sie alle Männer nur
verführen wolle. Das schmerzte besonders. Der Kapitän ließ
sich nicht beeindrucken. Unter anderem, weil er sehr beschäftigt
mit seiner Crew und dem Ablegemanöver war.

"Möchtest du zurück?", fragte er sie doch, als er einen Moment hatte, der
dafür ruhig genug war.

Kanta lehnte energisch ab. Und das war es. Keine weitere Diskussion.

Nun. Er kannte ihre Hintergrundgeschichte.

Je weiter sie sich schließlich vom Hafen Minzter entfernten, desto mehr
Anspannung fiel von ihr ab. Die Segel waren nun gefüllt mit Wind. Es brauchte
gar nicht so lange, bis die Stadt, in der sie ihr ganzes Leben verbracht
hatte, nicht mehr in ihrem Sichtfeld war. Es tat ihr nicht weh. Höchstens
um die Bibliothek. Und um Professor Lange. Sie liebte ihn durchaus. Trotzdem. Wieso
liebte sie ihn eigentlich? Aber manchmal fiel Liebe eben einfach irgendwohin.

Arwin gesellte sich zu ihr. Und plötzlich weinte Kanta doch. Arwin
legte einen Arm um sie. Sie verriet nicht, dass es Tränen der Erleichterung
waren. Vielleicht wusste er es. Vielleicht vermutete er auch
Abschied. Ihr war es gerade gleich.

---

Es passierte, als sie gerade mal drei Tage unterwegs waren. Der Kapitän hatte
eine andere Route wählen wollen, in der Hoffnung, dass sie dieses Mal vielleicht
zufällig an der Geisterflotte vorbei nach Grenlannd gelangen könnten. Die
Hoffnung war von vornherein kommuniziert gewesen und auch das wäre Kanta sehr recht
gewesen. Grenlannd war Kontinent der Nixen, hieß es, obwohl noch niemand
dort gewesen war. Außer Nixen vielleicht, aber jene sprachen nicht mit
Elben. Es war erst seit Kurzem wissenschaftlich belegt, dass Nixen überhaupt so etwas wie eine eigene
Sprache entwickelt hatten. Für die sich Kanta brennend interessierte. Daher
war es für sie ebenso eine Option, nach Grenlannd zu gelangen, wie es eine
war, auf eines der Geisterschiffe zu kommen.

Es würde nun also letzteres sein, wenn. Wenn sie überlebte.

Es tauchte am Horizont
auf. Der Mann im Mastkorb wusste schon, woran es erkannt werden könnte, und
bereitete sie darauf vor. Ein schmales Schiff, grünlicher Rumpf und blassblaugrüne
Segel. Segel mit einer Art Gestänge darin, schien es. Es war kein Schiffstyp, den
irgendein ihr bekanntes Volk verbaute.

Sie warteten ab, bis das Schiff etwas näher herangekommen war. Wie
Arwin beschrieben und gezeichnet hatte, stieg dabei ein Nebel um das Schiff
herum auf. Und das Wasser darum herum verfärbte sich rot. Es war alles sehr
leise. Es war tatsächlich unheimlich. Der Gedanke, dass sie durch das
rote Wasser und den Nebel mit einem Beiboot rudern würde, wenn
alles gut ginge. Der Gedanke, dass sie sich an Bord eines Schiffes
zu schmuggeln versuchte, das es schaffte, ein anderes Schiff anzugreifen, ohne
an Bord zu kommen.

Sie ermahnte sich, nicht unter Deck zu gehen. Denn Personen, die nahe
der Vorräte wären, würden schlafen und handlungsunfähig sein. Wie auf
magische Weise, aber Kanta vermutete Gift.

Und nun begann ihr wirrwitziger Plan, den sie sich überlegt hatten: Sie
wendeten. Kanta sollte mit einem Beiboot übersetzen. Aber ein Beiboot
war ja relativ langsam. Wenn das große Forschungsschiff voll besegelt
das Geisterschiff nicht einholen konnte, dann konnte sie es mit einem
Beiboot erst recht nicht.

Die Hoffnung war also, dass das Geisterschiff ihnen nachsegeln würde, für eine
kurze Weile, lang genug, um den Fernüberfall zu starten, und lang genug für
Kanta, um heimlich das Geisterschiff zu erreichen, weil es ihr so
entgegen käme.

Zumindest der Teil des Plans ging auf. Das Geisterschiff nahm ein wenig Fahrt auf, um näher
heranzukommen. Nur ein wenig. Arwin beobachtete es mit seinem
Fernglas und machte sich Notizen. Er war nervös. Kanta war auch
nervös, als sie sich in das Beiboot setzte, was auf der
dem Geisterschiff abgewandten Seite zu Wasser gelassen werden würde.

Aber dann beeilte sich Arwin noch einmal zu ihr herüberzukommen und sie fest
über die Bootskante hinweg in den Arm zu
nehmen. "Vielleicht sehen wir uns nie wieder.", murmelte er
mit belegter Stimme in ihr Haar.

"Vielleicht.", bestätigte sie. "Ich werde dich vermissen."

Und das stimmte.

Kanta fühlte dieses starke Gefühl, das wie eine Wucht alles andere aus
ihr wegfegte. Für ein paar Momente. Und dann wurde sie wieder ruhiger.

Sie würde sterben, oder es würde ihr dort gefallen, oder sie würde
wieder versuchen, zurückzukommen. Alles nicht die schlechtesten
Optionen. Es gab noch andere, aber über die dachte sie lieber
nicht so genau nach. Es würde auf jeden Fall aufregend werden.

Arwin selbst half bei der Betätigung der
Seilwinden, mit der sie ins Wasser herabgelassen
wurde. Sie löste die Befestigung und ruderte ein Stück vom Schiff
weg. Das war eine wackelige Angelegenheit, aber sie hatte sich
alles genau erklären lassen. Sie hatte damals in der Bibliothek
über Seefahrt viel gelesen, die Geschichten und das Wissen in sich
eingesogen. Sie fühlte sich nicht verloren. Obwohl sie
es vielleicht war. In einiger Entfernung zum Forschungsschiff
ruderte sie schließlich über das holprige Meer Richtung
Geisterflotte.

Vielleicht hätte sie Angst haben sollen. Sie, allein in einer
Nussschale auf einem Ozean mit vielen Unterwasserkreaturen unter
ihr, die sie vielleicht verspeisen wollten. Wer wusste das
schon. Aber sie hatte keine Angst.

Sie hatte auch keine Angst, als ihr Boot plötzlich in einer Weise
wackelte, die nicht so sehr zu den Wellen passte. Mehrfach. Es
gruselte sie ein bisschen. Aber sie war keine Person, die eine
nicht erklärbare Sache direkt an Geister glauben ließ.

Sie hatte ein wenig Angst, als ihr Boot scheinbar langsamer
vorankam, als würde
sie wie ein Magnet vom Geisterschiff weggedrückt. Sie ruderte
umso kräftiger dagegen an, aber sie hatte das Gefühl, für eine
lange Zeit eher rückwärts zu fahren, gegen ihre Ruderrichtung,
wieder näher zum Forschungsschiff.

Und schließlich beobachtete sie, wie das Geisterschiff
wendete. Nein!, dachte sie. Sie wollte dort hingelangen und
versuchte noch einmal angestrengter zu rudern, mit aller Gewalt, dass
ihre Arme schmerzten. Es machte ihr auf seltsame Art Hoffnung, dass
sie vielleicht dort angelangen konnte. Vielleicht träumte
ein kleiner, unsinniger Teil in ihr, dass die Mitglieder der
Geisterflotte alles Frauen wären, die schlimme, gewaltvolle Erfahrungen
gemacht hatten, unter denen sie auf Verständnis stoßen könnte, nicht
allein wäre.

Und dann, endlich, ließ die seltsame Kraft nach, die sie daran gehindert
hatte, vorwärts zu kommen. Sie merkte, wie sie plötzlich viel schneller
vorankam. Sie ruderte, so schnell sie konnte, ignorierte, dass
ihre Arme müde wurden, bis sie plötzlich, ohne erkennbare
Ursache, kenterte. Das eisige Wasser drang in ihre Kleidung. Ihre
neuen Optionen spülten durch ihr Gehirn.

Sie konnte versuchen, das Boot aufzurichten und weiterzurudern. Aber
sie hatte nicht aufgepasst, es beim Kentern nicht festgehalten
und es trieb immer mehr von ihr weg. Als sie das realisierte, versuchte
sie, ihm hinterherzuschwimmen, aber sie hatte keine Chance.

Das reduzierte die Möglichkeiten auf drei: Entweder die Geistercrew
sammelte sie ein, oder das Forschungsschiff sammelte sie ein, oder
sie starb im Meer, falls sie nicht vorher von irgendetwas gefressen
wurde, indem sie ertrank. Seltsamerweise fühlte sich die letzte Option nicht
wie die schlimmste an.

Ihre Kräfte ließen nach. Angst hatte sie immer noch nicht. Das
wäre vielleicht eine sinnvolle Gefühlsreaktion gewesen. Sie versuchte, nicht
unterzugehen, aber auch nicht mehr irgendwo hinzukommen.

Der Abend dämmerte. Vielleicht würden sie sie von beiden Schiffen
ohnehin nicht sehen. Sie waren inzwischen beide weit weg, sie
dazwischen ein unscheinbarer Kopf zwischen Wellen. Die
Befürchtung bestätigte sich, als ein zweites
Beiboot vom Forschungsschiff ihr Beiboot fand, aber sie nicht. Sie
schrie Worte, aber die Person war zu weit weg. Vielleicht schrie sie
zurück. Aber das wäre erst recht nicht bei ihr angekommen, gegen die
Windrichtung. Das Boot paddelte ein bisschen durch die Gegend, aber
kehrte irgendwann um. Es ließ ihr Beiboot zurück, vielleicht
in der Hoffnung, dass sie es doch wieder erreichen könnte. Aber
Kanta gab sich keinen Illusionen hin.

Als es dunkel wurde, war das Forschungsschiff nicht
mehr zu sehen, das andere konnte sie noch am Horizont
ausmachen, wenn die Wellen es zuließen. Unsinniger Weise fragte sich Kanta, ob die
Vorräte geklaut worden waren. Warum war das ihr erster Gedanke? Warum
war das relevant?

Ein Kopf tauchte direkt neben ihr aus dem Wasser auf und holte
Luft, als hätte die Person viel zu lange nicht mehr geatmet. "Sprichst
du Kazdulan?", fragte sie bald darauf in besagter Sprache.

Es war kein Zwerg. Kazdulan war die Sprache der Zwerge. Wie
kurios. "Ja.", bestätigte Kanta. Sie hatte die Sprache zwar nur
als Fremdsprache erlernt, aber sie hatte durchaus etwas
Übung.

"Wie ungeschickt kann sich eigentlich so eine Forschungscrew
anstellen, ein Crewmitglied wieder einzusammeln?", fragte
die Person. "Du willst an Bord der Schattenmuräne, nehme
ich an?"

Kanta klapperten die Zähne. Der Person neben ihr schien die Kälte
egal zu sein. "Ist das der Name des Schiffs?", fragte Kanta. Wieso
schwamm da ein Kopf direkt neben ihr im Wasser im nirgendwo. Ohne
Boot. Dachte sie, als sie die Erkenntnis traf: "Du bist eine Nixe!"

"Es haben schon Elben länger gebraucht, um auf die Idee zu
kommen.", sagte die Nixe. "Meine Aufgabe war, dich daran zu
hindern, auf die Schattenmuräne zu gelangen."

"Du hast mein Boot erst festgehalten und dann gekentert?", interpretierte
Kanta.

"Genau.", sagte die Nixe. "Letzteres war wohl keine so gute Idee.
Entschuldige."

"Du meinst, ungekentert hätte ich eine Chance gehabt, auf das
Forschungsschiff zurückzugelangen?", fragte Kanta.

"Ja. Aber leider auch die Chance, so weit weg vom Forschungsschiff zu
gelangen, dass sie nicht
einmal mehr gesucht hätten. Du hattest ein ganz schönes Tempo
drauf.", erklärte die Nixe.

"Ich würde gern auf das Schiff." Kanta deutete in die Richtung, in
der sie kaum mehr das Geisterschiff ausmachen konnte. Es war
zu dunkel geworden.

"Und du würdest dein Leben dafür riskieren, scheint es.", fügte
die Nixe hinzu.

"Sieht so aus.", antwortete Kanta. "Ich hatte gerade angefangen,
damit abzuschließen, als du aufgetaucht bist."

"Ich bringe dich an Bord.", beschloss die Nixe. "Ich kann
viel schneller unter Wasser schwimmen. Ich lege dir einen
Finger zwischen die Zähne. Wenn du Luft brauchst, beiß
vorsichtig zu."

"Zwischen die Zähne?" Auf einmal nahm Kanta die Kälte
stärker wahr. Aber ihr Hirn war trotzdem überwiegend
mit der Faszination der Situation beschäftigt: Eine
Nixe, die die Hauptsprache des Zwergenvolks sprach und
sie gleich retten würde. Das war keine Wendung, die sie
vorhergesehen hätte. Allerdings hatte sie ja einen
Plan gehabt, der in jedem Fall viel Unvorhergesehenes bereit
hielt. "In Ordnung.", sagte sie. "Allerdings klappern
mir die Zähne. Ich frage mich, ob das dabei irgendwie
Schwierigkeiten macht. Wäre es nicht praktischer,
wenn ich dich am Arm drückte oder so?"

"Ich habe sensorische Schwierigkeiten mit fremden Händen an meinen
Armen.", erklärte die Nixe. "Außerdem werde ich deinen einen Arm unter
deinem Rücken mit meiner einen Hand so fixieren, dass du dich gegen mich nicht
wehren können wirst, und meine andere Hand gehört eh an
dein Kinn, um deinen Kopf zu überstrecken, falls du mir irgendwann
wegdriftest, damit deine Zunge nicht in den Hals rutscht."

Kanta schluckte unwillkührlich, und dabei auch wieder eine gewisse
Menge kaltes Salzwasser. "Ich komme mir nicht so vor, als hätte
ich meine Zähne gut genug unter Kontrolle. Ich hoffe, das geht
gut.", murmelte sie.

"Ich kann Zähneklappern von bewusst vorsichtigem Beißen
unterscheiden. Ich mache das nicht zum ersten Mal.", sagte
die Nixe. "Ich bin Jentel. Keine Pronomen oder Pronomen
as/sain/ihm/as."

Pronomen. Ein Pronomen, das eigentlich nicht zu Kazdulan
gehörte. "Welche Person, singular oder plural?", fragte
Kanta.

Jentel grinste. Es sah gar nicht so beängstigend aus wie
auf den Bildern von Nixen in Büchern. Jentel hatte
einen relativ normalen Mund. "Das
Pronomen, mit dem du über mich mit anderen sprichst. Ich denke, das
ist dritte Person Singular? Ich bin nicht so sicher bei den Bezeichnungen
in der Grammatik dieser Sprache."

"Du sprichst sie ziemlich gut.", lobte Kanta. "Es ist
nicht die Sprache, mit der du groß geworden bist, oder?"

"Viele Nixen lernen von klein auf mehrere Sprachen. Ich habe
Mandulin und tatsächlich Ilderin gelernt. Die
Elbensprache. Dürfte deine Sprache sein.", sagte
Jentel.

Ilderin war tatsächlich eine weit verbreitete Elbensprache, die
in Minzter gesprochen wurde, und mit der Kanta großgezogen worden
war. "Aber dir ist unangenehm, mit mir die Sprache zu sprechen, mit der
ich groß geworden bin?"

"Unsere Bordssprache ist Kazdulan.", sagte Jentel schlicht. "Ich
unterhalte mich vielleicht später mit dir, wenn du willst. Jetzt sollten wir erstmal an
Bord gelangen. Die Schattenmuräne wartet nur noch auf mich."

Kanta willigte ein, ließ sich einen Finger zwischen die Zähne legen und sich
in einen Zangengriff nehmen. "Tief durchatmen und Lunge etwas mehr als
halb füllen.", empfahl Jentel.

Kanta folgte der Anweisung.

Es war ein unbeschreibliches Gefühl, erfüllte sie mit innerer
Freude, als ihr Körper unter Wasser gezogen wurde und Jentels
Fischschwanz ihnen Antrieb gab. Die Bewegungen waren so
unscheinbar und klein, drückten nur leicht gelegentlich
gegen ihren Körper, aber sie schossen unter Wasser dahin, viel
schneller, als sie je hätte Rudern können.

Sie hätte vielleicht Angst gehabt, sich Jentel anzuvertrauen, ihm
ihr Leben zu übergeben, wenn die Alternative nicht ohnehin zu
sterben gewesen wäre. Sie merkte, als ihr Körper langsam den
Drang hatte, wieder zu atmen. Sie ließ sich nicht zu lange Zeit,
bis sie sehr vorsichtig die Zähne näher zusammendrückte. Jentel
tauchte sofort auf und ließ sie atmen. Kanta blickte sich dabei
um. Die Schattenmuräne, wie
Jentel das Geisterschiff genannt hatte, war ein gutes Stück näher
gekommen.

"Es ist so unbeschreiblich!", rief sie aus, als sie ausreichend
geatmet hatte. "Ich würde sagen, atemberaubend."

Jentel legte ihr als Antwort wieder den Finger zwischen
die Zähne. Es waren Schwimmhäute zwischen den Fingern, die
sich dabei teils über ihr Kinn legten. As ermahnte sie, noch
einmal zu atmen, dann tauchte as wieder ab.

Wieder unter Wasser, die Wassermassen an sich vorbeirasen
fühlend, fragte sie sich, ob Jentel mit zur Crew der Schattenmuräne gehörte, oder
eher extern mit der Schattenmuräne zu tun hatte.

Irgendwie erschloss sich Kanta der Sinn von seefahrenden Nixen
noch nicht so sehr. Auf der anderen Seite, warum sollten sie
nicht zur See fahren? Vielleicht war es so unsinnig, zu fragen, warum
Nixen zur See fahren sollten, wenn sie doch rasch schwimmen
konnten -- wirklich rasch --, wie es Unfug war, zu fragen, warum
Elben und andere Landvölker Kutschen nutzten, während sie doch
schnell rennen könnten.

Sie tauchten nur noch ein weiteres Mal zum Luftholen auf, bevor Jentel
die Schattenmuräne erreichte. Sie tauchten durch den Rumpf
ein. Das war auch überraschend. Sie hatte keine Ahnung, wie
Jentel unter Wasser auf einen Eingang zugesteuert war, oder auch,
wie der Eingang ausgesehen hätte. Es
war stockfinster unter Wasser.

As gab sie an einer rutschenartigen Rampe frei, die von einem
unter Wasser stehenden Unterdeck in ein feuchtes Deck darüber
führte. As ließ ihr allerdings nicht den Vortritt.

"Ich bräuchte eine Augenbinde.", rief as.

"Hast du die Person mitgebracht?", fragte eine andere Stimme.

"Ja, ich hatte keine Wahl.", antwortete Jentel.

As versperrte mit dem eigenen Körper die Rampe, sodass
Kanta kaum daran vorbeisehen konnte. Sie fror inzwischen
jämmerlich, und wenn sie vorhin noch in der Lage gewesen war,
beim Zähneklappern keine Finger zu zermalmen, war sie sich
zum jetzigen Zeitpunkt nicht mehr sicher, ob das nicht nun Folge gewesen wäre, wäre
ihr ein Finger zwischen die Zähne gelegt worden.

Sie hörte einiges an Gerödel, bis eine Hand Jentel eine
Binde herabreichte. Es war ein interessantes Material, mit der
as ihr die Augen verband. Ein weiches, wie Stoff, aber es
hatte eine leicht glibschige oder zumindest sehr reibungsarme
Komponente.

Nun endlich robbte Jentel die Rampe ganz hinauf und zog
sie an den Händen hinterher. Das Material der Binde
war wirklich blickdicht. Kanta kämpfte mit dem Gedanken, ob
sie sie einfach wieder abnehmen sollte. Sie war schließlich
nicht gefesselt. Stattdessen stand sie zitternd auf.

"Ich weiß, du bist gerade erst wieder zurück und möchtest sicherlich
eigentlich ausruhen.", hörte sie eine Stimme. Eine beeindruckende
Stimme mit einem bestimmenden, unnachgiebigen Unterton. "Magst
du trotzdem noch einmal in den Mastkorb gehen und überprüfen, dass
uns niemand folgt?" Wie konnte eine Frage wie ein Befehl klingen?

"Selbstverständlich.", sagte Jentel.

Und wie stieg eine Nixe in einen Mastkorb? Und warum eine Nixe? Kanta
musste fast grinsen, weil sie so viele Fragen hatte. Sie liebte
Fragen. Und Antworten.

"Ich bin stark und würde dich in einen Baderaum tragen, wenn du
erlaubst.", wandte sich die selbe Stimme an sie. In Ilderin nun. Mit
Jentel hatte sie Kazdulan gesprochen.

"Habe ich da ein Mitspracherecht?", fragte Kanta. Sie fühlte sich
provokant dabei, und ein wenig ängstlich, dieser Stimme zu widersprechen.

"Oh ja.", sagte die Stimme. "Ich trage Leute nur mit Einverständnis. Es
sei denn, es besteht ansonsten eine akute Gefahr. Ich möchte, dass
du wenig vom Schiff siehst, bis wir uns unterhalten haben. Ich bin außerdem auch
bereit, das Unterhalten an einem anderen Ort als dem Baderaum zu tun. Diesen
schlage ich vor allem deshalb vor, weil du unterkühlt bist, aus der nassen
Kleidung rausmusst und aufgewärmt werden solltest."

Das klang gut, überlegte Kanta. Es wurde zuerst an ihr Wohlergehen
gedacht. Wie interessant! Sie fragte sich, auf welche Weise sie
mehr mitbekommen würde: Getragen, weil sie dann wüsste, wie sich
die Person anfühlte, die mit ihr sprach, oder geführt. "Würdest
du mich führen?", entschied sie sich.

"Möchtest du, dass ich dich mit einer Hand im Rücken führe, oder
möchtest du meinen Ellenbogen anfassen, um mir zu folgen?", fragte
die Person.

"Letzteres.", entschied Kanta.

Kurz darauf fühlte sie eine Hand, die ihre zu einem Ellenbogen
führte. Eine bestimmende Bewegung, die ihr wenig Raum
gab.

Es war eine gute Entscheidung. Sie wurde eine Treppe hinaufgeführt, die
sie sonst vielleicht nicht mitbekommen hätte. Das Holz unter ihren Füßen
fühlte sich anders an, als Holz es normalerweise tat. Vielleicht
war es gar kein Holz.

Sie gelangte in einen Raum, der sich unwesentlich wärmer anfühlte als
ihre bisherige Umgebung.

"Du kannst dir gern nun die Augenbinde abnehmen.", sagte die Stimme.

Kanta assoziierte sie mit Kälte. Warum, wusste sie nicht so genau. Sie
fürchtete sich ein bisschen vor der Person, stellte sie fest.

Sie nahm die Binde ab und erschreckte sich. Sie hatte schon festgestellt,
dass der Ellenbogen eine gewisse Höhe und Größe gehabt hatte. Die
Person vor ihr war fast zwei Köpfe größer als sie und kräftig
gebaut. Und Kanta konnte beim besten Willen nicht zuordnen, zu welchem
Volk sie gehörte. Die Person strahlte zusätzlich zur Größe noch
eine ganz andere, bestimmende Autorität aus, selbst während sie damit beschäftigt
war, warmes Wasser in einen Bottich zu füllen.

"Ich bin Sindra.", sagte die riesige Person. "Pronomen sie/ihr/ihr/sie. Wir
stellen uns hier mit Pronomen vor. Du kannst mich als Kapitänin bezeichnen. Ich
fühle mich mit der Bezeichnung Frau wohl."

Kanta blickte die Kapitänin verwirrt an. "Warum? Das hatte Jentel vorhin
schon gemacht. Da verstehe ich das noch, weil es ein Pronomen ist, das
ich nicht kannte."

"Woher solltest du das Pronomen einer Person wissen, wenn sie
es nicht sagt?", fragte die Kapitänin.

Kanta beschloss, das erst einmal so stehen zu lassen. Vielleicht war
das eine Sache, damit sie Nixen nicht anders behandelten als alle
anderen. Oder war es mehr? Sie würde es herausfinden. "Ich bin
Kanta.", stellte sie sich vor. "Sie/ihr/ihr/sie." Sie fühlte
sich ein bisschen befremdlich, als sie die Pronomen aussprach, aber
auch nicht sehr. Sie fühlte sich, als ob sie sich gut auf neue
Umgebungen einlassen könnte. Das war ein befriedigendes Gefühl, vielleicht
so etwas wie Stolz.

"Hast du Schamgefühl?", fragte die Kapitänin. "Oder fühlst du
dich ausreichend wohl, wenn ich dabei bin, während du dich wieder
aufwärmst?"

Kanta hatte kein Problem damit, von einer Frau beim Baden beobachtet
zu werden. Das war eher normal für sie. Sie fragte sich einen
Moment, ob sie Scham vortäuschen sollte, um ein wenig allein zu sein. Aber
sie entschied sich dagegen. Sie wollte etwas von der Kapitänin und
dazu wollte sie keinen unnötig schlechten Eindruck machen. "Du
kannst gern dabei sein.", sagte sie also. "Ich würde gern bei
euch anheuern. Ist das irgendwie realistisch?"

"Eins nach dem anderen.", bestimmte die Kapitänin.

---

Gefühlt brauchte es bis zur Mittagszeit des nächsten Tages, bis sich
Kantas Körper wieder aufgewärmt hatte. Sie hatte nicht die
ganze Zeit gebadet, aber selbst warme, trockene Kleidung
und heißes Getränk hatten nicht sofort die erhoffte Wirkung.

Die Kapitänin war eine zwiespältige Person, fand sie. Sie hatte Kanta
eigentlich respektvoll und großzügig behandelt, aber mit
einer unverkennbaren Skepsis und in einem Ton, der Kanta
Angst einflößte. Als hätte die Kapitänin keine Gefühle und
würde auch nicht verstehen, dass Kanta welche hatte. Die Unterredung hatte die ganze Nacht
angedauert und bei der Art der Fragen, die die Kapitänin gestellt
hatte, hatte sich Kanta daran erinnert gefühlt, dass sie nun
für Lügen bekannt sein würde. Sie hatte nicht vor, hier
ein großes Lügenkonstrukt aufzubauen. Sie wollte ein einfacheres
Leben haben und hoffte, dass es hier niemand bedrohte. Trotzdem
war die Unterredung deshalb unangenehm, auch wenn sie verstand,
dass die Crew des berüchtigten Geisterschiffs für die
Sicherheit ihrer Geheimnisse viel fragen
musste. Kanta hatte sich gewundert, dass die Kapitänin keine
anderweitigen Verpflichtungen hatte. Niemand hatte sie unterbrochen.

Kanta war unendlich müde, als sie das Deck endlich betrat. Es war
alles neu und interessant und faszinierend und ein bisschen
beängstigend. Sie fühlte sich etwas verloren, wie sie
so da stand, nun erstmals ohne Aufgabe. Die Kapitänin hatte
zwei Dinge sehr klar gemacht: Dass sie als Crewmitglied einen
Lebensvertrag einging und niemals wieder zurückkehren dürfe. Damit
hatte Kanta gerechnet. Und es galt striktes Alkoholverbot für
die ganze Flotte. Das hatte Kanta ziemlich überrascht. Auf
die Idee wäre sie nie gekommen. Aber sie hatte ohnehin nie
gern Alkohol gemocht. Ihr war gesagt worden, sie würde schon
dareinwachsen. Immerhin war sie gerade erst achtzehn. Mit sechzehn war
es sogar noch normal gewesen, dass nicht alle Alkohol
mochten. Vielleicht war sie nun sogar ganz froh, dass niemand
hier erwarten würde, dass sie dareinwüchse oder sich gewöhnte.

Sie blickte über das Deck. Gerade war fast niemand unterwegs. Hoch
oben im Mastkorb erblickte sie ein Gesicht. Vielleicht war es
Jentel, aber es war zu weit weg, um sicher zu sein. An der Reling stand eine
Person, die Kantas Interesse sofort weckte, als sie sie genauer
in Augenschein nahm. Sie beschloss der Neugierde nachzugehen
und sich neben sie zu stellen.

"Ilderin?", fragte die Person.

Kanta nickte. "Ich dachte Bordssprache ist Kazdulan.", sagte
sie. "Und nun spricht die zweite Person in meiner Sprache
mit mir."

"Hast du angeheuert?", fragte die Person, ohne weiter auf
den Kommentar einzugehen als mit einem Lächeln.

Kanta nickte erneut. "Es ist sehr aufregend.", sagte
sie außerdem. "Kanta." Nach kurzem Zögern probierte sie aus, wie
es war, nun die Pronomen als erstes hinzuzufügen: "Sie/ihr/ihr/sie."

"Ashnekov. Er/sein/ihm/ihn.", erwiderte Ashnekov, hob die Hand
Richtung Stirn als Gruß. "Willkommen im Schattenschwarm."

"Schattenschwarm?", fragte Kanta verwirrt.

"Wir sind eine Flotte aus", Ashnekov zögerte, "ein paar
Schiffen. Ich sage mal sicherheitshalber noch nicht wie viele. Wir
sind am Anfang ein bisschen vorsichtig mit der Weitergabe
von Informationen. Ich hoffe, du verstehst das."

Kanta nickte rasch, damit er ihre Frage beantworten würde.

Er tat ihr den Gefallen. "Sie heißen alle nach dem
Prinzip Schatten- und dann eine Fischart. Dieses
hier ist unser Hauptschiff mit Namen Schattenmuräne. Zusammen
heißt die Flotte der Schattenschwarm."

"Darf ich dir persönliche Fragen stellen?", fragte Kanta.

Ashnekov kniff die Augen zusammen, aber nickte dann. "Aye."

"Bist du Elb? Oder Zwerg?" Die Ohren, die sie sah, deuteten
auf Zwerg hin. Auch die gedrungene Gestalt. Aber ihm fehlte
das herbere Aussehen, seine Züge wirkten runder, wie bei
Elben, er war bartlos und er sprach ihre Sprache.

"Beides.", sagte er schlicht.

"Oh!", machte Kanta. Nach kurzem Zögern fügte sie sicherheitshalber
hinzu: "Ich hoffe, es war wirklich nicht zu persönlich. Ich
kann mir vorstellen, dass du das oft gefragt wirst."

"Hier an Deck inzwischen weniger.", sagte Ashnekov schmunzelnd.

Der Name, fiel Kanta auf, war auch eher ein typischer Zwergenname, kein
Elbenname. "Bist du zweisprachig großgeworden?"

Ashnekov nickte. "Aye."

Wie aufregend! Kanta staunte wieder darüber, was für Personen auf diesem Schiff
waren. Dabei fiel ihr ein, dass sie auf Basis der Vermutung hierher
geschickt worden war, dass die Schattemuräne nur Frauen anheuern
ließe. Was sie an ihre zweite Frage erinnerte: "Bist du ein Mann oder
eine Frau?"

"Ein Mann.", sagte Ashnekov, wirkte aber gar nicht zufrieden mit der
Frage. "Es gibt nicht nur die beiden Alternativen. Und die Frage
finde ich fast persönlicher als die letzte."

"Es tut mir leid.", sagte Kanta. "Ich glaube, da muss ich noch in
etwas reinwachsen." Sie unterdrückte wieder den Impuls zu fragen, ob
es etwas mit den Nixen zu tun hätte. Ihr kam es nun noch wahrscheinlicher
vor, dass es mehr war. Ashnekov hatte für sich gesprochen, als
er gesagt hatte, dass er die Frage persönlich fand. Glaubte sie.

Ashnekov seufzte. Es war ein freundliches Seufzen. Und er tat
es gleich zweimal. "Wo fange ich zu erklären an.", murmelte
er, mehr zu sich selbst. "Eigentlich möchte ich nicht
bei den Nixen anfangen.", sagte er nun wieder normal laut. "Aber
sie sind ein guter Einstieg ins Thema."

"Ich merke mir einfach, dass es auch nicht so gute Aspekte
daran gibt, mit ihnen anzufangen, und frage vielleicht
später, warum.", versicherte Kanta.

"Siren -- das ist die Sprache der Nixen --, und auch die Kultur
der Nixen, -- wobei es natürlich nicht nur eine homogene
Kultur gibt, wie immer --, sieht fünf Geschlechter vor.", leitete
Ashnekov ein. "Und zwar nicht, weil Körper fünf häufige
Ausprägungen hätten, sondern eher Kategorien, in die das
Geschlechtsgefühl oder die eigene Identität eingeordnet wird." Ashnekov
unterbrach sich. "Das ist nicht so gut ausgedrückt. Das klingt so, als
wären die Geschlechter von etwa Elben oder Zwergen irgendwas anderes, hätten
mehr mit ihren Körpern zu tun."

"Haben sie nicht?", fragte Kanta.

"Sie können etwas mit dem Körper zu tun haben, das ist
bei Nixen nicht anders. Aber der Körper
bestimmt nicht das Geschlecht. Der Körper ist nicht Ursache davon, dass
uns Geschlecht, Pronomen und Anreden und so weiter in Sprache so wichtig
sind.", hielt Ashnekov fest.

Kanta fielen allerlei Argumente ein, warum es wichtig sein
könnte, die zwei Kategorien, in die Körper grob unterteilt
werden konnten, in Sprache abzubilden, aber sie überlegte, dass, wenn
Ashnekov das so überzeugt sagte, er diese sicher schon alle
gehört hatte. Also sagte sie erst einmal nichts.

"Ich bin an sich auch nicht Experte, das alles zu erklären. Und
ich rede unstrukturiert und etwas wirr. Ich bin kein
guter Redner. Ich versuche dir nur einen Einstieg zu
geben.", warf Ashnekov ein.

"Da bin ich dir sehr dankbar drum.", sagte Kanta. "Ich habe
studiert. Ich habe dabei herausfinden können, dass ich
damit umgehen kann, wenn mein erstes Verständnis
einer Sache drei-, viermal über den Haufen geworfen wird."

"Das ist gut. Das passiert in dieser Gemeinschaft hier
viel.", sagte Ashnekov schmunzelnd. "Jedenfalls habe
ich gelernt, dass auch weder Elben noch Zwerge, und wahrscheinlich
auch nicht andere Völker, nur zwei Geschlechter haben. Und dass
es wiederum auch Männer gibt, die mit einer Vulvina auf die Welt kommen, oder
Frauen, die einen Penis haben. Das kommt gar nicht so selten
vor, wird aber ziemlich unterdrückt."

Kanta nickte langsam. Sie dachte automatisch an all die Leute, die
sie kannte, ob sie bei irgendeiner der Personen etwas erlebt hätte, das
für Ashnekovs Behauptung sprach. Sie dachte zuerst an Arwin. Sie
fühlte sich nicht zu Arwin hingezogen und sie hatte sich oft
gefragt, warum, weil sie eine gewisse Attraktivität zu
eigentlich allen Männern fühlte. Irgendwie erschien die Idee für sie plötzlich
stimmig, dass Arwin gar kein Mann sein könnte. Aber auf der
anderen Seite: Wer war sie, so etwas über andere Leute zu
vermuten. Es war ein Gedankenspiel, mehr nicht. Es konnte viele
andere Ursachen haben, warum sie sich nicht zu Arwin hingezogen
fühlte, obwohl er eine so großartige, liebe Person war. Jedenfalls
ergab der Gedanke, dass Geschlechter vielfältiger waren, als
sie bisher geglaubt hatte, für sie Sinn, wirkte realistisch. Es
machte sie sofort neugierig und viele Fragen bildeten sich
in ihrem Kopf, wie, hing es sehr mit dem Charakter zusammen?
Wieviel beeinflusste ein untypisch gebauter Körper das
Geschlecht? Was genau war Geschlecht und wie fühlte es
sich an? Sie stellte sich auch zum ersten Mal die Frage, wie
sich Geschlecht für sie anfühlte. Und ja, es war
für sie definitiv mehr als ihr Körper. Sie
nickte noch einmal. "Gibt es Personen an Bord, auf die
etwas davon zutrifft? Außer Nixen, die mit fünf Geschlechtern
vielleicht ohnehin außen vor sind." Sie runzelte die Stirn. "Das
war bestimmt nicht sensibel ausgedrückt."

"Da hast du recht, glaube ich, dass es nicht so sensibel
war. Nixen sind keinesfalls außen vor. Es
ist eher so, dass wir viel von Nixen lernen können.", sagte
Ashnekov. "Wir haben weitere Personen in der Flotte, auf
die etwas davon zutrifft. Manche finden so etwas erst bei
uns über sich heraus. Aber ich werde dir keine persönlichen
Details über die Geschlechter anderer Crewmitglieder
erzählen."

"Deshalb meintest du, die Frage ist persönlich.", überlegte
Kanta leise. "Weil es etwas über das Innerste einer Person
aussagt."

Ashnekov nickte. "Viele reden gern darüber, aber viele auch nicht."

"Das gibt mir viel zu denken.", sagte Kanta wahrheitsgemäß. Sie
mochte die Gedanken. Sie mochte sich darauf einlassen. "Wieso
eigentlich fünf Geschlechter?"

"Auch fünf Geschlechter decken unwahrscheinlich alles ab.", sagte
Ashnekov unsicher. "Aber der große Unterschied ist: Sie werden
nicht zugewiesen. Nixen finden sich im Laufe ihres Lebens selber am ehesten
in einer ihrer fünf Geschlechtskategorien wieder. Zu der Erkenntnis, wenn
sie da ist, gehört eine Feier. Verschieden
groß. Ich stecke da im Detail natürlich nicht drin. Vielleicht fragst
du besser eine Nixe."

"Das werde ich.", sagte Kanta. "Ich bin dir dankbar, dass du mir
darüber Auskunft gegeben hast."

---

Sie sprach durchaus mit einigen Nixen in den nächsten Tagen. Aber es war
gar nicht so einfach, das zu tun, und wenn sie es schaffte, fielen
die Gespräche kurz aus. Die Nixen kamen vorwiegend
nachts an Deck und blieben tagsüber im Nixendeck. Das Nixendeck
war ein privater Ort. Hier hatten Personen, die keine Nixen waren,
nichts zu suchen, solange sie nicht eingeladen worden waren, oder
ein Überfall auf ein Forschungsschiff anstand.

Kanta wurde nicht darüber informiert, wie sie die
Überfälle genau machten. Sie vermutete, dass
es irgendetwas mit dem Nixendeck zu tun hatte. Aber Details
erfuhr sie lange Zeit nicht.

Die Nixen wirkten ihr gegenüber größtenteils reserviert oder distanziert, weshalb
es ihr mit der Zeit unbehaglich wurde, mit
ihnen zu sprechen. Sie hätte sich am ehesten getraut, mit Jentel zu reden, und war
wegen des neuen Pronomens besonders interessiert an ihm, aber
as verbrachte Zeit im Wesentlichen im Nixendeck oder im Mastkorb.

Die Nixe, mit der Kanta am meisten sprach, war Kamira. Er war
Solstim. Das war ein Geschlecht, wie er erklärte, das in
ihre Sprache übertragen zwar eine männliche Komponente
hatte, aber nicht männlich war. Kamira sah allerdings eher kritisch, dass
sich das Konzept überhaupt in ihre Sprache übertragen ließe. Er
beschrieb das Geschlecht eher als schlicht, in sich ruhend, aber
auch offen für Bewegung. Er beschrieb es auch in Farben, aber
so richtig erschloss es sich für Kanta nicht. Das musste es
auch nicht, versicherte Kamira. Geschlecht war nicht unbedingt einfach
zu verstehen, aber es gab auch keine Notwendigkeit dazu, fand er.

Er wirkte stets sehr geduldig. Seine Aufgabe war die psychologische
Pflege der Besatzung, Abbau von Barrieren und Hilfestellung beim
Lösen von Konflikten. Kanta schreckte es irgendwie ab. Sie
fühlte sich nicht bereit, über ihre Probleme zu reden, und vor
allem nicht mit einer Person, deren Aufgabe das speziell war. Aber
Kamira bedrängte sie nie. Und so sprachen sie über Siren. Wenn
Kamira nicht anderweitig zu tun hatte, was häufig der Fall
war.

Rash war das erste Crewmitglied, das keine Nixe war,
von dem Kanta erfuhr, dass Rash
nicht-binär war. So nannte Rash es, dass Rash weder zu jederzeit
noch ausschließlich männlich oder weiblich war. Die Einschränkung
war wichtig, erklärte Rash. Denn manchmal war Rash nicht
ausschließlich etwas außerhalb der Kategorien männlich und
weiblich, sondern zum Teil eines davon und manchmal
auch beides zugleich. Es schwankte.

Rash sprach darüber unsicher, aber offen. Rash bevorzugte keine
Pronomen, sondern immer mit Namen referenziert zu werden.

Rash konnte schreiben und war für Briefverkehr zuständig. Wie
der Briefverkehr funktionierte, erfuhr Kanta nicht. Aber übers
Schreiben kamen sie viel ins Gespräch. Rash schlug ihr vor, Kanta
könne Rash das Schreiben abnehmen, weil Kanta viel flüssiger
schreiben konnte, wenn auch nicht unbedingt schöner. Aber
Kanta lehnte ab. Sie übte lieber mit Rash. Rash machte
der Unterricht Spaß. Vielleicht war das der Grund, warum Rash ihr vorschlug,
sie könne Sprachunterricht geben. Jedenfalls war es ein guter Vorschlag in
vielerlei Hinsicht. Nicht zuletzt, dass es ihr unbeschreiblich viel
Spaß machte, wieder mit Sprache zu arbeiten. Es bereitete ihr
auch Vergnügen, den Unterricht an Personen zu geben, die
mit verschiedenen Sprachen und Voraussetzungen kamen. Und es führte dazu, dass
sie endlich tatsächlich in Kontakt mit den Nixen kam und größeren Einblick
in Siren erlangte.

Die Kapitänin kam selten zum Sprachunterricht. Kanta erfuhr
erst sehr spät, warum: Die Kapitänin sprach selbst viele
Sprachen fließend. Sie kam, wenn es im Speziellen um Aussprache ging, nahm
aber nicht aktiv am Unterricht teil.

Ashnekov unterhielt sich nun mit ihr doch auf Kazdulan. Sie auf
Ilderin anzusprechen, war reine Höflichkeit gewesen. Er
mochte Kazdulan lieber und sie hatte nichts dagegen einzuwenden. Manchmal
brummte ihr Kopf nachts und sie träumte vielsprachig in den
Klängen der vielen Stimmen.

Die Gespräche mit Ashnekov wurden zu einer Tradition, jeden Mittag nach dem
Mittagessen, wenn nichts anderes anstand, an der Reling. Sie
mochte Ashnekov. Sehr. Sie mochte die Umgebung. Obwohl das
Schiff nicht besonders groß war und sie nie anlegten. Es
gab eigentlich gar keinen Grund, Heimweh zu haben. Trotzdem
vermisste Kanta Arwin. Manchmal erzählte sie Ashnekov von
ihm.
