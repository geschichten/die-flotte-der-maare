\BefehlVorKapitelDFDM{Rash-n.png}{Rash schreibt die Nachrichten, die von Bord gehen.}{Genitalien nur erwähnt, Petplay-artiges - eigentlich nicht Petplay, aber schon Rollenspiel als Tier, Bedrohung, Unterwerfung, Gegessen werden, Angst, Objektifizierung.}

Mäh
===

\Beitext{Rash}

Rash war auf dem Meer zu Hause.

Es war spät am Abend, die Sonne war untergegangen, und Marah hatte
sich zu Jentel in den Mastkorb hinaufgezogen, um mit ihm gemeinsam
zu singen. Rash war sich nicht sicher, ob es eine wortlose
Marlyrie war, die sie sangen, weil die Sprache der Nixen, Siren, in
der sie ebenfalls häufig sangen, auch ohne groß erkennbare Struktur
durch etwaige Konsonanten gesungen werden konnte. Zumindest
nicht gut erkennbar für Ohren, die Siren noch nicht gut kannten. Rash
lernte die Sprache seit Rash hier an Bord war, aber es fiel
Rash nicht leicht.

Marlyrien waren gesungene Gedichte, die oft ganz ohne Worte
auskamen. Es klang wie eine. Und es war wunderschön.

Rash war mit der Kapitänin verabredet und bereute es fast. Der
Gesang wäre sicher auch in der Kajüte noch hörbar, aber hier
draußen war es romantischer. In Rashs Sinne romantisch. Die
Romantik, die eine dunkel und dünn bewölkte Neumondnacht war, bei
der nur wenige Sterne glimmten, mit einer frischen Brise
in den Segeln und auf der Haut und dem Gesang der Nixen
in den Ohren.

Rash gehörte hier her, aufs Meer auf dieses Schiff. Das
fühlte Rash durch und durch, als Rash an die Tür der
Kapitänskajüte klopfte.

Dieses Mal öffnete Sindra wieder selbst, hielt Rash aber
nicht die Tür auf, sondern ging zurück durch den Raum
zu der Schublade, in der manchmal interessante Dinge
lagen. "Tür schließen und mittig im Raum auf die Knie.", befahl
die Kapitänin.

Das fing ja ohne Umschweife an. Rashs Körper mochte das. Rash
gehorchte, nicht eilig, fühlte dabei in sich hinein. Der
Moment, als die Tür geschlossen war, fühlte sich so an wie
der Beginn dieses Spiels. Rash durchschritt den halben
Raum und kniete sich hin. Das Gefühl, das Rash dabei durchströmte, war
Entspannung und Erwartung, und so etwas wie Sympathie, aber
eigentlich hatte Rash keine Worte dafür.

Sindra wandte sich wieder zu Rash um. Was sie aus der
Schublade genommen hatte, sah Rash nicht. Sindra holte
sich einen der Stühle heran und setzte sich Rash
gegenüber. Im Stehen wäre Sindra wohl viel zu groß
gewesen, um gut genug an Rash heranzukommen. Im Sitzen
konnte Rashs Kinn auf Sindras Knie reichen, wenn Rash
sich eine Spur aufrichtete. Rash tat es ohne zu
zögern. Wenn es nicht in Ordnung wäre, würde Sindra Rash vielleicht
bestrafen oder rügen, -- was Rash keineswegs wenig gefallen
würde.

Aber Sindra tat nichts dergleichen. Sie lächelte und strich
Rash über die Stirn. Die Haare waren Tabu. Rash mochte nicht
durch die Haare gestreichelt werden, das hatten sie ganz
am Anfang abgesprochen.

"Ich habe Auszeitfragen.", sagte Sindra. "Geht das im Knien, oder
sollen wir dafür ganz auflösen."

Das hörte sich spannend an. Auszeitfragen nannten sie es, wenn
sie Regeln absprechen wollten, Tabus zum Beispiel. Spielregeln, die
sie aber nicht im Spiel klären wollten. "Das geht im Knien."

"Du hattest mir vor längerer Zeit mal erzählt, was dich alles
interessiert.", leitete Sindra ein. "Darunter war: Du möchtest
gern mal so tun, als wärest du ein vierbeiniges Tier. Eines, das
zum Beispiel Menschen normalerweise töten oder ausnutzen. Und
du meintest, du magst es, extrem bedroht zu werden."

Rashs Körper reagierte bereits auf die Aussicht. Er fühlte
sich plötzlich sehr weich und wabbelig an, mehr noch, als
wenn Rash sich so etwas nur selbst vorstellte. Es war
ein unheimliches Gefühl, das Rash aber gleichzeitig sehr
mochte. Es war kein Gefühl, das in Rashs Genitalien
hineinreichte. Darum ging es nicht. Rashs Erregung war
eher eine unsexuelle, zumindest für den Begriff, den die
meisten Personen von Sex hatten. Es war manchmal schwer zu
vermitteln, was Kinks für Rash waren und Rash war umso glücklicher,
dass Sindra sehr genau zuhörte und gern ausprobierte. Allerdings
hatte Rash bei dem extremen Experiment, das sie nun andeutete,
auch etwas Angst. "Das habe
ich gesagt.", bestätigte Rash trotzdem. "Ich würde das gern
ausprobieren, wenn du möchtest." Es gab ein Aus-Wort. Ein
Wort, das zum Spielstop und zum Auflösen der Situation
führte, wenn Rash es ausspräche. Das sollte an
Sicherheit reichen.

"Ich habe dich noch nie ausgezogen.", sagte Sindra als
nächstes. "Wie stehst du dazu? Du hast es nie als Tabu
angebracht."

Warum würde Sindra Rash ausziehen wollen, wenn es darum
ging, ein Tier zu spielen? Es machte Rash allerdings
auch neugierig. Und Rash konnte nicht leugnen, das Gefühl
von Sindras Fingern direkt an Rashs Hals oder Oberarmen
zu mögen, sowohl wenn sie fast schmerzhaft zupackte, als
auch, wenn sie zart -- und bedrohlich -- darüber strich. "Ich
möchte meine Unterwäsche anbehalten.", sagte Rash. Und
verringerte die Einschränkung dann noch einmal: "Untenrum."

Sindra lächelte. "Gibt es weitere Regeln oder Sicherheiten, die
du gern neu hättest oder an die du erinnern möchtest?"

Rash dachte nur kurz nach. Der Gedanke hatte, seit Sindra
um Auszeit gebeten hatte, in Rashs Unterbewusstsein schon
gearbeitet. "Gummerlatech ist ein ziemlich langes Aus-Wort. Ich
mag es, aber ich hätte gern auch ein kürzeres."

"Ich habe mit einer anderen Person ein Farb-System
eingeführt. Rot, wie Gefahr, wäre dann ein kurzes Äquivalent
zu Gummerlatech.", erklärte Sindra. "Grün hätte die Bedeutung, dass
ich weitermachen darf, dass dir etwas gefällt. Und Gelb stünde
für Auszeit, für das Absprechen von irgendetwas, ohne alles
aufzulösen."

Wozu würde Rash Grün brauchen, fragte Rash sich. Rash
gefiel alles, oder Rash nutzte Aus-Wörter. Trotzdem
nickte Rash. Vielleicht würde es sich im Laufe irgendeines
zukünftigen Spiels ergeben. "Einverstanden.", sagte Rash.

"Dann beende ich die Auszeit mit der Frage: Magst
du Ziegen?" Sindra lächelte.

"Mäh?", fragte Rash.

Sindras Lächeln wurde noch eine Spur breiter. "Dann
haben wir nun zwei Ziegen an Bord.", sagte
sie. "Wobei die eine bitte in Frieden leben gelassen wird."

Die Andeutung, dass Rash die andere Ziege wäre, rauschte
durch Rashs Körper wie weiches Wasser. Sindra bemerkte es
wohl und ließ nicht locker. Sie griff Rash am Kinn, nicht
brutal, aber fest und ohne Spielraum für Rash, und schob
es von ihren Knien weg, soweit, bis
Rash Körperschwerpunkt eigentlich zu weit hinten
lag und Rash für diese sehr unbequeme Haltung so viel
Muskelkraft brauchte, dass Rash leicht zitterte.

Sindra schob ihren Stuhl nach hinten und rutschte davon
herunter. Ihre Knie landeten links und rechts von Rashs
Knien, aber Sindra gedachte nicht, sich viel kleiner zu
machen. Ihr Körper war dicht vor Rashs, berührte Rash
aber nicht, außer am Kinn. Mit der anderen Hand machte
sie sich an Rashs Knopfleiste zu schaffen und es brauchte
nicht lang, bis sie Rashs Hemd von den Schultern zog. Um
Rash das Unterhemd auszuziehen, nutzte Sindra
beide Hände.

Rash hätte halb damit gerechnet, dass Sindra Rash als
nächstes die Hose öffnen würde, aber das war nicht der
Fall. Stattdessen entknotete Sindra Rashs Halstuch, sodass
Rashs Hals eine bessere Angriffsfläche bot. Ob die
Handlung genau dafür gedacht war, wusste Rash nicht, aber
die Vorstellung war gut.

"Du kennst Aga, nehme ich an?", fragte Sindra.

"Mäh.", antwortete Rash wieder.

Sindra lächelte. "Aga hatte, als sie zu uns kam, ein
Halsband."

In Rash zog sich bei diesen Worten alles zusammen. Angenehm
zusammen. Rash atmete ein bisschen schneller. "Mäh?"

"Daran wurde Aga wohl zuvor festgebunden.", fuhr Sindra
fort. "Aga wollen wir nicht festbinden. Aber ich denke, du
verstehst, dass wir dich festbinden müssen."

Rash hätte ausgiebig genickt, wenn Rash keine Ziege gewesen
wäre. Rash, die Ziege, die bedroht werden wollte, verstand
das sehr gut. Rash hoffte, dass Sindra sich einfach darauf
verlassen würde, was Rashs Gesichtsausdruck sagte, oder
was sie ausgemacht hatten. Nicken gehörte nicht zum Spiel.

Sindra holte unter ihrem Hemd Agas Halsband hervor, bei
dessen Anblick Rash sich noch ein wenig weicher fühlte. Das
war fast die Grenze, bis zu der bei Rash dieses Gefühl je
ausgereizt worden war. Das Halsband war wohl in
der Schublade gewesen, mutmaßte Rash.

"Aga hat einen sehr viel dickeren Hals als du.", stellte
Sindra richtig fest. "Daher mussten wir das Halsband etwas
auf deine Größe zurechtkürzen."

Rashs Blick verweilte auf dem Halsband. Es hätte so in der
Tat nicht mehr um Agas Hals gepasst. Deshalb hatte Sindra
also bei ihrem letzten Spiel Rashs Hals so achtsam vermessen. Es
war unauffällig gewesen, hätte auch einfach Teil des Spiels
sein können, aber Rash war sehr aufmerksam. Rash hatte damit
gerechnet, dass es irgendeine Bedeutung hatte, die über
das letzte Spiel hinausging.

Sindra legte Rash das Halsband mit zarten Fingern an, während
sie sagte: "Damit du uns nicht davonrennst, wenn wir dich kochen
wollen."

Rash blieb die Luft weg. Das war eine Bedrohung, die Rash so
heftig spürte, wie Rash noch nie eine Bedrohung gefühlt
hatte. Rash verlor Kontrolle über den eigenen Körper, der
nun etwas zitterte und leicht schwankte.

Sindra überließ Rash einen Moment sich selbst, in dem sie
die am Halsband befestigte Leine an einem am Boden
verschraubten Schrank befestigte.

Es war ein sehr starkes Gefühl. Es stritt sich in Rash, ob
Rash es ausschließlich gut fand. Es war vielleicht ein
Teil davon kurz zu viel gewesen. Aber nun, nun lohnte es
sich. Rash fühlte sich in das Gefühl hinein, gegessen zu
werden. Gekocht zu werden. Es strömte durch Rashs Körper, machte
ihn weich und das Sympathie-Gefühl, das Rash nicht so
genau benennen konnte, verstärkte sich. Das Gefühl einer
ausweglosen Situation, in der Rash mehr ein Objekt war, mit
dem getan wurde, was andere wollten, bis hin zum völligen
Ausnutzen von allem, was Rashs Körper bot.

Als nächstes breitete Sindra ein dünnes Laken neben
Rash aus. Rash stieg der Duft nach Ziege in die Nase. Es
war ein Laken, mit dem die Ziege manchmal abgetrocknet
wurde, wenn es geregnet hatte. Rash tat es manchmal
selbst. Das Halsband war vielleicht gewaschen worden, roch
weniger intensiv.

Fast wie von selbst, ohne Aufforderung, krabbelte
Rash auf allen Vieren auf das Laken und kniete sich dort hin. Sindra den
Rücken zugewandt. Deshalb überraschte Rash vielleicht die
Hand, die plötzlich Rashs Nacken packte und Rash, die Ziege,
dieses Mal nach vorn drückte, bis Rash über den eigenen
Knien lag. Ein Finger bohrte sich von unten in
Rashs Kieferknochen und drehte den Kopf etwas zur Seite,
sodass Rash in Sindras Gesicht blickte.

Einen Moment fühlte sich Rash, als wäre eine Spielpause
gut, um Sindra Bescheid zu sagen, wie großartig dieses
Spiel war. Aber Rash fühlte sich nicht imstande, es
zu unterbrechen, nicht für ein Kompliment.

"Ich habe eine Marinade vorbereitet.", sagte Sindra.

Sie ließ Rash los, aber Rash blieb in der Haltung knien,
Sindras Gesicht beobachtend. Sindra holte ein Glas
mit einer glibschigen Masse darin zum Vorschein. So
gut Rash normalerweise aufpasste, wusste Rash nicht, woher
jenes herkam. Vielleicht aus dem Schrank, als Sindra
die Leine befestigt hatte.

Daraus, dass Rash es nicht mitbekommen hatte, schloss
Rash, dass Rash wirklich in einem anderen Universum des
Denkens gelandet war. Wo Rash wieder hinwollte.

Sindra öffnete das Glas und ein Geruch nach Minze und
Gewürzen stieg Rash in die Nase. Der Geruch nach Janaszs Küche an
Tagen, an denen Rash das Essen besonders mochte. Diesen
Geruch damit zu assoziieren, dass Rash damit eingerieben
und gegessen werden würde, holte das Universum von
eben vollständig und stark zurück. Rash spürte Angst, spürte
in die Angst hinein und Tränen schossen Rash in die
Augen.

Das war eine natürliche Reaktion, aber eine, die Sindra
bemerkte und dazu brachte, das Glas wieder zuzuschrauben. Sie
blickte Rash besorgt an.

"Grün.", sagte Rash.

Das hatte also bis zu so einem Moment gar nicht so lange
gedauert. Sindra schraubte den Deckel langsam wieder auf.
Sie wirkte nur leicht weniger besorgt. Sie rückte näher
an Rash heran und platzierte eine Hand in Rashs Nacken,
ohne Rashs Gesicht aus den Augen zu lassen. Aber
der Druck wirkte weniger überzeugend als sonst. Ängstlicher. Sindra
tauchte zwei Finger in die glibbrige Masse.

"Gelb.", sagte Rash schweren Herzens.

Sindra hielt inne, ließ den Druck noch etwas nach, aber wie
abgesprochen, löste sie die Situation nicht auf.

"Du wirkst unsicher auf mich, als ob du dich
nicht mehr so wohl fühlst.", sagte Rash. "Ich möchte erinnern, dass
die Aus-Wörter auch für dich gelten."

Sindra nickte. "Du hast recht.", sagte sie. "Ich sehe dir meistens an, wenn
wir spielen, wenn du genießt. Mir fällt das schwer,
das etwas in deinem Blick zu finden, wenn du dabei weinst
und dein Blick voll mit Horror ist. Wenn
ich das so sagen darf."

"Du darfst alles sagen." Rash senkte den Blick ohne den Kopf
zu bewegen. "Ich fühle den Horror. Aber ich möchte das. Ich mag
das Gefühl. Bis eben war es das beste Spiel, dass ich mir je hätte
erträumen können."

"Bis ich deine Tränen fehlinterpretiert habe?", fragte Sindra.

"Mach dich nicht selber runter.", erwiderte Rash, -- und ärgerte
sich direkt über sich selbst, als Rash einfiel, dass Sindra oft
solche Sätze eher sachlich und wertneutral meinte. "Ich meine: Wir haben noch
nicht ausreichend gespielt, dass du dir sicher sein könntest, dass
ich jederzeit im Stande bin, mich selbst zu schützen. Es ist
verständlich, dass du da lieber ganz sicher sein willst."

"Doch haben wir eigentlich.", widersprach Sindra. "Ich hätte
ganz sicher nicht dieses Spiel vorbereitet, wenn ich mir nicht
sicher wäre, dass du weißt, was du willst, und das auch während
eines Spiels sagen würdest."

"Warum hast du dann das Glas zugeschraubt, als ich anfing zu
weinen?", fragte Rash.

Sindra ließ nun doch die Hand von Rashs Rücken gleiten. "Ich
habe dich noch nie weinen sehen."

Das Universum war verflogen. Sindra hatte die Selbstsicherheit
verloren, die sie gebraucht hätte, um Rash wieder da
hineinzubringen. Es war nicht unmöglich, dass Sindra jene
Selbstsicherheit wieder gewann. Aber viel wichtiger war eigentlich, dass
Sindra sich wohlfühlte, und das tat sie nicht.

Rash richtete sich auf und nahm Sindras Hand in beide eigenen. "Ich
würde für dich rot sagen, oder Gummerlatech, wenn du willst.", sagte Rash sanft.

Der riesige Oberkörper der Kapitänin landete in Rashs Schoß. Vielleicht
war es Erleichterung.

Rash strich ihr über den Rücken. "Dir braucht nichts leid zu tun.", sagte
Rash vorsichtshalber, falls Sindra auf solche Gedanken käme. "Auch, wenn
du bei Spielen fast nur an die andere Seite denkst," -- Rash bezweifelte
nicht, dass es auch auf Spiele zwischen ihr und anderen zutraf --, "ist
es immer ein Spiel für alle Beteiligten. Du spielst Rollen aus. Du magst
das Theater dabei. Und die Reaktionen. Aber wenn du dich darin nicht
mehr wohl fühlst, weil du eine Rolle spielst, die du nicht magst, oder
weil du die Reaktionen nicht mehr lesen kannst, die du so genießt, schuldest
du niemandem, weiterzuspielen."

Sindra richtete sich wieder auf und blickte Rash liebevoll ins
Gesicht. Was auch schön war. Ihre großen Finger legten sich an
Rashs Wangen und sie küsste Rash vorsichtig und trocken auf
die Stirn. Auch nasse Küsse waren Tabu.

Wie Sindra es damit schaffte, Rash das Gefühl zu geben, doch wieder
so etwas wie bedroht zu sein, wusste Rash nicht. Vielleicht, weil
die Finger am Ende der Streichelbewegung unter Rashs Kinn
landeten. Vielleicht, weil Sindra absichtlich eine etwas höhere
Körperhaltung hatte, als nötig gewesen wäre. Manchmal waren
es Nuancen, die es für Rash kippten. Vielleicht auch, weil
diese Nuancen aus vorherigen Spielen eine entsprechende
Bedeutung bekommen hatten. Rash atmete leicht zittrig
ein. Absichtlich, nicht nur, weil es sich schön anfühlte, sondern
auch, um Bescheid zu sagen. "Und dass es dir dabei so sehr um die
Reaktionen geht, macht es so einfach, dir zu vertrauen und sich
in einem Spiel mit dir fallen zu lassen.", fügte Rash hinzu.

Sindra lächelte auf Rashs Stirn und nahm dann wieder
etwas Abstand von derselben. "Ich weiß nicht, ob ich noch einmal
abbrechen muss und ich bin dir sehr dankbar für die Pause.", sagte
sie. "Und für deine Worte. Das war eine wichtige Erinnerung."

Rash nickte.

"Ich habe mir aber nicht von Janasz erklären lassen, wie ich
Marinade herstelle, ohne sie dann auf dem Rücken meiner Ziege
zu verteilen.", fuhr sie fort.

Und da war es wieder. Das Gefühl von vorhin. Ein wenig weniger
stark. Rash musste nicht weinen. Rash stellte sich vorsichtshalber
nicht das Element daran vor, dass Rash dafür sterben müsste,
gegessen zu werden.

"Wenn das für dich passt, würde ich dich auf dem Boden fixieren
und deinen Oberkörper damit einreiben. Damit du für meine
Zwecke gut riechst.", schloss Sindra.

Rash nickte. Wie automatisch, ohne zu zögern.

Sindra fing auf dem Rücken an, kniete dabei vor Rashs Kopf, selbigen
auf die Seite gedreht, mit einer Hand in ihrem Schoß
fixiert. Fest, sodass Rash ihn nicht bewegen konnte. Sie
beobachtete das Gesicht, den wohl weggetretenen
Gesichtsausdruck, während ihre andere Hand die glibbrige, kalte,
und wohlriechende Masse auf Rashs Rücken verteilte. Ein
Geruch, der so sehr gutes Essen assoziierte. Auf
eine Weise, auf die sich Rash genau so geliebt fühlte, wie es
Rashs Kink war: Wie etwas, das benutzt würde, oder gegessen, wie
eine wirklich gute Speise.

Rashs Körper wurde weich und entspannte sich. Rashs Atem ging
regelmäßig, aber etwas schneller. Rash schloss die Augen
dabei und ließ sich fallen.

Sindra tat die Arbeit gründlich und genussvoll, achtete dabei
darauf, dass die Marinade gleichmäßig und schön über den ganzen
Rücken verteilt war. Etwas in der Marinade kühlte und
wärmte anschließend, durchblutete den Rücken, sodass er
sich noch verletzlicher anfühlte.

Schließlich drehte Sindra Rash, die Ziege, auf den Rücken. Das
Laken roch nach Ziege und würde bald auch nach Marinade
riechen. Vielleicht sollte Rash heute Nacht irgendwo an Deck in einer
Ecke schlafen, wo der Geruch andere nicht störte, und die
Decke mitnehmen. Dann käme das Schamgefühl hinzu: Was, wenn
Leute es mitbekämen und sich ihre Gedanken machen würden. Das
war auch etwas, das Rash prinzipiell gefiel. Solange sich
niemand gestört fühlte.

Rash wurde aus den Gedanken gerissen, als Rash Sindras
Hand auf der nackten Haut auf dem Brustbein spürte. Sindra
hatte Rash dort noch nie so angefasst. Und Rash schmolz, den
Kopf in Sindras Schoß in ihre eine Ellenbeuge eingeklemmt,
dahin, als die andere Hand die Marinade auch auf Brust und
Bauch verteilte. Sindras
Blick wirkte dabei konzentriert und entspannt. Als
würde Sindra wirklich eher etwas und nicht eine Person
marinieren, und hätte Freude daran, die Arbeit gründlich zu
tun.

Anschließend ließ Sindra Rash los, bettete Rash
zartfühlend auf das Ziegenlaken und wickelte
Rash darin ein.

"Spiel-Ende.", sagte sie sanft. "Wie fühlst du dich?"

Rash rollte sich auf den bereitgelegten Arm, arbeitete
einen eigenen aus dem Paket, das Rash war, und streichelte über
Sindras Wange. Rash lächelte. "Entspannt.", sagte Rash.
"Das war sehr gut alles. Ich danke dir."

"Ich danke dir!" Sindra betonte das Pronomen. Lächelte
dabei. Und ihr Blick verriet, dass sie Rash sehr lieb
hatte.

Sindra war eine Person voller Liebe. Liebe, die stets
irgendwohin musste. Deshalb spielte Sindra. Nicht nur
in kinky Spielen, sondern eigentlich permanent. Beobachtete
Leute, fand heraus, was sie brauchten, was sie liebten, und
erzeugte durch Performance die Atmosphäre, die gerade
dafür notwendig war. Sofern sie konnte.

Leider sahen das nicht viele. Aber Rash sah es. Rash
wünschte sich so sehr, Sindra beschützen zu können.
