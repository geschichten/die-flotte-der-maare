\BefehlVorKapitelDFDM{Kanta-n.png}{Kanta wurde von Jentel aus dem Wasser gefischt und ist seit dem an Bord. Sie befasst sich viel mit Sprachen.}{Lebensmüde Gedanken.}

Strichrichtung
==============

\Beitext{Kanta}

Als das Assassinan Kanta aufforderte, ihr in die
Kapitänskajüte zu folgen, konnte Kanta ihr
Unbehagen nicht mehr verdrängen, dass sie seit
ein paar Tagen untergründig spürte.

War 'Assassinan' überhaupt die richtige Bezeichnung
für Amira? Kanta hatte das Gespräch zwischen Jentel und
ihr mitbekommen, in dem es um Pronomen und Endungen
gegangen war. Jentel sprach selten mit anderen
Crewmitgliedern, außer vielleicht mit Nixen. Und mit
Janasz. Jentel mied besonders Kanta. Aber mit Amira
hatte as gesprochen. Daher wusste Kanta, dass die Endung
'-an' an geschlechtsspezifischen, -- oder dadurch eben
unspezifischen -- Bezeichnungen für Amira richtig war.

Und eigentlich war sich Kanta auch recht sicher, dass
Amira Assassinan war. Sie hatte einen durchtrainierten
Körper, war sehr gut darin, nicht im Weg zu stehen, es
sei denn, sie wollte es, und selbst Kanta fiel es schwer,
sie zu bemerken, wenn sie sich anschlich. Es war
wahrscheinlich nicht einmal beabsichtigt, dass sich Amira
anschlich, gehörte einfach zu ihren Gewohnheiten. Und
dann waren da die Messer. Sie waren so in die Kleidung
integriert, dass sie gut greifbar waren, aber nicht
direkt als Messer erkennbar. Die Kleidung war
perfekt auf den Körper zugeschnitten, robust und durch
und durch darauf ausgelegt, praktisch zu sein. Selbst
das Kopftuch hatte einen praktischen Zweck. Aber die
Ösen, die auf den ersten Blick wie Verzierungen ausgesehen
hatten, hätten keinen gehabt. Also hatte Kanta genau
beobachtet und schließlich mutig gefragt, was sie
für eine Bedeutung hatten. Amira hatte länger gezögert,
bevor sie eines der Messer herausgezogen hatte, um es
Kanta zu zeigen. Und das hatte Kanta schließlich davon
überzeugt, dass Amira nicht aus ähnlich harmlosen Gründen
wie sie an Bord war. Amira war hier aus einem gefährlichen
Grund. Kanta wusste nur nicht, für wen gefährlich.

Kanta war nicht so leicht unwissend zu halten, wie
sich der größte Teil der Crew das vorstellte. Sie wusste
zwar immer noch nicht, wie die Fernangriffe
funktionierten, aber mit Nixen an Bord waren da plötzlich
viel mehr vorstellbare Optionen. Vielleicht tauchten
die Nixen Janasz und Ashnekov an die anderen Schiffe heran, wo
sie irgendwie an Bord klettern konnten. Dass Janasz und
Ashnekov beteiligt waren, wusste Kanta daher, dass die
beiden bei Überfällen nie an Deck waren.

Aber wie genau die Überfälle abliefen, war zwar
interessant, aber ebenfalls weniger spannend, als sich die Crew
das für sie vorstellte. Die wesentlichen Informationen lagen
darin, welche Schiffe sie ausraubten. Diese Information
hätte Kanta höchstens vorenthalten werden können, wenn ihr
verboten worden wäre, während eines Überfalls an
Deck zu sein. Aber das Gegenteil war der Fall: Sie war
sogar angehalten dazu, während der Überfälle sichtbar
zu sein. Damit sie Befehle erhalten könnte, war die
Hauptbegründung, aber Kanta zweifelte nicht, dass die
Informationen, die sie unter Deck hätte bekommen können, einfach
noch viel mehr gewesen wären, also einer der Gründe war, dass
sie nicht herumschnüffelte.

Manchmal fragte sich Kanta, ob das ganze Misstrauen ihr gegenüber nicht
sehr gerechtfertigt wäre. Denn neugierig war sie. Sie
hielt sich an alle Auflagen, ohne je zu murren. Aber was
sie innerhalb der Auflagen erfahren konnte, sog sie auf wie
ein Schwamm. Wie sie es immer getan hatte. Und sie zog eben
ihre Schlüsse.

Es versuchten immer noch Schiffe aus Minzter an ihnen
vorbeizukommen. Sie hatte mangels Fernglas nie Gelegenheit
gehabt, zu sehen, ob Arwin auch an Bord gewesen wäre, aber
sie glaubte daran. Sie erkannte das Schiff, auf dem sie
selbst gekommen war, bei mindestens zweien der Überfälle.

Kanta konnte sich denken, dass die staatliche Finanzierung für
Forschungsreisen nicht so lange
gegeben gewesen wäre, wie für jenes Forschungsprojekt
augenscheinlich weiter welche floss, wenn die Reisen zwecklos
erschienen wären. Hinter den Versuchen, an ihnen
vorbeizukommen, steckte also mehr als die geringe
Hoffnung, irgendwann eben doch an ihnen vorbei und
vor Grenlannd anzukommen. Sie forschten
die Geisterschiffe aus, schloss Kanta. Und
Teil der Strategie, um die Geistercrew zu erforschen, -- auch da war
sich Kanta recht sicher --, war, die Überfälle zu provozieren und
zu beobachten, was dabei genau passierte.

Manchmal fragte sie sich, ob
sie versuchten, sie zurückzuholen, aber Kanta hatte kein Interesse
zu gehen. Und ohne, dass sie von sich aus aktiv würde, um die
Schattenmuräne zu verlassen, war jeder Versuch aussichtslos. Dazu
agierte Sindra zu durchdacht -- Kanta konnte nicht
umhin, die Kapitänin zu bewundern, wie rasch und kontrolliert
sie Entscheidungen mit genau der richtigen Vorsicht fällte, und
wie präzise und auf die jeweilig angesprochene Person abgestimmt
sie Befehle erteilte. Und schließlich war dieses Nixenschiff zu unsagbar schnell, als
dass ein Forschungsschiff es jemals eingeholt hätte. Manchmal hatte
Kanta das Gefühl, die Schattenmuräne selbst lebte, wenn sie sich
aus dem Wasser erhob -- das tat sie tatsächlich um ein paar Meter --, und
plötzlich ganz anders durch die Wellen schnitt als zuvor, oder manchmal
auch mit ihnen lief, von ihnen geschoben wurde.

Im Nachhinein war
Kanta eine Idee gekommen, wie sie sie damals einfacher hätten an Bord
schmuggeln können: Wenn sie sie unter Vorräten begraben in einer
der Kisten oder Säcke versteckt hätten, die geklaut werden
würden. Vielleicht war Amira auf diese Weise an Bord gekommen. Aber
selbst wenn, glaubte Kanta nicht, dass Amira zu ihrer ehemaligen
Forschungscrew gehörte oder sie von jener beauftragt
worden wäre. Das war zumindest damals nicht deren
Stil gewesen. Amira war außerdem ein Mensch, von weiter aus dem
Norden. Das musste nicht zwingend etwas heißen, aber es
legte es weniger nahe, dass sie von Minzter aus geschickt
worden wäre. Kanta konnte sich auch nicht vorstellen, dass Amira
ohne ein hübsches Sümmchen Geld hier hergelangt war, das von
einer Hand in eine andere geflossen war. Wahrscheinlich ohne, dass
Amira davon je etwas abbekommen hätte, von ihrer
Ausrüstung und Ausbildung abgesehen. Geld, das ihre Forschungscrew
von damals sicher nicht gehabt hätte.

Aber so ganz erschloss sich Kanta das alles
noch nicht. Ob Amira an Bord war, weil sie
hätte morden sollen und es dann doch nicht getan hatte, oder
weil sie es noch vorhatte, oder von wem sie dann geschickt
worden war, wenn nicht von ihrer Forschungsorganisation, war
Kanta alles unklar. Es war lediglich wahrscheinlich, dass
es nun eine weitere Interessengruppe gab, die sich auf die Jagd
der Schattenflotte fokussiert hatte, auf ungemütlichere
Weise. Oder Amira war im Auftrag der
Kapitänin an Bord, aber das glaubte Kanta eigentlich auch nicht. Das
wiederum passte nicht zur Schattencrew. Sindra selbst erschloss sich Kanta
als Charakter allerdings nicht.

Amira hatte, seit sie hier war, noch niemanden
umgebracht. Aber das hieß nicht, dass Amira
nicht irgendwelche Todesdrohungen ausgesprochen haben könnte und
auf diese Weise die Kapitänin oder irgendeine andere Person an
Bord im Griff hätte, die jetzt täte, was Amira jeweils
wollte. Es war Kanta zum Beispiel aufgefallen, dass Rash keine
Briefe mehr geschrieben hatte, seit Amira sichtbar in Erscheinung
getreten war. Rash und Kanta unterhielten sich über
Formulierungen. Kanta bekam immer mit, was und wann Rash
schrieb. Sie wusste nicht, wie die Post verschickt
wurde, aber es gehörte mit zu den Dingen, die Kanta an Bord
am liebsten geworden waren, mit Rash formulieren zu üben: Rash
war eine interessante Person, formulieren machte eben viel Spaß und
Rash vertraute ihr. Mehr zumindest als irgendwer sonst an Bord. Kanta
hatte nicht gewusst, wie sehr sie das Gefühl
gebraucht hatte, dass sie jemand ernst nahm und sie nicht so
behandelte, als wäre sie außen vor, wie alle anderen. Rash
hatte ihr versichert, die anderen bräuchten bloß Zeit, teils
viel Zeit. Vertrauen sei im Kontext von Piraterie nicht
einfach aufzubauen.

Und nun bat Amira sie, ihr in die Kapitänskajüte zu folgen. Kantas
frisch und zaghaft aufgebaute Welt kam wieder ins Wanken. Und
das war etwas, was sie auf See, wo sie keine Möglichkeit
hatte, zu fliehen, durchaus beunruhigte.

---

Sie betrat die Kapitänskajüte. Amira hielt ihr dabei die Tür
auf. Es war Kanta unbehaglich, dass sie dabei hinter ihr
stand. Sindra saß auf ihrem Stuhl, eine Tasse Tee in der Hand, die
Beine angewinkelt. Kanta war schleierhaft, wie eine Person so
sitzen konnte. Zu ihrer Erleichterung stellte sich Amira nicht
hinter sie, sondern schräg hinter Sindra, als sie der Kapitänin
gegenüber Platz nahm.

"Du kennst Amira bereits.", leitete die Kapitänin ein.
"Aber du weißt nicht, warum sie hier ist,
nehme ich an?"

War das eine Art Prüfung? "Sie ist Assassinan." Kanta
versuchte die Vermutung genau zwischen Frage und
Feststellung klingen zu lassen. Amira schien das nicht
zu beeindrucken.

Die Kapitänin hob die Brauen und nickte. "Habt
ihr euch schon vorgestellt?"

Nicht nur Kanta schüttelte den Kopf, sondern auch
Amira, außerdem durch ablehnende Geräusche zu
verstehen gebend, dass dies nicht der Fall gewesen war.

"Ist das irgendwodurch offensichtlich für dich?", fragte
die Kapitänin.

Kanta nickte vorsichtig. Sie fühlte sich sehr
unbehaglich. Sie hätte es also nicht wissen sollen. Nun
würde ihre Neugierde vielleicht als noch gefährlicher eingestuft
werden.

"Ich habe ihr ein Messer aus meiner Kleidung gezeigt.", setzte
sich überraschend Amira für sie ein.

Was mochten bloß ihre Motive sein?

Die Kapitänin nickte und senkte die Brauen wieder. Aber die
Skepsis fiel nicht ganz von ihr ab. "Amira ist Assassinan. Und
hat einen Mord an mir verhindert. Indem
sie ihn nicht ausgeführt hat, aber letzteres müssen wir
nicht überbetonen."

Sollte das ein Witz sein? War das ein subtiler Hinweis auf
irgendetwas? Ein Hilferuf vielleicht? Sollte Kanta versuchen, irgendwie
durch Rätsel mit der Kapitänin zu sprechen, durch irgendwelche
Informationen, die zwischen Kanta und der Kapitänin bekannt
waren, die Amira aber nicht wissen konnte?

Kanta nickte erst einmal einfach. Sie versuchte, sich alles genau
einzuprägen und hochkonzentriert mitzudenken, damit ihr nichts
entginge. Und das, obwohl sie Angst hatte, dass sie für
zu neugierig gehalten werden konnte. Es gab Dinge, die gingen
vor.

Die Kapitänin band einen Umschlag aus braunem, weichem Material
auf und legte ihn so in Kantas Blickfeld, dass das Schriftbild
des darin befindlichen Briefs für sie richtig herum
lag. Es war prinzipiell Rashs Schrift, das
erkannte sie. In Kanta zog sich alles zusammen.

"Denk laut.", befahl die Kapitänin.

Kanta blickte auf, konnte die Wut und Verzweiflung, die sie
fühlte, vielleicht nicht so gut verbergen. Aber vielleicht
war das auch okay, weil es ja auch eine Form des lauten
Denkens war. "Ist damit gemeint, dass ich alles sagen soll, was
ich denke? Am besten die ganze Zeit reden soll?"

Die Kapitänin nickte.

Deutlicher konnte sie Kanta wohl kaum mitteilen, dass sie andernfalls
ein Lügenkonstrukt erwartete, für das Kanta sich eben Zeit
hätte nehmen müssen, es zu erstellen. "Ich denke an blaugrüne
Flederflüffe.", sagte Kanta spitz. "Ich will damit sagen, dass
ich mein Gehirn darauf trainiert habe, auf Dinge zu fokussieren, auf
die ich mich fokussieren möchte. Der Trick würde bei mir eher
mäßig schlecht funktionieren. Ich kann sowohl sehr bewusst an
bestimmte Dinge nicht denken. Da habe ich Übung mit, weil ich sonst
an zu viele schlimme Dinge denke, die
mir widerfahren sind. Als auch kann ich während des
Lesens oder Redens relativ gut an andere Dinge gleichzeitig
denken."

"Ich verstehe.", sagte die Kapitänin. Keineswegs glücklich.

Kanta spürte, wie ihr die Angst die Kehle zuschnürte. "Ich
mag aber sagen, was ich denke, wenn ich darf.", sagte sie
mutig. Im selben aggressiven Tonfall wie eben.

"Ich bitte darum.", forderte die Kapitänin sie dazu auf, ihrem
Wunsch nachzukommen.

"Ich habe Angst.", sagte Kanta klar. Das hatte sie der Kapitänin
nie so direkt gesagt. Aber vielleicht war das jetzt gut. Ihr
war auch egal, dass das Assassinan dabei zuhörte. Es war alles
egal. "Ich habe Angst, dass ich nun beseitigt werden soll. Dass
das Misstrauen der Crew oder dein persönliches dazu führt, dass
ich nicht mehr leben darf." Ihr fiel erst dadurch, dass sie
es sagte, auf, dass sich etwas grundlegend in ihrem Leben
geändert hatte. "Ich habe, seit ich an Bord bin, das erste Mal, seit
ich mich zurückerinnern kann, tatsächlich einen Lebenswillen." Kanta
glaubte, dass ihre Stimme eigentlich hätte zittern müssen, aber
das tat sie nicht.

"Wer hat die Nachricht geschrieben?", fragte die Kapitänin.

Kanta fragte sich, wie eine Person nach einer solchen
Ansprache so unbarmherzig sein könnte. Stand tatsächlich
schon im Raum, dass sie sie vielleicht hinrichten würden? Kanta
senkte den Blick auf das Papier. Sie wusste, wie Rash Buchstaben
malte. "Ich weiß es nicht.", sagte sie.

"Kennst du die Handschrift?", fragte die Kapitänin.

Kanta zögerte. Hatte es einen Sinn? "Du möchtest hören, dass
es Rashs Handschrift ist."

Die Kapitänin schüttelte den Kopf. "Eigentlich wünschte ich
mir, es wäre eine andere.", sagte sie leise, weicher. "Was meinst du
damit? Ist es nur eine ähnliche?"

Waren sie wirklich nicht selbst in der Lage, zu sehen, was
hier los war? Dass das zwar Rashs Handschrift war, aber Rash
das nicht geschrieben hatte? Sollte sie es sagen? Würde es
ihre Überlebenschancen verbessern oder eher verschlechtern?

Sie glaubte kaum, dass dieses Detail, das sie hier so
offensichtlich sah und die anderen nicht, plötzlich
ein großes, spürbares Vertrauen in sie aufbauen würde.

Und dann verknüpfte sie ganz andere Informationen miteinander: Der
Schriftverkehr war mit dem sichtbaren Erscheinen von Amira
eingestellt worden. Auf Basis dieses Schriftstücks dann wohl. Amira
war hier mit dem Schriftstück eingetroffen, und vielleicht ausgelöst
durch das Schriftstück. Sie war
Assassinan, und wenn die Kapitänin recht hatte, eines, das einen
Auftragsmord an selbiger nicht durchgeführt hatte.

Amira würde vielleicht nicht das letzte Assassinan sein, das
auf die Schattenmuräne geschickt würde, wenn ihr Auftrag nicht
erfolgreich wäre.

"Es geht um Leben und Tod der Crew.", formulierte Kanta vorsichtig. Sie
konnte schnell denken. Sie vermutete, es hatte kaum ein paar Sekunden
gebraucht von der Frage der Kapitänin bis zu ihrer Reaktion.

Amira nickte sofort. Die Kapitänin zögerte, tat es dann aber
auch.

"Rash fängt manche Buchstaben von woanders an aufs Papier zu bringen.", informierte
Kanta. "Ich sehe es, wenn Rash schreibt, wie Rash die Feder zieht. Das
Papier hier hat leicht haarige Fasern, fast wie ein Fell, die sich unter dem
Druck des Stifts in eine bestimmte Richtung gelegt haben. Die Strichrichtung
stimmt nicht überall mit der von Rash überein und ist auch in sich
nicht konsistent."

"Was heißt konsistent?", fragte Amira.

Wieso durfte das Assassinan eigentlich sprechen? Vielleicht erlaubte
Amira sich das einfach. Es spielte eigentlich keine Rolle. "Nicht
überall gleich. Die Person hat für die selben Buchstaben mal die eine
und mal die andere Strichrichtung benutzt."

"Das ist sehr interessant.", sagte die Kapitänin nachdenklich.

Es war das erste Mal, dass Kanta das Gefühl hatte, dass sie sie als
eine Person wahrnahm. Sie fragte sich, ob sie ihr den letzten
Gedankenschritt auch noch abnehmen musste oder ob sie selber
darauf kam.

"Ich würde raten, dass eine Person abgepaust hat, die selbst
nicht flüssig schreiben kann?", fragte sie.

Sie war also selbst daraufgekommen. "Das war auch mein Schluss.", sagte
Kanta.

"Aber müsste dann nicht eine Originalnachricht von Rash existieren?", fragte
die Kapitänin.

Kanta hatte gerade angefangen, sich zu entspannen, aber nun zog es sich
in ihr wieder heftig zusammen vor Angst. Dieses Mal um Rash. Das
war ein naheliegender Schluss, das hätte ihr klar sein müssen. Sie
hatte eigentlich gehofft, Rash dadurch entlasten zu können.

Sie nickte vorsichtig. "So etwas in der Art."

"Sprich aus, was du weißt.", forderte die Kapitänin sie auf.

"Ich habe eher eine Frage: Warum sollte eine Person einen
Brief abpausen?" Kanta rang die Hände. Es konnte doch nicht sein, dass
die Information, dass die Briefe aus abgepausten Buchstaben
bestanden, Rash nicht entlastete.

Die Kapitänin blieb still und musterte Kanta nachdenklich.

"Zum Kopieren?", fragte Amira.

"Unwahrscheinlich.", antwortete die Kapitänin, ohne sich
umzusehen. "Das ergibt nur dann einen Sinn, wenn es zwei Parteien
gibt, von denen eine die Originale bekommt, und von der
zweiten nichts weiß, die die Kopien anfertigt, während die
einzigen Personen der zweiten Partei, die Zugang zu den Unterlagen der ersten
Partei haben, nicht schreiben können. Andernfalls wäre Kopieren
durch eine Person, die schreiben kann, viel einfacher."

Das war eine erstaunliche Schlussfolgerung und erklärte
Kanta einiges.

"Gibt es nicht so manche Partei, die die Schattenflotte
am liebsten zerstört sehen will?", fragte Kanta, was sie
sich vorhin schon überlegt hatte.

Die Kapitänin nickte. "Schon.", sagte sie. "Aber ich glaube
eigentlich an eine einfachere Begründung für das Abpausen."

"Eine bestimmte, oder erstmal nur, dass eine einfachere
Begründung da sein könnte, von der du noch nicht weißt, wie
sie aussehen könnte?", fragte Kanta.

"Letzteres.", antwortete die Kapitänin. "Hast du noch
eine Idee?"

Kanta wühlte in ihrem Gehirn. "Eine merkwürdige.", sagte
sie schließlich. "Vielleicht geht es darum, im Zweifel
Rash zu belasten."

"Erkläre dich genauer?", forderte die Kapitänin.

"Es gibt keine Originalnachricht, aber die Personen, die
diesen Brief erzeugt haben, haben Schriftmaterial in Rashs
Schrift.", kam Kanta dem Befehl nach. "Entsprechend pausen
sie Buchstaben ab, sodass sich ein neuer Brief ergibt, der
aber, falls er in die falschen Hände gerät, Rash belastet, und
nicht eine andere Person."

"Das ist eine interessante Idee.", sagte die Kapitänin. Aber
glücklich wirkte sie dieses Mal nicht. "Nur dass dann wieder
wahrscheinlich eine Person zum Abpausen gewählt worden wäre, die
schreiben kann, bei der die Strichrichtung konsistent wäre. Vielleicht
nicht mit Rashs, aber innerhalb des Briefs."

Das stimmte. Warum hatte Kanta daran nicht gedacht?

"Aber dein Hinweis ist trotzdem wertvoll.", versicherte
die Kapitänin. "Du hast dabei ins Spiel gebracht, dass die
Buchstaben einzeln abgepaust werden können, und trotzdem ein
stimmiges Schriftbild herauskommt."

"Bei Rashs Handschrift schon.", stimmte Kanta ohne Umschweife
zu. "Kazdulan ist dabei auch eher ein Vorteil."

"Vielen Dank für all das, was du uns anvertraut hast.", sagte
die Kapitänin. "Du kannst gehen. Wenn dir noch etwas einfällt, bist
du mit jedem Hinweis hier sehr willkommen."

Kanta stand langsam auf, drehte sich aber nicht direkt um. Der
Gedanke, Amira könnte ihr ein Messer in den Rücken werfen, sobald
sie sich umdrehte, flackerte unaufgefordert durch ihre
Vorstellungen. "Besteht für mich die Gefahr, dass ich
hingerichtet werde?", fragte sie. Sie musste das fragen.

"Du hast mein Wort, dass ich und meine Crew, solange
ich das Kommando habe, niemals eine Person hinrichten werden.", versicherte
die Kapitänin. Leise und gelassen, aber in einem Tonfall, bei dem
Kanta sich fühlte, dass ihr das hätte sehr klar sein müssen.

Kanta schluckte. "Danke.", murmelte sie. Aber obwohl sie der
Kapitänin glaubte, fühlte sie sich in keinster Weise beruhigt, als
sie die Kajüte wieder verließ.
