\BefehlVorKapitelDFDM{Aga-n.png}{Aga ist eine Bergziege. Sie ist von den Maaren bei einem Überfall auf ein Forschungsschiff an Bord gelangt.}{Mord, morbide Gedanken, gegessen werden, Frust, körperliche Übergriffigkeit, Trauma/Trauma-Trigger - Metapher zumindest.}

Wesen
=====

\Beitext{Aga}

Sehr viele Beine waren das. Und Flossen. An sehr vielen Körpern von Wesen. Leute
sind ja in Ordnung, aber nicht so viele auf einmal.

Einige denken, ich hätte nicht mitbekommen, worum es ging. Das
weiß ich. Sie denken, ich trabte unbeschwert über das Deck
und würde mich für ihren Trost kuscheln lassen. Der Satz
hat angestrengt. Gucken die nicht richtig hin? Ich bin
überhaupt nicht getrabt, und falls doch, dann weg. Weg
von Händen. All den Händen. An den
Rand. Den Blick auf das Wassergebirge, das nie still
hält. Sodass ich mir nicht einmal vorstellen könnte, auf einen
Gebirgsrücken zu steigen.

Es ist alles ganz anders.

Ich mag Kraulungen. Kraulen? Diese Sache mit den Händen in
meinem Fell. Aber nicht von jeder Person. Und nicht die
ganze Zeit. Und nicht von zu vielen auf einmal.

Tatsächlich ist mir diese Feiersache suspekt.

Ich war auf Trauerfeiern. Von anderen Wesen. So einer Art
Wesen wie hier heute rumlungerte. Weniger aus Fisch,
meistens. Das sind keine guten Erinnerungen. Für
deren Trauerfeiern machen sie nämlich besonders diese
Sache mit dem Kochen.

Ich weiß, dass Rash Fleisch isst und ich mag Rash trotzdem. Das
ist nicht das Ding. Die können argumentieren, dass manche
eben Leute wie mich essen müssen. Manche Wesen müssen
andere Wesen zum Überleben essen, oder weil sie
aus anderen Gründen nicht anders können. Wir können darüber streiten, wie
vertretbar das ist, aber ich habe da eigentlich keine Lust
drauf. Ich glaube, ich habe mehr Verständnis dafür, als
manche Person hier an Bord.

Es wäre nur eben schön, wenn das Betrauern der Wesen über
ähnliche Wesen nicht zur Folge hätte, dass Wesen wie
ich ermordet würden, infolgedessen Wesen wie ich durch Trauerfeiern
anderer Wesen um die ihnen ähnlichen Wesen trauern müssen. Läuft
mit den komplexen Sätzen. Das habe ich mir von Rash abgeguckt.

Es gab keine Wesen zu essen bei dieser Trauerfeier. Das
war erleichternd. Aber das ändert nichts an der Verknüpfung. Feiern
sind schrecklich. Alle Feiern erinnern mich an solche Feiern,
wo Wesen Wesen wie mich essen.

Das ist alles falsch. An solchen Tagen möchte ich in
Ruhe verwesen. Das ist ein schlechter Wortwitz, aber ich
fühle das heute tief in mir drin.

Morgen wird besser. Morgen wird das alles schon eine Weile
her sein.
