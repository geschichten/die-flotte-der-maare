pandoc.utils = require 'pandoc.utils'

-- return {
--   {
--     Str = function (elem)
--       if elem.text == "K\\exdose" then
-- 
--         return pandoc.Emph {pandoc.Str "Kexdose"}
--       else
--         return elem
--       end
--     end,
--   }
-- }

-- function RawBlock(elem)
--   if elem.format ~= "tex" then
--     return elem.text
--   elseif elem.format ~= "epub" then
--     if string.match(elem.text or '', "BefehlVorKapitelDFDM") == "BefehlVorKapitelDFDM" then
--       return (elem.text or ''):gmatch "{.-}"(1)..(elem.text or ''):gmatch "{.-}"(2)
-- --      return string.match(elem.text or '', "{.-}")
--     else
--       return string.match(elem.text or '', "Be")
--     end
--   else
-- --    return string.match(str or '',"%a") .. "kex"
--     return elem.text
-- --    if str then
-- --	    return "dose"
-- --    else
-- --	    return "kex"
-- --    end
--   end
-- end

function RawBlock(elem)
	if elem.format ~= 'tex' then
		return
	end

	local cmd, rest = string.match(elem.text, '^\\([^{]+)(.+)$')

	local args = {}
	for arg in string.gmatch(rest, '{([^}]*)}') do
		table.insert(args, arg)
	end

	if cmd == 'BefehlVorKapitelDFDM' then
                return {pandoc.Image(args[1], "/home/kaulquake/Fischtopf/Geschichten/DieFlotteDerMaare/img/" .. args[1]), pandoc.BlockQuote(args[2]), pandoc.HorizontalRule(), pandoc.Strong("Content Notes:"), pandoc.Str(args[3])}
	elseif cmd == 'Beitext' then
		return {pandoc.Emph(args[1]), pandoc.HorizontalRule()}
	elseif cmd == 'BefehlFigurInVerzeichnis' then
		if string.match(args[4], '\\Zeilenumbruch') then
			args[4] = string.gsub(args[4],'\\Zeilenumbruch', "\n\n")
		end
                return {pandoc.Image(args[2], "/home/kaulquake/Fischtopf/Geschichten/DieFlotteDerMaare/img/" .. args[2]), pandoc.Str(args[3]), pandoc.HorizontalRule(), pandoc.Para(args[4])}
	elseif cmd == 'Begriffliste' then
		return {pandoc.Header(1,args[2],'unnumbered')}
	elseif cmd == 'Leerzeil' then
		return {pandoc.blankline}
	elseif cmd == 'vertikalDehne' then
		return {pandoc.blankline}
	elseif cmd == 'neueSeit' then
		return {pandoc.blankline}
	elseif cmd == 'Zeilenumbruc' then
		return {pandoc.cr}
	elseif cmd == 'setzeBegrifflistenStil' then
		return {pandoc.cr}
	elseif cmd == 'setzeBegrifflistenEinzugtiefe' then
		return {pandoc.cr}
	elseif cmd == 'BefehlFigurInVerzeichnis' then
		return {pandoc.cr}
	else
		return elem.text
	end
end
