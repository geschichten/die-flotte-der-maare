\BefehlVorKapitelDFDM{Jentel-n.png}{Jentel ist die Mastkorb-Nixe. As hat einen sehr guten Sehsinn und bezeichnet sich deshalb auch manchmal als Nachtsicht-Nixe.}{Narben, Interfeindlichkeit, Genital-OP - erwähnt, Operation im Zusammenhang mit Rassismus, Romantik zwischen Geschwistern - erwähnt, toxische Beziehungen, Misgendern, Luftnot.}

Sinken
======

\Beitext{Jentel}

Jentel bekam eine Gänsehaut, als as die Stimme hörte. As
musste grinsen. Janasz hatte as irgendwann, als
sie angefangen hatten, mehr miteinander zu reden, gefragt, ob
as auch eine Gänsehaut bekommen könnte.

Es stimmte wohl: Nixen waren durchschnittlich weniger behaart,
als das meiste Landvolk. Aber Jentel stammte von Zwergen ab. As
hatte durchaus feine Härchen auf den Armen, die sich aufstellten. Wenn
as Sirenu hörte, zum Beispiel. Oder eine Marlyrie. Wenn as mit
Marah zusammen sang. Oder wenn as diese Stimme hörte. Diese
alt vertraute Stimme saines Geschwisters.

Jentel legte den tiefenentspannten Wels ins Seegras neben sich
und tauchte ab. Auch darüber musste as grinsen: Bis gerade
hatte as mit einer liebevollen Gründlichkeit die Barteln
des Briefwelses -- also auch eine Art Haare -- geputzt. Nicht, dass
die Barteln Putzen nötig gehabt hätten. Aber dieser Wels liebte die
Form der Aufmerksamkeit, hatte dabei ganz still gehalten und die
Augen geschlossen. Er öffnete nur ein Auge, als Junita die
Hand für ihn ins Wasser hielt. Und schloss es wieder, als er
bemerkte, dass Jentel sie stattdessen berührte.

Jentel ließ einen Finger zart über die Handinnenfläche
streichen, ergriff sie dann ganz und streckte den Kopf
aus dem Wasser. Junitas Gesichtsausdruck verwandelte sich
in, hm, einen emotionalen. Jentel war nie so richtig gut
darin gewesen, Emotionen zu lesen, vor allem nicht, wenn es
viele auf einmal waren. Jedenfalls wirkte sie glücklich oder
so etwas.

As musste ein weiteres Mal in saine Gedanken hineingrinsen. So, wie
Junita auf dem Steg lag und as darunter, wirkten sie, wie aus so
einem romantischen Buch entnommen, in dem sich ein Mensch in
eine Nixe verliebte. Nur war hier alles anders: Sie war ein
Zwerg und kein Mensch. Und sie war auch eine Nixe. Außerdem war
sie weiblich und as Neutre. Die Geschlechtsstereotypen solcher
Geschichten waren nicht erfüllt. Und as liebte sie nicht
auf romantische Weise. Nicht, dass sie sich darum geschert hatten, dass
romantische Gefühle zwischen Geschwistern irgendwie Tabu
waren oder so etwas. Jentel hatte noch nie romantische
Gefühle empfunden und fühlte sich mit dem Gedanken durchaus
einverstanden, dass sich das nicht ändern würde.

Junita war da allerdings anders. War das eine Träne in
ihrem Gesicht?

"Bist du verliebt?", fragte Jentel.

"Was?" Junita wirkte verwirrt. Und überrumpelt?

"Du wirkst glücklich.", erklärte Jentel. "Und weinst."

"Ich bin sehr froh, dich zu sehen.", erklärte sie. "Und
traurig, dass ich nicht viel Zeit habe."

So schlecht Jentel darin war, Emotionen aus Gesichtern
abzulesen, so sehr fielen ihm aber doch gelegentlich
interessante Elemente in der Kommunikation auf. Junita
hatte nicht widersprochen. "Bist du verliebt?" Dieses
Mal betonte as die Frage etwas anders, mehr auf dem
Verb.

Junita schloss die Augen. "Ich weiß es nicht. Ich
glaube schon."

"In mich?", fragte Jentel.

Junita grinste und schüttelte den Kopf. "Du bist schon
sehr erstaunlich.", sagte sie. "In eine Person, die du
nicht leiden kannst."

Jentel musste nicht lange überlegen. "Die Zarin."

Junita nickte und öffnete die Augen wieder. "Ich weiß
auch noch nicht, was das soll.", sagte sie. "Vielleicht
trifft das Wort 'verliebt' auch nicht ganz den Punkt."

"Viel wichtiger ist, dass ich dir an der Stelle zusichere,", leitete
Jentel ein, aber hatte die Worte oder auch den genauen Inhalt
dessen, was nun wichtig war, noch nicht zu Ende gedacht. Deshalb
stockte as. Mit dieser Einleitung ließ Junita ihm immerhin die
Zeit. "Es ist sehr wichtig, dass ich dir zusichere,", wiederholte
as, "dass ich sie schon mögen lernen werde, wenn sie dir gut
tut. Es gibt kaum etwas Schlimmeres, als nahestehende Bezugspersonen, die
sich gegenseitig nicht akzeptieren."

"Hast du dich doch schon einmal verliebt?", fragte Junita
überrascht. "Weil das so klang, als hättest du die Erfahrung, meine ich."

<!--Hier ist ein gm in "einem"-->
"Es gibt verschiedene Weisen, auf die einem eine Person
nahestehen kann.", erinnerte Jentel. "Ich habe einfach vermutet, dass
es bei Romantik irgendwie eine Parallele gibt."

Junita lächelte. "Ich habe dich lieb.", sagte sie. "Ich möchte nicht, dass
du die Zarin auf einmal unkritisch siehst. Es ist für mich wichtig,
die wenigen Male, die wir uns sehen, mit dir darüber reden zu können, wie
sehr auch ich sie eigentlich nicht mag."

Jentel runzelte die Stirn. "In Ordnung.", sagte as. "Ich kann nicht
sagen, dass mich das nicht verwirrt."

"Ich glaube, mein Interesse ist oberflächlicher.", sagte
Junita. "Vielleicht ist es einfach nur Zärtlichkeit, die ich
vermisse."

Jentel, das immer noch ihre Hand hielt, strich sanft mit den
Fingern an den Rändern selbiger entlang. "Das ist eigentlich
nicht sehr oberflächlich.", sagte as. "Es ist für manche Leute
so wichtig wie essen. Oder für viele. Jedenfalls gibt es Personen, die
daran kaputt gehen, wenn das Bedürfnis nicht gedeckt ist."

Junita nickte. "So weit ist es bei mir noch nicht.", sagte
sie. Sie zog die Hand allerdings nicht weg. "Ich weiß nicht einmal, ob
die Zarin dahingehend irgendetwas tun würde. Aber wenn, muss ich
sehr, sehr vorsichtig sein. Das ist alles mit Stress verbunden."

Jentel fuhr die feinen Narben an Junitas Fingerrändern nach. Ihre
Familie hatte sich entschieden, als sie noch sehr klein gewesen war,
die Schwimmhäute zu entfernen, damit Junita nicht auffiele. Das
wäre gefährlich gewesen. Die Narben waren kaum sichtbar an den
rauen Händen. Jentel hatte nie gewagt zu fragen, wie sie sich
damit fühlte.

Junitas Augenfarbe war dunkel. Es fiel kaum auf, dass sie für
Zwerge vielleicht einen überraschend hohen Orange-Anteil
hatten.

Und dann waren da Junitas Brüste und andere
Körpermerkmale, die atypisch waren. Wenn Zwerge mit einem
Körper wie ihrem zur Welt kamen, entwickelten
sie typischerweise während der Pubertät Brüste und
eine weichere Körperstruktur, aber das
war bei ihr nicht passiert. Das lag
nicht daran, dass Junita eine Nixe war. Bei etwa einem in
hundert oder auch dreihundertzwölf Zwergen -- so sicher
war sich die rudimentäre Forschung da nicht -- gab es Variationen
bei der Körperentwicklung während der Pubertät, die
nicht so häufig bei Zwergen vorkamen, und
Junita war eben einer davon. Es war lediglich ein ungünstiger
Zufall, dass es gerade Junita betraf, eine Person, die möglichst
wenig auffallen sollte.

Das war auch ein Grund dafür gewesen, dass ihr eines Elter mit
ihr nach Mizugrad verzogen war. Mizugrad war eine eher prüde
Stadt. Dort fiel es nicht auf, dass ein Kind einen Kleidungsstil
hatte, der nicht viel Haut zeigte und viel der Fantasie
überließ, und sich nie vor anderen zum Baden auszog
oder bei der Hygiene eine gewisse Scham zeigte. Junita
hatte gelernt, sie vorzuspielen.

In Junitas Leben war so einiges nicht gut.

Jentel nickte. "Pass auf dich auf. Ich sehe zu, dass ich
dir Informationen zukommen lassen kann, sodass du aufpassen
kannst, dass die Beziehung nicht toxisch wird, wenn sie
entstehen sollte." As würde Kamira fragen. "Oder wie du dich verhalten
kannst, wenn sie es wird."

"Danke.", sagte Junita bloß. Sie zitterte. "Geschwisterherz, ich
könnte dich eigentlich gerade wirklich zum Trösten gebrauchen, aber
ich sollte nicht verheult sein, wenn ich zurückkehre und ich
habe keine Zeit mehr."

"Irgendwie sehen wir uns wieder.", versprach Jentel. Aber sie
wussten beide, dass das Risiko bestand, dass etwas dazwischen
passieren könnte, oder es sehr lange brauchen würde.

Junita überreichte ihm einen Brief, der eigentlich für
den Briefwels gedacht gewesen war. "Wenn alles gut geht, bekomme
ich die Zarin morgen noch dazu, das Gespräch mit der Kapitänin auf
der Schattenscholle führen zu wollen.", sagte sie. "Als Gleichberechtigte
reden und so. Gerade
klang sie bereit, zu verhandeln. Aber egal wie sie sich
entscheidet, es haben sich bei ihr Einstellungen zu unseren
Gunsten geändert."

---

Zum Morgengrauen war Jentel wieder im Hafen. Wenn alles gut
gegangen war, dann hatte Junita auch noch geschafft, den
Gefangenen einen Brief zuzustecken. Aber es war möglich, dass
selbst dann Amira über den Plan nicht informiert wäre, weil
sie ja in Einzelhaft gesteckt worden war.

Aber als sie tatsächlich am frühen Vormittag in
den Hafen geführt wurden, stand Amira dicht
neben Rash.

Es war Jentels Aufgabe, als einzige der Nixen überwiegend den
Kopf über Wasser zu halten. Um zu lauschen und zu gucken. Wie
es immer saine Aufgabe gewesen war. Aber in diesem Hafen unter
so vielen Landleuten mit ihren Füßen konnte as nicht leugnen,
furchtbare Angst zu haben.

Sindra ging neben Katjenka, umringt von vier Wachen. Die
Zarin hatte wieder nicht mit Wachen gespart. Das machte
den Plan heikel, aber weit entfernt von unmöglich.

Sindra nieste, zweimal und dann noch einmal. Sie konnte
es absichtlich. Jentel hatte es schon geahnt -- as war
immer gut darin gewesen, Muster zu beobachten -- , war aber
erst im Zusammenhang mit den jüngsten Ereignissen auch
offiziell darüber informiert worden, dass Sindra es konnte und
manchmal zur Kommunikation einsetzte. Wie auch nun.

Jentel tauchte ab und gab auf Sirenu die Nachricht an die
anderen weiter, dass der Brief angekommen war und sich die
Crew mit ihrem Plan einverstanden erklärte. Und tauchte
wieder auf.

Rash ging direkt neben Amira, einen Arm um sie gelegt. Jentel
wurde beim Anblick wärmer. Sie waren gut zueinander. Jentel
hatte Amira fast von Anfang an gemocht. Und Rash sowieso. Es
erleichterte as außerdem, sie dicht beieinander zu sehen, weil
Rash so bestimmt in der Lage wäre, Amira über den Plan zu
informieren, wenn sie ihn noch nicht kennen würde.

Amira trug ein Kleid, das ihr besser passte, als Sindra
das Kleid, in dem diese steckte. Es war auch kein so
bauschiges Kleid wie das von Sindra. Amira hatte also
all die schönen Messer und die Uniform zurücklassen müssen. Immerhin
hatten sie ihr das Kopftuch gelassen. Aber Jentel war davon
überzeugt, dass es gründlich auf jegliche Form von Waffe untersucht
worden war. Und genau so war as überzeugt, dass Amira auch
ohne jegliches Messer recht tödlich sein könnte. Wenn sie
gewollt hätte. Jentel hatte den Eindruck, dass ihr das
Kleid eher wegen der dadurch ausgedrückten Geschlechtszuweisung
zu schaffen machte, als dadurch, dass es keine Waffen enthielt. Dann
wiederum konnte as das nicht aus ihrem Gesicht, ihrer
Haltung oder sonstigem Ausdruck lesen, sondern nur durch
frühere Konversationen mit ihr einschätzen und
projizierte hier fröhlich herum. Es würde bald vorbei sein.

Die Zarin, die Wachen und die Gefangenen blieben
zunächst gemeinsam vor der Schattenscholle stehen. Die
Zarin hatte Skepsis, ob es so eine gute Idee war, eine Crew
zurück auf ein Schiff zu führen, das sie gut kannte. Das war
deutlich und verständlich. Die Skepsis hatte sie sehr zurecht. Sie
sprach schließlich ihre Gedanken laut aus, was Jentel
tatsächlich für eine sehr zielführende Idee hielt, wenn es
um Verhandlungen mit sainer Kapitänin ging.

"Eigentlich wollen wir ja, dass ihr wieder zur See fahrt.", sagte
sie. "Solange meine Leute, die ich mit euch schicken möchte, zuerst
auf dem Schiff sind, kann eigentlich nichts passieren, selbst
wenn ihr wie magisch plötzlich alle Segel hisst und den Hafen
verlasst. Ihr würdet sie nicht von Bord schmeißen. Ihr gefährdet
keine Leben."

"Wir gefährden keine Leben, außer unsere eigenen.", korrigierte
Sindra gelassen.

Jentel kannte ihre analytische Art und fürchtete fast, sie
könnte dazu sagen, dass, Personen im Hafen von Bord zu werfen, keine
Lebensgefährdung wäre. Aber die Zarin verstand es irgendwie
auch ohne, dass Sindra es aussprach.

"Es ist vollkommen unrealistisch, wie wir an gestern gesehen
haben, dass ihr alle
Wachen zugleich überwältigt, ohne ihnen zu schaden.", hielt
sie fest. "Das schafft ihr nicht, bis ihr draußen auf
See seid."

Jentel lächelte. Die Kapitänin reagierte nicht, zumindest nicht für
Jentel sichtbar, das sie ja nur von unten durch die Ritzen im Steg
sehen konnte.

Katjenkas Plan war, über das Schiff geführt zu werden, gegebenenfalls
erste Punkte für ihren Vertrag festzuhalten. Sindras Plan war das
nicht.

Jentel wartete, bis das letzte Mitglied der Crew die Schattenscholle
betreten hatte und gab das Zeichen, das unverzüglich Chaos
auslöste. Die Mitglieder der Schattencrew hatten sich nahe der
Reling verteilt. Das war tatsächlich etwas, was der Wache intuitiv
gut vorgekommen war: Die Crew von den Niedergängen
fernzuhalten, von den Schoten und allem. Als die Nixen im Hafen aber
hohe Wasserfontänen zur Ablenkung spritzten und den Sinkmechanismus
der Schattenscholle auslösten, konnte sich die Crew festhalten
und sich unter Wasser ziehen lassen. Jentel überwachte, dass sich
keine Person festhielt, die nicht zur Crew gehörte. Aber die
Wachzwerge hatten da eine für sie vorteilhafte Intuition, lieber
zu schwimmen und nicht in die Tiefe gesogen zu werden.

Jannam hatte die komplizierte Aufgabe, den Crewanteil mit Füßen
rasch in das Unterdeck zu verfrachten, bevor es sich ganz schloss, bevor
zu viel Luft entweichen konnte. Aber immerhin war das Tauchdeck
leer gewesen, was ihnen etwas mehr Zeit verschaffte.

Jentel behielt die im Wasser schwimmenden Beine im Auge, die
strampelnd die zugehörigen Personen Richtung Leitern und Ufer
manövrierten. Nicht alle. Jentel hörte die wütenden Rufe und
Schreie, schwamm etwas abseits, um noch ein bisschen zu Spritzen
und sie davon abzulenken, wo eigentlich ihre Aufmerksamkeit
hätte sein müssen, wenn sie etwas hätten verhindern wollen.

Dann hörte as den Befehl der Zarin selbst durch das
klangabdämpfende Wasser hindurch: "Versperrt die Hafenausfahrt
mit Schiffen und Netzen!"

Jentel grinste. Und trotzdem fühlte sich sain Körper vor Aufregung
zittrig. Was, wenn irgendein kleines Angelboot, das sie nicht
präpariert hatten, mit Angelhaken um sich warf? Jentel tauchte wieder zurück, wo
die Schattenscholle gerade von den anderen Nixen Richtung
Hafenausfahrt geschoben wurde. Es sah so aus, als könnte
alles klappen. Sie hatte sich geschlossen und lag
angenehm schwer und gleitbereit im Wasser. Jannam befestigte das
letzte Segel darüber, schon während as anschob.

Jentel überlegte, ob das der Zeitpunkt wäre, in dem as
aufhören könnte, den Hafen zu überwachen, was saine Aufgabe
war, und stattdessen mitanpacken sollte. Es wäre saine
Intuition gewesen, aber sie kannten as ja und hatten as
immer wieder ermahnt, wirklich wachsam zu bleiben und
das Schieben solange Aufgabe der anderen sein zu lassen.

As sah sich ein letztes Mal um. Und... erkannte ein Paar
Beine, das dort nicht hingehörte. Zu lange Beine. Kantas
Beine.

Es stimmte, Jentel hatte sie nicht an der Reling gesehen. As
hatte sich gefragt, ob as sie einfach übersehen hätte und
den Gedanken wieder verdrängt, als anderes wichtiger
war. War sie nicht informiert gewesen? Nein, das war es nicht. Sie
hatte sich dazu entschieden, nicht mit unterzutauchen.

Plötzlich war Jentels Kopf voller Gedanken. Warum tat sie das?
Wollte sie von der Flucht ablenken? Wollte sie
Informationsperson im Zarenreich der Zwerge werden? Aber hier
hatten sie schon Junita. Auch wenn Jentel froh gewesen wäre, wenn
Junita irgendwann frei sein würde.

War es abgesprochen?

Und dann schloss as: Sie hatten beim letzten Mal nicht
alle in die Schattenmuräne gepasst. Kanta glaubte wahrscheinlich, dass sie
eine Person zu viel wären. Es war auch sehr eng. Aber Yanil war
ja nicht dabei. Yanil war an Bord der Schattenmuräne. Der
Platz war da. Wahrscheinlich hatte Kanta nicht daran gedacht.

"Kommst du?", rief Jannam ihm zu.

Jannam war mit den anderen und der Schattenscholle bereits bei
der Hafenausfahrt. Die kleineren Schiffe, die auf Befehl der
Zarin hin startklar gemacht wurden, versuchten, Richtung
Hafenausfahrt zu gelangen, und begannen,
ohne es zu wissen, Seile durch
den Hafen zu spannen. Eines spannte sich zwischen ihm und
Kanta. Aber es war nur ein Seil, kein Netz. Kein
wirkliches Hindernis für eine Nachtsichtnixe.

"Schafft ihr das ohne mich?", fragte Jentel.

Jannam gab ein zustimmendes Geräusch von sich. As vertraute einfach.

Und Jentel schoss unter dem Seil hindurch zu jenen Füßen. Den
Füßen zwischen all den anderen Füßen in der Nähe, viel zu
dicht. Jentel hielt sich einen Moment nur einen guten Meter unterhalb der
aufgewühlten Wasseroberfläche auf, wo sie alle strampelten, aber
entschied sich dann. As packte Kanta bei den Füßen und zog sie
mit Wucht in die Tiefe. As legte rasch eine Hand über
ihren Mund. Sie hatte natürlich Wasser geschluckt und
as wusste, wie unangenehm es war, dann nicht noch eine
Weile über Wasser husten zu können. Aber es musste
auch so gehen.

Jentel griff sie, wie as es damals gemacht hatte, und legte
ihr einen Finger zwischen die Zähne. Ihr Haar war bei
dieser Rettungsaktion offen und streichelte schwebend
um sainen Hals. Bis as Geschwindigkeit aufnahm. Das Wasser
rauschte an ihnen vorbei, als Jentel um die Seile im Wasser
und um aufbrechende und sinkende Schiffsrümpfe herum
Slalom schwamm. Der Hafen war Chaos.
Während größere Schiffe sich gegenseitig ihre vorgesägten
Löcher aufrissen und Wassermassen in sich sogen, versuchten
sich die kleineren auf Befehl der Zarin um die großen
herumzuschlängeln. Einige Personen versuchten es sogar mit
schwimmen und für Zwerge waren sie gar nicht so
schlecht darin.

Trotzdem ließ Jentel mit Kanta im Arm die Schlacht binnen
Sekunden hinter sich. Das Adrenalin hielt aber noch an. Sie
waren noch lange nicht sicher.

Kanta biss viel zu früh vorsichtig zu. Das war kein guter
Zeitpunkt, sie waren noch in viel zu dichtem Gemenge. Jentel versuchte
ihr durch sanften Druck zu signalisieren, dass as es mitbekommen
hatte. Was sollte as tun, wenn sie das Bewusstsein verlöre? Sie
durfte dann auf keinen Fall atmen. Jentel hatte keine Ahnung, ob
das eine gute Idee war, aber verschloss vorsichtshalber ihre
Atemwege mit sainen gespreizten Fingern und blickte sich
nach einer Stelle zwischen den Schiffen um, die vor
dem Hafen vor Anker lagen, oder den Hafen gerade anfuhren, die
in der Nacht noch nicht da gwesen waren.

Da, da war eine größere Lücke. Sie würden gesehen werden,
wenn Kanta zu viel strampelte, aber es könnte trotzdem
passen. Ab wann starben Elben, wenn sie keine Luft
bekamen?

Aber noch weniger riskant wäre, wenn as sie beatmete. Ihm
war vollkommen egal, wie sie es interpretieren könnte, hier
ging es um Sicherheit.

As drückte sie weiter unter Wasser, als as Luft holte, und
ihr dann sachte Luft in die Lunge blies, als as wieder
abgetaucht war. Es war alles so absurd, dass as ein bisschen in ihren
Mund lachte. As wiederholte die Sache. Kanta grinste
ebenfalls. Dann schoss Jentel mit ihr weiter durchs Wasser, bis
wohin die Schattenmuräne vor Ufer startbereit ruhte.

Sie lag dort beigedreht, bewegte sich also nur langsam, und wartete auf
die Ankunft der Schattenscholle. Jentel tauchte mit Kanta
zum Eingang des Tauchdecks, der dieses Mal wie geplant, knapp
oberhalb der Wasseroberfläche lag. As rollte erst Kanta
hinein und dann sich. Sie holte gierig Luft, hustete, spuckte Wasser. Dabei
blickte sie Jentel an. War das Verwunderung?

"War das in Ordnung für dich?", fragte Jentel. "Wolltest du
an sich schon bei uns bleiben?"

Kanta nickte. "Ich,", sie haderte, hustete noch einmal, "ich
dachte, ich passe nicht. Oder wollte das nicht riskieren, dass
es für die anderen nicht klappt."

"Yanil ist hier an Bord.", informierte Jentel. "Du hättest
gepasst."

"Ich wollte kein Risiko eingehen.", wiederholte Kanta. "Und Smjer
hatte halt recht. Ich habe immer noch die besten Chancen, wenn
ihr mich zurückließet. Aber..."

Sie sprach nicht direkt weiter. Jentel war jedenfalls erleichtert, dass
saine Entscheidung richtig gewesen war. "Aber?"

"Danke!", sagte Kanta. "Ich bin gerade sehr überwältigt. Dass
du mich freiwillig rettest. Danke."

Waren das Tränen? Erst war sich Jentel nicht sicher, aber dann war
es ziemlich deutlich. "Falls du das hören musst:", sagte Jentel, "Du
gehörst dazu."

Brauchte sie irgendeine körperliche Zuwendung, weil sie sich
nun noch mehr in Tränen auflöste? Das war kein guter Zeitpunkt. Jentel
wusste nicht so recht wie das ging. Und dann tauchte auch noch
Jannam neben ihm auf. Sie waren schnell mit der Schattenmuräne. Das
war gut.

Kanta schniefte und atmete tief ein und aus. "Danke.", flüsterte
sie. "Geht schon wieder. Kann ich was helfen?"

"Einmal Smjer runterholen.", sagte Jentel. "Das wäre wirklich
eine Hilfe."

Kanta nickte und kletterte den Niedergang hinauf, während
Jannam und Jentel die Seile, die Jannam mitgebracht
hatte, an der die Schattenscholle hing, fest an Griffen verknoteten,
die im Tauchdeck als Halterung für das Tauchboot gedacht
waren. Jenes lag gerade fast auf dem Trockenen.

Sie waren gerade fertig, als Smjer auftauchte. "Das
wird kippelig.", informierte Smjer. "Gleitfahrt ist immer eine
kippelige Sache, aber wir schleppen dabei auch noch die
Schattenscholle. Das wird herausfordernd. Ist sie gut vertäut?"

Jentel und Jannam nickten.

"Jentel, in den Mastkorb.", befahl Smjer außerordentlich gut
gelaunt. "Jannam zurück zur Schattenscholle. Ich sehe an den
Seilen, dass sie ein bisschen schwer ist und jede Hilfe dort
willkommen ist, bis wir genug Fahrt haben." Er blickte
sich zu Kanta um, die mit ihm
wieder hinabgestiegen war. "Vielleicht ist ganz gut, dass
sie dieses Mal eine Person leichter ist. Magst du dich meinem
Kommando fügen?"

"Ohne Einschränkungen.", sagte Kanta.

"An Deck mit dir. Hilf Marah mit den Segeln.", befahl er. "Es ist so
gut, sie wiederzuhaben. Das macht uns so viel schneller bei gleicher
Sicherheit."
