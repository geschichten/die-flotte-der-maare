\BefehlVorKapitelDFDM{Marah-n.png}{Marah wurde von Ushenka großgezogen und hat die Flotte der Maare quasi mitgegründet. Sie ist verliebt in die Kapitänin.}{Schmerz.}

Knoten
======

\Beitext{Marah}

Es war anscheinend für diese Leute keineswegs naheliegend, dass Nixen
womöglich schwimmen könnten. Vielleicht waren sie aber auch einfach
sehr überfordert gewesen, als Sindra mit ihr aus der Kramelin
geflohen war. Sindra hatte Marah einfach über die Schulter gelegt und
war gerannt. Nicht so furchtbar schnell, nicht sonderlich
geschickt, aber ihre Körpergröße und ihr überzeugtes Auftreten
hatte ausgeglichen. Wachen waren ihr einfach im ersten Moment
ausgewichen. Marah kannte Sindras Überzeugungskraft schon. Sie
konnte auftreten, als wäre sie im Recht, und die Gegenseite
glaubte erst einmal daran.

Trotzdem, es waren viele, so viele. Es war ein Wunder, dass
Sindra es bis nach draußen geschafft hatte. Dass sie bis auf die Brücke
gekommen war, die den Hof der Kramelin von der Stadt abtrennte. Aber
dort, auf der breiten Brücke, hatten sie sie schließlich eingekreist. Sie
hatte Marah sanft abgelegt und die Hände erhoben. Eine geschickte Geste, die
die Aufmerksamkeit auf sie lenkte. Marah brauchte nicht erst zu hören, dass
sie wegrobben sollte. Sie schaffte es, während Sindra
freundliche Worte an die Wachen richtete, und durch Albernheit und
Höflichkeit jene noch mehr ablenkte, zwischen den Füßen hindurch zum
Geländer zu kriechen und zwischen den breiten Steinsäulen
hindurch den Abgang zu machen. Sie konnte Sindra besser frei als
gefangen helfen. Auf jeden Fall konnte sie nichts an Land ausrichten, gegen
die Wachen der Zarin mit nichts als einem Fischschwanz.

Das kühle Wasser umschloss sie. Durch die Wasseroberfläche erblickte
sie einige Zwerge, die auf sie herunterschauten und durcheinander
schrien. Aber sie konnte nicht nur so ein bisschen schwimmen, sondern
ausgezeichnet.

Ihr Körper fühlte sich zerschunden an. An der Fluke
zu hängen, ging sehr auf die Knochen und der Flukenansatz war
wundgescheuert, brannte noch einmal besonders, als er
mit dem Brackwasser in Berührung kam. Sie verdrängte den Schmerz und
schwamm, wo sie die leichte Strömung hinsaugen fühlte, dorthin, wo
der Kanal um die Kramelin-Insel herum mit dem Fluss verbunden war, vom
Fluss aus ins Meer, auf Angelhaken und Netze achtend, und von dortaus
Richtung Hafen. Ein riskanter Aufenthaltsort für eine Nixe.

Es war anscheinend auch für diese Leute keineswegs so naheliegend, wie
für sie, dass ein Hafen, hielt die zugehörige Stadt Maare gefangen und
liefe eine bereits bekannte Rettungsaktion für selbige, voller
Nixen wäre. Sie hörte es natürlich am Sirenu, das durch das Wasser
zu ihr erscholl.

Sie holte kurz unauffällig Luft und antwortete. Das Sirenu, das sie
willkommen hieß, fasste Erleichterung in Musik, prickelte unter
der Haut.

Der Hafen war bereits mit Seilen durchspannt, als sie ankam, aber
es gab noch genug zu vertüddeln, woran sie sich noch beteiligen
konnte. Sie verknoteten die Schiffe miteinander. Einen einzigen, nur
ihnen bekannten Fluchtweg seilfrei lassend. Sie blockierten außerdem
die Scharniere der Ruder vieler der Schiffe mit Seegras und Tang. Sie sägten Löcher
in die ein oder andere Bilge, die sie direkt wieder schlossen, aber
verbanden die Deckel miteinander. Yanil führte die Säge, Kamira und
sie schoben das Tauchboot.

Wenn es zu einer Flucht käme und ihnen Schiffe nachsetzen würden -- sie
wusste, welche es wären --, so würden sie nicht weit kommen. Schließlich, als
sie fertig waren, sammelten sie sich unter einem der Stege. Eine ganze
Sammlung an Köpfen, die in feindlichem Gebiet ihre Nasen aus dem Wasser
streckten. Es war aufregend und erfüllte Marah mit einem gewissen
Stolz. Das war ein Ereignis, das vielleicht
Geschichte schreiben würde.

Es würde wohl zu sinkenden Schiffen kommen, das hatten sie entschieden. Nicht
unbedingt ihr Stil, aber ausreichend überlebenssicher für die
Abfahrenden. Es war vor allem Sabotage. Nun galt es,
abzuwarten: Würde es der Rest der Crew irgendwie
aus der Kramelin und auf die Schattenscholle schaffen, bevor ihr Schiff
den Hafen verlassen musste? Marah und die anderen würden sich im Hafen bereit halten, um
die verhedderten anderen Schiffe wieder zu entknoten, die den Hafen verlassen würden, bevor
die Schattenscholle dies täte. Andernfalls würden ihre Vorbereitung
zu früh auffliegen. Aber bevor zu viele Schiffe von außerhalb, die
von ihnen folglich nicht präpariert hatten werden können, den Hafen
erreichen würden, mussten sie ihn verlassen haben. Dann
wäre es besser, die Schattenscholle aus dem Hafen zu fahren, ob der Rest der Schattencrew
an Bord wäre, oder nicht. Mit der Schattenscholle hatten sie wesentlich bessere Optionen, als
ohne sie.

Aber das Abwarten war eine Härteprobe ohne Gleichen. Sie erfuhren lange
nichts Neues. Marah beschäftigte sich damit, leise über jedes Detail
zu reden, das sie hatte miterleben oder interpretieren können, bis der
Morgen graute und das Hafenleben wieder erwachte.
