\BefehlVorKapitelDFDM{Kamira-n.png}{Kamira ist an Bord für Konfliktmanagement, psychischen Beistand und Barrierabbau zuständig. Er hat am Anfang den Nixen-Anteil der Crew zusammengesetzt und Smjer gefragt, ob er das Vize-Kommando übernehmen würde.}{Gefangennahme, Speziesismus, Panik, Ertrinken - erwähnt.}

Wenden
======

\Beitext{Kamira}

Kamira hatte es gewusst.

Was in seinen Sitzungen besprochen
wurde, blieb dort. Das versprach er, damit Personen über wirklich
alles reden und sich dabei sicher fühlen konnten, und er
hielt sich auch daran. Außer, es gefährdete Leben, psychische
Gesundheit eingeschlossen. Auch das machte Kamira in ersten Sitzungen
stets transparent.

Kanta hatte damals hin- und herüberlegt und sich ihm schließlich doch
anvertraut. Erst, als sie auf dem Weg der Entscheidungsfindung
damit gewesen war, ob sie aufhören oder weitermachen wollte. Sie hatten gemeinsam
diskutiert, inwiefern Kantas Verhalten Leben riskiere und waren
zum Schluss gekommen, dass es das tat, aber auf ganz andere
Weise, als es sich nun herausgestellt hatte: Kamira hatte Kanta
erklärt, was passieren würde, wenn Forschungsschiffe in Grenlannd
ankommen würden. Sie hatten über die Forschungsmethoden der
Elben geredet, die Kanta sehr gut kannte. Kanta hatte zugeben
müssen, dass sie an Bord viel über Respekt gegenüber anderen
Kreaturen als sich selbst gelernt hatte, und dass sie zuvor
eine andere Grenze gezogen hätte, wen sie als Person bezeichnen
würde, basierend auf ähnlicher Denkfähigkeit zu ihrer eigenen, oder
ähnlichen Denkweisen, ähnlicher Kultur, -- angefangen bei Nixen. Kamira
hatte Kantas unterbewusste Abwertung anfangs und die Änderung
in den Wochen, in denen sie an Bord war, durchaus bemerkt. Wäre
Kamira nicht sehr gut gewesen, sich zu distanzieren, hätte
er die ersten Sitzungen mit ihr kaum ausgehalten.

Auf der anderen Seite hielt sich Kanta selbst auch nicht für
sonderlich viel wert. Vielleicht hatte dieser Kontext bei
der Distanzierung etwas geholfen.

Kanta hatte dann mit ihm überlegt, warum sie trotzdem
Nachrichten verschicken wollte. Was ihre Motive waren. Ein Motiv, das
sich herausstellte, war ein Vermissen und eine Hoffnung,
Kontakt zu einer ihr sehr lieben Person wieder aufzunehmen: Arwin. Aber
Arwin hatte sich nie zurückgemeldet. Das konnte so viele Gründe haben: Dass
die Forschungscrew, zu der Kanta gehört hatte, nun keine Gelder mehr
bekam, um weitere Schiffe voraussichtlich ohne Erfolg loszuschicken, zum
Beispiel.

Kanta hatte sich gefragt, ob sie mit dem Streuen unauffällig falscher
Informationen nicht all ihre Interessen zugleich verfolgen konnte: Die
Flotte der Maare schützen, Briefe an Arwin schicken und vielleicht
dafür Sorge tragen, dass doch ihre Forschungscrew noch einmal finanziert
würde. Das würde zwar auf der einen Seite bewirken, dass wieder
ein Forschungsschiff losgeschickt würde, gegen das die Flotte
der Maare vorgehen müsste, aber es wäre ein Routineangriff, während, wenn
die besagte Crew die staatliche Unterstützung verlieren sollte, ja
das Forschungsziel nicht aufgegeben würde, sondern stattdessen
in andere Forschungsgruppen investiert würde, die die
Maare noch nicht kannten. Im ersten Fall wäre es ein vergleichsweise
sicherer Angriffsablauf für die Flotte der Maare, während
Arwin ihr vielleicht wieder eine Nachricht schicken könnte.

Kamira war nicht glücklich damit, aber als betreuende Person ließ
er andere auch einfach viel sie selbst sein, ihren eigenen Weg
finden. Sie hatten sich gemeinsam Gedanken über weitere, mögliche
Gefährdung gemacht. Kanta hatte ihm die Texte vorgestellt, die
sie in Rashs Rückmeldungen an Ushenka einfließen lassen würde. Sie
waren nicht lang. Eine gewisse Bewunderung hatte Kamira schon
für die Methode übrig. Sie wären beide nicht auf die Idee gekommen, dass
die Briefe mehr oder weniger indirekt zu Morden beitragen könnten, wie
sie nun geschehen waren. Zumindest ältere Briefe hatten
wahrscheinlich dazu beigetragen, die Kanta schon längst
geschrieben hatte, bevor sie sich ihm anvertraut hatte. Trotzdem.

Kamira hatte es gewusst.

Und ein schlechtes Gewissen nagte an ihm, dass er falsch entschieden haben
könnte. Dass das doch ein Fall gewesen sein könnte, in dem er
hätte etwas aus den Sitzungen preisgeben sollen. Niemand hatte
ihm einen direkten Vorwurf gemacht.

Kanta hatte bei ihrer Offenlegung an Deck für alle auch transparent gemacht, dass
er davon gewusst hatte. Er verstand das. Ihr Gedanke war dabei
gewesen, ihre Entwicklung und überhaupt alles endlich offen
darlegen. Das Verhältnis in den Sitzungen war auch durchaus
so angelegt, dass Personen, die zu ihm kamen, sich selbst entscheiden
konnten, mit anderen über ihre Probleme auch zu reden. Das
Schweigen galt für jene nicht, nur für ihn.

Aber Smjer hatte ihn in einer Art angesehen, die Kamira nicht
gut hatte interpretieren können. Und der Teil in ihm, der sich
fragte, ob seine Entscheidungen richtig gewesen wären und was Leute
über ihn denken könnten, der in Kamira meistens relativ ruhig
war, hatte sich verselbstständigt.

Manchmal wünschte Kamira sich, nicht die Person für Psycho-Hygiene
zu sein. An Bord war nur für eine solche Person Platz und
er hätte eine gebraucht. Lyria hatte die Rolle auf der Schattenscholle
gehabt, aber Lyria war tot.

Er hätte gern wenigstens mit Marah
Zeit verbracht. Eigentlich egal, was für Zeit. Marah hatte eine
sehr eigene Art, zu sein, die ihn schon glücklich machte, wenn
er sie dabei nur beobachtete. Aber Marah schlief schon wieder. Das
war gut. Sie hatte nicht nur ihr wichtige Personen verloren und
zunächst, statt dem Gefühl Raum zu geben, mehrere Nächte allein auf
dem Wasser verbracht, sich um Personen gesorgt und einige gerettet, sondern
hatte sich auf der langen Fahrt mit Ushenka den Rücken schlimm
verbogen und war damit viel zu spät zu ihm gekommen. Erst nachdem
sie damit einen halben Tag lang dann auch vor Schmerz kaum hatte
schlafen können, obwohl sie so müde war. Nach der Trauerfeier
hatte er sich dann um ihren Rücken gekümmert: Eine schlimme
Verspannung, die sich mit Reiben, etwas gezielter Bewegung
und Schmerzmittel behandeln ließ. Nun endlich schlief sie. Hoffentlich
lange. Kamira freute sich trotzdem schon auf danach, sich
selbst vielleicht etwas entlasten zu können.

---

Dazu kam es nicht. Kamira tauchte in einer geplanten Pause
zwischen zwei Sitzungen ins Tauchdeck und stellte fest, dass
der Briefwels von der Schattenscholle mit Post eingetroffen war. Einer der schnellsten
Welse, denen Kamira je begegnet war, etwas scheu, kam weniger gern
zum Kuscheln, aber hatte dafür umso größeren Appetit. Außerdem
verbrachte er gern Zeit mit Kamira. Er hätte dem Wels seine
Zeit gern gegönnt, hatte aber im Gefühl, dass es gerade Priorität
hätte, den Brief der Kapitänin zu übergeben. Immerhin spielte
Kamira auch viel mit dem Wels, wenn er ohne Post kam oder wenn
er auf einen Brief der Schattenscholle wartete, um ihn zurückzubringen.

Der Brief enthielt Informationen über ein Forschungsschiff aus dem
Zarenreich der Zwerge, das eilig versandt worden war, während
die süd-ost-maerdhische See unbewacht gewesen war. Sie hatten
halb damit gerechnet, dass so etwas passieren könnte. Die verschollene
Schattenscholle und das teilerfolgreiche Attentat des
Assassinans auf selbiger waren sicher nicht unbemerkt an den
Landvölkern vorbeigegangen. Sie hatten natürlich nicht wissen
können, wie schnell die Flotte der Maare diesen Teil des
Meeres mit neuen Schiffen abdecken
könnte. Es gab trotzdem viel, was sie nicht voneinander wussten. Aber
es war damit zu rechnen gewesen, dass die Zarin, die
in ihrem Reich die Entscheidungen bezüglich Forschung fällte, probieren
würde, genau jetzt an der Flotte der Maare vorbeizukommen.

Die Kontaktperson berichtete, dass das Schiff überstürzt mit Vorräten
versehen und ohne viel Vorbereitung aufgebrochen war. Allerdings
früh genug und auf östlicherer Route, sodass leicht hochzurechnen
war, dass es den Bereich der See, wo die Schattencrew üblicherweise
angriff, schon passiert hatte.

Der Trick war ja, die Vorräte so zu dezimieren, dass die Forschungsschiffe
zwar noch mit nicht hungernder Crew die Rückfahrt überleben würden, aber
keine Chance hätten, die ganze Hinfahrt
zu überleben. Das könnte immer noch gegeben
sein, wenn sie es rechtzeitig einholten und ausraubten, aber
es war dieses Mal ein zeitkritischeres Rennen. Immerhin kannte
die Kontaktperson die neu geplante Route, die von den üblichen,
bisherigen abwich.

Entsprechend fiel Kamiras zweite Therapiesitzung aus. Sindra leitete Hochbetrieb
ein, sie wendeten sofort, Kurse wurden diskutiert, Fahrt aufgenommen, die das untere
Deck innerhalb kurzer Zeit leersog. Gelangte ein gut gebautes
Schiff in eine Fahrt über Wasser, die sich Gleitfahrt nannte, so
war es nicht notwendig, Wasser von Bord zu pumpen, es wurde
durch die vom Schiff erzeugte Strömung hinausgesaugt. Nixen
nutzten diese Technik schon lange bei kleinen Jollen, wie Marah
eine hatte. Smjer war es gelungen, einen Schiffstyp zu entwickeln, der
größer war und dasselbe konnte. Nicht zuletzt, weil sie sehr leichtes,
dafür geeignetes Material verbaut hatten.

Gleitfahrt war bei diesem leichten Schiff allerdings eine Fahrtform, bei
der sie sehr wachsam sein mussten. Im Gegensatz zu anderen Schiffen dieser
Größe konnte die Schattenmuräne tatsächlich kentern. Es wäre dann nicht
unmöglich gewesen, sie wieder aufzurichten und fahrtklar zu machen, aber kein
einfaches Unterfangen.

Die Person, die sich am besten mit Wind auskannte und deshalb genau
wusste, welche Segelstellung und Krängung welches Risiko barg, zu kentern,
war Marah. Deshalb wurde sie doch geweckt.

Jentel wurde in den Mastkorb berufen, obwohl noch Tag war. As beschwerte
sich nicht. Alle waren informiert, halfen oder warteten in
Bereitschaft und hielten gefühlt den Atem an. Von der Trauerstimmung
vom Vortag war nichts mehr übrig.

---

Es hätte kaum einen besseren Zeitpunkt geben können, als das gejagte
Forschungsschiff endlich in Sicht kam.

"Wir fahren an ihnen vorbei!", kommandierte Sindra. Mehr brauchte
sie nicht zu sagen. Der alte Teil der Crew war eingespielt und
wusste, was zu tun war. Sie würden vor dem Forschungsschiff
die Fahrt verlangsamen, sodass ihr Abstand gleich bliebe, und dann
auf die Weise ausrauben, die sie gewohnt waren.

Sindra blickte sich um und sprach kurz leise mit einer Person
der alten Crew der Schattenscholle, bevor sie brüllend ergänzte: "Gebt
ihnen Dampf!"

Das war ein Kommando für Kamira und Marah. Aber Jannam, eine Nixe
der alten Crew der Schattenscholle, hielt ihn auf. "Ich hatte
die Aufgabe auf der Schattenscholle. Wenn du magst, übernehme
ich und du bereitest das Tauchboot vor." Kamira wechselte
mit Marah und Smjer einen Blick, bevor er sich einverstanden erklärte. Das
würde alles etwas entspannen.

Kamira traf Janasz und Ashnekov unter Deck, die im langsam hineinströmenden
Wasser standen und das Tauchboot hielten. Sie hatten die Klappe für die
Ausfahrt ein Stück geöffnet. Kamira fühlte sich im fließenden
Wasser sofort wohler.

"Die Schattenscholle hatte das Schiff schon einmal ausgeraubt.", informierte
Ashnekov.

"Das verwundert mich nicht.", sagte Kamira. "Sie brauchten schnell
ein Schiff. Natürlich nehmen sie dann eines, das samt Crew am
besten schon zur Verfügung steht und alles schon einmal geplant
hatte."

"Gut für uns.", sagte Janasz. Damit spielte er darauf an, dass
sie kein neues Loch sägen mussten, sondern ein altes wieder benutzen
konnten.

Kamira nickte und packte mit an. Als Jannam und Marah zurückkehrten,
stand das Deck unter Wasser und die Luke war voll geöffnet. Myrken, ebenfalls
von der alten Schollencrew, erschien im Niedergang
und gab Sindras Befehl zum Angriff weiter, sowie eine
Richtung. Das klappte alles viel zügiger als sonst mit der
Crewergänzung. Kamira lächelte, als Janasz und Ashnekov im
Bauch des kleinen Tauchboots verschwanden und Marah und er es
gemeinsam hinausschoben.

Dunkelheit umgab sie. Und Kälte. Kamira schlug die Nickhaut nicht
sofort auf, genoss die Dunkelheit noch einen Moment. Aber nicht
zu lang.

Als er den Rumpf des Schiffs ausmachen konnte, tauschte er mit Marah einen
Blick und ein Lächeln aus. Sie schwamm vor und suchte den Rumpf
nach der getarnten Rille ab, die die Crew der Schattenscholle hinterlassen
hatte. Es dauerte nicht lange. Die Beschreibung war gut und der
letzte Überfall noch nicht so lange her. Sie befestigten das Tauchboot
und zogen die Schleusen.

Und dann passierte nichts mehr, wie gewohnt. Von Innen drangen
laute Stimmen, die sie außen nicht gut verstehen konnten. Marah
und er blickten sich an. Sie fragte sich vielleicht genau
wie er, was sie nun tun sollten. Es war wahrscheinlich, dass
die Öffnung schon offen war. Sie hatten gleichzeitig den Reflex, kurz einen
Blick hinein zu werfen, und hielten wieder inne, als sie sahen, wie
die jeweils andere Person sich bewegte. "Du!", beschloss Kamira auf
Sirenu. Das Innere des Tauchboots war mit Sicherheit der
gefährlichere Ort und Kamira hätte Marah lieber geschützter gewusst, statt sie
vorzuschicken. Aber ihr Atemdrang war höher als seiner. So schlimme
Dinge auch passieren mochten, falls sie gleich überstürzt
wegmüssten, wäre besser, wenn sie noch einmal geatmet hätte.

Marah brauchte immerhin nicht lang und kam zu Kamiras
Erleichterung wenig später wieder hervor. Auch, wenn es
selbst das sich wie Ewigkeiten angefühlt hatte. "Abdocken.", sagte sie und war
schon dabei, die Dichtungen zu öffnen. Kamira
folgte der Anweisung. Innen gab es Geschrei. Am kräftigen
Wassersog erkannte Kamira, dass der Deckel nicht zuvor geschlossen
worden war. Das war nicht gut. Es bedeutete so viele Dinge
auf einmal, dass Kamira nicht gut fokussieren konnte. Er
folgte weiter Marahs überzeugten Bewegungen. Große Blasen brachen
zwischen dem sich abdockenden Tauchboot und dem Forschungsschiff
hervor. Die Wassermassen, die in den Schiffsbauch strömten, spülten
Personen, die dort gestanden hatten, mit sich von der
Öffnung ins Schiffsinnere. Die Schleuse
am Tauchboot, die Ashnekov einzusetzen versuchte, hielt große Mengen Wasser nicht
ab, in selbiges zu fluten. Janasz hielt sich darin mit
einem Arm fest, mit dem anderen hatte er Ashnekov umklammert. Das
Tauchboot sank erst langsam, aber je mehr Wasser die
Luft darin verdrängte, desto unaufhaltsamer beschleunigte es. Und
Kamira war die einzige Person, die es davon abhalten konnte. Er
kämpfte gegen den Sog des zu schweren Tauchboots
in die Tiefe an, hielt es an den Griffen, und
schaffte es schließlich, es so zu drehen, dass die Vorderseite oben
war, vielleicht noch ein winziger Rest Luft darin gespeichert
bliebe, der wegen der Bootsform und Ashnekovs Bemühungen
nicht gleich entwichen war.

Wo aber war Marah?

Kamira tauchte das Tauchboot langsam und schwer kämpfend
Stück für Stück nach oben, zurück zum Forschungsschiff, in
der Hoffnung, Marah zu sehen. Aber Marah war verschwunden. Und
das Loch im Schiff auch. Kamiras Körper durchströmte ein
heißes Gefühl, das er kaum kontrollieren konnte. Panik? War
das ein Moment, in dem sich seine Psyche entschied, nun wäre
ein sinnvoller Moment, ihm Panik zu präsentieren?

Kamira schrie auf Sirenu. Nach Marah, und um Hilfe. Nach
Marah hatte wenig Sinn. Er wusste es eigentlich: Marah
war auf der anderen Seite des Lochs. Wahrscheinlich hatte
Marah es selbst geschlossen. Kaum eine dieser Landskreaturen
wäre in der Lage gewesen, so ein Loch gegen Strömung von hineinflutenden
Wassermassen zu schließen. Ohne Marah wäre ihnen wahrscheinlich
ihr Schiff abgesoffen. Landsvolk baute Schiffe nämlich zwar
häufiger unkenterbar, aber dafür nicht unsinkbar. Löcher dieser Größe
in Schiffen waren bei jener Bauart fatal. Marah wusste das. Es
war wohl durchdacht und gut, wie sie entschieden
hatte. Es war schrecklich. Nichts war gut. Aber
das Fußvolk sollte nicht sterben. Und Marah hatte getan, was
am meisten Schaden reduzierte.

Kamira kämpfte während dieser Gedanken immer noch gegen
das Sinken des Tauchboots an. An Fortkommen war nicht zu denken. Die Luft
war aufgebraucht, so oft hatte Kamira auf Sirenu um Hilfe
geschrien. Aber auftauchen kam noch nicht infrage. Kamira schloss
die Augen und hoffte, dass Ashnekov und Janasz da drin wirklich
eine kleine Luftblase hatten, und dass schnell genug Hilfe
von der Schattenmuräne eintreffen würde.
