\BefehlVorKapitelDFDM{Jentel-n.png}{Jentel hat im vorherigen Kapitel Kanta aus dem Wasser gefischt.}{Dysphorie.}

Seereis
=======

\Beitext{Jentel}

"Seereis?", fragte Jentel und versuchte es weniger wie
eine Frage und außerdem sehr abfällig klingen zu lassen. "Ich bin
nicht amüsiert."

Eigentlich war as amüsiert und das wusste auch Janasz. "Ich
habe Kamira gefragt. Er meinte, das Getreide heißt auf
Kazdulan so." Er lächelte unsicher.

Sie unterhielten sich auf Kazdulan. Es war Deckssprache und
die einzige Sprache, die Janasz ausreichend fließend für eine
Konversation sprach. Kazdulan war die Sprache der Zwerge und
auch nicht unbedingt die unangenehmste Sprache, fand Jentel. As hatte
sich vergleichsweise wenig schwer mit dem Lernen der Sprache getan. Aber
sie war eben landzentriert. Daher wurden darin Namen für Tiere und Pflanzen
im Meer Tieren- und Pflanzennamen am Land
nachempfunden mit einem 'See-' davor. Beispiele
waren Seepferdchen, Seegras oder eben nun Seereis.

Jentel war mehrsprachig großgezogen worden, wie fast alle
Nixen. Saine Sprachen waren Siren, die Sprache der Nixen, Ilderin, die
Hauptsprache der Elben, und
Mandulin, die Sprache des Zwergen- und Elbenvolks an der
südöstlichen Küste Maerdhas, wo Jentel groß geworden
war. Wo einen halben Tagesausflug vor den Küsten die
Seereiswiesen -- dieses Wort, ehem -- in den seichteren Wassern in den
Wellen wiegten.

Es war eine der sehr wenigen Regionen, in denen Elben
und Zwerge zusammen eine Gemeinschaft aufgebaut hatten. Vielleicht
hatte der dort entwickelte Sprachzweig deshalb Ähnlichkeiten
mit Kazdulan und Jentel hatte dadurch bedingt so wenige Schwierigkeiten
mit der neuen Sprache gehabt. Mandulin klang allerdings
viel weicher in sainen Ohren.

Bei ihrem letzten Gespräch hatten sich Jentel und Janasz über
Sprache und Essgewohnheiten ausgetauscht. Janasz kochte für die
Crew. Reihum fragte er, was die Crewmitglieder am liebsten
essen würden und näherte sich jeweils eine halbe Woche
bis Woche den Essgewohnheiten der jeweiligen Person an, die an der Reihe war. Jentel
wusste das sehr zu schätzen, hatte das Angebot aber aus Gründen lange
nicht angenommen. Trotzdem fand er, hätte er mit Janasz vielleicht
freundlicher umgehen sollen statt in sainer üblichen sarkastischen
Art. Auf der anderen Seite wiederum hatte Janasz gestanden, dass
er Jentel unter anderem genau dafür mochte. Also hatte as
sich nicht um mehr Feinfühligkeit bemüht.

Es war jedenfalls durchaus amüsant, dass sich herausstellte, dass
die Kazdulan-Vokabel des Grundnahrungsmittels aus Jentels früheren Essgewohnheiten
auch ein Beispiel für die Landzentriertheit der Hauptzwergensprache
war. Wörtlich übersetzt aus Siren, wenn das überhaupt möglich
wäre, hätte es vielleicht so etwas wie Langschwammkorn geheißen.

Janasz schnaufte immer noch ein wenig vom Aufstieg. Er stieg, um
Jentel Essen zu bringen, und nun auch für
Gespräche mit ihm, zu Jentel in den Mastkorb. Ihm machten
Wetterumschwünge aber zu schaffen, und auch seine Oberweite, die
er sich versuchte, abzubinden, so gut das ging, weshalb
er nun ganz schön aus der Puste war. Smjer hatte ihm immerhin aus
Gummerlatech ein Kleidungsstück angefertigt, das besser
funktionierte und gesünder war, als was Janasz zuvor probiert
hatte.

Jentel hätte gern Brüste gehabt und jederzeit mit Janasz
getauscht. Aber bis die Medizin soweit wäre, so
einen Tausch möglich zu machen, müssten wahrscheinlich
noch mindestens 100 Jahre übers Meer streichen -- die
Redewendung hatte Jentel von 'ins Land ziehen' aus
Kazdulan übertragen --, wenn nicht 427 oder noch länger. Jentel
mochte es, willkürliche Konkretheit bei Schätzungen anzubringen.

Jentel wartete jedenfalls geduldig ab, bis Janasz weniger schwummrig
war. Es war sicher kein so gutes Gefühl, schwummrig in den
Finkennetzen zu hängen. Das Wetter war drückend. Der Himmel
war noch klar, aber irgendwo am Horizont sammelten sich einige
Wolken mit Gelbstich, von so ähnlichen Kamira mal erklärt hatte,
dass sie viel Regen und Wind bringen würden. Besagtes Geschnaufe
Janaszs deutete ebenfalls auf den Wetterumschung hin, und
auch, dass Sindra die Reffs nicht aus den Segeln nehmen ließ. Wenn
Segeltuch nicht vollständig genutzt, sondern am Mast
oder Baum zurückgebunden wurde, sodass nur ein Teil der
Segelfläche gefüllt war, dann nannte sich der eingerollte
Teil Reff. Speziell dafür gab es in den Segeln der Schattenmuräne
je zwei Reihen Ösen, für das erste und
für das zweite Reff. Gerade hatten sie nur
das erste Reff drin, aber für die derzeitige Windstärke
hätten sie eigentlich keines benötigt.

Jentel kannte sich eigentlich nicht so gut mit Wetterdingen aus, und
das als Mastkorb-Nixe. As fühlte den sich ändernden
Luftdruck nicht stark genug und Wolken waren zu
uneindeutig. Es waren lediglich Metadaten, aus denen Jentel
ableitete, wie das Wetter werden würde -- was andere, die
es besser fühlten oder wussten, entschieden oder wie
sie reagierten.

Vielleicht sollte Jentel mehr auf
Janasz zugehen, statt ihm diese Kletterei
für as zuzumuten. Der Gedanke war neu. Jentel war immer hier
oben, es sei denn, es war Schlafenszeit. Dann hielt as sich
im Nixendeck auf und jenes war für Janasz Tabu. Ein
Schutzraum für Nixen, in denen andere eigentlich nichts zu
suchen hatten. Schlimm genug, dass die Kapitänin dauernd
hereinschneite. Ein-, zweimal in der Woche, um Marah
abzuholen, aber Jentel fand das eigentlich nicht so gut.
As hatte Bedenken, dass das einen Standard setzen würde und
dem Beispiel der Kapitänin irgendwann andere folgen würden.

Jentel hatte kaum Kontakt zum Fußvolk-Anteil der Crew. Außer eben
zu Janasz. Der ihm zum Einbruch der Nacht und kurz vor
Morgendämmerung jeden einzelnen Tag Frühstück und Abendbrot
in den Mastkorb gebracht hatte, über nunmehr drei oder
vier Jahre, ohne sich ein einziges Mal zu beschweren. Jentel
hatte darum nicht gebeten, sich also auch nicht für
irgendwelche Dankesgesten verantwortlich gefühlt.

Wieso sie ausgerechnet dann endlich vor zwei Tagen das
erste Mal ins Gespräch gekommen waren, -- also ein
längeres als Janaszs Fragen, ob Jentel einen Essenswunsch für die Zukunft
hätte, Jentels traditionelle Ablehnung, und anschließend
Janaszs Wunsch, es möge schmecken --, war Jentel schleierhaft, aber
es war schön gewesen. Der Zwerg hatte keine Erwartungshaltung
an Jentel. Als Jentel von den Ufern sainer Heimat erzählt
hatte, hatte Janasz nichts gefragt, was den Fokus auf
Jentels Fischschwanz oder Tauchen oder sonst etwas gelenkt
hätte, was Fußvolk an Nixen normalerweise so interessierte. Jentel
hatte ihm sogar sainen Vortrag über den Vergleich zwischen
Siren und Landsprachen gehalten, wie sich Landzentriertheit
und Seezentriertheit in den Sprachen äußerte, die as kannte.

Kanta hatte mehrfach versucht, as über Siren auszufragen. Das
war auch ein Grund, warum Jentel sich im Moment noch weniger gern an Deck
aufhielt. As ging Kanta lieber aus dem Weg. Sie glaubte, dass
sie eine besondere Bindung hätten, weil Jentel sie gerettet
hätte. Den Fakt, dass Jentel auch ihr Boot gekentert hatte, was
besagte Rettung erforderlich gemacht hatte, ließ sie dabei außen vor. Und
selbst diese Zusammenfassung wäre eine Verdrehung der
Tatsachen gewesen. As verstand Beziehungs- und Bindungsgedöns
nicht. Die Regeln dafür erschlossen sich ihm nicht.

Es war nicht so, dass as keine Freundschaften gehabt hätte. As
verstand sich vor allem mit Marah sehr gut. Marah kam
oft zu ihm nachts in den Mastkorb, um mit ihm zu singen. Das
einzig Nervige an der Tradition war, dass as deshalb gelegentlich angesprochen
wurde, das sie schön sängen. As konnte mit Komplimenten nicht
so viel anfangen. As verstand die Intentionen nicht. Marah
hatte ihm erklärt, dass Leute sich normalerweise über Komplimente
freuten und das Ansinnen entsprechend wäre, ihm eine Freude
zu machen. Aber Jentel konnte das nicht nachempfinden. As
fühlte nichts als mindestens leichtes Unbehagen, wenn sain Gesang bewertet
wurde. Was, wenn as grausam schlecht gesungen hätte? Hätte
as dann keine positive Zuwendung verdient? Wenn das Ansinnen
von Leuten wäre, dass as sich freute, sollte es nicht davon
unabhängig sein, wer as war oder was as gut konnte?

Wegen solcher Fragen empfanden viele as als eher anstrengend. Marah
war da keine Ausnahme, aber sie mochte es, auf diese Art
angestrengt und gefordert zu werden. Sie mochte es, die ganze
Welt noch einmal reflektieren zu müssen, wenn sie ihm erklärte, warum
wer wie handelte. Ihr eines Elter war ein Lobbud
gewesen. Lobbuds waren das Volk mit den besonders großen Füßen, das
noch kleiner als Zwerge war. Auch Marahs anderes Elter war nicht
besonders groß gewesen, was sich bei Marah in besonders
kleiner Körpergröße und weichen Locken niederschlug. Sie waren
vielleicht eigentlich hellbraun oder blond, aber Marah färbte
sie regelmäßig grün an. Es stand ihr gut, fand Jentel.

"Jedenfalls habe ich einen Sack Seereis bestellt.", sagte
Janasz schließlich.

"Einen ganzen Sack?", fragte Jentel skeptisch.

Janasz zeigte mit den Händen die Größe von vielleicht drei
Litern.

"Du wirst uns damit vier Wochen ernähren." Jentel
schmunzelte.

"Vier Wochen?", fragte Janasz überrascht.

Er kannte sich so überhaupt nicht aus. "Ich nehme doch an,
der Seereis" -- Jentel überbetonte das Wort -- "wird
nass geliefert." Dann zögerte as. "Äh, was meinst du
mit bestellt? Kannst du süd-ost-maerdhische Lebensmittel
bestellen?"

Janasz grinste. "Die Lieferung ist heute eingetroffen. Und ja,
nass." Janasz runzelte die Stirn.

"Weiß die Kapitänin davon?", fragte Jentel.

"Natürlich. Ich habe das bei ihr genehmigt.", versicherte
Janasz.

Das war beruhigend. Aber vermutlich auch irgendwie
einleuchtend oder so. Die Kapitänin war nicht immer im Detail
darüber informiert, welche Lebensmittel an Bord waren. Sie
stahlen einfach, was die Forschungsschiffe so an Bord hatten. Da
sie eher südwestlich Maerdhas unterwegs waren, waren
Lebensmittel wie Seereis aber nie dabei. Das war der Hauptgrund, weshalb
Jentel einfach keine Lust gehabt hatte, mit Janasz über saine
Essgewohnheiten zu reden. Es war ihm nicht zielführend für irgendeine
Seite vorgekommen. Wenn nun Seereis an Bord wäre, würde Marah
als Person, die für den Transport mittelkleiner Güter quer durch den Ozean verantwortlich
war, diesen geliefert haben, und was Marah wusste, wusste auch
die Kapitänin. Wenn also ein Genehmigungsvorgang für so etwas
vorgesehen war, dann hatte er in diesem Fall auch nicht unterschlagen
werden können.

Jentel fragte sich, ob die Gedanken über Genehmigungen überhaupt Sinn ergaben. Vielleicht
würden alle Crewmitglieder der Kapitänin auch einfach immer all
solche Sachen erzählen. Jentel hatte solche Konflikte, Dinge, die
Genehmigung bedurften, normalerweise nicht und war da ganz froh drum.

"Hältst du den Sack nass?", erkundigte sich Jentel.

"Irgendwie hatte ich mir gedacht, dass das besser
ist.", antwortete Janasz. "Er wurde von der Seescholle verschickt. Jenes
Schiff unserer Flotte raubt süd-ost-maerdhische Schiffe
aus. Er war nicht trocken
verpackt. Ich dachte, das würde schon seinen Sinn haben und
habe ihn erstmal in Wasser gelegt. War das richtig?"

Jentel stellte sich vor, wie Marah den Sack transportiert
haben könnte. Vielleicht in Schlepp? Oder in einem mit
Wasser gefüllten Seesack?

Für kleinere Postsendungen zwischen den Schiffen und
mit ihren wenigen Kontaktpersonen an Land halfen ihnen
Briefwelse. Faszinierende Salzwasserwelse, verwandt mit den Katzenwelsen, die
sich sehr gut orientieren konnten, und den Deal Futter, Zuwendung und
Medizin gegen Briefverkehr wohl recht gut verstanden. Größere
Lieferungen transportierte Marah von und an Bord, mit einem
kleinen, sehr schnellen Segelboot, typischer grenlänndischer
Nixenbaustil, dass sich im Zweifel rasch und gut
tarnen ließ.

Marah war eine der mutigsten Personen, die Jentel kannte. Und
damit meinte as nicht, dass sie wenig Angst vor Gefahr
gehabt hätte, sondern dass sie viele Dinge trotzdem tat, wenn
sie eben dran waren. Wie, allein durch feindliche Gewässer
zu segeln.

Marah hatte Jentel nichts verraten. Vielleicht hatte sie geplant, as
zu überraschen, oder hatte Janasz seine Freude nicht
daran nehmen wollen, Jentel zu überraschen.

Es war darum gegangen, ob Seereis nass gelagert werden musste, erinnerte
sich Jentel mühsam an das laufende Gespräch.

"Jap." Jentel bewunderte Janasz schon ein wenig dafür, mitgedacht
zu haben. Das erlebte as nicht so oft. "An der Luft quillt
das Getreide."

"Quillt?", fragte Janasz. "An der Luft?"

Jentel rollte die Augen. "Wie eine Hummel die trocknet." Ein
besseres Beispiel war ihm nicht eingefallen. "Nur, dass
die Körner eine weniger haarige Struktur haben. Aber
an der Luft fluffen sie halt aus."

Janasz grinste. "Du erfindest Wörter in der Fremdsprache."

"Ist fluffen gut?", fragte Jentel skeptisch.

"Ausfluffen ist ein schönes, erfundenes Wort. Ja.", bestätigte
Janasz.

Reden mit ihm war ganz leicht. Eines ihrer ersten Gespräche
hatten sie über Dysphorie geführt. Über Körper, die sich nicht
richtig anfühlten, wie sie waren. Eben, wegen fehlender oder
zu viel Brust. Aber Jentel erinnerte sich nicht mehr, wie sie
darauf gekommen waren.

"Findest du mich anstrengend?", fragte as.

Janasz schüttelte den Kopf. "Sollte ich?"

"Mich finden Leute meistens anstrengend. Aber ich merke es
ihnen dann nicht an.", sagte Jentel. "Ich kann daher nur durch
Nachfragen wissen, ob es bei dir auch so ist."

"Eigentlich finde ich es entspannt, mit dir zu reden.", sagte
Janasz. "Du lässt mir Zeit zum Nachdenken und du sagst nur Dinge, über
die du nachgedacht hast."

"Dass Zeit lassen entspannt, verstehe ich.", sagte Jentel. "Aber
warum entspannt es, dass ich nur Dinge sage, über die ich nachgedacht
habe?" Was allerdings stimmte. Jentel dachte viel nach, bevor
as etwas sagte. Wenn Gelegenheit da war. Sonst war as sarkastisch.
Oder so.

"Du sagst nicht erst das eine und dann doch das andere und kurz darauf
doch wieder das eine.", erklärte Janasz. "Ich komme bei anderen
nicht so leicht hinterher, welcher Meinung sie gerade sind."

"Das ergibt Sinn.", murmelte Jentel.

Dann schwiegen sie wieder. Das Essen, das Janasz mitgebracht hatte,
stand hinter ihnen. Jentel hatte bisher immer allein gegessen. Aber
in den vergangenen zwei Tagen war es
jeweils deshalb vorm Essen kalt geworden, weil sie sich
unterhalten hatten.

Jentel seufzte und drehte den Mastkorb im Kreis, wie as das alle
naselang tat, um sich umzublicken. Es war saine Aufgabe, Ausschau
zu halten, weil as sehr gute Augen hatte und selbst für eine
Nixe gute Nachtsicht. Jentel tat es sehr gewissenhaft. Und das
war schließlich wohl der ausschlaggebende Grund, warum as sich
nur hier oben besuchen ließ, aber sich nie lange an Deck aufhielt.

"Stört es dich, wenn ich esse?", fragte Jentel.

"Überhaupt nicht.", antwortete Janasz. "Isst du ungern als einzige
Person von mehreren? Das kenne ich von anderen. Soll ich beim nächsten Mal
meine Portion mitnehmen und mit dir essen?"

"Ja.", sagte Jentel. "Und ja."
