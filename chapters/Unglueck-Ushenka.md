\BefehlVorKapitelDFDM{Ushenka-n.png}{Ushenka hat Marah großgezogen und ist Informantin der Flotte der Maare in der Minzter.}{Mord, Blut, etwas Gore, Schock, Trauer, Kinderleid, Transfeindlichkeit, Sexismus, Gatekeeping.}

Unglück
=======

\Beitext{Ushenka}

Ushenka schlotterte und konnte nicht so genau ausmachen, aus
welchem Grund. Da war der offensichtliche: Es war kalt, stürmte
und schüttete. Sie war dem Wetter schutzlos ausgeliefert gewesen, als
sie sich zur Bucht aufgemacht hatte, in der sie damals Marah gefunden
hatte. Immerhin trug sie ihre Tracht des Hafenmeisters, die wenigstens
etwas wasserbeständiger war, als die Kleider, die sie für andere ihrer
Rollen trug, als sie realisiert hatte, dass sie wegmusste.

Sie hatte sich zwischendurch ausgezogen, ihre Kleidung ausgewrungen
und versucht, sie unter der Überdachung der kleinen Höhle zu
trocknen, aber sie hatte sie klamm wieder anziehen müssen. Nun
saß sie hier seit zwei Tagen fest. 

Sie schlotterte auch, weil sie trauerte und unter Schock stand. Nichts
ahnend war sie vom Hafen nach Hause gegangen. Nun, nicht nichts. Dass
Nachrichten von der Schattenscholle und der Schattenmuräne ausgeblieben waren, war
sicher ein Vorbote für irgendetwas gewesen. Aber damit, ihren Mann
ermordet auf der Straße unter ihrer Wohnung vorzufinden, hatte sie nicht
gerechnet. Das Bild schmerzte und ließ Blut durch ihr Gehirn rauschen, wenn
sie nur daran dachte. Viel zu viel Blut. In ihrem Gehirn, und auf
der Straße und nicht mehr in ihm. Und dann war da ihr Flederkind
aufgetaucht. "Du musst hier weg.", hatte es ihr zugeflüstert, ohne
sie anzusehen, als hätte es nicht mit ihr reden wollen. "Sie sind
auch hinter dir her."

"In Ordnung. Komm mit, ich passe auf dich auf.", hatte sie bloß
geantwortet. Aber das Kind war nicht mitgekommen. Es hatte erst
so getan, aber war dann -- für Ushenka ersichtlich, die durch schlechtes
Schauspiel brauchbar hindurchsehen konnte -- absichtlich in der Menge der Schaulustigen verschwunden,
die die Leiche ihres Mannes begafften. Und heulten. Bei Ushenka war das
Heulen erst später gekommen.

Ohne das Anliegen des Kindes wäre sie wahrscheinlich nicht hier. Sie
hatte in der Situation den Schutz des Kindes über irgendeinen Drang
gestellt, in der Wohnung nachzuschauen, was los gewesen wäre, der
Information des Kindes zum Trotz, dass jemand hinter ihr her wäre. Aber
sie hatte nicht gewollt, dass dem Kind etwas zustieße. Also hatte sie
sich entfernt, und als sie schon mal den Entschluss gefasst hatte, hatte
sie ihn beibehalten, obwohl das Kind so zügig verschwunden war.

Sie hatte es bei sich aufgenommen. Es hatte nie verraten, wie es
hieß, also hatten sie sich auf Flederkind geeinigt. Wenn ihr Mann mit seinen Studien an der
Universität beschäftigt gewesen war, hatte sie mit dem Kind gekocht, Geschichten
erzählt, das wissbegierige Kind unterrichtet und sie hatten
gekuschelt. Es hätte so schön sein können. Und nun zerriss die Sorge
und Angst um das Kind sie so sehr, dass sie nicht mehr atmen konnte, wenn
sie daran dachte, wie die kleinen, nackten Füße durch das
Blut ihres Mannes gepatscht waren und kleine rote Abdrücke hinterlassen
hatten. Für diejenigen, die diesen Mord verübt hatten, wäre es vielleicht
ein Leichtes gewesen, dem Kind zu folgen. Oder auch nicht. Sie
wusste es nicht.

Nun, nach zwei Tagen des Wartens, machte sich Ushenka allmählich auch Gedanken
um Marah. Vorher hatte bereits eine untergründige Unruhe in ihr gebohrt, wie eigentlich immer, aber
nun sorgte sie sich bewusst. Gesetzt, die Briefe erreichten die Schattenmuräne noch, hätte
sie allmählich hier sein können. Dass die Briefe die Schattenmuräne erreichten, hatte
Ushenka zu hoffen gewagt, weil die Briefwelse mit gelöschtem Papier zurückgekehrt
waren. Es gab also noch Personen, die die Briefe entnehmen konnten, welche, die
wussten, wie die Tinte gelöscht werden konnte oder Zugang zu recht viel dieser
Art Papier hatten. Es
war auch sonst schon mal vorgekommen, dass es einfach nichts zu berichten
gegeben hatte, zumindest nichts, was Ushenka nicht ohnehin mitbekommen hätte, weil sie von
zurückkehrenden Forschungsschiffen und den erfolgreich sabotierten
Plänen eben auch so erfuhr. Aber keine Nachrichten von der Schattenmuräne und Schattenscholle
zugleich, das war selten. Wobei der Briefwechsel mit der Schattenscholle
langsamer verlief. Briefe von dort brauchten natürlich länger. Und die Person,
die hauptverantwortlich für Informationen bezüglich Forschung im süd-ost-maerdhischen
Meer war, saß im Zarenreich.

Ushenka war dennoch eine wesentliche Informationsquelle, die jetzt wegfiel. Das
war alles nicht gut. Trotzdem machte sie sich darüber weniger Gedanken als
über ihren Mann, oder ihr Flederkind, oder Marah.

Irgendwo untergründig spürte sie allmählich auch eine gewisse Angst
um sich selbst. Vielleicht sollte sie doch zurück zum Hafen gehen, in
der Hoffnung, dort trockene Kleidung zu finden, ohne umgebracht zu
werden. Aber was, wenn in der Zeit Marah endlich käme? Und wie sollte
sie heimlich trockene Kleidung aus Minzter heraustragen, wenn nicht am Körper, wo
sie bei dem Dauerregen sofort wieder durchweichen würden?

Ushenkas Gedanken gingen in ein Wirrsal über, das genauso zu schlottern
versuchte wie ihr Körper. Ihr Geist und Körper holten sich den Schlaf, den
sie über die ganze Zeit zu vermeiden versucht hatte, damit sie
nicht aus Versehen erfröre. Oder damit nicht, während sie schliefe, doch noch irgendetwas
Schlimmes passierte.

---

Sie wachte vom Knistern eines Feuers auf, das versuchte, warm zu sein, aber
dafür eigentlich viel zu klein war. Sie war trotzdem dankbar darum. Sie
richtete sich mühsam auf und blickte in Marahs ernstes Gesicht.

"Zieh dich aus.", sagte sie. "Wir räuchern jetzt dich und deine
Kleidung."

Ushenka warf einen Blick ins Feuer. "Dein Ersatzpaddel wird nicht
lange brennen."

"Das nicht, aber nun bist du wach und ich kann Nachschub holen.", erwiderte
Marah. "Irgendeine Gegend, die ich meiden sollte, außer Minzter?" Dass
sie Minzter meiden sollte, hatte Ushenka im Brief geschrieben.

Ushenka schüttelte den Kopf. "Bleib noch ein bisschen.", bat sie. "Bitte?"

Marah blickte sie einige Momente an. "Gut, dich zu sehen.", sagte
sie.

Ushenka blieben die Worte im Hals stecken, die sie hatte erwidern
wollen, als ihr Kind sich lang streckte und ihren Arm in beide
seiner Arme nahm. Sie wollte Marah zurückumarmen, aber die Nixe hatte sich schon
wieder weggebeugt.

"Ist das genug bisschen?", fragte sie.

Ushenka blickte sich um und entdeckte einige Meter neben dem Feuer
Marahs Bordlaterne. Marah war nur mäßig ausgerüstet gewesen, ein
Feuer zu machen, schloss Ushenka, und hatte mit dem Zündmechanismus
der Lampe experimentiert, wahrscheinlich große Teile des
Brennmaterials verwendet, um das schwer brennbare Paddel anzuzünden. Es
war nicht klar, ob Marah es noch einmal hinbekommen würde, wenn
das Paddel ausgebrannt hatte. Es roch nicht gerade nach Material, das
zum Verbrennen gedacht war.

Ushenka nickte. "Ich ziehe mich aus.", sagte sie. "Ich würde
so unsinnige Dinge sagen, wie: Beeil dich! Oder: Pass auf
dich auf! Aber das würde dich nerven, oder?"

Marah grinste. "Ich beeile mich und passe auf mich auf." Und
weg war sie. Bewegte sich geschickt robbend den kleinen Hang hinunter, und
mit der Verschmelzung ihres kleinen Körpers mit dem Wasser
war sie nicht mehr zu sehen.

---

Einige Stunden später lag Ushenka im Rumpf der kleinen Jolle. Es
war gut gewesen, dass ihr Körper einmal durchgewärmt worden
war. Marah hatte darauf geachtet, abzuwarten, obwohl sie es
beide eilig hatten, bis Ushenka kein Schütteln mehr
durchfuhr, oder zumindest Grund dafür nicht mehr die Kälte
war. Dann hatte sie Ushenka in die kippelige Jolle gelotst
und es irgendwie geschafft, beim Ablegen nicht zu kentern, indem
sie, noch bevor sie sich selbst ins Boot schwang, die Segel
dichtgeholt und mit Wind gefüllt hatte.

Ushenkas letzte Reisen auf dem Meer waren gemütlicher
gewesen, aber darum ging es nicht. Sie lag auf engem Raum, der
für ihren großen, alles andere als schmalen Körper nicht gemacht
war, hatte quasi keine Bewegungsfreiheit. Trotzdem musste sie sich regelmäßig
umdrehen, weil ihr ihre Gliedmaßen einschliefen, und musste für
jedes Mal Marah Bescheid geben, damit diese mit ihrem Gewicht Ushenkas
Bewegungen ausgleichen konnte. Aber besser nur halb erfroren im Rumpf einer
zu kleinen Jolle, als irgendwo in Minzter tot.

Ziel war die Schattenmuräne. Marah
hatte erzählt, dass sie dabei war, eine weitere Crew an Bord zu
nehmen, und sie ihnen anschließend wieder entgegenkommen würde. Der
Kurs der Schattenmuräne führte dazu, dass sie eher zwei als nur einen Tag
unterwegs sein würden. Obwohl die Jolle über das Wasser fegte, als
wäre sie der Wind selbst. Ushenka versuchte zu schlafen, aber
Marah weckte sie in regelmäßigen Abständen auf. Das war
abgesprochen. Es war nicht zu vermeiden, dass Wasser an Bord
kam. Es brauchte nicht lange, da war die Wirkung des Feuers
aufgebraucht. Oder brauchte es lange? Ushenka hatte kein
Zeitgefühl. Marah wollte sichergehen, ob sie noch lebte und nicht
heimlich erfror.

Obwohl sie kaum miteinander kommunizierten, merkte Ushenka, dass Marah
zunehmend gereizt war. Vielleicht redeten sie auch genau deshalb
so wenig miteinander. Sie konnte es verstehen. Marah hatte vermutlich
etwa drei oder vier Tagen fast keinen Schlaf gehabt. Auf dem Weg
zu ihr hatte sie wohl noch kurz geschlafen, aber mit ihr an Bord ging das
nicht. Ushenka wünschte sich so sehr, etwas daran ändern zu können. Es
sollte nicht ihr Kind sein, das sie rettete, sie hätte in der
Position des Rettens sein sollen.

Als endlich die Schattenmuräne in Sicht kam, wussten wohl auch
die anderen an Bord, was los sein musste. Ushenka hatte den Anblick
von Nixen, die sich bewegten, als wären sie zu Hause, vermisst: Sie
erblickte, wie eine mit dieser wunderschönen Körperspannung von Bord
sprang, unter Wasser verschwand, ohne erheblich zu spritzen, und
nur wenige Momente später ein Kopf neben ihnen auftauchte. Marah
fierte die Segel, sodass die Nixe mithalten konnte.

"Wenn du magst, halte ich das Boot, während du nur noch das
Segel einholst, und ich kümmere mich um den Rest.", sagte sie.

Marah blickte sich kurz um, nahm aber eigentlich nichts so genau
in Augenschein. Sie nickte. "Vorn am Bug lässt es sich am besten
am Kentern hindern."

Obwohl Kentern nicht unbedingt ein gefährliches Manöver bei
einer Jolle war, graute es Ushenka davor schon beim darüber Nachdenken. Ihr
war so kalt, dass sie das Gefühl hatte, ein unfreiwilliges
Bad im Ozean würde ihre Restenergie vollends entziehen.

Die andere Nixe folgte Marahs Anweisungen. Es kippelte wieder
sehr, als Marah das Segel herabließ. Es landete zunächst auf Ushenkas
Körper. Marah rollte es noch ein und platzierte es anschließend neben
Ushenka. "Es tut mir leid, dass ich nicht bei dir bleiben kann.", sagte
sie. Dann platzte es erschöpft aus ihr heraus: "Ich kann nicht mehr."

"Erhol dich gut. Wir sehen uns noch lang genug in nächster
Zeit, hoffe ich.", beruhigte Ushenka sie.

Marah wirkte unendlich müde. Sie nickte und ließ sich
rücklings ins Wasser fallen. Der letzte Ausdruck in ihrem Gesicht war
einer, der endlich etwas entspannter wirkte.

"Ich bin Kamira.", informierte sie die andere Nixe. "Pronomen
er/sein/ihm/ihn, aber sehr wichtig ist mir das auch nicht. Wichtiger:
Dass du mich niemals deswegen als männlich bezeichnest."

Ushenka nickte. Das Vorstellungsritual kannte sie
noch. "Ushenka. Sie.", sagte sie. Und
dann konnte sie nicht lassen, ihre Lieblingsbezeichnung
hinzuzufügen: "Hafenmeisterin."

Kamira lächelte. "Ich bin zuständig für gesundheitliche
Vorfälle aller Art an Bord, also auch die psychologischer Natur, und
helfe bei Konflikten.", sagte er. "Mit Jollen kenne ich mich
allerdings eigentlich nicht aus. Aber das schaffen wir schon. Sie
muss ja nur an Bord. Und irgendwann kleingebastelt werden, aber
das hat Zeit." Er tauchte unter der Jolle hindurch, versuchte, sie bei
dem Manöver stabil zu halten, was einigermaßen funktionierte, und
schob sie dann am Heck über die letzte Distanz Richtung
Schattenmuräne. "Wir werden sie gleich vorn und hinten an Seilen
hochziehen. Deine Aufgabe wird sein, sie von der Bordwand abzuhalten, damit die
Jolle nicht zu sehr gegen diese schlägt."

---

Ushenka atmete auf, als sie endlich wieder an Bord war. Nach einem längeren
Bad und nun wieder in einigen Lagen trockener
Kleidung war ihr angenehm warm. Auch eine Lage Nixenkleidung
war dabei. Kamira hatte etwa ihre Körperfülle. Die meisten
an Bord waren schmaler gebaut oder viel kleiner. Die Kleidung der Kapitänin
war dagegen zu lang, aber das störte sie für die dritte Schicht gerade weniger.

Ushenka kannte die
meisten Personen an Bord nicht, nur aus Briefen. Einige erkannte sie von
Marahs Beschreibungen. Es herrschte gedrückte Stimmung. Marah hatte es ja schon erzählt, dass
vier Crewmitglieder der Flotte einen Anschlag nicht überlebt
hatten, oder zumindest wahrscheinlich nicht. Über drei Personen
wussten sie es sicher, aber die Hoffnungen, dass die vierte
noch lebte, waren gering.

An Deck hielten sich zurzeit nur wenige Personen auf. Ein Pärchen
stand an der Reling und hatte sich in den Armen. Ushenka mochte
nicht stören. Aber überraschender Weise entdeckte sie Rash dabei, wie
Rash eine Ziege fütterte. Eine Ziege!

Rash hatte ganz am Anfang des Projekts
angeheuert und war über Minzter an Bord gekommen. Dazu hatte Rash ein
paar Nächte bei Ushenka übernachtet. Ushenka trat auf Rash zu. "Erinnerst
du dich noch?"

Rash nickte und richtete sich auf. "Hattest du schon eine Umarmung?"

"Nein." Ushenka schüttelte den Kopf. "Ich kann nicht leugnen, eine
gebrauchen zu können, aber du magst das eigentlich nicht so, oder?"

"Ist schon in Ordnung.", sagte Rash. "Manchmal schon. Komm her!"

Ushenka ließ sich umarmen. Sie hätte damit gerechnet, dass ihr wieder
Tränen kommen würden. Aber das passierte nicht. Stattdessen fühlte
sie Wut, die sie versuchte, wegzuschieben. "Danke."

---

Später setzten sie sich auf eine Bordkiste für Seile und unterhielten
sich stockend. Es fiel Ushenka schwer, eine gemeinsame Basis
zu finden. Sie konnte Rash nicht so recht greifen. "Was hat sich
geändert, dass du nun Umarmungen magst?", fragte sie.

"Es hat sich nicht grundsätzlich etwas geändert.", antwortete Rash matt.

Nichts, was Ushenkas Frage befriedigend beantwortet hätte. Und Rash
sprach auch nicht von sich aus weiter. Es war immer Ushenka, die das
Gespräch am Laufen hielt.

Vielleicht war es Rashs Art mit der Trauer umzugehen, so einsiblig zu
sein.

"Du umarmst also nur, weil dir kalt ist! Das ist es!", scherzte Ushenka.

Das trieb Rash ein Lächeln aufs Gesicht und sogar um die
Augen ein paar Lachfältchen. Es hielt nur einen
kurzen Moment. Und dazu hatte Rash Ushenka nicht einmal angesehen.

Ushenka wollte es noch einmal auslösen. Sie wünschte sich, dass
es vielleicht eine Weile in Rashs Gesicht bleiben möge. Sie sagte: "Da
ist man als Frau, oder wenn man irgendwie in eine
ähnliche Kategorie fällt, benachteiligt." Sie bereute ihre
Worte sofort, nicht, weil es grundsätzlich falsch wäre, sondern
weil es ein Thema war, mit dem
sie sich zu wenig auskannte. "Entschuldige, ich wollte dich auf
keinen Fall als Frau bezeichnen. Es ging mir nur um die Hormone."

Rash seufzte. "Schon gut. Ich verstehe, was du meinst."

"Wir sind ja eben doch benachteiligter als Männer.", fügte
Ushenka erklärend hinzu. Sie hoffte, dass es sich für Rash gut
anfühlen würde, wie sie das sagte, weil sie damit Rashs Geschlecht
abseits von Männlichkeit anerkannte. Zu oft wurde nur in den
Kategorien Männern und Frauen gedacht und Personen wie Rash
bei Geschlechterungerechtigkeiten nicht berücksichtigt. Aber Ushenka
fühlte sich wie auf brüchigem Eis.

Rash reagierte überhaupt nicht. Eine ganze Weile nicht. Nicht einmal die
Mimik regte sich, als wäre Rash einfach alles egal.

Zu allem Überdruss fing Ushenka passend zum Thema mit der kalten
Böe, die über das Deck wehte, wieder an zu frösteln. "Nimmst
du eigentlich noch Hormone? Ist die Frage zu persönlich?", erkundigte sie sich.

"Es gibt einen Engpass für eine Zutat, die Kamira für die Salbe
braucht." Rashs Stimme war leise geworden. "Ich
möchte nicht über mich reden."

Ushenka nickte. "Natürlich nicht. Es tut mir leid." Sie hatte keinen
guten Gesprächsbogen eingeschlagen. Der Drang, Rash zum Lächeln zu
bringen, wuchs in ihr nur umso mehr, jetzt wo sie Rash selbst an
Unangenehmes erinnert hatte. "Ich würde dir ja anbieten, dich
in den Arm zu nehmen. Wäre das okay?"

"Ich möchte nicht." Rashs Blick war in die Ferne gerichtet, aufs Meer. Das
Resttageslicht glitzerte auf den Wellen und in Rashs dunklen Augen. Die
Ziege, die Rash bis gerade gekrault hatte, gab ein leises Geräusch
von sich, wie um sich zu verabschieden, und wechselte den Ort.

Ushenka unterdrückte ein Seufzen. Ihr Blick fiel wieder auf die beiden Personen, die
immer noch an der Reling standen und sich im Arm hielten. "Ashnekov und
Janasz?", fragte sie.

Rash nickte.

Ushenka schmunzelte plötzlich, als ihr ein Gedanke kam. Ein Scherz hatte Rash vorhin
zum Lächeln gebracht, also versuchte sie es nochmal: "Sind
das nicht Männer? Bringen Männer an Bord nicht Unglück?"

Rash blickte sie an, in einer Art und Weise, die sie nicht interpretieren
konnte, aber sie hatte nun plötzlich Rashs volle Aufmerksamkeit. "Nicht
sexistischen Scheiß einfach umdrehen, ja? Bist du dir
überhaupt darüber im Klaren, über was für Männer du da sprichst?"

"Einem Mann, den viele nicht für einen halten, dem was anderes zugewiesen
worden ist?", fragte Ushenka. Das war dann wohl keine gute Idee gewesen. Sie
hatte sich überlegt, dass es vielleicht besser wäre, Janasz zu behandeln, wie
sie alle anderen Männer auch behandelte. Sie hatte Angst, dass ihre Reaktion irgendwie
schmerzhaft war. Und auf der anderen Seite hatte sie umgekehrten Sexismus
bisher eigentlich immer recht witzig gefunden, um mit der sexistischen
Gewalt klarzukommen, die sie alltäglich erfuhr.

"Und?", bohrte Rash nach.

Das irritierte sie umso mehr. "Einem zierlichen Mann?", versuchte
sie es. "Keine Ahnung, was willst du andeuten?"

"Einem zierlichen Mann, der Kind eines Elben und eines Zwergs ist, und
der auf einem Auge blind ist.", informierte sie Rash. Da
war nun eindeutig Wut in der Stimme. "Du sprichst über zwei Männer, als täte
es ihnen gut, mal zu sehen, wie es ist, nicht privilegiert zu sein, die
aber von vornherein nie privilegiert waren. Auch bezüglich Sexismus
nicht. Und das tut weh. Scher dich woandershin."

Ushenka rührte sich einige Momente nicht vom Fleck. Aber Rash wirkte nicht, als
würde Rash irgendetwas zurücknehmen wollen. Ushenka fielen ein Haufen
Entschuldigungen ein, die sie aber alle, noch bevor sie ihren Weg über
ihre Lippen fanden, wieder verwarf. Dass männliches Privileg ja immer
noch da wäre, wenn anderes Privileg fehlte. Oder dass sie nur hatte
einen Scherz machen wollen. Aber alles wirkte nicht richtig. Wahrscheinlich
war es das auch einfach nicht.

Ushenka stand auf und nickte, wandte sich zum Gehen.

"Hey!", rief Rash ihr nach.

Ushenka drehte sich noch einmal um.

"Es war echt scheiße, was du da gesagt hast.", informierte Rash sie
noch einmal. "Aber wenn du nicht selber drauf kommst und irgendwann
ernsthaft wissen willst, was so schlimm daran war, dann darfst du mich
in den nächsten Tagen darauf ansprechen."

Ushenka wusste, was das für ein Angebot war. "Danke!", sagte
sie. Es war ein Angebot, das Rash vermutlich viel Kraft kosten würde. Sie
würde vielleicht darauf zurückkommen, aber vielleicht auch besser
zunächst mit Marah reden und solange Geschlechterthemen außen vor lassen.
