SHORT_TITLE = DFDM
CHAPTERS := \
	Vorwort\
	DieSchattenmuraene-Smjer\
	EineMaerUndGeisterschiffe-Kanta\
	Seereis-Jentel\
	Hafenmeisterin-Ushenka\
	DieFlotteDerMaare-Marah\
	Mist-Aga\
	Jammeruebungen-Janasz\
	Lesehinweis-Tauchen-Kamira\
	Tauchen-Kamira\
	Unheimlichkeiten-Sindra\
	Lesehinweis-Ueberwindungen-Marah\
	Ueberwindungen-Marah\
	Anreden-Sindra\
	Beobachten-Jentel\
	Sortieren-Sindra\
	Befehle-Rash\
	Strichrichtung-Kanta\
	Lesehinweis-Maeh-Rash\
	Maeh-Rash\
	Lesehinweis-Kuscheln-Amira\
	Kuscheln-Amira\
	Zusammenfassung-Kuscheln-Amira\
	Stille-Marah\
	Psychen-Kamira\
	Lesehinweis-Beruehren-Amira\
	Beruehren-Amira\
	DieSchattenscholle-Katjenka\
	Unglueck-Ushenka\
	Scherben-Rash\
	Leben-Amira\
	Wesen-Aga\
	Wenden-Kamira\
	Abstand-Smjer\
	Verhalten-Katjenka\
	Toeten-Kanta\
	Aussichten-Amira\
	Knoten-Marah\
	Einsichten-Junita\
	Sinken-Jentel\
	TraumaUndTraeume-Rash\
	Schnee-Aga\
	Figurenverzeichnis
INPUTS := $(CHAPTERS:%=chapters/%.md) metadata.yaml
OUTPUTS := DieFlotteDerMaare.pdf DieFlotteDerMaare.epub DieFlotteDerMaare.tex DieFlotteDerMaare.html DieFlotteDerMaare.odt ContentNotes.tex $(SHORT_TITLE)/ContentNotes.md $(SHORT_TITLE)
SPBUCHSATZV := SPBuchsatz_Projekte_1_4_6
#SPBUCHSATZV := SPBuchsatz_Projekte_1_4__Pre_Beta_11
#SPBUCHSATZV := SPBuchsatz_Projekte_1_3

all: $(OUTPUTS)

DieFlotteDerMaare.html: $(INPUTS)
	pandoc \
		$(INPUTS) \
		-o $@


DieFlotteDerMaare.epub: $(INPUTS)
	pandoc \
		-t epub2\
		--css epub.css\
		-L SPEpub.lua\
		$(INPUTS) \
		-o WASUSZwischen.epub
	ebook-convert WASUSZwischen.epub $@
	rm WASUSZwischen.epub

DieFlotteDerMaare.odt: $(INPUTS)
	pandoc \
		-L SPEpub.lua\
		$(INPUTS) \
		-o $@

DieFlotteDerMaare.tex: $(INPUTS) SPBuchsatz.lua
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$(INPUTS) \
		-o $@

ContentNotes.tex: chapters/ContentNotes.md
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$^ \
		-o $@


# changed Ifstr to ifstr in SPBuchsatz_System/Helferlein/Hilfsfunktionen.tex 
DieFlotteDerMaare.pdf: DieFlotteDerMaare.tex ContentNotes.tex TitleAuthor.tex DFDMBefehle.tex
	cp DieFlotteDerMaare.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/Haupttext.tex
	cp ContentNotes.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/ContentNotes.tex
	cp TitleAuthor.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/TitleAuthor.tex
	cd deps/$(SPBUCHSATZV)/Buch/build/ ; \
		lualatex ../tex/Buch.tex
	cp deps/$(SPBUCHSATZV)/Buch/build/Buch.pdf DieFlotteDerMaare.pdf

$(SHORT_TITLE)/titlelist.txt: Makefile
	mkdir -p $(shell dirname $@)
	@echo '$(CHAPTERS)' | xargs -n 1 | cat -n >$@

get_chapter_number = $(shell \
	grep -E '^[[:digit:][:space:]]+$(1:chapters/%.md=%)$$' $(SHORT_TITLE)/titlelist.txt | \
		awk '{ print $$1 - 1 }' \
)


chapters-epub/%.epub: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	pandoc \
		-t epub2\
		--css epub.css\
		--metadata title="$(call get_chapter_number,$<) - $(shell \
			sed -n '3{p;q}' '$<' \
		)"\
		$< \
		-o $@

$(SHORT_TITLE)/%.md: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "$(call get_chapter_number,$<) - $(shell \
			sed -n '3{p;q}' '$<' \
		)"'
	@echo >>$@ 'number: $(call get_chapter_number,$<)'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	pandoc \
		-L SPmarkdown.lua\
		$< \
		-o tmp-md-to-md.md
	@cat tmp-md-to-md.md >>$@
	rm tmp-md-to-md.md

$(SHORT_TITLE)/ContentNotes.md: chapters/ContentNotes.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "Content-Notes"'
	@echo >>$@ 'number: 0'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE): $(CHAPTERS:%=$(SHORT_TITLE)/%.md) ;

clean:
	rm -rf $(OUTPUTS)

.PHONY: clean $(SHORT_TITLE)
