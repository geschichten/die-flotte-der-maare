

Zusammenfassung: Kuscheln {.unnumbered}
=========================

\Beitext{Schreibfisch}

Amira möchte Rash eigentlich nicht beobachten, möchte mehr zur Crew gehören und
hadert mit sich und ihren Eigenschaften, sich mit Sozialem nicht so gut
auszukennen. Sie hat von Rashs BDSM-Spiel mit der Kapitänin am Rande mitbekommen
und traut sich, Rash danach zu fragen, sowie in Sachen Geschlecht. Sie kommen
sich relativ zügig näher und probieren ein BDSM-Spiel, aber entscheiden sich
eigentlich zu schnell dazu, weshalb es zwischendurch zu einem Abbruch und einem
kurzen Diskutieren von Unsicherheiten und Problematiken kommt.

Später führen sie ihr Spiel in der Kapitänskajüte fort, aber auch dort brechen
sie es bald ab, weil Amira ein Heulkrampf überkommt. Das Spiel geht über in eine
Art heilsames Kuscheln.

