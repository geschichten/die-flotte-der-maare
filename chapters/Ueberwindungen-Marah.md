\BefehlVorKapitelDFDM{Marah-n.png}{Marah wurde von Ushenka großgezogen und hat die Flotte der Maare quasi mitgegründet. Sie ist verliebt in die Kapitänin.}{BDSM, Vorübergehend vulgäre Sprache, Erniedrigen, Objektivizieren, Ausliefern, Würgen - angedeutet, Tease and Denial, Nacktheit, Sex, Genitalien.}

Überwindungen
=============

\Beitext{Marah}

Eine Weile hatte nun niemand von ihnen mehr etwas gesagt. Mit
Worten nicht, aber Sindra blickte gelegentlich zu ihr herüber, mit
einem vorsichtigen Lächeln auf dem Gesicht. Bald häufiger,
bis ihr Blick auf Marah ruhte, sie sie eingehend musterte,
und das vorsichtige Lächeln zu einem vielleicht sogar selbstgefälligen
Schmunzeln geworden war. Marah mochte die Spannung, die
sich dadurch aufbaute.

"Dein Gesicht und Körper sehen zunehmend so aus wie das erste Mal, als
du mich gefragt hast, ob ich dich ficken möchte.", stellte Sindra fest.

Wie konnte sie so etwas so sachlich sagen? "Ich bereue die Wortwahl, die
ich damals getroffen habe, jedes Mal, wenn ich wieder so aussehe und
du mich daran erinnerst.", sagte Marah. Sie fragte sich, ob in ihrer
Stimme hörbar war, dass sie sich ein bisschen schämte.

"Wirklich?", vergewisserte sich Sindra.

Angesichts der Tatsache, dass ihr Körper auf das Wort 'ficken' reagiert
hatte, und das wahrscheinlich sogar sichtbar -- sie hatte vielleicht
etwas unruhiger geatmet und spannte den Körper anders an --, war
die Frage mehr als berechtigt. Sie holte tief Luft und schüttelte den
Kopf. "Ich", sie stockte. Sie versuchte es noch einmal: "Ich möchte", aber
sie war sehr nervös.

"Komm näher, meine Hübsche, und erzähl mir, was du möchtest.", befahl
Sindra sanft.

Das Spiel hatte begonnen. Immerhin war das Interesse nicht einseitig.

Marah robbte näher an Sindra heran. Legte sich so neben sie, dass es
für Sindra nur eine relativ kleine Bewegung gewesen wäre, von ihrer
halb auf der Seite liegenden Haltung ein Bein über sie zu schwingen
und ihren Fischschwanz zwischen ihren Knien einzuklemmen. Und ihre
Handgelenke auf den Boden zu drücken. Und voraussichtlich würde sie es
irgendwann tun.

Dann lag Marah ruhig da, machte sich schmal. Sie wollte, dass Sindra
es tat. Sie müsste es vielleicht nur aussprechen. Sie wollte, das
Sindra noch so einiges tat. Aber sie wusste nicht in welcher
Reihenfolge. Vielleicht war das auch nicht so wichtig. Sie fand
ihre Sprache nicht. "Ich", versuchte sie es noch einmal. Manchmal
half es, schonmal den Anfang zu sagen. "möchte", es klappte
nicht.

"Den einen der beiden Befehle hast du ja ganz gut ausgeführt.", stichelte
Sindra.

Das war Folter. Die süßeste Folter. Marah schluckte laut und spürte
in ihr Verlangen hinein.

Sindra zog langsam das Marah noch abgewandte Bein an. Sodass der Weg, es über sie
zu schwingen, noch kürzer war. Sie berührte Marah dabei nicht, aber
lächelte sie an.

"Was willst du?", fragte sie noch einmal. Leise und sanft.

Marahs Blick war von der Bewegung und neuen Position des Beins
gefangen genommen. Sindra trug heute eine kurze Hose mit vielen
Taschen. Sie endete mittig auf ihren Oberschenkeln. Die Haut darunter
war rau von Wetter. Marah stellte sich vor, sie anzufassen. Mit
ihren kleinen Händen. Also, Hände, die für ihre Körpergröße
normalgroß waren, aber gegenüber diesen Beinen wirkten sie sehr
klein.

"Hat es dir die Sprache verschlagen?", fragte Sindra.

Marah schloss kurz die Augen und nickte. Das war erniedrigend. Ihr
erniedrigter Körper reagierte mit einer neuen Welle Erregung.

"Das kann ich verstehen, wenn sich so ein ominöses, schönes
Bein bewegt.", lobte Sindra sich selbst. Zu Marahs Enttäuschung streckte
Sindra es wieder aus. Sie legte sich gemütlich auf den
Rücken. Die Frage "Möchtest du meine Beine weiter entkleiden?" ließ
die Enttäuschung allerdings rasch wieder verfliegen.

"Ja.", wisperte Marah. Sie fragte sich, ob sie einen Befehl abwarten
sollte.

Der dann auch kam. Nicht in Worten allerdings. Sindra winkte sie mit
einem Finger näher heran und deutete auf ihren Schritt, bevor sie die
Hände wieder gemütlich hinter dem Kopf verschränkte.

Marah atmete zitternd, als sie ihren Oberkörper hochstemmte und sich mit
den Händen den Verschlüssen der Hose näherte. Es war Sindra zu langsam.
Sie fädelte eine Hand wieder unter dem Kopf hervor, griff nach Marahs
Handgelenken und platzierte sie auf der Knopfleiste der Hose. Marah
sog hastig die Luft ein.

"Zu viel?", fragte Sindra besorgt. Ihre Hand löste den Griff, blieb
aber weich auf Marahs Haut liegen.

"Nein, überhaupt nicht.", widersprach Marah. Wimmerich. Sie liebte
es so sehr, wenn Sindra sie so behandelte. Beides. Das raue, das
Bewegungsfreiheit raubende, sowie das sanfte. Sindra war so unbeschreiblich
lieb und sanft. Aber Einschränkungen in Bewegungsfreiheit waren nunmal
Marahs Fetisch. Das hatte sie Sindra einmal gestanden. Nicht, dass es
etwas gewesen wäre, wofür sich Marah hätte schämen müssen, aber
ihr fiel es schwer, darüber zu reden. Ohne das Element des Auslieferns
lief bei Marah gar nichts, und wenn eine Person sie so bespielte, wie
Sindra es tat, war es eines der schönsten Geschenke.

Sindras Hand strich sachte ihren Arm hinauf bis zur
Schulter, bevor sie sie wieder zur anderen
hinter den Kopf schob und ein leises, behagliches Geräusch von sich
gab.

Marah knöpfte Sindras Hose auf. Sie tat es nicht sehr eilig, aber sah zu,
dass sie es auch nicht zu langsam tat. Sindra stemmte die Füße in den Boden, um
das Gesäß zu heben, als Marah soweit war, sie ihr von den Beinen zu
ziehen. Ein paar Momente lag Sindra anschließend einfach ruhig neben ihr, wirkte
zufrieden, sagte nichts. Marah war unschlüssig. Sollte sie Sindra irgendwo
berühren? Sollte sie sich wieder hinlegen?

"Möchtest du auch meinen Oberkörper ausziehen?", fragte Sindra.

"Wenn du das möchtest, sehr gern.", sagte Marah.

"Dann los!", orderte Sindra an.

Marah robbte neben Sindras Körper ein Stück aufwärts, um besser
an das Kleidungsstück heranzukommen. Sindra behielt die Augen geschlossen, als
Marah die Knöpfe des Oberhemdes öffnete. Sindra rührte sich dieses Mal
nicht, als Marah damit fertig war. Sie konnte das Oberhemd deshalb nur zur Seite, aber
nicht ganz abstreifen. Unter dem rauen Stoff war weiche Haut verborgen. Sindra
war nicht nur muskulös, sondern hatte auch viel Fettgewebe, das Marah sehr
mochte. Die Brüste waren mit einem speziellen Kleidungsstück zurückgebunden, das
Smjer mit Sindra entwickelt hatte, speziell für sie. Sindra schmerzten die
Brüste oft, wenn sie sie in herkömmlichen Kleidungsstücken trug, und auch, wenn
sie sie nicht extra bekleidete.

Marah fasste all ihren Mut zusammen und entfernte auch dieses
Kleidungsstück. Darunter kamen Sindras Brüste zum Vorschein. Für die
Körpergröße gar nicht so große Brüste. Sie liefen ein bisschen spitz
zu, saßen vielleicht etwas höher als an durchschnittlichen Körpern. Marah
mochte sie sehr. Sie robbte neben Sindras Körper noch ein Stück aufwärts
und berührte sie vorsichtig mit der Hand. Sindra wehrte sich nicht und gab durch nichts
zu verstehen, dass dies nicht in Ordnung gewesen wäre.

Marah streichelte die Brüste erst nur mit ihren Fingern, dann mit der ganzen
Hand. Schließlich streichelte sie vorsichtig Sindras Bauch, während sie
die Brust, die ihr näher war, auf der Seite küsste. Im nächsten Augenblick
lag sie unter Sindra. In einer fließenden, nicht allzu eiligen Bewegung, hatte
Sindra sie auf den Boden neben sich gedrückt, und ihr Becken, wie Marah
es sich vorhin ausgemalt hatte, zwischen ihren Knien eingeklemmt.

Marahs Atem flatterte. Sie spürte, wie ihre Klineris sehr fest wurde und
sich aus der Tasche herausdrängen wollte. Klineris hieß ein Teil der
Genitalien bei Nixen, dünner, als die meisten Penisse, etwas mehr daran
gewachsene Schleimhäute, aber ansonsten hatten sie ähnliche Funktionen wie
Penisse. Sie hätten auch einfach Penis genannt werden können, aber Nixen
fanden ein eigenes Wort dafür besser. In der Genitaltasche befand sich außerdem ein Organ, das
einer zum Beispiel menschlichen Vulvina sehr ähnlich war, und das sie
auch Vulvina nannten. Das aber unter Fußvölkern
verschiedene Namen hatte, wie Scheide, Mund oder Muschel, die
je nach Region nur verschieden viel der ganzen Vulvina meinten. Ihre
Vulvina schwoll an einigen Stellen an und wurde schleimig, was ihre Klineris, die
dazwischen lag, noch mehr stimulierte.

Ob Sindra sie dort anfassen würde? Das hatte sie noch nie. Vielleicht
musste Marah nur fragen. Aber wie vorhin hatte sie zwar eine Menge mehr
oder weniger klarer Vorstellungen, was sie mögen würde, für die
sie in ihrem Kopf sogar teils Worte fand, aber diese auszusprechen in
einer Situation, in der sie so sehr erregt war, war ihr geradezu unmöglich.

Sindras rechte Hand schloss sich vorsichtig um ihren Hals. Sie drückte nicht
zu. Aber die Geste reichte dafür, dass ihre Klineris den Weg aus der Tasche
nach draußen fand und von innen gegen den Stoff ihres Kleides drückte.

"Möchtest du mich auch gern nackt haben?", fiepte Marah. Es
war so unvorhersehbar, was sie sich zu sagen dann doch traute.

"Sehr gern. Ist das eine Einladung, dich ausziehen zu dürfen?", fragte
Sindra.

Warum fragte sie noch? Sie durfte alles! Marah hatte nie eine Grenze
festgelegt. "Ja.", flehte sie.

Vielleicht sollten sie irgendwann über Grenzen reden. Vielleicht würde
Sindra sich dann mehr herausnehmen, wenn sie wusste, was zu viel wäre. Aber
Marahs Überlegungen stoppte jäh, als sie Sindras andere Hand, die, die nicht
um ihren Hals platziert war, unterhalb ihres Beckens unter dem Rocksaum an
ihrer Schwanzflosse fühlte. Ihr Atem zitterte, als sich die große Hand
unnachgiebig unter ihre Kleidung schob. Gar nicht soweit weg von ihrer
Klineris. Oh, wie sehr sie dort angefasst werden wollte. Aber die Hand zog
sich wieder zurück, um als nächstes den ganzen Rock hochzuschieben. Marah
versuchte, ihr Becken so anzuheben, wie Sindra es vorhin getan hatte, damit
Sindra Stoff unter ihr nach oben schieben könnte. Aber Sindra hatte
eigene Pläne und drückte ihren Körper wieder zu Boden. In Marah zog
sich vor Auf- und Erregung alles zusammen. Sie atmete schneller. Sie
fühlte beim Atmen Sindras Hand an ihrem Hals umso deutlicher. Sindra lächelte.

Vielleicht hatte Sindra Angst, sie dort anzufassen, kam Marah
in den Sinn. Vielleicht... "Hast du", Marah stockte wieder.
Vielleicht hatte sie noch nie... "Hast du schonmal eine Klineris
oder eine Nixe an Genitalien angefasst?", fragte sie.

"Ja.", sagte Sindra. Sie wirkte so sanft und auf freundliche
Art sachlich dabei. "Allerdings nicht an stimulierten."

"Huch?", entfuhr es Marah. Das klang so unwahrscheinlich. Nicht, dass
es gar keinen Sinn ergeben hätte, aber es kam ihr ungewöhnlich vor, nicht
mit stimulierten Genitalien anzufangen, wenn es ums Anfassen ging.

"Als Kamira und ich uns kennen lernten, waren wir gegenseitig sehr
neugierig aufeinander und wissbegierig.", erklärte Sindra. "Wir haben uns in
gegenseitigem Einvernehmen studiert. Wissenschaftlich."

Das passte zu Kamira, überlegte Marah. Und zu Sindra auch. "Ich
kann wirklich damit umgehen, wenn es so ist, wüsste es aber
gern:", leitete Marah ein, mit erstaunlich ruhiger Stimme, wie sie
feststellte. "Hast du Sex mit mir aus eher rein wissenschaftlichen
Gründen? Weil du mich erforschen willst?" Und da war sie wieder, die
Erregung. Marah stand durchaus auch darauf, objektifiziert zu
werden. Solange sie die Möglichkeit hätte, sich damit nicht
einverstanden zu erklären.

Sindra aber blickte sie ernst an. "Ich liebe dich, Marah.", sagte
sie sachlich. "Ich erforsche gern, ja. Aber du genießt. Das
ist, worum es mir geht. Ich sehe dir so gern beim Genießen zu
und ich bin noch viel lieber Auslöser davon."

Marah schluckte. Keine Objektifizierung. Sondern etwas viel
Schöneres, Wertvolleres, das sie stattdessen auf eine andere
Weise fühlte. Sie hätte die Arme um Sindra geschlossen und eine
Weile geweint, wenn ihre Hände nicht unter Sindras Knien festgesteckt
hätten. "Ich liebe dich auch.", sagte sie rasch, bevor Erregung
sie wieder überrollte, die mit dem Bewusstsein kam, dass
sie die Hände nicht bewegen konnte.

"Ich weiß.", sagte Sindra leise. Und mit dieser weichen
Stimme, die Marahs Körper kribbeln
ließ.

Sie ließ sich in das Gefühl fallen, ausgeliefert
zu sein, als Sindras Hand wieder unter ihren Rock fuhr,
gefährlich nah an ihre Klineris, die sich wieder mehr
aufrichtete.

Sindra berührte sie dort nicht. Ihr Blick
huschte aber für einen kurzen Moment zur Ausbeulung des
Kleides und kam mit
einem Lächeln wieder zurück. "Ich weiß, dass es allerlei
Varianzen bei euren Genitalien gibt. Das hat Kamira
auch erzählt.", fuhr sie fort, und ergänzte rasch: "Gibt
es bei unseren ja auch. Sieh dir allein meine Brüste an."

Marahs Blick huschte zu Sindras Brüsten, ob es nun als
Befehl gemeint gewesen war oder nicht. Sie hörte Sindra
glucksen. So ein schönes Geräusch. So schöne Brüste.

"Jedenfalls weiß ich, dass ich dir mit meinen großen
Fingern im Genitalbereich sehr weh tun kann. Selbst, wenn
ich vorsichtig bin.", kam Sindra wieder zur Sache. "Zumindest, wenn
ich sie einführe."

Ob sie das bei Kamira gemacht hatte? Der Gedanke ging in
Marahs Atem unter, der sich schon wieder stark beschleunigt
hatte, als die Worte 'weh tun' ihren Weg
in ihre dafür passenden Gehirnwindungen
gefunden hatten. "Ich möchte", flüsterte sie. Und brach schon wieder
ab. Hätte es neben der Erregung und dem überwältigendem
Gefühl, wehrlos zu sein, auch noch Platz für eine weitere
Emotion gegeben, hätte sie sich vielleicht über sich selbst
und ihre Sprachlosigkeit geärgert.

"Wenn du möchtest, dass ich dir Schmerz zufüge, würde ich mich
nur langsam daran herantasten wollen.", stellte Sindra
klar. Ihre Stimme war immer noch so sanft. So eine sanfte
Dominanz.

Marah zerfloss in Gefühl. Die Worte taten etwas mit ihr.
'Schmerz', obwohl sie gar nicht so sehr auf Schmerz stand, hatte
sich stark angefühlt. Und nun war es das Wort 'herantasten'.

"Ich möchte dir gehören.", wisperte sie. Sie spürte die
schleimigen Flüssigkeiten, die sich in ihrer Vulvina
sammelten. Sie fühlte sich, als könnte sie die ganze
Erregung nicht mehr umfassen, wenn Sindra sie nicht
bald dort berührte. "Ich möchte, dass du mich dort
anfasst.", flüsterte sie.

Sie hatte es gesagt. Es hatte sie alles an Überwindung
gekostet. Sie fühlte sich nun wund, schämte sich vielleicht, und
es erregte sie noch mehr. Sie fühlte sich, als würde sie wirklich
dieser Frau über sich ganz und gar gehören und ausgeliefert
sein. So sehr, dass sie sogar solche Dinge sagte.

Die Hand um ihren Hals bewegte sich, fasste sie am Kinn und
drehte es so, dass sie Sindra ins Gesicht sehen würde, wenn sie
wieder aufblickte. Sie interpretierte es als einen Befehl und tat
es dann auch. Sindra beugte sich etwas zu ihr herunter, mit
diesem sanften, ernsten Gesichtsausdruck. Ihr langes, dichtes,
braunes Haar strich seicht an Marahs Körper entlang. Marah atmete
schneller. Sie spürte, wie Feuchtigkeit aus ihrer Tasche
lief. Sie spürte, wie Sindras andere Hand sich langsam mit
einem gewissen Druck, der Dominanz kommunizierte, über ihren
Körper bewegte, und dann bei der Berührung ihrer Klineris
wieder ganz zartfühlend wurde. Marah hyperventilierte. Sie konnte
es nicht vermeiden. Sindras große Hand lag dort erstmal. Das
war auch aufregend genug. Die andere hingegen legte sich doch
noch etwas fester um den Hals. Marah stockte der Atem, vielleicht
dadurch, aber vielleicht auch, weil sich Sindras Kopf zu ihrem
bewegte und die Lippen der Kapitänin wieder auf ihrer Stirn
landeten. Sie spürte Sindras Atem durch ihre Haare blasen. Ihre
Klineris bewegte sich ein bisschen wie von selbst, aber
Sindras Hand folgte der Bewegung, umfasste sie sanft und
streichelte vorsichtig.

Marah hätte um nichts in der Welt jetzt sprechen gekonnt. Sagen
gekonnt, dass sie sich wünschte, dass Sindra einen Finger zwischen
ihre Vulvalippen schieben möge, in der Genitaltasche. Aber
ihr Körper sprach es für sie aus. Ohne, dass sie es steuerte, bewegte
er sich in der eingeklemmten Position doch so gut es ging Sindras
Hand entgegen. Ihr Atem fiepte gegen Sindras Hals. Und Sindra, bei
der es auch wahrscheinlich gewesen wäre, dass sie sich gegen
die Erfüllung ihrer Wünsche entschieden hätte, weil es Marah
so wunderschön quälte, gab dieses Mal nach. Sindra strich
mit sanften Bewegungen zwischen ihren Vulvalippen und über
ihre Klineris entlang. Marah würde wahrscheinlich davon keinen
Orgasmus bekommen. Dazu war ihr Körper viel zu kompliziert, als
dass das ohne Absprachen möglich gewesen wäre. Aber die Berührung
war deshalb nicht weniger erlösend, nicht weniger, wonach Marah
sich verzehrte, was sie sich so lange gewünscht hatte. Sie
ließ sich in die Berührungen fallen, ließ sich von Sindra
küssen und verwöhnen, bis sie müde wurden.

Wenn Sindra sie nun allein gelassen hätte, hätte
sie vielleicht zu Ende masturbiert. Aber
eigentlich war sie auch zu erschöpft dazu. Es war so, wie es
war, viel schöner. Als sie irgendwann auf Sindras nacktem Oberkörper
lag, endlich selber nackt, weil Sindra sie am Ende doch noch
ausgezogen hatte, und Sindras Streicheleinheiten weniger verlangend, weniger
dominant waren, sondern einfach nur noch aus Zuneigung und
Liebe bestanden.
