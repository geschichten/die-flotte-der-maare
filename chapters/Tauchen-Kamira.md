\BefehlVorKapitelDFDM{Kamira-n.png}{Kamira ist an Bord für Konfliktmanagement, psychischen Beistand und Barrierabbau zuständig. Er hat am Anfang den Nixen-Anteil der Crew zusammengesetzt und Smjer gefragt, ob er das Vize-Kommando übernehmen würde.}{BDSM, Ausliefern, Atemnot, Aufgeben, Verlieren, Erektion.}

Tauchen
=======

\Beitext{Kamira}

Er saß mit Marah auf der Reling, die Fluken
hinter die Streben gefädelt. Eine angestrengte Haltung, nicht
zum Entspannen gedacht, sondern für einen Sprung in die
Tiefe. Es war eine sternenklare Nacht mit hellem Mondlicht, eine
Nacht vor Vollmond. Der Wind blies kühl von der Seite, vermischte
sich aber mit dem Fahrtwind zu einem, den sie
von schräg vom Bug her fühlten. Das
Schiff krängte etwas. Sie saßen auf der zum Wasser geneigten
Seite. Kamira roch die feinen Tröpfchen, die jedes Mal
aufgewirbelt wurden, wenn der Bug der Schattenmuräne in
eine neue der vom Wind abgeschnittenen Wellen stampfte. Ein
vergleichsweise zartes Stampfen heute. Kamira liebte es, wie
sich über den größeren Wellen unzählige kleinere Wellen
bewegten, die die See dunkler erscheinen ließen. Er
atmete noch einmal sehr bewusst die Tröpfchen ein, um sie
auf den Schleimhäuten zu schmecken, und richtete
dann den Blick auf Marah neben sich, ein Schmunzeln im
Gesicht verteilt.

Marah hatte Kamira nach ihrem letzten Überfall gebeten, ob
sie tauchen üben könnten. Marah, diese Person, die auf
der See daheim war, wie keine andere, hatte ihm gestanden, dass
sie es mit der Angst zu tun bekam, wenn sie zu lange unter
Wasser war, sodass sie merkte, dass ihr die Luft ausginge. Kamira
hatte langes Luftanhalten und Techniken dafür geübt und war
bereit, das Wissen weiterzugeben. Allerdings
hatte sich das Gespräch anders entwickelt als geplant.

Während Kamiras einleitendem Vortrag hatten beide
zunehmend festgestellt, dass sich das Thema, die
damit verbundenen Ängste und anderen Gefühle für ein
Spiel eigneten. Da waren nämlich andere Gefühle.

"Bereit?", fragte Kamira mit einem fiesen Grinsen. Zumindest
versuchte er ein fieses Grinsen. Ihm war
gelegentlich mitgeteilt worden, dass er eigentlich gar
nicht fies grinsen könnte.

Marah holte einige Male tief Luft und nickte dann. "Auf
drei?"

Eine angenehme Aufregung kribbelte durch Kamiras Körper. Er
zählte. Die 'drei' betonte er überzeugt und sie stießen
sich so sehr ab, dass die ganze Schattenmuräne erzitterte. Hoffentlich
wachte davon niemand auf. Stürzten sich ins dunkle Wasser, dicht
nebeneinander. Der Schwung und ihre schweren Körper beschleunigten
sie in die Tiefe. Es brauchte nur wenige Momente, bis sie die Stille
des Meeres und die Dunkelheit umschloss, die sie selbst mit
geöffneter Nickhaut schon als dunkel wahrnahmen. Und er umschloss Marah mit
den Armen. Marah merkte es sofort und kämpfte dagegen an. Ein
Machtkampf, während sie weiter in die Tiefe trudelten. Marah
war geschickt, und Kamira ein wenig stärker, vor allem größer. Ihre
Arme waren überall, ihre Finger drückten sich schmerzhaft in
Druckpunkte in seinen Händen und Armen, sodass er seine
Kraft nicht so gezielt einsetzen konnte. Er liebte das
Gefühl zu gewinnen und zu verlieren gleichermaßen. Er ließ nicht
locker, sortierte seine Arme um, versuchte, sie mit seinem Fischschwanz
zu umwickeln. Weiter sanken sie in die Tiefe dabei, es wurde dunkler, der
Wasserdruck stärker. Der Kampf war eine ganze Weile einigermaßen
ausgeglichen, mal glaubte er, er würde es schaffen, ihre Arme zu
fixieren, und dann war doch wieder einer frei, ihre Hand irgendwo
an seinem Hals. Bis unvermittelt ihr Körper schwächer wurde. Zeitgleich
rann ein Zittern durch den Körper, den er halb umschlungen hatte. Ein
Zittern, von dem er sehr wohl wusste, dass es Ausdruck ihrer Erregung
war, weil sie aufgeben musste.

Sie hätte Rot sagen können, auf
Sirenu. Die Sprache eignete sich dafür, so etwas unter Wasser
zu sagen. Aber stattdessen sagte sie "Luft". Sie wehrte sich
kaum dagegen, als er ihre Arme unsanft umklammerte und in
einer ausweglosen Position auf ihrem Rücken fixierte, bevor er die
Bewegungsrichtung wechselte, -- wieder nach oben. Kein kurzer
Weg. Ausreichend, um auszukosten, dass er sie nun besaß. Für
eine Weile. Es war ein Genuss, nicht nur für ihn. Er hatte, kurz
bevor er sie umgedreht und ihr unters Kinn gepackt hatte, um ihren
Kopf an seiner Schulter zu fixieren, ihre Erektion gegen
seinen Bauch pressen gespürt. Er biss ihr zart in den Hals. Ein
Schaudern durchlief ihren ganzen Körper. Und dann sprach er
Worte, in Sirenu, die sie hören wollte, die sagten, dass sie
verloren hatte, und die sie beide physisch unter der Haut
spürten. Marah gab ein genussvolles Fiepen von sich.

---

Später in der Nacht sprachen sie darüber. Jentel
verweilte nachtsüber wie immer im
Mastkorb, hatte vielleicht eine Idee, was sie gemacht hatten, aber
sagte nichts dazu. Smjer war früh aufgestanden, um Routen zu
planen. Also hatten Kamira und Marah das Nixendeck für
sich.

"Dieser Augenblick, in dem ich realisiert habe, dass ich zwar
vielleicht irgendwann gewinnen könnte, aber Angst hatte, dann nicht
genug Luft zum Auftauchen zu haben, war anders als erwartet, aber sehr
schön.", sagte Marah leise. "Ich hatte mich auf verlieren
eingestellt, nicht auf aufgeben."

Kamira strich ihr zärtlich über den Arm. Marah erwiderte
die Geste. "Gab es irgendetwas, was nicht gut für dich war?", fragte
Kamira.

Marah schüttelte den Kopf und gab ihm einen Kuss auf die
Nasenspitze. "Ich hatte ein bisschen Angst, ob ich es dir
zu leicht gemacht habe durch das Aufgeben.", sagte sie. "Dass
du dabei, wie hast du es beschrieben, das Besitzen nicht so
stark wahrnehmen konntest." Sie grinste. "Und da ist sie
wieder, die Erregung."

"Besessen werden, hm?", fragte Kamira.

Marah kicherte und stimmte zu. "Sehr."

"Der Moment war genau richtig für mich. Mit 'Luft' hast
du mir perfekt mitgeteilt, warum du dich mir dann leider
überlassen musstest." Kamira brachte gespielte Ironie in
seinen Ausdruck. Dann streichelte er nachdenklich ihr
Gesicht. "Würdest du jetzt gern noch einmal besessen werden?"
