Lesehinweis {.unnumbered}
===========

Neben den Content Notes am Ende des Buches hat dieses Buch auch
Content Notes auf der Seite vor den Kapiteln. Zudem gibt es Lesehinweise,
die jeweils vor Kapiteln stehen, die fast ausschließlich explizite Kink-
oder Sex-Szenen beinhalten. Diese sind vor allem für meine sex-repulsed
Lesenden gedacht, aber auch gern allgemein für Lesende, die jene Kapitel gern
überspringen möchten. Wenn ihr gern einfach allen Buchinhalt lesen möchtet, überblättert
diese gern.

Es ist keine Lösung, die für alle gut passt, weil für manche diese Seiten, auch wenn sie
nur kurz im Blick sind, eine Voreingenommenheit auslösen, die sie nicht gern haben (ich
fühle das), aber ich habe mich für dieses Buch trotzdem für diesen Kompromiss entschiedena. Ich
werde es nicht immer so halten.

---

Dieses Buch steht unter Creative Commons Lizenz:

[http://creativecommons.org/licenses/by-nc-nd/4.0/](http://creativecommons.org/licenses/by-nc-nd/4.0/)


![image of the by-nc-nd 4.0 licence](/home/kaulquake/Fischtopf/Geschichten/Myrie/license.png){width=0.8 height=0.4}

Das Buch ist Ende des Sommers 2021 innerhalb von so etwa 4-8 Wochen entstanden, in einer Zeit, in
der ich psychisch nicht gesund und oft nicht so klar denkfähig war. Es wurde aber Ende 2023
einmal gründlich überholt. Unter anderem floss dabei ein Sensitivity Reading von E.\,V. Ring ein, das
mich sehr viel zufriedener mit dem Werk macht.

Das Cover habe ich mit gimp erstellt.

---

Ich bin autistisch und habe kPTBS und viele meiner Figuren
haben verschiedene Anteile davon. Das schlägt sich auch in meinem Schreibstil
nieder. Dinge sind oft direkter beschrieben, und manchmal erst, wenn sie vorbei
und verstanden sind. Zu manchem gibt es mehr emotionale Verbindung, zu anderem
weniger. Es gibt relativ viel Einblick ins Innere von Figuren, und vielleicht
sogar Dinge, die scheinbar lustig sind, aber eigentlich nur die so fremd
wahrgenommene, neurotypische Welt wiederspiegeln. Es wird mehr und mehr Kern
des Buches.

Hoffentlich könnt ihr dem Eintauchen in diese Wahrnehmung etwas abgewinnen!

---

Ich behandele in wörtlicher Rede Punkte gleichberechtigt mit anderen Satzzeichen. Ich setze
mich damit über gängige Grammatik-Regeln hinweg, einfach, weil es mir so besser
gefällt und konsistenter vorkommt.

---

In diesem Buch kommen Neopronomen vor, darunter 'as/sain/ihm/as', 'sey/ser/sem/sem', sowie
ein Charakter, der nicht mit Pronomen referenziert wird. Das ist etwas, was in unserer
Welt im Umgang mit vielen nicht-binären Menschen durchaus üblich ist -- und nicht
auf nicht-binäre Menschen begrenzt. Menschen dürfen sich ein Pronomen aussuchen, mit dem
sie referenziert werden möchten. In meinem Umfeld gibt es Menschen, die aus
verschiedenen Gründen irgendwann eines für sich gewählt haben. Neopronomen
sind fester Bestandteil in vielen vor allem queeren Communities. Es besteht
unter den Menschen, die es betrifft, weitgehend Konsens, dass das Konzept ein
Gutes ist. Pronomen werden zunehmend in Signaturen oder in Profilen angegeben.

In diesem Buch gibt es aber außerdem erfundene neutrale Wortendungen. Es kommt
ein Volk vor, das gegenderte Sprache so modifiziert, dass zuvor gegenderte
Wörter hinterher Personen unabhängig von Geschlecht meinen sollen. Die
dafür erfundene Endung ist '-an' im Singular und '-anen' im Plural. Beispiele:
"Mein Nachbaran ist freundlich, ich mag es. Ich hätte gern weitere so freundliche
Nachbaranen."

Diese Form habe ich erfunden und in einigen Kurzgeschichten mit weiteren
Sprachmodifikationen ausprobiert (*Tee im Schnee* und *Jurin Raute - Windschwinge*). Es gibt
auch diesbezüglich Ansätze in queeren Communitys, zum Beispiel mit der Endung '-y' (Nachbary), aber
es besteht in den Communitys, die sich darüber
Gedanken machen, weder eine verbreitete, klare Akzeptanz, noch ist ausdiskutiert, wie
sinnvoll das ist.

Das Buch ist aus der Sicht von dreizehn Charakteren geschrieben, die jeweils
verschieden konsequent verschiedene Systeme ausprobieren oder durchziehen. Viel
Spaß mit dem Chaos.

![map of Maerdha in grayscale](/home/kaulquake/Fischtopf/Geschichten/DieFlotteDerMaare/GrenlanndMapRand.png)
