\BefehlVorKapitelDFDM{Sindra-n.png}{Sindra ist die Kapitänin der Flotte der Maare. Sie ist sehr groß.}{Grusel, Selbstkritik.}

Unheimlichkeiten
================

\Beitext{Sindra}

Sindra legte die Briefe in die Flüssigkeit, die die wasserfeste
Tinte von den Seiten fraß. Die schwarze Farbe bildete
wunderschöne Gewitterschlieren in der noch klaren
Flüssigkeit. Sindra bewegte das wasserbeständige Papier
zunächst nur vorsichtig, um die umherwirbelnde Farbe nicht
gleich so mit der Flüssigkeit zu vermischen, dass die
hübschen Muster verschwammen. Aber irgendwann musste sie
es doch tun, damit auch keine Tinte zurückbliebe.

Es war wasserfeste Tinte auf Papier aus einer bestimmten
Algenbasis. Sindra kannte inzwischen etwa vierzig
Namen verschiedener Algensorten auf Siren, von denen aber
in besagter Sprache viel weniger der Pflanzen die sirensche Vokabel
für 'Alge' enthielten. So ähnlich, wie in den verschiedenen
Landsprachen zwar viele Blumen tatsächlich auch Blumen hießen, aber
eben nicht alle.

Jentel hatte darüber mal einen Vortrag gehalten. Darüber, dass
es im Sirenschen umgekehrt war. Zum Beispiel hießen Tulpen wörtlich
aus Siren übersetzt Glattblume und Rosen Duftblume. Sindra
verstand Jentels Faszinantion für dieses Thema sehr gut. Sie übte
Siren täglich und Marah nannte Sindras Siren inzwischen brauchbar.

Sindra wischte das blassgelbe Papier mit einem Handtuch ab und
hängte es zum weiteren Trocknen auf, sodass das Papier hinterher
wiederverwendet werden konnte. Die Inhalte der Briefe informierten
sie über die Routen der Forschungsschiffe und zugehörige
Politik an Land, und wurden
vor der Löschung von drei Crewmitgliedern gelesen und
verinnerlicht: Von Smjer, der das Vize-Kommando hatte, von
Marah und von ihr. Marah gehörte zum Projekt, seit sie ein Kind
gewesen war. Sie war, mehr noch als alle anderen hier, gefühlt
auf dem Meer groß geworden. Marah segelte ein kleines, eigenes
Segelboot zwischen den Schiffen der Flotte hin und her, als
wäre es eine Erweiterung ihres Körpers. Es war eigentlich
kein Platz für eine weitere Person auf der Jolle, aber sie
hatte Sindra trotzdem manchmal mitgenommen. Zum Beispiel auf die kleine Insel
nahe der Küchengärten hinüber, in deren Nähe sie gerade
wieder einmal ankerten. Marah hatte auch versucht, ihr die Gärten zu
zeigen. Es waren Gärten unter Wasser, in denen einige Nixen, die
zur Crew der Flotte gehörten, Lebensmittel für sich anbauten, die
dann geerntet und an Bord gebracht wurden. Marah mochte die
Gärten. Fische und Garnelen hausten zwischen verschiedenen Sorten
von Tang, Gräsern, Getreide, Fruchtranken
und Polypengewächsen. Das Meer bot so eine
Fülle an Lebensmitteln, die an Land größtenteils unbekannt
waren. Zumindest, wenn Sindra den anderen Crewmitgliedern
glaubte. Sie hatte ihre Zeit an Land weder in glücklicher
noch in sonderlich detaillierter Erinnerung. Sie konnte gut
darauf verzichten. Wenn sie aus der Kapitänskajüte trat, der
Wind versuchte, an ihrem langen, braunen Zopf zu zerren, das
Schiff über die Wellen schnitt und das Salz über das Deck flog, wenn
sie sich darüber bewusst wurde, dass eine bisher so
sehr abgelehnte Person wie sie nun akzeptiert genug war, dass
ihr das Kommando einer ganzen Flotte in die Hand gelegt worden
war, durchströmte sie ein Gefühl von Sein, von Präsenz, von
einer Verantwortung, die sie gern trug, und von so etwas wie
Liebe. Als könnte sie all jenes ein wenig umschließen und
hüten.

Trotzdem kannte Sindra die Gärten mehr aus der Beschreibung, als
dass sie sie selbst hätte sehen können. Sie war mit Marah
getaucht. Sie konnte nicht selbst lange genug tauchen, weil sie
mehr Luft brauchte als Nixen. In dem Zusammenhang hatte sie
Marah das erste Mal geküsst. Es war sachlich geplant gewesen: Dass
Marah einfach mit viel Luft tauchen und Sindra dabei schleppen würde, weil
das schneller ging, als wenn Sindra selbst schwamm, und sie
Sindra dann, sobald jene es brauchen würde, einmal die gespeicherte
Luft spenden würde. Sie hatten den Austausch im flachen
Wasser geübt. Es war dabei nicht, wie eben eigentlich
geplant, sachlich geblieben. Es war nicht unbedingt das Küssen
gewesen, aber es war ihnen einfach unmöglich gewesen, diese körperliche Nähe
zu erleben, ohne sich dabei einzugestehen, dass der ganze
Plan von vornherein eigentlich nicht ein "Komm, ich möchte
dir irgendwelche interessanten Gärten zeigen." war, sondern eher ein
"Ich möchte dir einen persönlichen Teil von mir zeigen.". Marah
hatte sich zuvor schon in Sindra verliebt, ein wenig zumindest. Bei
der Aktion war der Funke übergesprungen und Sindra hatte
so sehr gefühlt, wie noch nie zuvor.

Es war nicht schlimm, dass Sindra die Gärten am Ende dann kaum
hatte erkennen können. Sie probierten es immer wieder, mit
mehr Tricks. Erst war es der Druck auf den Ohren gewesen, der
für Sindra in der Stärke unerwartet gewesen war. Nun
wusste Sindra, wie ein Druckausgleich ging, aber es war zu
dunkel. Sie hatten also Leuchtpflanzen ausprobiert, aber diese
beleuchteten auch immer nur einen kleinen Teil der Gärten. Das
Gesamtbild, wie Marah es sah und beschrieb, ergab sich nicht.

Nach den Tauchgängen hatten sie sich auf die Insel zurückgezogen
und sich gegenseitig erforscht, nicht nur körperlich, was
nicht weniger anspruchsvoll gewesen
war. So forsch Marah oft nach außen wirkte, in ihrem Inneren
sah das ganz anders aus. Sie hatte Schwierigkeiten, über ihre
Bedürfnisse zu reden, und das besonders, wenn sie dabei gerade
viel fühlte. Sie tasteten sich nun schon seit einigen Jahren
aneinander heran und es gab immer noch so viel, was nicht
erforscht war. Sie hatten Zeit. Vielleicht würde
sich in ihrer Auszeit hier vor Anker wieder eine Gelegenheit
ergeben.

---

Es klopfte an die Tür. Kurz fragte Sindra sich, ob es Marah wäre, aber
Marah klopfte anders. Tiefer an der Tür -- Marah war sehr klein und
stand nicht auf Beinen --, und weniger zurückhaltend. Vielleicht war es eine
Person, die gerade besonders unsicher war oder selten
klopfte. Oder beides. Oder beides nicht.

"Einen Moment!", rief Sindra zur geschlossenen Tür und hängte zunächst
das Papier wieder ab. Sie wischte außerdem die zwei Tropfen von der
Platte darunter. Sindra achtete sehr genau darauf, welche Spuren
in der Kapitänskajüte sichtbar wären, wenn eine Person sie betrat.

Als sie sich sicher war, dass der Raum den richtigen Eindruck machen
würde, öffnete sie die Tür. Es war Janasz. Janasz besuchte sie
in der Tat selten. "Was kann ich für dich tun?", fragte Sindra, die
Tür so weit öffnend, dass Janasz es als Einladung auffasste.

Der Zwerg trat ein, schloss die Tür hinter sich und blieb
unschlüssig oder unsicher stehen, blickte sich um. Sindra hätte
ihm gern einen Tee angeboten, aber der aktuelle Aufguss war inzwischen
kalt und sie hatte gerade kein neues Wasser da. "Möchtest du dich
setzen?", fragte sie.

Janasz kam der Einladung nach und nahm auf einem der Stühle Platz. Sindra
setzte sich ihm gegenüber auf den anderen Stuhl in einen Schneidersitz. Die
Stühle waren vielleicht die losesten Möbel an Bord.

"Moin.", sagte Janasz mit eher dünner Stimme.

Janasz war keine übermäßig selbstbewusste Person, aber dieser Ausdruck
seiner Stimme und Körperhaltung kam Sindra auch für ihn ungewöhnlich
unsicher vor. "Du hast Angst." Sindra hatte ihre Deutung ungeplant
ausgesprochen, zu überrascht über ihre eigene Eingebung. Sie war
eigentlich wirklich nicht gut darin, Emotionen von Leuten zu erraten,
die ihr nicht sehr nahe standen. Und sie lag, selbst wenn
sie Ideen hatte, oft genug falsch. "Entschuldige. Hast du überhaupt Angst?"

Selbst mit der Korrektur war es vielleicht keine so
gute Idee gewesen, das laut auszusprechen. Janasz
wirkte darüber eher entgeistert oder zumindest noch ängstlicher. Trotzdem
nickte er.

Sindra dachte über eine gute Strategie nach, zu handeln oder etwas zu sagen, das
ihm Sicherheit geben könnte. "Ich werte das nicht. Es ist in Ordnung,
Angst zu haben.", sagte sie sanft. Und dann stellte
sie ihm einfach die Frage, die sie sich selbst versuchte, zu
beantworten. "Kann ich etwas tun, damit du dich wohler fühlst?"

"Ich", Janasz stockte und schloss die Augen. "Ja. Schon. Können
wir in alle Schränke schauen und dann sehr leise reden?" Sindra
sah förmlich den Schweiß, der ihm ausbrach. "Mich gehen deine
Schränke nichts an.", fügte er hinzu.

Sindra warf einen Blick auf den hohen Schrank an der Seite. Ja,
darin könnte eine Person versteckt sein und lauschen. Sie war nicht
sicher, ob Janasz darauf hinauswollte, aber stimmte ohne
weiteres Zögern oder Abklären zu. "Vertraust du mir genug, dass ich
überall nachschaue, während du dir die Augen zuhältst?", fragte
sie.

Janasz nickte und kam der Bitte sofort nach. Sindra ging durch die
Kajüte, betrachtete alles noch genauer als sonst und blickte nicht
nur in den Schrank, sondern auch in die
wenigen verschließbaren Schubladen und Fächer. Sie tat es nicht
hektisch. Sie fand nichts anders vor, als sie es zurückgelassen
hatte. Aber nun im Nachhinein ordnete sie eine Erinnerung von
gestern anders ein: Die Blende der Schublade, die am meisten
ins Sichtfeld Besuchender fiel, war schon immer etwas lose gewesen. Nach
dem Zuschieben war die Ritze unter der Kante, unter der sie war,
auf einer Seite ein wenig schmaler als auf der anderen. Sindra
richtete es eigentlich jedes Mal, nachdem sie sie zugeschoben hatte. Es
war ihr gestern aufgefallen, dass der Spalt asymmetrisch gewesen
war, aber sie hatte vermutet, dass sie das Geraderichten eben doch mal
vergessen hatte, als sie für Rash in Eile etwas daraus hervorgeholt
hatte.

Sie verschob ihren Stuhl um den Tisch herum und setzte sich
wieder, sodass sie näher bei Janasz sein würde. Sie tat dies
nicht ganz leise, damit er sich nicht erschrecken würde. "Es
ist niemand hier in der Kajüte außer uns.", sagte sie leise. "Aber
es ist gut möglich, dass eine Person an Bord, die spioniert oder so etwas. Du
darfst die Augen wieder aufmachen." Sindra hatte sich gefragt, ob
sie Janasz die Überlegung wirklich hätte bereits mitteilen
sollen. Aber sie glaubte, dass es für Janasz hilfreich
wäre, nun ernst genommen zu werden. Dass er dann leichter
sagen würde, warum er hier wäre.

Janasz nahm die Hände von den Augen und blickte auf. "Ich hatte
nach dem Kochen wie immer das Essen direkt in Schalen umgefüllt.", berichtete
er leise. "Dann habe ich mich umgezogen und gewaschen, auch
wie immer nach dem Kochen, und habe die Schalen verteilt. Und Smjer
hat sich dann bei mir beschwert. Er hat ja die Galnussunverträglichkeit. Und
ich bin mir absolut sicher, dass ich sie nicht in seine Schale
gerieben habe. Ich achte sehr auf so etwas. Das muss jemand gemacht
haben, während ich weg war. Anders kann ich mir das im Moment
nicht erklären."

Sindra nickte. "Danke, dass du mir darüber berichtest.", sagte
sie. "Wie schlimm wäre es? Hat Smjer rechtzeitig zu essen aufgehört
und es hätte ihn töten können?"

Janasz schluckte. "Eigentlich hätte ihn die Menge, die ich in die
anderen Schalen gerieben habe, zumindest sehr viel mehr mitnehmen
müssen, als die Dosis, die hineingeraten ist, dann verursacht
hat.", sagte er nachdenklich.

"Das ist interessant." Sindras Blick huschte abermals über die Schubladen. Dachte
Janasz, dass sie hier vielleicht Galnuss lagerte? "Warum wolltest du,
dass ich durch die Schränke und Schubladen sehe?"

Auch Janaszs Blick wanderte über die Schubladen und er runzelte die
Stirn. "Die Schränke hätten wohl gereicht. Ich hatte Angst, dass
jemand lauscht. Es tut mir leid, das war unnötig."

"Es war keine Kritik, ich sammele nur Information." Sindra
lehnte sich zurück und dachte nach. Es
gab so viele Möglichkeiten und keine ergab so richtig Sinn. "Hast
du einen Verdacht oder eine Idee?"

Janasz schüttelte den Kopf. "Eigentlich nicht."

"Eigentlich?", fragte Sindra.

"Rash weiß über die ganzen Kochdinge Bescheid.", sagte
Janasz zögerlich. "Ich
habe Rash nie für irgendetwas verdächtigt. Es ergibt auch keinen
Sinn. Rash ist schon so lange an Bord. Aber Smjer hat mir damals
eigentlich gesagt, niemand solle von der Unverträglichkeit etwas erfahren. Ich
habe ihn nun gefragt, ob ich dir davon erzählen darf, und er hat
zugestimmt. Und Rash hat es eben mal mitbekommen. Sonst weiß davon
niemand."

"Weiß Smjer, dass Rash davon weiß?", fragte Sindra.

Janasz nickte. "Ich habe es ihm heute auch gestanden."

Sindra betrachtete ihn nachdenklich. Er machte immer noch
keinen entlasteteren Eindruck. "Kann ich etwas tun, damit
es dir besser geht? Hast du weiterhin Angst?"

"Mir geht es nicht gut und ich habe Angst, weil ich mir Vorwürfe
mache deswegen." Janasz blickte zu Boden. "Vor allem
deshalb.", korrigierte er und verknotete dabei die
Hände ineinander. "Aber auch, weil ich Angst habe, dass jemand
Smjer töten möchte. Oder andere."

"Mach dir wegen der ersten Sache keine Sorgen.", beruhigte
Sindra. "Es ist natürlich nicht in Ordnung, dass du Rash davon
mitbekommen lassen hast. Aber es ist nun mal
passiert. Du warst ehrlich darüber."

Sindra rechnete nicht damit, dass sie die richtigen Worte gefunden hatte.
Vielleicht wäre für Janasz hilfreich gewesen, hätte sie behauptet, es wäre kein
Fehler gewesen. Aber das war es nunmal, wenn auch sie glaubte, dass er
vertretbar und unbedenklich war. Die Gradwanderung zwischen Ehrlichkeit und
aufbauender Bestätigung, die sie aber oft für unehrlich hielt, forderte sie in
Konversationen nicht selten zu sehr heraus. Sie überlegte einen Moment, ob sie
ihre Einschätzungen über die Unbedenklichkeit mitteilen sollte und tat es dann
nicht, weil sie nicht sicher war, inwiefern sie durch ihr Vertrauen in Rash
eingefärbt waren.

Und doch: Als Janasz wieder aufblickte, wirkte er immer
noch nicht glücklich, aber
vielleicht doch ein wenig erleichtert. "Was machen wir jetzt?"

Sindra war keine Person, die, solange es nicht nötig war, Strategien
spontan plante. Sie durchdachte alles lieber. "Eigentlich
vermute ich einen relativ harmlosen Hintergrund für das Ganze, denn
hätte jemand einen Anschlag auf Smjer geplant, dann vermutlich
mit etwas, was zielführender ist, als Auslösen einer Reaktion auf
eine Unverträglichkeit. Merkwürdig ist es aber schon." Sindra
unterdrückte den Reflex nach ihrer leeren Teetasse zu
greifen. "Nehmen wir trotzdem einmal das Szenario an, es gäbe
ein oder mehrere Personen, die Smjer etwas antun möchten. Und
sagen wir, diese haben vermutet, dass es
mehr als eine Unverträglichkeit wäre, dass es tödliche Folgen
haben würde. Und sagen wir, dieses Gespräch wird nicht belauscht, was
wir sicher zu stellen versucht haben. Dann kann ein weiterer
Vergiftungsversuch über Essen stattfinden. Wenn
du aber jetzt auf einmal anfängst, dich nicht mehr nach dem Essenaufteilen
erst zu waschen und umzuziehen, sondern bei den Schalen bleibst, fällt das
auf. Auch, wenn eine Person sie bewacht, die sich sonst nie in der Nähe der
Kombüse aufhält. Damit fällt Smjer wohl auch raus. Es sei denn, wir
beschädigen dort etwas, was er dann reparieren muss. Ich mache mir
Gedanken mit ihm."

Sindra runzelte nachdenklich die Stirn. Vielleicht wäre das auch zu
verdächtig, wenn ausgerechnet das potenzielle Ziel des potenziellen
Anschlags am möglichen Tatort wäre. Vielleicht machte sie
sich aber auch zu viele Gedanken. Sie hatte den Eindruck, dass sie
an der falschen Baustelle arbeiteten.

"Es sollte, denke ich, eine Person bei den Schalen bleiben, der du
absolut vertraust. Nur für den Fall der Fälle. Klingt
es für dich machbar, zusammen mit Ashnekov
eine Ausrede zu finden, warum er zufällig in der Nähe der Schalen
sein muss, während du dich wäschst und umziehst?"

Janasz nickte. "Das traue ich mir zu." Dann runzelte er die
Stirn. "Oder Ashne. Ashne traue ich das zu. Er würde es auch
tun und eine überzeugende Ausrede finden, ohne dass ich ihm
erkläre, warum."

Sindra lächelte ein wenig, als sie unter Janaszs Fassade
ein wenig Glückseligkeit erkannte. Ashne und Janasz waren als
politische Geflüchtete zusammen an Bord gekommen. Zu dem Zeitpunkt
war ihre Beziehung noch sehr jung gewesen. Es war schön zu
beobachten, wie sie sich von Tag zu Tag noch vertiefte und sie
allmählich an Angst verloren. Dass Janasz heute zu ihr
gekommen war, war ein schönes Zeichen.

"Ich werde versuchen, beim Schnack mit dem Rest der
Crew ein paar passende Fragen unterzubringen, aufmerksam zuhören und
beobachten, um mir einen Reim
darauf zu bilden.", versprach Sindra. "Hast du ein weiteres
Anliegen oder möchtest du etwas ergänzen?"

---

Als Sindra sich am Abend zu Marah ins Segelboot quetschte, fragte
sie sich, ob sie eigentlich besser an Bord hätte bleiben
sollen. Aber dann wiederum vertraute sie Smjer, der das Kommando
dann übernahm, hatte ihm alles weitergegeben, hatte mit einigen
an Bord gesprochen und war sich relativ sicher, nicht viel mehr
tun zu können. Auszeiten waren wichtig, hatte Marah erklärt. Wenn
es nicht gerade wörtlich brannte oder so etwas, dann brauchte auch Sindra
Erholung.

Und eine Person zum Reden, bei der sich Sindra sicher fühlte.

Marah umrundete die halbe Insel und schoss gegen den Wind auf, dass die Segel
flatterten, dann rutschte sie von Bord.

"Schauen wir nicht zuerst die Gärten an?", fragte Sindra.

"Du hast was auf dem Herzen.", widersprach Marah mit einem Grinsen. "Du
meintest zumindest vorhin, du möchtest mit mir reden. Mir
schien das wichtiger. War das voreilig?"

Sindra schüttelte den Kopf. "Das habe ich wohl. Du hast recht."

Marahs Formulierung berührte sie heute besonders. Sie bedeutete, dass
Marah davon überzeugt war, dass Sindra ein Herz hatte, auf dem
etwas liegen konnte. Das fühlte sich warm an.

Marah hielt die Jolle mit aller Muskelanstrengung, die sie aufbringen
konnte, als Sindra ausstieg, damit die Jolle dabei nicht umkippte. Sie
hatten es inzwischen so oft geübt. Anfangs waren sie häufig dabei gekentert, aber
inzwischen passierte es nur noch selten. Sindra zog die leichte Jolle das
Ufer hinauf. Es war weich und von einer feuchten Moos-Algen-Mischung
überwuchert. Marah robbte neben ihr an Land und holte das Segel ein. Anschließend
legten sie sich nebeneinander auf das Grün. Es roch nach Muscheln und
Seetang. Sie beide liebten den Geruch.

Sindra berichtete Marah vom Gespräch mit Janasz und der Entdeckung mit
der Schublade. Marah hörte nachdenklich zu.

"Die meisten an Bord wissen, dass du in der Schublade keine geheimen
Sachen lagerst.", sagte Marah schließlich. "Vielleicht hilft das als
Ausschlussverfahren, um nur diejenigen zu verdächtigen, die es nicht
wissen."

Das stimmte nur bedingt. Es waren zumindest keine geheimen Sachen, die
für die Überfälle oder im politischen Kontext interessant gewesen
wären. "Ich verstecke manchmal Gegenstände darin, die in Spielen
mit Rash zum Einsatz kommen." Sie ging nicht ins Detail. Marah gingen
Rashs Fantasien nichts an.

Marah nickte bloß. "Ich mag Rash schon ein wenig.", sagte sie. "Aber
Rash gehört im Prinzip auch zum Fußvolk. Rashs Herkunftsvolk gehört
zu den Unterdrückungsmächten. Ich
traue Rash nicht, und ich frage mich manchmal, ob
das nur daran liegt, dass ich mich bei Elben schwertue, oder ob
Rash auch was verbirgt. Außerhalb irgendwelcher Spiele."

Sindra sah nicht, wieso es mehr Sinn ergeben würde, Rash zu
verdächtigen, als irgendwen sonst. Sie verstand Marahs Abwehr gegen
Elben, zumindest jenen, die zu tyrannisierenden Landvölkern gehörten, und
hoffte, dass sie nicht von einem für Marah gerade wichtigem Thema
ablenkte, wenn sie dem Gespräch nun die Wendung gab, die
sie brauchte. "Warum traust du mir?", fragte Sindra.

Marah blickte sie geradezu entgeistert an. "Warum zur Rochade
sollte ich dir nicht trauen? Du gehörst doch eigentlich
sogar zu einem gejagten Volk. Oder?"

Sindra zuckte und sagte nichts, blendete für einen Moment alles
aus.

Marah merkte vielleicht, dass etwas nicht stimmte. Sie
ging nicht weiter darauf ein und wirkte mit
einem Mal nachdenklich. "Das ist, was dich am
Gespräch mit Janasz eigentlich wieder belastet, oder? Dass
du glaubst, du würdest dich nicht vertrauenswürdig verhalten."

Sindra nickte. "Ich habe gesagt: Ich versuche mir einen Reim darauf
zu bilden.", gab sie wieder. "Ist das eine angemessene Redewendung? Wenn
es darum geht, dass eine Person umgebracht hätte werden können, oder dass
das noch passieren könnte?"

"Und du fühlst auch wieder weder Horror noch Angst.", ergänzte
Marah.

"Kein bisschen.", stimmte Sindra zu. Sie zögerte nicht einmal mehr, wenn
sie es Marah gegenüber zugab. "Es fügt sich auch nicht zu einem Bild. Es
könnte sein, dass etwas Merkwürdiges an Bord vor sich geht, und es ist
gut, dass wir Ungereimtheiten früh sehen, selbst wenn sie sich als
harmlos herausstellen. Aber ich fühle Neugierde, vielleicht Skepsis. Angst
oder Horror? Keine Spur."

Marah strich ihr mit ihren kleinen Händen über die Wange. "Irgendwann
solltest du dich mal mit Jentel unterhalten.", sagte sie. "As ist
ein bisschen, wenigstens ein bisschen genau so. Dann bist du nicht so
allein."

"Ich bin nicht allein.", sagte Sindra sanft. Strich mit einem Daumen
über die Hand an ihrer Wange. Ein Daumen, der den ganzen Handteller
ausgefüllt hätte. "Ich habe dich."

"Aber ich fühle Angst und all das.", sagte Marah. "Das ist unwichtig, dass
wir da unterschiedlich sind. Ich
traue dir. Du hast Gefühle. Sie sind halt nur anders als bei
den meisten anderen. Das macht dich nicht zu einer schlechten Person."

"Warum nicht?", fragte Sindra.

"Warum sollte es?", fragte Marah. "Du bist doch sonst so logisch. Warum
solltest du dahingehend normal sein müssen, wenn doch alles, was
du tust und entscheidest, Bände für dich
spricht. Wem nützten die Horrorgefühle oder die
Trauer, die du nicht fühlst?"

Sindras Daumen auf Marahs Hand wurde sanfter. Sie antwortete nicht. Es
war auch nicht nötig.

Marah hatte recht und trotzdem war es ein komplexes Thema. Trotzdem
war da die Angst, dass Janasz sie nun für herzlos halten könnte. Weil
sie so sachlich und mit zu lockeren Worten über ein Thema sprach, in
dem es um Lebensgefährdung ging. "Kannst du dir auf die Sache mit
der Galnuss einen Reim machen?"

"Es ist schon einmal passiert, aber nicht mit Galnuss und nicht bei Smjer und
nicht so dramatisch.", sagte Marah. "Als Aga frisch an Bord war, hatte
Janasz die Ziege vergessen. Rash hat dann von der Speise, die es an
dem Tag gab, jeder Person einen Löffel stibitzt und in eine Extraschale
getan und diese der Ziege gebracht."

"Hat Aga das vertragen?", fragte Sindra grinsend.

"Ja, an dem Tag gab es einen Gemüseeintopf mit karamellisierter Zwiebel. Das
fand Aga ganz gut.", berichtete Marah weiter. "Ich vertrage
aber nicht so gut zu rohe Zwiebel. Also, ich vertrage sie schon, aber
ich habe nach dem Essen den ganzen Tag einen unangenehmen Geschmack
im Mund. Daher hatte Janasz da für mich besonders drauf
Rücksicht genommen, und die hinterher hinzugefügten karamellisierten
Zwiebeln für mich extra lange durchgaren lassen. Jedenfalls ist Rash
beim Umfüllen eine von den Zwiebeln der anderen Schalen in meine
geraten. Rash hat das aufgeklärt und sich entschuldigt, keine weiteren
schlimmen Folgen. Seitdem kümmert sich Janasz auch noch um das Zubereiten
einer Mahlzeit für Aga. Eigentlich hatte Rash damit entlasten wollen. Janasz
arbeitet zu viel."

"Oh.", machte Sindra. Es waren viele Informationen auf einmal, aber
die letzte blieb ihr am präsentesten. "Können wir Aufgaben besser verteilen? Ich
dachte, Kanta würde ihm jetzt helfen."

"Aber zuvor hatte heimlich Rash für ihn die Arbeit erledigt, die
Kanta jetzt macht.", sagte Marah. "Sie
wollten das nicht sagen, weil es gegen deine Aufteilung war, und ich
hätte davon nicht wissen sollen. Es ist nur wegen des einen Zwiebelrings
damals rausgekommen. Ich glaube, es wäre besser, wenn du sie momentan noch
eine Weile so weitermachen lässt, und dich später erst um
das Überlastungsproblem kümmerst. Es ist, wenn gerade keine Überfälle
anstehen, auch nicht so schrecklich schlimm."

Sindra erschloss sich der Vorschlag noch nicht so ganz, warum es
Sinn ergeben würde, abzuwarten, wenn ein Crewmitglied überlastet
war. Vielleicht konnte die Überlastung sogar auch ein Grund für
die Sache mit der Galnuss sein. Auf der anderen Seite war sich
Janasz wirklich sicher gewesen, aufgepasst zu haben. Es war
müßig, zu grübeln. Es gab noch zu wenig Daten für eine
Schlussfolgerung.

Sindra beließ es trotzdem dabei. Sie hatte
den Eindruck, dass Marah einfach noch ein paar weitere
Details wusste, die eigentlich nicht für ihre Ohren bestimmt waren. Also
nickte sie. "Sag mir gern Bescheid, wenn ich etwas anordern soll."

Marah nickte. "Jedenfalls fiele mir zwar kein Grund ein, warum Rash
oder irgendwer sonst wieder von allen einen Löffel in eine Extra-Schale
umgefüllt haben sollte, aber wenn die Dosis
so klein war, dass sie fast nichts ausgelöst hat, dann könnte es wieder
auf die gleiche Art passiert sein."

Sindra drehte sich auf den Rücken und legte die Arme nachdenklich
unter den Kopf. Das waren spannende Gedanken, aber ihr Gehirn
brauchte eine Pause, um das alles zu sortieren. "Ich habe dich sehr gern."

"Ich dich auch.", sagte Marah. "Ganz furchtbar gern. Ganz entsetzlich
gern. So unfassbar gern."

Sindra konnte nicht anders, als über das ganze Gesicht zu grinsen.
