\BefehlVorKapitelDFDM{Rash-n.png}{Rash schreibt die Nachrichten, die von Bord gehen.}{BDSM mindestens angedeutet.}

Befehle
=======

\Beitext{Rash}

Rash hatte keine Ahnung, wie Amira an Bord gekommen war, oder
wer diese Person auch nur ungefähr war. Sindra hatte sie nicht
vorgestellt. Normalerweise stellte Sindra Personen, die neu
an Bord waren, kurz der Crew vor. Dieses Mal nicht.

Amira war so geisterhaft mitten auf See an Bord aufgetaucht, wie
ihre Überfälle auf die Forschungsschiffe wirken mussten. Sicher
war Amira schon länger da gewesen. Oder sie war über
das Nixendeck eingetaucht worden. Und es hatte nicht zu
Drama geführt, als sie dann plötzlich offiziell da war. Aber
auch nicht zu Erklärungen.

Seitdem hatte Rash keinen Auftrag mehr bekommen, eine
Nachricht zu verfassen. Rash verfasste nicht jeden
Tag Nachrichten. Auch drei Tage am Stück ohne Postaufgabe
waren schonmal vorgekommen. Aber das Muster passte
nicht.

Etwas stimmte nicht.

Und Rash wurde das Gefühl nicht los, dass es etwas
mit Rash zu tun hatte. Als eine der Steuerpersonen wurde
Rash vieles frühzeitig anvertraut. Rash hatte nie darum
gebeten und war nicht böse darum, dass das aktuell nicht
der Fall war. Aber es fühlte sich unheimlich an, dass
Rash nicht einmal in Kenntnis darüber gesetzt worden
war, dass sich etwas geändert hatte, sodass diese Änderung
auch wie Zufall, wie Einbildung aussehen konnte.

Deshalb erleichterte es Rash auch, als Amira Rash aufforderte,
mit ihr zu Sindra in die Kapitänskajüte zu kommen. Rash
hätte eine Unterredung mit der Kapitänin ohne Amira
bevorzugt. Amira beobachtete Rash, das hatte Rash bemerkt. Und
wieder war es die Frage, ob Rash es sich nur einbildete oder
es richtig wahrnahm, dass Amira Rash in besonderer
Weise beobachtete, und nicht bloß ebenso wie alle anderen
Crewmitglieder auch. War Amira zum Beobachten hier? Im
Auftrag von Sindra?

Die Kapitänin öffnete nicht selbst die Kajütentür wie
sonst. Rash hörte ein "Kommt rein!" durch die Tür. Amira
öffnete sie und trat hinter Rash.

Die Kajüte sah vertraut aus, wie sonst. Abgesehen von
Sindra, die auf dem Stuhl saß. Zwar wie meistens
mit angewinkelten Beinen, aber sonst setzte sie sich erst
hin, wenn die anderen Personen den Raum betreten hatten
und die Tür geschlossen war. Etwas stimmte ganz
und gar nicht.

"Setz dich.", forderte die Kapitänin eine Person
von ihnen auf.

Rash war nicht ganz sicher, welche. Rash drehte sich zu Amira
um, hätte ihr den Sitzplatz überlassen. Amira
hob die Brauen.

"Setz dich, Rash.", konkretisierte Sindra den Befehl, dieses
Mal etwas energischer.

Rash schritt durch den Raum und setzte sich mit geradem
Rücken Sindra gegenüber. Amira schloss die Tür und trat
schräg hinter Sindra, stand ganz still da, mit wachen
Augen. Rash konnte den Blick nicht von ihr lösen. Rash mochte
die Gelassenheit, die Amira ausstrahlte. Es war eine
andere Gelassenheit als Sindras. Sindras Gelassenheit
war dazu da, andere aufzufangen, zu beruhigen und
in Situationen anzuführen. Amiras Gelassenheit kam
Rash eher so vor, als wäre sie vorbereitet auf
ungefähr jegliche Situation, die da kommen möge.

"Du bist üblicherweise gut darin, zu schließen, wenn
etwas nicht stimmt.", sagte Sindra.

Rash nickte langsam.

"Ich vertraue dir. Uneingeschränkt." Sindra holte
tief Luft und ließ sie langsam entweichen, wie um
dem Gesagten mehr Raum zu geben. "Es gehen Dinge vor sich,
die ich nicht verstehe, und ich möchte dich deshalb
auf eine Weise verhören, als täte ich es nicht. So
sehr es mich schmerzt, ich möchte in Frage kommen lassen,
dass mein Vertrauen abwegig wäre, und Methoden anwenden,
die dich aus der Reserve locken können."

Rash runzelte die Stirn und nickte. Ein Gefühlsball
aus Angst und Liebe für Sindra verzwirbelte sich in
Rashs Bauch. "Ich vertraue auch dir. Und ich werde alles
mit mir machen lassen, was du für hilfreich hältst."

"Du kennst Amira bereits.", leitete Sindra
ohne weitere Umschweife ein. Die
Kapitänin. Heute hatte sie Rash gegenüber sehr stark
diese Rolle. "Aber du weißt nicht, warum sie hier ist,
nehme ich an?"

War das eine Art Prüfung? "Sie beobachtet.", erwiderte
Rash.

<!-- zu viel? -->
Sindra blickte sich zu Amira um. Überrascht. Sie
versuchte, es zu verbergen, aber das klappte bei
Rash nicht so gut. "Hast du die Crew weiter beobachtet?", fragte
Sindra.

Amira nickte. "Es tut mir leid, wenn das nicht in
Ordnung war."

"Vor allem mich.", fügte Rash hinzu. Rash war sich
nur einigermaßen sicher, dass es stimmte, aber vielleicht
würde es durch die Behauptung gleich herauskommen.

<!--Gibt es was besseres als "fair"?-->
Im nächsten
Moment bereute Rash die Worte. Es war nicht so fair, wie
Rash sein wollte. Aber in der Anspannung war der Mund
schneller gewesen, als Rash sich hatte bremsen können.

Sindra hob die Brauen. Amira senkte den Blick. Sie
tat Rash fast leid. Rash hätte für möglich gehalten, dass
Sindra nun irgendetwas Tadelndes sagen würde. Es
war nicht Rashs Zielsetzung gewesen, sicher nicht.

Die Überraschung der Kapitänin bedeutete immerhin, dass
das Beobachten nicht von ihr veranlasst worden
war, und das erleichterte schon.

<!--Trotzdem hatte es sicher einen Grund, warum Amira gerade
Rash beobachtet hatte.-->

Sindra beließ Rashs Behauptung unkommentiert. "Amira
ist Assassinperson.", richtete sie sich wieder
an Rash, wand sich dann aber noch einmal um. "Ist die
Bezeichnung so korrekt?"

Rash fragte sich einen Moment, ob Sindra mit Amira
abgesprochen hatte, dass sie das äußern durfte, was
Sindra normalerweise tat. Amira war überhaupt nicht glücklich mit der
Gesamtsituation. Zumindest war das Rashs erster
Gedanke, als Rash die Körperhaltung zu lesen
versuchte. Natürlich kannte Rash Amira kaum. So oder
so mochte Rash nicht, davon Ursache zu sein. Das würde Amira
Rash gegenüber nicht gerade gut stimmen. Und eigentlich
fand Rash Amira sympathisch. Vielleicht war das
zu viel gesagt. Amira hatte einfach etwas an sich, das
Rash sehr gefiel.

"Assassinan. Ein oder das Assassinan.", sagte
Amira nach einigem Zögern. "Jentel
meinte, dass das eine Neo-Wortendung ist -- eine
neu erfundene Wortendung --, um Personen
kein Geschlecht zuzuweisen und ich mag sie."

Rash lächelte und bemerkte die Veränderung in Sindras
Gesicht zu einem glücklich-mitfühlenden Ausdruck.

Sindra wandte sich wieder Rash zu. "Amira ist
Assassinan. Und hat einen Mord verhindert. An mir. Indem
sie ihn nicht ausgeführt hat, aber letzteres müssen wir
nicht überbetonen."

Wie Amira wohl mit Sindras Gesprächsführung zurechtkam? Es
war diese flapsige Art, in die Sindra manchmal fiel, einfach
so.

Rash nickte, und fragte sich, ob erst einmal
nur zuhören dran war oder ob Rash auch schon Fragen stellen
durfte. Rash entschied sich für Abwarten, als Sindra
ohne aufzustehen einen Lederumschlag aus einer Schublade
hervorholte. Sie öffnete ihn und legte ihn so unter
Rashs Nase, dass Rash die eigene Schrift auf einem
Papier richtig herum lesen konnte.

Einen Moment verhakten sich Rashs Gedanken. Nicht, weil
Rash in stressigen Situationen nicht hätte denken
können, sondern weil zu viele Gedanken gleichzeitig
ins Bewusstsein drängen wollten.

Rash hatte, seit Rash bei der Crew war, nicht mit
einem Kohlestift geschrieben.

Die Kapitänin holte Rash aus den Gedanken. "Denk
laut."

Das war ein Befehl. Rash schloss für einen Moment
die Augen. "Sehr wohl.", sagte Rash. "Es fällt mir
schwer. Aber ich versuche es."

"Ich weiß. Gib dein Bestes, wie immer.", sagte
Sindra ruhig, aber unnachgiebig.

Etwas kribbelte in Rash, was da nicht hingehörte. Sindra
war selten so klar über andere bestimmend, außerhalb
von kinky Kontexten. Oder Situationen, in denen
es schnell gehen musste, oder schlimmere Gefahr
lauerte vielleicht. Vielleicht war jetzt so
eine Situation. Rash verdrängte den Gedanken
rasch, um ihn nicht auch aussprechen zu müssen. Darum
ging es schließlich nicht.

"Unverkennbar ist das meine Handschrift.", begann
Rash. "Ich nehme an, deshalb hast du mich zu dir gebeten."

Sindra bestätigte dies mit einem Nicken und einem
zustimmenden Geräusch.

"Ich habe das nicht geschrieben.", sagte Rash. Aber
Rashs Finger griffen wie von selbst nach dem Papier. Rash hielt
es sich näher vor die Augen. Etwas stimmte nicht. "Da
stimmt etwas nicht.", murmelte Rash also. "Ich bin
mir nicht sicher, ob ich das geschrieben habe. Eigentlich
bin ich mir sicher, dass ich seit Langem nicht mehr mit
Kohlestiften geschrieben habe. Seit ich hier an Bord
bin spätestens nicht mehr. Das ist nun wie lange? Vier
Jahre?" Rash war fast von Anfang an dabei gewesen.

"Vier und ein halbes.", bestätigte die Kapitänin.

Sie hatten nicht von Anfang an auf die selbe Weise
Schiffe überfallen. Der Dampf war zum Beispiel neu. Oder
dass sie nach im Sturm verunglückten Schiffen Ausschau
hielten und die Überreste so umgestalteten, dass sie
von einem brutalen Überfall hätten stammen können, und jene
Überreste dann vor Küsten größerer Städte anschwemmen ließen. Das schürte
Angst und half bei Überfällen.

"Du vergisst, laut zu denken.", ermahnte Sindra.

Wieder zuckte ein untergründiges Verlangen durch Rashs Körper. Nach
Sindras Hand um Rashs Hals oder über Rashs Mund. "Ich
habe auch Gedanken im Kopf, die mit dem Brief überhaupt
nichts zu tun haben. Ich glaube nicht, dass sie hier
irgendwie zuträglich sind, oder hergehören. Aber
wenn du darauf bestehst, spreche ich sie auch aus."

"Ich bestehe darauf.", machte die Kapitänin klar.

Rash wusste, dass Rash durchaus einen Befehl verweigern
könnte. "Ich weiß, dass ich nicht muss.", sagte Rash also.

"Was soll das?", mischte sich Amira plötzlich ein. "Warum
denkst du, dass du einem Befehl der Kapitänin nicht
Folge zu leisten hättest?" Das Assassinan hatte plötzlich
ein Messer in der Hand.

"Ich habe wiedergegeben, was ich gedacht habe.", sagte
Rash ruhig. "Mir wäre neu, dass Sindra mich mit
dem Leben oder irgendetwas bedroht, was mir ernsthafte
Schwierigkeiten machen würde."

"Das werde ich auch nicht.", sagte Sindra, keineswegs
sanft und wandte sich an Amira: "Steck das Messer weg
und misch dich nicht ein!"

Amira gehorchte. Glücklich wirkte sie aber nicht.

"Zuvor habe ich kurz Revue passieren lassen, wie sich
unsere Angriffstechniken über die Zeit, die ich da bin, geändert
haben.", sagte Rash. "Und deine Befehle erregen mich."

Rash konnte sich ein Grinsen nicht verkneifen, als
Amiras Gesichtsausdruck ihr für einen kurzen Moment entglitt. War
es Empörung? Aber dann war Amira wieder gelassen, als
wäre nichts gewesen.

Sindra hingegen wirkte eine Spur weicher. "Es wäre
besser, wenn Erregung gerade außen vor bleibt. Ich
nehme an, du kannst es nicht vermeiden?"

"Ich vermeide es, so gut ich kann.", versicherte Rash. "Es
sind kurze Erinnerungen. Sie verfliegen rasch."

Sindra nickte einmal. "Weiter im Text.", orderte sie an.

Rash senkte den Blick wieder auf das Papier. "Wenn ich die
Buchstaben ansehe, machen sie etwas mit mir. Etwas fühlt
sich so an, als würde ich mich daran erinnern, sie geschrieben
zu haben.", sagte Rash. Das war nicht gut. "Das macht mir
ein wenig Angst." Rash blickte kurz auf in Sindras
Gesicht. Aber die Kapitänin blickte Rash einfach weiter
an mit dieser Strenge, unter die aber vorhin ein Stück
Wohlwollen geraten war, das immer noch da war. Und vielleicht
ein wenig Lust. Rash seufzte tief. "Ich denke schon wieder
daran. Soll ich es wirklich ausführen?"

"Das würde es schlimmer machen, nehme ich an?", fragte
Sindra.

"Es geht.", sagte Rash. "Es lenkt ab. Was es schlimmer macht,
ist, dass du auch daran denkst."

Sindra runzelte die Stirn. "Tatsächlich nicht, nein.", widersprach
sie. Dann seufzte sie. "Es wäre wohl naheliegend gewesen, da
hast du recht. Ich sollte wissen,
was es mit dir macht, zumal es Thema war. Es tut mir leid, ich
wollte das nicht. Wir lassen das Thema nun bleiben, aber treffen uns heute
Abend, wenn du möchtest."

Eine innere Vorfreude durchströmte Rashs Körper. Nur Freude. Und
vor allem, weil das sehr sicher hieß, dass Rash heute
Abend noch leben würde. Als Amira das Messer gezückt hatte, hatte
Rash begonnen, eine untergründige Angst zu entwickeln. Und obwohl
Rash rational wusste, dass das außer Frage stand, hatte sich
diese irrationale Angst entwickelt, die Rash jetzt erst wirklich
realisierte. "Ich habe unsinniger Weise Angst, dass
ich als Gefahr eingeordnet werde und deshalb auf die ein
oder andere Art beseitigt.", sprach Rash aus. Irrte Rash sich, oder
war da eine gewisse Bereitschaft in Amiras Blick? Rash
hoffte sehr, sich zu irren. "Ich verstehe aber sehr gut, wenn
ich mit diesem Material als eine eingestuft werde. Ich werde
mich nicht dagegen wehren, aber hoffe, dass es auf höchstens
Gefangennahme hinausläuft, bis die Sache geklärt ist."

"Höchstens.", beruhigte Sindra Rash.

"Das erleichtert mich sehr.", sprach Rash die Gedanken aus, die
Rash im Normalfall für sich behalten hätte. Rash blickte
erneut auf den Zettel. "Ich würde gern lesen, aber ich
kann nicht gleichzeitig lesen und reden."

"Kannst du vorlesen?", fragte Sindra.

Rash nickte. Und tat es, als Sindra den Befehl dazu gab. Rash
pausierte nur einen Moment, als Rash fertig war. Es war
kein langer Brief gewesen. "Das habe ich nicht geschrieben.", wiederholte
Rash. "Aber", Rash stockte. Das war etwas, was Rash wirklich
ungern zugab. "Aber das ist eher der Stil, in dem ich schreiben
würde, als ich es tatsächlich tue."

"Wie meinst du das?", fragte Sindra.

"Ich kann nicht gut formulieren.", gab Rash zu. "Ich", Rash
brach wieder ab. Wenn Rash nun weitersprechen würde, würden
sie Kanta vielleicht auf ähnliche Weise verhören. Und Kanta
fürchtete sich immer noch vor Sindra wegen des Verhörs bei
ihrer Ankunft. "Deine Art, mit Personen umzugehen, können
Personen verschieden gut ab.", sagte Rash.

"Hast du etwas ausgelassen, oder ist der Gedankensprung
so in deinem Kopf passiert?", fragte Sindra.

Rash seufzte erneut. Sindra hatte recht. "Kanta hilft mir
beim Formulieren.", gab Rash schließlich zu. "Kanta weiß
nicht, wie wir die Post verschicken. Darauf achte ich
sehr. Und Kanta hat mir ganz sicher nicht diesen Brief
diktiert.", fuhr Rash fort.

"Hast du Angst, dass wir als nächstes Kanta verhören?", fragte
Sindra schlussfolgernd.

"Ja.", bestätigte Rash. "Was ich auch verstehen kann. Aber
das letzte Mal, als du mit ihr geredet hast, hat das bei
Kanta eher Traumata hochgeholt." Rash hatte das nicht so
direkt teilen wollen. Aber der Befehl, dass Rash alle
Gedanken darlegen sollte, galt noch.

"Ich verstehe.", sagte Sindra. Klang aber eher nicht so, als
würde sie viel Einsehen haben. "Wie hast du vorher
formuliert, bevor Kanta an Bord war?"

"Es hat mich viel mehr angestrengt und manchmal hat
mir Janasz geholfen.", berichtete Rash. "Ich war froh, als
Kanta da war. Ich habe so viel von ihr gelernt." Rash
verkrampfte sich. Der nächste Gedanke war wahrscheinlich
sehr wesentlich. "Ich vertraue Kanta nicht. Ich mag sie. Ich
habe einen Reflex, sie zu beschützen, weil sie sehr
viel Schlimmes erlebt hat. Aber mir wird nun klar, dass es
keine gute Idee war, sie mir helfen zu lassen."

"War es nicht.", bestätigte Sindra.

"Was ich nun sage, klingt wie eine Verteidigung.", berichtete Rash
weiter. "Und vielleicht ist es eine, aber ich würde sie nicht
anbringen, wenn nicht der Befehl im Raum stünde, alles zu
sagen, was ich denke." Rash fing an, den Befehl zu mögen. Es
war seltsam erleichternd. Der Kapitänin gegenüber. Amira gegenüber
weniger.

"Fahre fort.", orderte Sindra an. "Ich wäre auch sonst nicht
ungewillt, deine Verteidigung zu hören."

"Die Nachrichten, die rausgehen, sind nicht sehr
genau. Sie dienen vor allem dazu, zu sagen, wie gesund wir sind, wer
da ist, einigermaßen kodiert, und, dass wir überhaupt in der
Lage sind, zu reagieren. Noch ein paar Sachen.", sagte
Rash. "Davon weiß Kanta alles. Das sind Informationen, die
allen an Bord klar sind. Es ging nur um Formulierungen. Ich
habe nicht mehr Details geschrieben, sicher nicht aus Versehen
weiteres Papier mitgeschickt. Ich habe, gerade weil
ich Kanta nicht vertraue, sehr gut auf alle notwendigen
Utensilien fürs Schreiben achtgegeben. Ich dachte, so könne
nichts passieren."

"Warum vertraust du Kanta nicht?", fragte Sindra.

Das war eine spannende Frage, weil Rashs
Antwort darauf verschwommen war und Rash selbst nicht
befriedigte. "Zum einen brauche ich lange, bis
ich einer Person vertraue. Es ist nicht unbedingt ein
besonderes Misstrauen. Höchstens ein wenig.", antwortete
Rash. "Aber eben doch ein wenig. Ich weiß nicht genau, warum. Ich
glaube, wegen Ashnekov und Janasz." Rash machte eine
bewusste Pause und atmete dabei langsam ein und aus. Inzwischen war Rash sehr
angespannt. Rash verriet außerordentlich ungern Leute, und
auch, wenn es das nicht zwangsläufig war -- Rash
sagte ja bloß, dass Kanta beim Formulieren geholfen hatte --,
fühlte es sich so an. "Kanta ist verliebt in Ashnekov. Sie
hat nicht mitbekommen, dass Ashnekov und Janasz ein
Paar sind."

"Es würde an sich nichts dagegen sprechen, wenn es
eine offene Beziehung ist. Was ich nicht weiß.", sinnierte
Sindra.

"Es geht mir nicht darum.", sagte Rash. "Das weiß ich
auch nicht. Es könnte auch sein, dass Ashnekov schwul ist, was
ich auch nicht weiß. Aber Kanta wüsste wohl, dass sie
ein Paar wären, wenn sie je mit Ashnekov über ihre
Gefühle gesprochen hätte. Sie ist sich aber recht sicher, dass
Ashnekov ihre erwidert, weil er so lieb zu ihr ist. Sie
kann sich nicht vorstellen, dass ein Mann einfach lieb
zu ihr ist, weil es sich um eine liebe Person handelt."

"Und das macht dich misstrauisch?", fragte Sindra.

"Das wäre zu viel gesagt. Ich fühle mich auch ungerecht
deswegen. Die Überlegungen sind roh, unfertig und
befriedigen mich selbst nicht.", antwortete
Rash. "Auf der Argumentationsgrundlage
würde ich keiner Person misstrauen. Aber in der Art, wie
Kanta darüber redet, fühlt sie sich als Charakter
für mich so an, dass ich ihr lieber nicht zu viele Informationen
geben würde. Das ist alles."

"Danke, dass du alle Gedanken mit mir geteilt hast.", sagte
Sindra.

"Ist das eine Aufhebung des Befehls alles zu sagen, was ich
denke?", fragte Rash.

Sindra nickte und hielt Rash die Hand hin, um den Brief wieder
entgegen zu nehmen.

Obwohl Rash irgendwann angefangen hatte, es zu mögen, war
Rash nun auch sehr erleichtert, dass Rash wieder freier denken
konnte. Rash zögerte allerdings, den Brief
auszuhändigen. "Wegen des Erinnerungsgefühls, das
ich nicht ganz einordnen kann, würde ich den Brief gern noch länger ansehen."

"Jetzt?", fragte Sindra.

"Am liebsten würde ich ihn mitnehmen.", gestand Rash.

"Frühestens nach meinem Verhör mit Kanta.", bestimmte
Sindra.

Rash reichte ihr das Papier ohne weiteres Zögern und stand auf.

"Alles, was in diesem Raum heute gesagt wurde, und nicht
schon außerhalb bekannt ist, bleibt in diesem Raum.", bestimmte
Sindra. "Niemand wird informiert, dass dieses Gespräch
stattgefunden hat."

Rash nickte. Damit hatte Rash gerechnet. Rash blickte zu Amira
hinüber. Rash hatte keine Zweifel, dass Amira schweigen konnte.
Rash hatte das Bedürfnis noch etwas zu ihr zu sagen, und entschied
sich schließlich dafür, von den Gefühlen am Anfang zu reden: "Es
tut mir leid, sollte ich dich in eine Situation gebracht haben, in
der du dich unbehaglich gefühlt hast."

Sindra schmunzelte. "Das war hauptsächlich ich.", korrigierte
sie.

"Das mit dem Beobachten.", konrektisierte Rash.

"Das war gut.", erwiderte Amira und überraschte Rash damit. "Ich
habe nie gelernt, was Privatsphäre ist. Ich habe immer beobachten
müssen. Es gehörte zu meiner Arbeit, sowie als fester Bestandteil
in mein Leben. Ich möchte so nicht mehr sein, aber dazu muss
ich auch wissen, was ich mir da angeeignet habe."

Rash spürte den Drang, zu ihr hinüberzugehen und sie in
den Arm zu nehmen. Bezüglich Amira fühlte Rash kein zusätzliches
Misstrauen über Rashs normales Maß hinaus. Es
hätte viel eher dort hingehört. Einem
Assassinan sollte Rash nicht einfach vertrauen. Aber
Rash hatte bei ihr nicht den Eindruck, dass sie versuchte, etwas
zu überspielen.
