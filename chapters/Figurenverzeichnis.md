\neueSeite

\Begriffliste{Einzug}{Personenverzeichnis}

\setzeBegrifflistenStil{Einzug}

\setzeBegrifflistenEinzugtiefe{40.5}
\setzeBegrifflistenJustierungRechts{l}


\BefehlFigurInVerzeichnis{1.6}{Kanta-n.png}{Kanta}{Und der Ehering war weder einfach zu beschaffen gewesen, noch hatte sie Geld dafür gehabt, eigentlich. Sie kramte ihn aus der kleinen Tasche, die sie dafür in ihren Rock eingenäht hatte. In jeden ihrer Röcke. Ein Ehering für eine Ehe mit sich selber. Und sie wollte sich eigentlich nicht von sich selbst scheiden lassen.}

\vertikalDehnen

\BefehlFigurInVerzeichnis{1.6}{Jentel-n.png}{Jentel}{Wieso schwamm da ein Kopf direkt neben ihr im Wasser im nirgendwo. Ohne Boot. Dachte sie, als sie die Erkenntnis traf: »Du bist eine Nixe.«\Zeilenumbruch»Es haben schon Elben länger gebraucht, um auf die Idee zu kommen.«, sagte die Nixe.}

\BefehlFigurInVerzeichnis{1.6}{Ushenka-n.png}{Ushenka}{Ushenka war Hafenmeister. Oder vielleicht Hafenmeisterin. Auf dem Papier war sie Hafenmeister. Aber für sich selbst und Daheim nannte sie sich Hafenmeisterin und das nicht ohne ein Gefühl von Genugtuung.}


\BefehlFigurInVerzeichnis{1.3}{Smjer-n.png}{Smjer}{Die Schwimm-Entsprechung eines gemütlichen Spaziergangs fand Smjer jetzt gar nicht so übel.}

\BefehlFigurInVerzeichnis{1.3}{Marah-n.png}{Marah}{Marahs Jolle war eine Einhandjolle für Nixen, eine schnelle, die Marah liebte, als wäre sie ein Teil von ihr.}

\BefehlFigurInVerzeichnis{1.6}{Janasz-n.png}{Janasz}{»Ich fühle mich ausgebrannt.«, murmelte Janasz. »Als würde ich die Arbeiten dreier Crewmitglieder ausführen, nicht nur, was eine Person gut leisten kann.« Er beschwerte sich selten und fühlte sich schlecht dabei.}

\BefehlFigurInVerzeichnis{1.3}{Aga-n.png}{Aga}{Ich denke, ich sollte nicht hinaufsteigen. Noch nicht zumindest. Es ist nicht schlimm, wenn jeder Tag ein Noch-Nicht-Tag ist.}



\BefehlFigurInVerzeichnis{1.6}{Sindra-n.png}{Sindra}{»Du redest mit einer Person, die dir gedroht hat, dich zu ermorden, und gibst ihr eine Wahl, mit welchen Pronomen du über sie mit anderen reden würdest, gesetzt den Fall, du überlebst?«\Zeilenumbruch»Ich halte das für interessantere und wesentlichere Manieren als Sitzhaltung.«, kommentierte Sindra.}

\Leerzeile

\BefehlFigurInVerzeichnis{1.6}{Rash-n.png}{Rash}{In Rashs Kopf zerstörte Rash ganze Ladungen an Geschirr auf den Planken. Es half wenig. Rash schloss die Augen und ließ die Scherben in kleinere Scherben zerbrechen, mit einem Knarzen, in noch kleinere, bis der feine Sand zwischen den Planken hindurch in das Deck darunter rieselte oder vom Wind verwehte.}

\Leerzeile

\BefehlFigurInVerzeichnis{1.4}{Kamira-n.png}{Kamira}{Das Nixendeck hatte im Heck einen Raum, der für Psychohygiene gedacht war und dafür einen Rückzugsort bot. Psychohygiene war nicht bei allen ein beliebtes Wort dafür. Manche nannten es eher Seelenpflege oder Therapie, mentalen Beistand oder vereinfacht Konfliktgespräche. Kamira selbst hatte da keinen Vorzug, welchen Namen andere dem gaben. Er fand es viel mehr auch interessan.}




\BefehlFigurInVerzeichnis{1.6}{Amira-n.png}{Amira}{»Es tut mir leid, dass ich dich in eine Situation gebracht habe, in der du dich so unbehaglich gefühlt hast.« (...)\Zeilenumbruch»Das war gut.«, erwiderte Amira und überraschte R. damit. »Ich habe nie gelernt, was Privatsphäre ist. Ich habe immer beobachten müssen. Es gehörte zu meiner Arbeit sowie als fester Bestandteil in mein Leben. Ich möchte so nicht mehr sein, aber dazu muss ich auch wissen, was ich mir da angeeignet habe.«}

\Leerzeile

\BefehlFigurInVerzeichnis{1.5}{Katjenka-n.png}{Katjenka}{Sie war schon inzwischen erfahrener, schon länger in Politik involviert. Sie wusste, dass manche Verhandlungen, vor allem solche, die nicht so freiwillig passierten, wiederholter Angebote bedurften, bis sie in Erwägung gezogen werden würden.}

\BefehlFigurInVerzeichnis{1.5}{Junita-n.png}{Junita}{Junita konnte nicht verhindern, dass ein warmes Gefühl sie durchströmte. Die Zarin war nicht unbedingt sparsam mit Komplimenten. Ihr gegenüber aber schon. Weil sie sie nicht als Motivationsantrieb brauchte, Komplimente also keinen direkten Zweck erfüllten. Und weil sie immer gleich agierte, es also keine besseren Tage gab, die die Zarin über schlechtere hinausstellen konnte.}
