\BefehlVorKapitelDFDM{Marah-n.png}{Marah wurde von Ushenka großgezogen und hat die Flotte der Maare quasi mitgegründet. Sie ist verliebt in die Kapitänin.}{Amputation, Nervengift, Betäubung, Tierleid erwähnt, vegetarische Ernährung, Fäkalien.}

Die Flotte der Maare
====================

\Beitext{Marah}

"Gebt ihnen Dampf.", erscholl das Kommando der Kapitänin über das
Deck der Schattenmuräne. Sindra meinte das wörtlich.

Marah wusste, dass sie sich hätte beeilen sollen. Dampf gehörte zu
ihren Aufgaben. Aber sie robbte nur träge Richtung Rutsche. Sie hätte
sich gewünscht, dass Sindra ihr noch einen Abschiedskuss geben
würde, oder irgendeine andere Art von persönlicher Aufmerksamkeit.

Auf der anderen Seite wusste sie auch, dass sie nicht die einzige
Liebesperson für Sindra war. Sie wusste nicht, wievielen Crewmitgliedern
Sindra nun diese Aufmerksamkeit schenken müsste, wenn sie es
bei ihr täte. Sie drehte sich auf den Rücken und schob ihre
Fluke auf die Rutsche.

Und dann stand Sindra doch über ihr. Ihr riesiger Körper
ging in die Hocke, und weil sie damit immer noch zu groß
war, ließ die Kapitänin ihre Knie links und rechts von
ihrem Kopf auf den Boden plumpsen. Marah fühlte die Vibration
im Boden unter ihrem Rücken. Sie legte den Kopf in den
Nacken. In die dafür bereitgelegten Hände. Hände, die von
ihrem Scheitel bis in ihren Nacken reichten. Die großen, weichen
Finger flochten sich in ihr feuchtes Haar. Der Kopf beugte sich
herunter und Marah schloss vertrauend die Augen, als sie einen
zarten Kuss auf die Stirn bekam. Ihr ganzer Körper fühlte sich
elektrisiert. Sie atmete hastig ein. Als sie die Augen wieder
öffnete, blickte sie in das stets schwer zu lesene Gesicht
der Kapitänin. Sindra. Sindra hatte sie gebeten, dass Marah
sie häufiger bloß Sindra nennen sollte.

"Pass gefälligst auf dich auf!", befahl sie trocken. Bevor
sie ihre Hände sachte aus Marahs Haar wieder ausfädelte und
aufstand.

War das doch Angst in ihrem Blick. Wer wusste das
schon. "Du auch.", flüsterte Marah.

"Ja, ich passe auch auf dich auf, so gut ich kann.", sagte
Sindra. "Ich kann es nur viel zu wenig, also übernimm
das für mich! Und nun husch!"

Marah zögerte nicht mehr, gab sich Anschwung und rutschte
durch das Schiffsinnere ins Nixendeck. Hier war es angenehm
dunkel und feucht.

"Kamira ist schon draußen mit einem der Eimer.", informierte
sie Smjer.

Marah nickte. Damit hatte sie gerechnet. Sie griff sich
den anderen. "Hat er gesagt, wo er anfängt?"

"Ah richtig, das war, was ich eigentlich sagen gewollt
habe.", ärgerte sich Smjer. "Hinten. Am Heck. Aus
Symmetriegründen."

"Ergibt Sinn.", überlegte Marah. So richtig eingespielt waren
sie noch nicht. Die Erkenntnis machte sie ein bisschen nervös.

Und dass Smjer sich mit einem genuschelten "Ihr solltet mich
von Bord werfen." selbst niedermachte, trug sicher nicht
zur Verbesserung ihres Gefühls bei.

"Smjer!", rief sie.

"Du hast gute Ohren.", brummelte er.

"Und du sagst Unfug!"

Smjer war eine behinderte Nixe. Und hatte ein Trauma. Nixen schafften
es meistens, sich aus den Affären des Fußvolks
rauszuhalten, bisher. Landsleute oder Leute mit Füßen, zumindest
jene von ihnen, die zu den Unterdrückenden gehörten, nannten sie
gern abfällig Fußvolk. Allen voran waren das Menschen, dann Elben, mit ihren Ideen, alles
erforschen zu müssen und nicht die Finger von den Meeren zu
lassen. Und sie töteten laufend, unter sich und gegenseitig und alle
Völker, die nicht irgendeine vergleichbare Sprache hatten. Aber
auch Zwerge gehörten zu diesen mörderischen, ausbeutenden
Fußvölkern. Manchmal inzwischen sogar Lobbuds, ein
Volk unbärtiger, kleinerer Zwerge mit besonders
großen Füßen, sozusagen. Es
war ein Lobbud-Schiff gewesen, das an einen besonders einsamen
Ort zum Fischen gefahren war, -- vielleicht um den anderen Fußvölkern
zu zeigen, dass sie es auch drauf hatten --, bei dem Smjer irgendwie ins
Netz geraten war. Smjer hatte nie im Einzelnen erzählt, wie er
von dort entkommen war, aber er hatte seitdem keine
Fluke mehr. In Marahs schlimmsten Vorstellungen hatte er sie sich
selbst abhacken müssen, um zu entfliehen, aber es konnte auch
ganz anders gewesen sein.

Die meiste Zeit über war Smjer gelassen, verbreitete eine angenehme
Stimmung, während er sich mit Technik auseinandersetzte. Schwimmen
mochte er nicht mehr. An Deck bewegte er sich mit Rollbrettern, -stühlen, Seilwinden
und den Rutschen.

Aber wenn Smjer Angst hatte -- und das hatte er bis jetzt bei jedem Überfall --, erfüllte
ihn diese Selbstverachtung. Kamira versuchte sich mit ihm
darüber auseinanderzusetzen. Kamira war an Bord für Konfliktmanagement,
psychischen Beistand und Barriereabbau zuständig.

"Ich hab' dich lieb.", murmelte Marah, bevor sie mit dem zweiten
Eimer und einem großen Pinsel in das unter Wasser stehende Unterdeck
unter dem Nixendeck eintauchte, um die Schattenmuräne durch einen der Ausgänge zu
verlassen.

---

Das Heck der Schattenmuräne brodelte bereits. Die Eimer enthielten eine lebendige und deshalb
nicht weniger chemische Substanz, die Algenreste fraß und dabei
Hitze produzierte. In den natürlich vorkommenden Mengen jener Kleinstlebewesen
wurden Flächen, auf denen sie fraßen, maximal ein wenig wärmer. Aber
vor einigen Jahrzehnten hatten Nixen herausgefunden, was passierte, wenn
sie in großen Mengen zusammengesammelt und sie dann alle auf einmal
auf die Algenreste losgelassen wurden. Das Wasser wurde dabei so heiß, dass
es kochte und dampfte.

Es hatte selten besonderen Nutzen. Wasser konnte wesentlich einfacher anders
zum Kochen gebracht werden. Mit Feuer. Nur, dass Feuer unter Wasser schwierig
war, allerdings auch nicht unmöglich.

In diesem Fall nutzten sie es, um die Gerüchte zu verstärken, dass sie
eine Geisterflotte wären. Sie hatten einen Streifen sehr hitzebeständigem
Materials an ihren Schiffen angebracht, direkt unterhalb des Wasserspiegels. Dort
brachten sie nun die Kleinstlebewesen auf, die ihn sofort algenfrei nagten und
sich anschließend wieder in dünnen, roten Flächen auf der Wasseroberfläche
verteilten. Auf diese Weise stieg um die Schattenmuräne Dampf auf und das
Wasser um sie herum verfärbte sich vorübergehend rot.

Es war ein Manöver, um Angst zu schüren und Aufmerksamkeit von dem eigentlichen
Angriff abzulenken.

Kamira erwartete sie und hob eine Braue über seinem leuchtend orangen Auge. An
Land waren seine Augen eher braun, aber unter Wasser, ohne die Nickhaut darüber, die
vor grellem Tageslicht schützte, waren sein eines Auge leuchtend orange, das andere
blasser orange.

Vielleicht war die Geste ein Tadel, dass sie so spät war. Marah war sich
nicht sicher und versuchte reuevoll zu schauen.

"Wir schaffen das.", sagte Kamira. Auf Sirenu.

Siren war die Sprache der Nixen, und
Sirenu der Teil davon, der auch unter Wasser gesprochen werden konnte. Eine
Sprache, die Marah nicht nur hörte, sondern auch unter der Haut fühlte. Sie
hatte lange gedacht, dass es nur Nixen vorbehalten war, die Sprache zu
fühlen, aber hatte von Sindra gelernt, dass sie es auch konnte. Dass es
nicht alle Landsleute konnten, aber manche schon. Ein Kribbeln unter
der Haut, das beim Hören mancher Klänge gefühlt wurde.

Marah nickte und versuchte ein ermutigendes Lächeln.

Sie machten sich parallel
an die Arbeit. Kamira und sie arbeiteten sich auf je einer Seite der
Schattenmuräne zum Bug vor, sich zurufend, wie weit sie jeweils wären, damit
es symmetrisch wäre. Sie wagten nicht, aufzutauchen, um das Schiff
in seiner Dampfwolke zu betrachten. Das hatten sie allerdings oft getan, als sie
den Showeffekt ausprobiert hatten, weit auf See, während Sindra aus einem Beiboot
beobachtet und befohlen hatte, um alles einzutrainieren.

Marah tauchte durch eine der vorderen Lenzklappen wieder in den
Schiffsbauch ein, fühlte das kühlere Wasser besonders angenehm
an ihrem aufgewärmten Körper vorbeistreichen, als sie neben Kamira
wieder durch die Wasseroberfläche stieß und tief Luft holte. Kamira
war weniger aus der Puste.

Nun kam der eigentlich gefährliche Teil des Überfalls.

---

Wenn unter Fußvolk von der Dichtkunst der Nixen gesprochen wurde, dann
war die Assoziation eher Gesang. Und ja, Gesang gehörte zu Marahs
Leben. Es war fester Teil der Kommunikation, des Ausdrucks, eigentlich
viel mehr als diese Worte bedeuteten. Des Lebens eben.

Aber so widersprüchlich das auch mit einem Schiff scheinen mochte, dessen
unterstes Deck unter Wasser stand, waren auch die besten Techniken
zum Abdichten von Nixen entwickelt worden.

Die zwei Crewmitglieder mit Füßen, die sie mitnehmen würden, warteten
an der Luke zum Lenzdeck. Smjer hatte das Tauchboot vorbereitet. Es
war unbehaglich klein für diese Fußpersonen.

Ashnekov hatte ihr einmal gestanden, wie es sich für
ihn anfühlte, in diesem Gefährt zu
sein und sich ganz und gar darauf verlassen zu müssen, dass Marah oder
die anderen Nixen sie nicht einfach auf den Grund des Meeres sinken
ließen. Mit Luft, die nur für ein paar Stunden reichen würde. Er
vertraute. Mit Ashnekov unterhielt sich Marah ganz gern.

Er zwängte sich zuerst in das Tauchboot, dann seine Liebesperson
Janasz. Marah konnte sich Schlimmeres vorstellen, als direkt
vor einem beängstigendem Überfall mit der Liebesperson zu
kuscheln. Auf der anderen Seite wurde die Vorstellung vielleicht
dadurch entromatisiert, dass die beiden das Tauchboot mit einer
beachtlichen Menge Exkrementen teilten, -- und getrocknetem
und wiedergenässtem Seegras. Immerhin war alles wenigstens
einigermaßen geruchsdicht verpackt.

Marah fragte sich, wann sich mit Sindra das nächste
Mal Gelegenheit zum Kuscheln bieten würde. Und dann kamen ihr ihre
Worte wieder in den Sinn, dass sie gefälligst auf sich aufzupassen
habe. Sie grinste und zeitgleich ergriff sie eine Panik, die
sie vergeblich versuchte, abzuschütteln. Kamira legte ihr eine
Hand auf die Schulter.

"Wir schaffen das.", wiederholte er, dieses Mal auf Kazdulan, der
Deckssprache.

Kanta lehrte sie die Sprachen. Kanta war eine
begeisterte Lehrerin und die Begeisterung übertrug sich so sehr, dass
Marah sogar manchmal vergaß, dass Kanta ein Elb war. Kanta war
auf ihrer Seite, das sollte reichen. Trotzdem spürte Marah eine
Grundwut auf Elben, die sich als Volk so sehr damit beschäftigten, auf
möglichst elegant wirkende Weise und mit scheinbar überzeugenden
Argumenten das Leben anderer zu zerstören.

Die Wut verdrängte die Angst ein wenig. Und sie drängten nun gemeinsam
das Tauchboot aus dem größten Ausgang. Smjer hatte ihn kurz zuvor
geöffnet. Er war die meiste Zeit über geschlossen. Im
geöffneten Zustand unter Wasser bremste die Strömung. Bei viel Fahrt, sobald
das Wasser durch kleinere Lenzklappen aus dem Schiff gesogen worden war, -- daher
rührte die Bezeichnung 'Lenzdeck' --, lag die
Öffnung etwas oberhalb des Meeresspiegels, sodass höchstens
Mal Wasser hineinschwappte, das aber sofort wieder
hinauslief. Bei viel Fahrt bereitete es Marah und Jentel besonderes Vergnügen, ihre
Fischschwänze ins Wasser zu halten, während sie sich am Ausgang
festhielten, und das Wasser an den Seiten entlangstreichen zu
fühlen. Nun aber lag der Ausgang einen guten halben Meter unter dem
Wasserspiegel.

Niemand hielt sich während Überfällen in einer Bilge auf. Außer
vielleicht nicht registrierte Passagiere. Selbst Personen mit panischer
Angst eher nicht. Eine Bilge war feucht und dunkel und aus ihr wäre es mit
am schwierigsten, sich zu retten, sollte das zugehörige Schiff sinken.

Hier dockten sie an. Sie wendeten das Tauchboot dafür, weil es das Heck war, das
docken konnte. Auf die Weise bot es beim Vorankommen am wenigsten Widerstand
und sie konnten am besten ablegen. Kamira und sie drückten das flexible, runde
Heck an den Rumpf des Forschungsschiffs, das sie auf diese Weise
überfielen, und zogen die Saugnäpfe mit Schraubmechanismus fest. Als sie
sich sicher waren, dass alles dicht war, zogen sie die Ventile. Ein oberes, durch
das Luft vom Tauchboot in den Zwischenraum dringen konnte, und ein unteres, durch
das das Wasser aus dem Zwischenraum ins Tauchboot abfloss. Sie lauschten darauf, wie
Janasz mit dem Bohren und Sägen anfing. Es war relativ leise und trotzdem der
unheimlichste Part. Wenn doch eine Person in der Bilge auf der anderen Seite war,
würde sie davon mitbekommen und direkt angreifen können. Oder schlimmer, leise Alarm
schlagen können. Es war unwahrscheinlich. Es war unwahrscheinlich, wiederholte
Marah in Gedanken gegen ihre Panik an.

Dann war es vorbei. Hoffentlich, weil Janasz fertig war. Sehr hoffentlich, denn
Marah brauchte allmählich Luft. Sie fühlte sich leicht schwummrig. Sie
hasste das. Sie hatte noch nie zu lange getaucht, hatte nur große Angst davor. Vielleicht
sollte sie es mit Kamira zusammen ausprobieren. Kamira kannte sich
mit sogenanntem Apnoetauchen aus und würde sie bei einer Übung retten können. Vielleicht
wäre es gut, ein Gefühl für das Bewusstseinverlieren zu bekommen.

Jemand von innen klopfte das Zeichen. Es war alles gut gegangen. Die Fußleute
verließen über das gesägte Loch das Tauchboot in die Bilge des größeren
Schiffes. Marah begab sich in die Rolllappen, mit denen sie ins Innere des
Tauchboots gelangen konnte, ohne es zu fluten und holte gierig Luft. Kamira
folgte. Immerhin schien auch er zur Abwechslung mal wenigstens etwas
außer Atem zu sein.

Ohne miteinander zu sprechen, machten sie sich daran, Dichtungsmaterial an das
ausgeschnittene, runde Loch anzubringen. Sie saßen dazu in der Bilge, weil
Janasz und Ashnekov mit dem ersten Proviant ankamen, den sie im
Tauchboot ablegten und verstauten, und für deren Gewichtsausgleich sie
jedes Mal entsprechende Mengen abgepackter Fäkalien und
Seegras in den Ozean entleerten. Eigentlich war es nicht nur Seegras, sondern
vor allem der nicht verwertbare Teil der verschiedenen Getreide, die sie unter
Wasser bei den Garteninseln anbauten. Die Fäkalien
eigneten sich, um sie bei den Überfällen zu
entsorgen, um das Tauchboot gewichtstechnisch austariert zu halten, aber
sie reichten nicht aus. Es war wichtig, dass
das Tauchboot immer einigermaßen gleich schwer war. Proviant für Crews einer
Stärke von grob fünfzehn Personen, die für mehrere Monate reichen sollten, überschritten
ein Gewicht, das Kamira und Marah hätten am Sinken hindern können. Der Luftraum
im Tauchboot musste sie tragen. Aber eben jener Luftraum ohne Ladung an Bord
hätte das Boot an die Wasseroberfläche getrieben. Also transportierten
sie auf dem Hinweg Wegwerfmaterial von ähnlichem Umfang und Dichte, wie
sie an Vorräten gedachten zu stehlen. Früher hatten sie die
unnütze Ladung noch zumindest teilweise zurück in die Voratskammern der
ausgeraubten Schiffe getragen, aber inzwischen war ihnen das zu
heikel. Nicht zuletzt war es auch eine Schlepperei, die für keinen
Körper gesund war.

Die Bilge, in die sie nun gelangen konnten, war für so ein Forschungsschiff
eher mittlerer Größe überraschend hoch, sodass sie nicht einmal kriechen
mussten.

"Vier dieses Mal.", flüsterte Ashnekov. "Sie beginnen, ihren
Proviant zu bewachen."

Ashnekov meinte damit die Anzahl der Personen, die sie mit Giftpfeilen
mit Blasrohren vorübergehend außer Gefecht setzten. Die Technik kannten
sie von Yanil, einem Ork auf einem der anderen Schiffe ihrer
Diebesflotte. Janasz hatte gelernt, damit exellent umzugehen. Außerordentlich
exzellent, kam Marah nicht umhin zu bewundern, angesichts dessen, dass
er nun vier Personen sehr dicht aufeinanderfolgend erwischt haben musste, sodass sie
nicht mehr dazu hatten kommen können, Alarm zu schlagen. Es war
nicht nur deshalb ein Gewinn, Janasz an Bord zu haben. Er konnte außerdem
sehr gut kochen. Und das war ein starkes Kompliment von einer
Nixe an einen Zwerg, weil sich die Küchen eigentlich sehr
unterschieden. Janasz interessierte sich aber nicht dafür, dass es
ihm schmeckte, sondern dass es allen schmeckte. Er liebte die
Herausforderung mit nicht einkalkulierbaren Zutaten und unterhielt
sich reihum mit allen Crewmitgliedern, was sie gern äßen, um sich
an ihren jeweiligen Geschmack heranzutasten. Und Nixen machten
einen großen Teil der Crew aus. Aus dem Grund gab es fast nie Fleisch. Marah hasste
die Tage, an denen es doch Fleisch gab. Seit Janasz kochte, hatte
es aber immerhin dann eine Alternative gegeben.

"Nicht in Gedanken abdriften.", mahnte Kamira leise, und eine
Spur verspielt, um die Mahnung nicht wie eine Strafe klingen zu
lassen. Auf Siren, woraus Marah schloss, dass Ashnekov wieder
weg war. Kamira sprach nur dann Siren, wenn alle Anwesenden auch
Siren sprachen.

Marah atmete tief durch und nickte.

"Ich frage mich ja, ob es eher gut oder schlecht wäre, wenn du als Berufung
Geschichten erzählen lernen würdest.", murmelte Kamira. "Entweder,
es würden wunderschön ausgeschmückte Geschichten werden, oder du
würdest ständig den Faden verlieren."

Marah schmunzelte. "Das ist kein hilfreicher Kommentar, was das Verlieren
von Fäden anbelangt."

"Du warst ohnehin wieder dabei, ihn zu verlieren.", flüsterte
Kamira.

Dann schwiegen sie wieder. Janasz brachte zwei Schläuche, mit denen sie
die Trinkwasserbehältnisse leeren wollten -- nicht vollständig
leeren natürlich. Er schloss sie an dafür vorgesehene
Ventile am Tauchboot an. Ashnekov folgte mit weiteren zweien und belegte
damit die übrigen zwei Ventile. "Sie haben Trinkwassertanks, keine Fässer.", teilte
er mit.

Das war ein Trend, der sich anfing, durchzusetzen, und ihnen
in die Hände spielte. Sie mussten dann während der Umfüllprozedur
die Schläuche am anderen Ende nicht von Fass zu Fass umplatzieren, wie
es bei solchen der Fall gewesen wäre. Marah
mutmaßte, dass die Forschungsgruppen glaubten, dass Fässer besser
stehlbar wären, und deshalb zunehmend auf Tanks setzten. Und ohne ihre
Saugtechnik wäre die Vermutung vielleicht richtig gewesen. Marah
freute sich innerlich immer wieder über ihre Technik.

Als Ashnekov und Janasz wieder verschwunden waren,
unterbrachen Kamira und Marah die Klebarbeit. Das
Dichtungsmaterial musste ohnehin nun ein wenig einziehen. Sie tauchten wieder
aus dem Tauchboot hinaus, um den inneren Dichtungsring an einem doppelten
Flaschenzug auf beiden Seiten des Tauchboots vom
Heck nahe der Andockstelle zum Bug zu verschieben. Der
Mantel des Tauchboots war derzeit noch mit Meerwasser
gefüllt, das sie auf diese Weise nach hinten aus dem Boot herausdrängten, sodass durch
den entstehenden Unterdruck auf der anderen Seite des inneren
Dichtungsrings das Trinkwasser in die doppelwandige
Bootswand hineingesaugt wurde. Als Kamira und Marah sich wieder durch das
Tauchboot in die Bilge begaben, um die Schläuche wieder zu lösen, kam
Ashnekov gerade mit dem nächsten schweren Fass, das er
vielleicht besser hätte rollen sollen, wenn ein heiler Rücken das
Kriterium für die Wahl der Transportmethode
gewesen wäre. Aber das Kriterium war leider, nicht erwischt
zu werden. Er versenkte es im Tauchboot -- ein weiterer Ballen
Seegras verschwand dabei in die Tiefe des Ozeans, und als er wieder
umkehrte, weiteren Proviant einzusammeln, setzten Kamira und Marah testweise
die ausgeschnittenen Planken wieder ein. Die Sozusagen-Tür passte. Aber natürlich war
die Rille vom Sägen sichtbar. Die kaschierten sie nun möglichst geschickt
mit Holz, Flecken und Farbe, die sie auch überall andershin verteilten. Außen
würde es hoffentlich zualgen, bis das Schiff das nächste Mal aus dem Wasser gehoben
würde, wenn das überhaupt je passierte.

"Was ist das für ein Geräusch?", fragte Kamira, als Ashnekov das nächste
Mal erschien.

Marah hatte es auch schon gehört. Es klang fast wie ein Weinen, etwas
quäkiger. Es hatte ihr Angst gemacht, aber dann wiederum war sie davon
ausgegangen, dass sich Ashnekov und Janasz schon kümmern würden, wenn
das Mären eine Gefahr dargestellt hätte. Oder etwas gesagt hätten. Aber
warum stellte es keine dar?

Ashnekov war aus der Puste, obwohl er durchaus sehr trainiert war. Aber
er trug einen wirklich riesigen Sack über dem Rücken. "Eine Ziege."

"Bring sie.", ordnete Kamira an.

"Ich soll die Ziege bringen?", fragte Ashnekov.

"Zum Schluss.", überlegte Kamira. "Betäubt sie, damit sie sich auf der
Überfahrt nicht beschwert."

Anders konnte eine Ziege wahrscheinlich auch nicht ohne
weiteres dazu überredet werden, sich in eines der
Fächer im Tauchboot zu legen, vermutete Marah.

"Du möchtest, dass sie nicht getötet wird.", verstand Ashnekov.

Kamira nickte energisch und blickte ihn böse an. "Allein wie du
das sagst.", murmelte er.

Auf Siren.

Marah wusste nicht, ob Ashnekovs Siren-Brocken ausreichend waren, um
Kamira zu verstehen. Aber er nickte. "Ich bringe die Ziege zum
Schluss, wenn uns das nicht zusätzlich in Lebensgefahr bringt."

Marah fühlte sich auf einmal weicher. Sie würden heute ein Leben
retten. Direkt und nicht nur indirekt.

Die Forschungsschiffe waren unterwegs nach Grenlannd. Einem südlichen
Kontinent, von dem das Fußvolk sich nicht einmal absolut sicher war, dass
er existierte. Natürlich existierte Grenlannd. Viele Nixen verbrachten
vor den Küsten Grenlannds die ersten Jahre ihres Lebens oder
ihres Elterndaseins.

Aber Forschung beim Fußvolk hieß halt nicht, angucken, beobachten und
kommunizieren, sondern töten, zerschneiden und Experimente jeder
ekligen Art. Das harmloseste noch, dass Intelligenz getestet wurde, indem
Personen in Situationen gezwungen wurden, in denen sie durch
Rätsellösen an Nahrung kamen und das der größte Anreiz war, Dinge
zu tun, weil Freiheit nicht gegeben war. Personen, die vom Fußvolk
nicht für Personen gehalten wurden, weil sie zum Beispiel einen
Fischschwanz hatten, Flügel, oder viel kleiner waren.

Dass das Fußvolk sich so widerlich verhalten würde, hatte es
auf ihren zwei Kontinenten bewiesen, auf dem einen mehr, als
auf dem anderen, aber letztendlich auf beiden. Oft genug. Das musste
nicht auf Grenlannd übertragen werden. Dagegen kämpfte ihre Flotte der
Geisterschiffe.

Sie fingen an, sich die Flotte der Maare zu nennen, unter anderem weil
sie als so unheimlich wahrgenommen wurden. Sie stahlen Vorräte. Sie taten es vor
der Hälfte der Überfahrt, sodass die Forschungsschiffe umdrehen mussten, weil
sie die Fahrt über sonst nicht überleben würden. Sie stahlen auch manches
technisches Gedöns, wie Karten, Bücher und Navigationsmaterial. Und
sie stahlen all dies, während das Überfallschiff nicht einmal in die
Nähe kam. Die Besatzung der bestohlenen Schiffe verstand die Überfälle
nicht. Was Fußvolk nicht verstand, erklärte Fußvolk mit Märchen, Sagen
oder Horrorgeschichten. Sie wurden die Geisterschiffe, die
Wasserhexen oder die Mahre der Meere genannt. Mahre waren
Albträume, das war nicht, wie sie sich selbst nennen wollten. Aber der Klang
war schön. Da sie Piraterie als Umschreibung dessen, was sie taten, unpassend
fanden, nannte sich ihre Flotte nun seit Kurzem die Flotte der Maare. Sie hatten
also bloß eine andere Schreibweise als die der Albtraummärchengestalten gewählt. Vielleicht würde
sich das irgendwann durchsetzen und in irgendeine
Geschichtsschreibung einfließen. Ob das je passierte? Ob die Aktion groß
genug war? Und falls ja, wie? Ob die Flotte der Maare je von wesentlichen Teilen des Fußvolks
nicht mehr ausschließlich negativ wahrgenommen würde?

Janasz kam mit einem neuen Sack an, unter dem er fast zusammenbrach. Zumindest
sah es so aus. Das
Meckern hatte aufgehört. Es war Zeit zu gehen. Sie verpackten alles
gründlich, schnürten es mit Gurten fest. Ashnekov brachte neben der Ziege, die ihm
wie tot über den Schultern hing, noch einen Sack mit irgendetwas Weichem, das
auf die Vorräte platziert werden konnte, und ihre Rücken bei der Überfahrt
abpolstern würde. Nachdem alles gut verstaut war, tauchten
Ashnekov und Janasz vorübergehend aus dem Tauchboot heraus, weil es zu eng für sie alle
darin war. Kamira und Marah blickten sich um, dass sie auch nichts zurückgelassen
hätten, und verließen das Forschungsschiff durch das Loch, drehten
den vorbereiteten Deckel hinein und warteten ein paar Momente, bis
die Dichtung sich festgesogen und selbst verschlungen hatte. Das
Bootsmaterial, aus dem Nixen Boote bauten, war nicht selten so etwas
wie lebendig.

Es wirkte dicht, als sie es rasch aber sorgsam mit den Fingern untersuchten. Sie
nickten sich angespannt zu, ein Zeichen, dass sie alles erledigt hatten, was
im Trockenen passieren musste. Sie tauchten eilig hinaus und halfen Janasz und Ashnekov, sich
wieder hineinzurollen. Nun kam wieder ein Teil des
Vorgehens, der Marah sehr nervös machte. Sie
verschlossen die Schleuse und dockten das Tauchboot ab. Wasser strömte in
die Schleuse. Kamira und sie betrachteten die freigegebene Stelle im
Rumpf des Forschungsbootes. Eine einzelne Blase bildete sich und perlte
aus der Ritze an die Wasseroberfläche. Sonst nichts. Sie verrieben die Algen
etwas darüber und betrachteten noch einmal, ob es wirklich keine
weiteren Blasen geben würde.

Nichts.

Erleichterung durchströmte Marah. Sie nickten sich zu, beeilten sich, hinter
das Tauchboot zu gelangen und es zurück in die Schattenmuräne zu schieben. Ihre
Schwanzflosse fühlte sich stark an, als wäre die Auslastung nun nach der
Panik genau das Richtige. Im Nixendeck würden sie von großen Teilen
der Crew erwartet, die ihnen rasch beim Aus- und wieder Einladen helfen würde. Dann
war... die Hälfte der Gefahr vorüber, die Hälfte der Arbeit getan. Es
lauerte ein zweites Forschungsschiff.

<!--Kanta ist eigentlich nicht da unten, oder?-->
"Eine Ziege? Ist sie tot?", fragte Rash, statt einer Begrüßung.

"Nur betäubt.", widersprach Kamira.

"Mit einem der für die Besatzung vorgesehenen Pfeile?", fragte Rash.

Janasz nickte.

"Hoffentlich ist die Dosierung nicht zu hoch für den kleinen Körper.", sagte Rash.

Rash nahm ihnen die Ziege behutsam ab und trug sie selbst an Deck. Und
mehr musste man eigentlich auch nicht machen, um sich mit Marah gut zu
stimmen.

---

Das zweite Forschungsschiff würde einfacher auszurauben sein. Sie hatten
es schon einmal überfallen. Das hieß, es gab bereits ein Loch mit Dichtung,
das sie noch einmal benutzen könnten. Und da niemand auf dem ersten Schiff
informiert gewesen war, dass sie über die Bilge enterten, wäre es noch
unwahrscheinlicher, dass sie in der Bilge des anderen Schiffs erwartet
würden. Trotzdem hatte Marah eigentlich schon Adrenalin genug für einen Tag
gehabt.
