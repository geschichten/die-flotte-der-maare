\BefehlVorKapitelDFDM{Jentel-n.png}{Jentel ist die Mastkorb-Nixe. As verbringt sowohl Zeit mit Janasz beim Essen im Mastkorb, als auch mit Marah zum Singen}{Keine bisher.}

Beobachten
==========

\Beitext{Jentel}

Jentel hatte noch nie zuvor einen Menschen gesehen. So
aufregend war das jetzt auch nicht. Der Menschheits-Anteil
seiner Anwesenheit war es nicht. Wie er an Bord gekommen
war, war durchaus eine mysteriöse Frage. Jentel war sich
schon einigermaßen sicher, dass an ihm nicht so leicht
eine Person vorbeischleichen könnte. Das bedeutete, dass
diese Person entweder durch das Tauchdeck hineingekommen
war, oder tagsüber. Und dass sie auch bis jetzt höchstens tagsüber
an Deck gewesen war. Sindra hatte nur eben mitgeteilt, der Mensch
wäre schon etwa drei Tage unbemerkt an Bord gewesen. Über mehr
hatte sie as nicht informiert, als sie den Menschen gerade
aus ihrer Kajüte aufs Deck geleitet
und zum Warten aufgefordert hatte. Lediglich darüber, dass da nun
eine Person mehr an Bord wäre, damit as keinen Alarm schlagen
würde. Und sie hatte as gefragt, ob ihm nicht doch irgendetwas
auffällig vorgekommen wäre. Ihm war nichts auffällig vorgekommen
und das war gewissermaßen unheimlich.

Nun stand dieser Mensch, von dem Jentel nicht einmal wusste, wie
er hieß, also einfach an Deck, vor niemandem versteckt. Nur
Janasz hatte schon zuvor etwas bemerkt. Oder sie beide wussten noch
nichts davon, dass jemand anderes auch etwas bemerkt hatte. Zudem hatten
sie auch nicht geahnt, dass es sich um eine unbekannte, heimliche
Person an Bord handelte, sondern nur, dass etwas Merkwürdiges
vor sich ging. Etwas, das sich nun so erklären konnte: Der Mensch hatte sich sehr
unauffällig Essen stibitzt. Es war für Jentel überraschend
gewesen, dass Janasz ihn über die Unheimlichkeiten eingeweiht
hatte. Janasz hatte Angst. Und er glaubte, die Kapitänin nähme das alles
zu locker. Sie hatten sich darüber unterhalten, dass sie
ihn nicht unbedingt in all ihre Pläne eingeweiht haben musste, mit
der sie gedachte, die Sicherheit der Crew zu garantieren. Und
dass Sindra eben ohnehin selten Beunruhigung zeigte.

Jentel drehte sich im Mastkorb im Kreis, um gründlich Ausschau
zu halten, bevor as sainen Blick wieder auf die Gestalt senkte, die
dort verloren allein an Deck hockte und in die Ferne blickte. Genauso
wie as beobachtete sie die
dunklen Wolken, die den Himmel überdeckten, aus denen es
noch nicht regnete, aber feinste Tröpfen lagen bereits
in der Luft, legten sich auf sain Gesicht. Jentel hatte gehofft, dass
die Wolken auch Wind bringen würden, aber ungewöhnlicherweise
taten diese es nicht. Am Horizont waren sie wunderschön. Jentel
vermutete, dass die Person dort unten das auch so empfand, oder
wenigstens, dass es sie beruhigte, in die Ferne zu blicken. Von
hier oben könnte sie weiter blicken, dachte Jentel trocken. As
beobachtete sie nun schon einer Viertelstunde. Sie blickte gelegentlich
zu ihm hinauf. Nicht mit so einem Blick wie Kanta, nicht, weil
sie saine Aufmerksamkeit suchte, sondern eher als routinierte
Kontrolle, um as im Blick zu behalten. So wie as immer wieder
über das Meer blickte.

Jentel mochte die Kleidung der Person: Praktische, enge Kleidung mit
Verzierung, die Haare unter einem Kopftuch verborgen, weiches
Schuhwerk. Jentel mochte manche Schuhe sehr, und das war
der einzige, alberne Grund, aus dem sich Jentel je überlegt
hätte, dass Füße auszuprobieren auch ganz spannend sein
könnte. Die Kleidung an diesem Menschen war sehr
ordentlich gepflegt und achtsam getragen. Die Bewegungen waren unscheinbar
und doch perfekt, als durchdachte die Person jede davon. Fast als
versuchte sie mit den Schatten zu tanzen. Jentel
wusste nicht so genau, was as zu der Entscheidung brachte, die
Person heraufzuwinken, als ihr Blick das nächste Mal in saine
Richtung schweifte.

"Ich muss hier warten.", lehnte sie ab, gerade so laut, dass der zarte Wind ihre
Worte zu Jentel wehte. "Aber vielleicht bald!"
