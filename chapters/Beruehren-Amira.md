\BefehlVorKapitelDFDM{Amira-n.png}{Amira ist erst kürzlich an Bord und hatte mit Rash ein wenig BDSM ausprobiert.}{BDSM, aber vielleicht auch nicht?, Bisschen D/s-Spiel, Psychisches Hingeben, Sex am Rande erwähnt, Küssen, erwähnt: Messer, Bedrohung.}

Berühren
========

\Beitext{Amira}

In Amira tobte noch immer eine innere Auseinandersetzung, die sie
selbst kaum steuern konnte. Es waren hier Personen lieb zu
ihr, die überhaupt nicht von ihr profitiert hatten. Sie
hatte auch das Problem kurz bei Kamira angesprochen. Es
war witzig, wie nah ihre Namen beieinander lagen. Aber sie
wurden anders betont: Ihrer wurde auf der zweiten Silbe, während 'Kamira'
eher auf allen betont wurde und das r etwas anders war. Jedenfalls
hatte Kamira ihr erklärt, dass es nicht immer ein Geben
und Nehmen sein musste. Das war ein neues Konzept, eines von
vielen, das in ihr arbeitete.

Sie schob es zur Seite. Sie fühlte sich zu Rash hingezogen, wollte
wieder Rashs körperliche Zuneigung erfahren. Es war nicht einfach, sich
das einzugestehen, aber es war ein starkes Gefühl, das schließlich
an die Oberfläche brach, als Amira realisierte, dass Rash
das gern tat und eben doch auch selbst davon profitierte. Sie
dachte an Rashs körperliche Reaktion, als sie Rash das
Messer an den Hals gelegt hatte. Sie dachte an Rashs Hals, an
Rashs Atem. Und sie dachte an Rashs Kontrolle, die sie immer noch störte. Allein, dass
Rash nun ihre Gefühle schon wieder dominierte.

Ohne allzu sehr zu beobachten, legte sie es doch darauf
an, Rash am Abend allein anzutreffen. Rash schrieb wieder
Buchstaben, als sie sich entschied, die Interaktion
wieder aufzunehmen. "Ich
möchte gern ein Spiel mit dir spielen.", teilte Amira Rash
mit, als sie sicher war, dass sie wieder allein waren.

"Mit Messern und Kuscheln? So eines?", fragte Rash. Das
Lächeln, das in Rashs Gesicht trat, war vielleicht amüsiert. Und
selbstsicher. Wie immer. Viel zu selbstsicher. "Oder
eines mit Karten oder so?"

"Mehr in die erste Richtung. Ich möchte mehr kuscheln, aber
mit so etwas, was du damals Spiel genannt hast.", antwortete
Amira.

War das nun ein vorfreudiges Lächeln in Rashs Gesicht? Rash
verstaute jedenfalls, was es zu verstauen gab und stellte
sich ihr gegenüber hin. "Was für eines?"

"Ich habe mir Gedanken gemacht.", leitete Amira
ein, aber das Grinsen, das sich nun über Rashs Gesicht ausbreitete, ließ
sie unsicher innehalten.

Vielleicht, weil Rash die Unsicherheit bemerkte, entspannte das Gesicht wieder
ein wenig. Und Amiras Herz schlug eine Spur schneller. Sie
war sich nicht sicher, ob sie das mochte. Rash las und beeinflusste
ihre Emotionen in einer für sie eben unkontrollierbaren Weise. "Du
bist mir in Alltagssituationen zu dominant. Du hast mich sozusagen
zu viel im Griff.", sagte sie. Wie passend, dass das gerade
passierte. "Das haben wir mit dem Messerspiel versucht
auszugleichen. Aber das war eigentlich auch was, was du wolltest."

"Ich verstehe.", sagte Rash nachdenklich.

"Spielst du auch dominante Rollen in solchen Spielen?" Das
war Amiras entscheidende Frage.

Rash schüttelte den Kopf. Amira mochte, wie Rashs Gesichtsausdruck
so wirkte, als hätte Rash Fragen, als wäre Rash zur Abwechslung etwas
unklar.

"Ich habe die Hypothese, dass, wenn du mich in einem Spiel
versuchst zu dominieren, es dir Sicherheit nehmen könnte.", sagte
Amira. Sie fühlte einen gewissen Triumph, als Rashs Körperhaltung
darauffolgend weicher zu werden schien und Rashs Lächeln ganz
verschwand.

Einen Moment sagte niemand von ihnen etwas. Rash wandte den
Blick nicht von ihr ab, wirkte vielleicht etwas verloren.

"Du findest, es ergibt Sinn, dass ich weniger dominant werden
könnte, wenn ich dir gegenüber versuche, dominant zu sein?", fragte
Rash. Vielleicht hätte es eine veralbernde Scherzfrage sein
können, aber das war sie nicht. Rashs lockere Art war verloren
gegangen.

Amira nickte lächelnd, -- und atmete dann hastig ein, als Rash nichts
weiter tat, als einen Schritt auf sie zuzugehen, sodass zwischen
ihnen eine gute Handbreit Platz blieb. Rash konnte also dominant sein, auch
in so einem Spiel. Das war überraschend. Aber ihr Körper reagierte
nicht mit Angst oder Abwehr, sondern mit einer elektrisierten
Anspannung, einem Verlangen, wieder gestreichelt zu werden von
Rashs einfühlsamen, wertschätzenden Händen.

Rash fuhr mit einem Finger zart an ihrem Hals entlang, platzierte ihn
unter ihrem Kinn. Rash musste es nicht bewegen, damit ihr Blick genau
auf Rashs Augenpartie ruhte. Das war bereits der Fall. Sie spürte
den Atem in ihrem Gesicht, den Geruch, den sie zuvor schon
gerochen hatte, und den sie mit der Nacht in der Kapitänskajüte
verknüpfte. Mit einem Vertrauen, dass Rash sie nicht ausnutzte. Und
nun hatte sie beinahe genau danach gefragt. Sie stellte fest, dass
sie sich den Atem noch näher am Gesicht wünschte, dass Rash sie
vielleicht küssen würde.

"Ich bin eigentlich wirklich nicht dominant.", sagte Rash leise.

Diese weiche Stimme strich direkt unter ihrer Haut entlang
und über ihre wundgescheuerten Gefühle. "Ich finde, eigentlich
schon.", sagte sie ebenso leise. Immerhin klang ihre Stimme dabei
nicht allzu verunsichert.

"Ich bin dir näher gekommen, habe dich an die Wand gedrängt. Aber
was jetzt?", fragte Rash. "Was mache ich in einer dominanten Rolle? Was
magst du? Und wenn du mir genau erklären würdest, was du magst, wäre
ich dann nicht wieder submissiv?"

Berechtigte Fragen. "Gibt es nichts, was du wollen würdest? Angenommen, ich
würde dir sagen, du dürftest alles haben, was ich bin?"

Einen kurzen Moment schloss Rash die Augen, der Atem fühlte sich wärmer
an, und als hätte Rash ihn halb versucht zurückzuhalten. Ein
Ausdruck von Gier flackerte vielleicht kurz darin, wenn Amira richtig
las, dann war es vorbei.

Aber Amira lächelte. "Du möchtest etwas."

Rash strich mit dem Finger von ihrer Kinnkante aus über ihre Wange, so
zartfühlend, dass die Linie hinterher unter der Haut vibrierte. "Es
ist kompliziert.", sagte Rash. "Ich bin submissiv. Ich mag den Moment, in
dem es andere schaffen, mir den Boden unter den Füßen wegzureißen. Mich
klein zu kriegen, wie ich es zuvor genannt habe. Ich fühle mich nicht
wohl dabei, es bei anderen zu tun." Rash bewegte die Finger auf Amiras
Wange, bis Rashs ganze Hand dort lag. Amira atmete schneller, schon
allein, weil sie so dicht bei einander standen und eine gewisse Erotik
zwischen ihnen lag, ob Rash nun dominant war oder nicht. "Aber
ich mag dich gern fühlen sehen. Ich mag, wenn dein Atem stockt, oder
du schneller atmest, wenn dein Blick weggetreten wirkt."

Wahrscheinlich sah ihr Blick genau jetzt weggetreten aus. Rash verringerte die
ohnehin schon kleine Distanz noch eine Nuance mehr. Amira
versuchte zu atmen, versuchte es wirklich, aber ihr Zwerchfell
zitterte unkontrolliert. Was war das für eine Person? Wie schaffte Rash das mit
nur so wenigen Bewegungen und bestimmt auch mit diesen Worten, diese
Gefühle und Sehnsüchte auszulösen?

"Wenn du dich mir wirklich hingeben möchtest,", Rash pausierte
für ein ungewöhnlich unsicheres Lächeln, als Amira bei dem
Wort 'hingeben' kurz sichtbar zitterte. "dann möchte ich deinen Körper
berühren, mit Fingern, mit Händen, deine Haut streicheln, dich
zärtlich küssen, bis du zarte Geräusche von dir gibst, weil du
sie nicht mehr zurückhalten kannst. Leises Stöhnen vielleicht, oder
sehnsuchtsvolles Wimmern. Ich möchte dich vorsichtig etwas entkleiden
und dich behutsam anfassen, bis du die Vorsicht nicht mehr
ertragen kannst und nicht mehr anders kannst, als einzufordern,
was du dann jeweils brauchst."

Amira atmete zitternd ein und aus. Kontrolle hatte sie längst
nicht mehr. Oder doch? Vielleicht hätte sie ohne jegliche Kontrolle Rash
einfach geküsst. Auf die Wangen, mit weichen, trockenen
Lippen. Die Haut gespürt. Aber sie hielt sich doch zurück. So viel
war noch da.

"Magst du, was ich sage? Fühlst du dich wohl?", fragte Rash.

Amira hätte am liebsten genickt, aber dann hätten sich ihre
Gesichter berührt. "Ja." Die Stimme kaum mehr als ein Fiepen.

"Möchtest du, dass ich weitermache?", fragte Rash.

"Ja.", wiederholte sie. Und fügte flüsternd hinzu: "Du
bist sehr dominant."

Rash hob die bis jetzt noch inaktive Hand, um auch über ihre andere Wange
zu streicheln. "Meinst du?"

Amira antwortete nicht direkt. Sie brauchte eine Weile, um die
Berechtigung hinter der Frage zu erkennen. Rash war vielleicht
dominant, aber durchaus sehr unsicher. Es ging um sie. Rashs
Wunsch war es, dass sie genoss, vielleicht Lust empfand. "Sind
Körper für dich uninteressant, wenn sie nicht genießen?", fragte
Amira schließlich.

"Sehr.", antwortete Rash. "Es ist vielleicht auch schwierig
zu beantworten. Körper gehören dazu. Ich mag auch die physischen
Gefühle, wenn ich gestreichelt werde, schon. Aber wenn nicht wirklich starke
psychische Genussgefühle im Spiel sind, interessieren mich
Körper selber nicht."

"Du möchtest meine Psyche bespielen?", folgerte Amira.

Rash hielt ihr Gesicht nun mit beiden Händen fest und näherte
sich so nah, dass sie sich gerade so nicht berührten. "Wenn
ich darf?", fragte Rash, auf unheimliche, aber wunderschöne
Art weich.

Amira spürte den Impuls einfach sofort 'ja' zu sagen. Sich
hinzugeben. Rash zu vertrauen. Es gab das Aus-Wort. Es gab
keinen Grund, Rash in diesem Punkte nicht zu vertrauen.

Es wäre ein Spiel, in dem Rash herausfinden würde, was
ihr gefiel, wonach sich ihr Körper und ihre Psyche verzehrte. Es
hatte schon angefangen. Rash würde ihre Reaktionen lesen und
darauf antworten. Es wäre ein anderes Ausziehen, als ein
Ausziehen von Kleidung. So nackt war Amira noch nie gewesen. "Ja.", flüsterte
sie.

"Ich bin sehr verliebt in dich.", antwortete Rash.

Amira bezweifelte nicht, dass es der Wahrheit entsprach, auch
außerhalb des Spiels, aber dass Rash sich bewusst diesen Zeitpunkt
ausgesucht hatte, es ihr zu sagen. Blut rauschte durch ihren Körper
und ihr wurde etwas schwummrig, nicht so sehr, dass sie hätte
gestützt werden müssen.

Als sie sich fragte, ob sie ebenso empfand, zögerte sie nur
einen Moment. "Gegenseitig.", flüsterte sie gegen Rashs
Wange.

Dieses Mal war Rash die Person, die hastig einatmete. "Damit
habe ich nicht gerechnet.", gab Rash zu.

Rashs Finger lösten sich von ihren Wangen und rannen über ihren
Hals, über ihre Schlüsselbeine und Brüste in ihre Taille. Amira
fühlte die Berührungen nur gedämpft durch die Kleidung. Ihr
Körper verlangte nach mehr. Nicht unbedingt Sex, wie Rash
das beim letzten Mal schon vermutet hatte, aber vielleicht
mehr als bloß kuscheln. Verliebtes Kuscheln. Aber nicht hier. Nicht, wo
jederzeit eine Person sie entdecken und unterbrechen könnte. Sie
wollte fragen, ob sie wieder um die Kapitänskajüte bitten sollten, als
Rashs Lippen sie an ihrem Mundwinkel berührten und sie
nur noch aus Atem und Gefühl bestand.
