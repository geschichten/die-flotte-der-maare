\BefehlVorKapitelDFDM{Amira-n.png}{Amira hat den Auftrag, die Kapitänin zu töten, verweigert.}{BDSM, Es geht um kuscheln, Teil-Nacktheit, Messer, Bedrohung, Heulkrampf, Trauma, Misgendern, Sex ist Kern einer Unterhaltung, aber wird nicht ausgeführt.}

Kuscheln
========

\Beitext{Amira}

Rash war gut mit Worten. Sowohl auf Kazdulan, als auch auf Ilderin. Das
passte nicht zur These, dass Rash Unterstützung von Kanta benötigte, um
Briefe zu formulieren. Da war etwas im Argen. Oder es gab
einfach etwas, was Amira nicht verstand.

Amira sprach drei Sprachen einigermaßen fließend: Salvenit, Ilderin
und Kazdulan. Letztere zwei Sprachen hatten zu ihrer Ausbildung gehört, damit sie
Gesprächen lauschen und Informationen für ihre Auftraggebenden
erlangen konnte.

Die Unterredung mit Rash hatte -- vielleicht sogar ohne, dass
es der Kapitänin oder Rash selbst bewusst gewesen wäre -- zwischen
Ilderin und Kazdulan gewechselt. Und Rash hatte in keiner
der beiden Sprachen Schwierigkeiten gehabt, Sätze gut zu
formulieren.

Rash war eine beeindruckende Person. Ein Elb. Heute trug Rash das
Haar das erste Mal, seit Amira an Bord war, offen. Es war
der vierte Tag. Rash hatte klar definierte, dunkelbraune
Locken, die Rash bis auf die Schulter herabreichten. Sonst
trug Rash die Haare bis in den Nacken in fest an den Kopf
geflochtenen Zöpfen. Ein Zopfband fasste die
Zöpfe im Nacken zusammen, ab wo die Haare
offen einen einzelnen voluminösen kurzen Zopf bildeten. Sie mochten
im ausgestreckten Zustand vielleicht knapp in Rashs Hüfte
reichen. Amira wusste es nicht, hatte kein Gefühl
dafür.

Es war früher Vormittag. Rash hatte in der Nacht an Deck geschlafen, in
ein dünnes Laken gewickelt, das nach Ziege und Minze
roch, mit nacktem Oberkörper, und hatte im Schlaf gefroren. Amira
hatte sich gefragt, ob sie Rash mit einer
wärmeren Decke hätte zudecken sollen oder dürfen.

Aber sie kannte die Gründe nicht, aus denen Rash das tat. Und
sie kannte ihren Platz hier noch nicht, wusste nicht, was
sich gehörte, und was eigentlich ein Eingriff in eine
Privatsphäre wäre. Amira fühlte sich überfordert von
der Aufgabe, sich in das Miteinander der Crew einzufügen.

Rash hatte sich am frühen Morgen ausgiebig und lange
gewaschen. Amira hatte das nicht beobachten wollen. Sie
hatte es einfach so mitbekommen. Sie fühlte sich schuldig
gegenüber Rash, dass sie das immer noch alles mitbekam.

Inzwischen hing auch das Laken, in dem Rash geschlafen
hatte, gewaschen an Deck und trocknete im Wind. Es roch nur noch
ein wenig nach Minze. Rash war unter Deck
verschwunden.

Amiras Gedanken zerrissen sie. Sie wollte an ihrer Beliebtheit
arbeiten. Ohne daran zu arbeiten. Sie wollte akzeptiert
sein. Sie wollte aber auch kein Schauspiel darlegen, damit
sie akzeptiert würde. Dann wiederum traute sie sich nicht
so richtig, ein Gespräch anzufangen. Sie konnte sich nicht
erinnern, je ein Gespräch angefangen zu haben, das nicht auf
einer Drohung basierte. Sie mochte sich dafür nicht.

Sindra hatte schon recht mit der Feststellung, dass
sie ein Trauma hatte. Und sie hatte erst jetzt das erste
Mal in ihrem Leben Gelegenheit, sich damit auseinanderzusetzen
und es überhaupt erstmal so zu benennen.

Rash war neben Sindra nun seit gestern die einzige
Person an Bord, zu der sie eine Art Bindung fühlte. Eine
seltsame zwar, aber Rash war sehr persönlich geworden, hatte
so viele Anhaltspunkte in den Raum geworfen, die Amiras
Interesse weckten.

Amira seufzte, etwas wütend auf sich selbst, und ging
ins Unterdeck. Leise, wie sie bemerkte. So wie
ein selbstverständlicher Schatten. So war ihr ihre Wirkung
oft von anderen beschrieben worden. Sie verhielt
sich nicht so, weil sie es wollte, sondern
weil es zu ihr gehörte, besonders, wenn sie Angst hatte.

Rash saß in der Schreibnische und schrieb. Rashs Blick war
konzentriert, wirkte vielleicht verträumt, während Rash die
Feder in die Tinte tauchte, abstrich, und langsam Buchstaben
aufs Papier brachte. Amira hätte eigentlich bei der Ruhe und
Selbstvergessenheit, die diese Person ausstrahlte, nie
vermutet, dass Rash sie bemerkt hätte, aber auf der anderen
Seite hatte Rash sie über die vergangenen Tage ja auch bemerkt.

Amira wollte nicht unbemerkt beobachten, dazu war sie nicht
hier. Sie trat aus ihrer Nische neben Rash.

Rash erschreckte sich heftig, und hätte Amira das
Tintenfass nicht festgehalten, wäre es vielleicht durch
die Gegend geflogen. So plötzlich Rash sich erschreckt
hatte, so übergangslos saß Rash wieder ruhig da, wusch
gelassen die Tinte aus, legte die nasse Feder zur Seite und
blickte Amira an. "Was kann ich für dich tun?"

Das hatte Sindra auch gefragt, mit den selben Worten, erinnerte
sich Amira. Sie fragte sich, wie eng die beiden waren. Laut fragte sie
aber: "Ist es nicht sehr verdächtig, wenn du nach der ganzen
Sache direkt zum Schreibpult gehst und schreibst?"

"Kommt drauf an.", sagte Rash. "Wenn ich dabei herausfinde, warum
ich dieses besagte Erinnerungsgefühl hatte, und es direkt
berichte, halte ich das für eher von Vorteil. Nicht
nur für mich, versteht sich."

Amira nickte. Sie schraubte das Tintenfass zu und
stellte es auf das Schreibpult.

Rash verstaute es zusammen mit Feder und Papier in der
Schublade.

"Ich kenne den doppelten Boden.", informierte Amira.

Rash blickte für ein paar Momente in ihr Gesicht, dann
verstaute Rash das Papier unter dem besagten doppelten Boden
und seufzte. "Weißt du, wer noch davon weiß?"

"Kanta?", fragte Amira direkt. "Ich habe sie nie daran gehen
sehen."

Rash wirkte beruhigter. "Aber eigentlich bist du nicht
deswegen hier, richtig?"

Woher wusste Rash das? Amira nickte. "Ich habe", sie
zögerte, "eine Menge Fragen."

"Ich bin offen für eine Menge Fragen.", sagte Rash. "Und
ich finde dich sympathisch."

Amira fühlte Unbehagen bei der Mitteilung. "Ich vertraue
dir nicht.", stellte sie klar. Es entsprach der Wahrheit. Rash
war eine faszinierende Person, aber für Amira steckte
Rash voller Geheimnisse. Und Rashs Aufmerksamkeit schüchterte
Amira ein wenig ein.

"Verständlich.", sagte Rash. "Oder besser gesagt, es ist
der Standard. Mir wird nicht leicht vertraut."

<!--Ist das rassistisch?-->

"Warum?", fragte Amira.

"Du hast mir doch gerade gesagt, dass du mir nicht
vertraust. Das müsstest du also besser wissen.", sagte
Rash. Mit einem Blick über das Schreibpult versicherte Rash sich
wohl, dass alles aufgeräumt war, und stand anschließend auf. "Wollen
wir irgendwo hingehen?"

Amira schüttelte den Kopf. "Hier sind wir ungestört."

Rash nickte. "Das ist richtig. Nicht sehr gemütlich, aber einsam."

Auch das stimmte. Amira lehnte sich an die Wand, von wo aus
sie alles am besten im Blick hatte, und atmete, um sich
auf die Fragen zu konzentrieren. "Du bist", aber Amira stockte, als
ihr auffiel, dass sie die Worte dafür nicht korrekt benennen
konnte. Also versuchte sie es anders: "Ich bin keine Frau.", sagte
sie. "Und auch kein Mann. Und du erlebst etwas, was vielleicht
vergleichbar ist. Ich habe noch nie mit einer Person über die
Erfahrung reden können und würde das gern."

Rash lächelte. "Sehr gern. Was möchtest du wissen?"

"Ich weiß es gar nicht genau.", gestand Amira leise. "Vielleicht, woher
du weißt, dass du nicht einfach nur eine aufgedrückte Rolle nicht
spielen willst. Der Gedanke verunsichert mich bei mir."

"Ich würde jede Person dafür lautstark kritisieren, wenn sie
Erwartungen an Personen hat, weil sie ein bestimmtes Geschlecht
haben. Auf Basis dieser Rollen.", antwortete Rash. "Immer, wenn
ich so etwas mache, ordnet mir mein Gegenüber besonders stark zum Beispiel das
Geschlecht männlich zu, weil ich ja gerade verteidige, was
Männer dürfen oder was Frauen dürfen. Das ist ein mieses
Gefühl, weil ich nicht, oder nur manchmal und teilweise, ein
Mann bin, und vor allem, weil ich es hasse, wenn Leute versuchen, mir ein
Geschlecht von außen aufzuzwingen. Ich nehme
es aber für den Kampf gegen die Rollenzwänge in Kauf. Verstehst
du, was ich meine?"

Amira nickte langsam. Rash sah für sie nicht aus wie eine
Person, der sie das Geschlecht männlich zugeordnet hätte, als
sie noch geglaubt hatte, dass sie Geschlechter zuordnen müsste. Sie
war froh über beides. Dass sie es nicht mehr tat und dass sie
auch nicht wusste, was sie Rash zugeordnet hätte. "Ich glaube, ich
kann das nachempfinden.", sagte sie. "Ich habe oft darüber geschimpft,
welche Ungerechtigkeiten Frauen erleben, und mich dabei aber nur
wie eine Person gefühlt, die das gleiche Schicksal erfährt und
dadurch der Gruppe zugehörig, und nicht, als
würde ich zu der Gruppe Frauen gehören, weil ich eine Frau wäre."

"Weil du keine bist.", sagte Rash sanft und weich.

Wieder fiel Anspannung von Amira ab. Es war so unbeschreiblich
erleichternd, unter Leuten zu sein, unter denen sie nicht
so gesehen wurde. Es überraschte sie immer noch, wie stark
dieses Gefühl war, während sie ja gleichzeitig realisierte, dass
sie ein Trauma hatte, und das hätte viel stärker sein sollen. "Wieso
ist das Gefühl der Erleichterung, dass dieser Zuweisungszwang
nicht mehr auf mich angewendet wird, so viel stärker als
die Gefühle, die mit meinem Trauma zusammenhängen?", murmelte
sie. Vielleicht, weil sie hoffte, dass
Rash eine Antwort darauf hatte. Weil Rash so viele Antworten hatte.
Aber eigentlich ging Rash das nichts an. Es war ein seltsames Gefühl, sich
einer Person mit so etwas anzuvertrauen, der sie nicht traute.

"Traumata packen einen oft in sehr üblen Wellen.", antwortete Rash
unbarmherzig. "Es kann sein, dass du jetzt erst Teile davon
kennst, aber sobald du dich einigermaßen sicher fühlst, realisierst
du in Schüben immer mehr davon, sodass es dich zerfetzt."

Amira senkte den Blick. Ob es wirklich so gut war, mit Rash
darüber zu reden?

"Wir sind dann für dich da, wenn du das annehmen kannst.", fügte
Rash hinzu. "Es kann sogar Ursache sein: Wenn du bereit bist, Hilfe
anzunehmen, dann fetzt es dich besonders, weil deine Psyche die
benötigte Sicherheit dafür erst dann hat."

Amira lächelte bitter. "Du scheinst Erfahrung zu haben."

"Ein wenig.", sagte Rash. "So wenig vertrauenswürdig ich wirke, so sehr
vertrauen sich Personen mir auch gern an. Ich kann gegebenenfalls recht gut
auffangen, wenn ich gelassen werde. Es kommt natürlich auch
darauf an, ob ich zu der jeweiligen Person in der Hinsicht passe."

Amira schüttelte den Kopf. "Ich möchte das nicht."

Rash nickte. "Ich hätte auch nicht damit gerechnet. Nicht innerhalb
des nächsten halben Jahrs oder so."

Amira runzelte die Stirn. Es wurde dringend Zeit das Thema
zu wechseln. Rash war unheimlich. Und sie wollte wieder wenigstens
das Gefühl haben, so etwas wie eine Oberhand zu haben. "Was hast
du mit der Kapitänin bei Einbruch der Nacht gemacht?"

"Du glaubst, es ginge dich etwas an?", fragte Rash. Rash wirkte
amüsiert dabei.

"In der Unterredung mit dir ist gefallen, dass es was mit
Erregung zu tun hat. Die wiederum damit zu tun hat, dass
du Befehle erhalten hast.", sagte Amira. "Hat dir die Kapitänin
befohlen, an Deck zu schlafen, mit dem Laken, das nach Ziege
riecht? Hat sie dich erniedrigt? Ist es eine Strafe? Oder
ist es etwas, was sie mit allen
macht? Kommt das auf mich auch zu?"

"Ah, daher fragst du.", sagte Rash gelassen. "Es war keine
Strafe und das kommt nicht auf dich zu, es sei denn, du bringst es selber
an. Sindra ist offen für Spiele und Zuwendung in vielerlei
Hinsicht. Einige hier an Bord sind das."

Das war nicht genug Information. Amira fühlte sich in diesem
sozialen Gefüge an Bord verloren. Dieses neue Universum um
sie herum funktionierte nach Regeln, die ihr noch so unklar
waren, dass sie nicht einmal wusste, wie sie danach
fragen sollte. Sie fühlte sich unwohl, es mit ihren Fragen
zu versuchen, aber Rash sprach nicht weiter, stand stattdessen
ganz ruhig da. "Hatte sie Sex mit dir?", fragte Amira schließlich mutig. Es
ging sie wirklich nichts an.

"Nicht im herkömmlichen Sinn.", sagte Rash und seufzte dann
tief. "Wir haben ein Spiel gespielt, in dem ich ihr
untergeordnet war und sie mich bedroht hat. Auf meinen
Wunsch hin. So etwas
erregt mich. Aber eher auf einer anderen Ebene, als es
normalerweise als sexuelles Interesse bezeichnet
würde. Eher, weil ich sehr auf den Gedankenzustand
meines Gehirns stehe, wenn ich untergeordnet bin." Rash
lehnte sich mit dem Gesäß am Schreibpult an. "Sie
hat mit einigen anderen Crewmitgliedern durchaus Sex. Die sie
ebenfalls unterordnet, wenn sie darauf stehen. Ich
weiß nicht, ob es auch eine Person gibt, die mit ihr
Spiele spielt, bei denen Sindra untergeordnet ist. Sind
das Informationen, die du gern haben möchtest?"

Amira blickte Rash lange und nachdenklich an. Etwas
in ihr sperrte sich gegen den Gedanken, dass Personen
sich freiwillig unterordnen würden, es mögen würden. Und
etwas in ihr fühlte, dass sich so etwas auch gut
anfühlen konnte. Sie mochte den Teil spontan nicht. "Wie
hast du herausgefunden, was du magst und willst?"

"Ich wusste es einfach.", sagte Rash schlicht. "Schon
als ich ein Kleinkind war, habe ich beim Spielen mit
anderen Kindern eine gewisse Denk- und Körperreaktion gefühlt, wenn
sie mich gefesselt haben oder so etwas. Ich bin
dem damals unschuldig auf den Grund gegangen." Rash
lächelte, vielleicht verspielt. "Und nun bin ich froh, Spiele spielen zu
können, bei denen alle Beteiligten wissen, was
es für mich bedeutet."

Amira fühlte sich seltsam unbehaglich im Inneren. Immer
noch wegen der Zerrissenheit. "Ich habe so etwas noch
nie gespielt.", sagte sie nachdenklich. "Es fühlt sich
in meinem Kopf gleichzeitig furchtbar an, aber hat
auch irgendetwas Gutes an sich, was ich möchte. Was
bedeutet das?"

"Ist es eine rethorische Frage, mehr eine Frage an dich, oder
soll ich mir herausnehmen, einen Antwortversuch zu wagen, obwohl
ich dich kaum kenne?", fragte Rash.

Amira runzelte die Stirn. "Ich weiß es nicht einmal, aber
kann es denn schaden, wenn du es versuchst?"

"Ich hatte den Eindruck,
es hat dir nicht so gefallen, als ich die Frage bezüglich Trauma
beantwortet habe.", sagte Rash. "Daher frage ich lieber, bevor
ich es wage."

Das stimmte. Aber woher wusste Rash das? "Kannst du irgendwie
so etwas wie Gedanken lesen?"

Rash schüttelte den Kopf. "Wenn du damit nicht meinst, dass
ich sehr genau zuhöre und beobachte und daher meine Einschätzungen, was
du denkst, oft nicht so weit von der Wahrheit entfernt
sind, dann nicht."

"Ist dir bewusst, dass du gut darin bist?", fragte Amira.

Rash lächelte milde. "Ja.", bestätigte Rash. "Es verunsichert
Leute manchmal ganz schön."

"Ich hätte gern eine Antwort, wenn du eine hast.", beschloss
Amira. Wenn Rash schon Antworten hatte, war es doch Unfug, wenn
Rash sie für sich behielt.

"Ich vermute, du möchtest körperliche Zuwendung.", sagte
Rash sanft. "Du möchtest in den Arm genommen und zärtlich
gestreichelt werden. Körperlich geliebt. Vielleicht oder
wahrscheinlich sogar
ohne Sex. Einfach eine Person, die dich liebevoll berührt
und dir dabei das Gefühl gibt, sehr wertvoll zu sein."

Amira ließ sich die Vorstellung durch den Kopf gehen. Rashs
so zart gesprochene, weiche Worte rannen bereits über ihren
Körper, als wären es zärtliche Finger. War das ein Zeichen dafür?
"Es macht mir Angst.", sagte Amira. Ihr ganzer Körper
versteifte sich. Sie würde sich niemals Rash in dieser
Art hingeben. Nicht innerhalb benannten halben Jahres zumindest.

"Auch, wenn du mir dabei ein Messer an den Hals legen
würdest?", fragte Rash.

Amira blickte Rash beinahe alarmiert an. Wieso würde diese
Person sich freiwillig ein Messer an den Hals legen lassen.
Von einem Assassinan. Und gleichzeitig spürte sie wieder
-- wie eine Erinnerung oder sogar etwas realer --
sanfte Hände auf ihrer Haut. Sie hatte nicht so richtig
bemerkt, wie sie das Messer in die Hand genommen hatte, aber
es lag da plötzlich. Rash blickte es an. Ein vielleicht
neugieriger Ausdruck trat in das Gesicht, aber auch etwas
anderes. "Ist es nicht ungerecht?", fragte Amira.

"Wieso ungerecht?", fragte Rash.

"Ich traue dir nicht, aber du mir so sehr, dass du dir von
mir ein Messer an den Hals legen lassen würdest.", erklärte
Amira.

"Bist du sicher damit?", fragte Rash. "Kannst du Personen
Messer an Kehlen legen und dabei garantieren, dass nichts
passiert?"

Amira nickte. Aber das war keine Antwort auf die Frage.

"Dann ist es gar nicht so wichtig, wie ungerecht das ist.", sagte
Rash. "Erstens: Du vertraust mir nicht. Gut, das ist einfach
gegeben. Das ändert sich ja nicht dadurch, dass irgendwer
von uns da Erwartungshaltungen hätte. Zweitens: Ich
vertraue deinen Fähigkeiten und Selbsteinschätzungen. Mehr
Sicherheit brauche ich nicht. Und drittens: Ich
mag das Risiko und wäre gewillt, es einzugehen."

Amira trat von der Wand weg und einen Schritt auf Rash zu. Sie
war unschlüssig. Blieb vor Rash stehen, etwas näher, als für
eine Konversation üblich.

"Ich habe Tabus.", informierte Rash. "Du fasst meine Haare
nicht an. Wenn du sie aus Versehen berührst, oder sie über
deinen Händen liegen, weil du in meinen Nacken fasst, in
Ordnung. Aber nicht durch die Haare streicheln oder so
etwas."

Amira nickte. "Ich weiß noch nicht so richtig, was hier
passiert. Ich fühle mich etwas überfordert."

"Du musst überhaupt nichts tun.", sagte Rash. "Ich hatte
deinen Schritt auf mich zu und den
verringerten Abstand so interpretiert, dass
du vielleicht etwas ausprobieren möchtest. Und falls
du möchtest, solltest du vorher über meine Grenzen
informiert sein. Dass du sie dann kennst, sollte dich aber
nicht dazu drängen, etwas zu tun."

Amira nickte wieder. "Wie lang ist die Liste?"

"Nicht lang.", sagte Rash.

"Dann haken wir das erst einmal ab." Das Gefühl von Verwirrung
ließ Amira sich etwas surreal fühlen. Und dann fühlte sie
wieder die Idee von Rashs Händen an ihrem Körper. Warum verzehrte
sie sich so sehr danach?

"Du entkleidest mich nicht weiter als bis auf die Unterwäsche.", sagte
Rash. "Du lässt meine Kleidung heile. Wenn du meine Haut verletzen
möchtest, fragst du vorher. Und machst es dann höchstens mit sehr
sauberen Klingen."

"Ich möchte dich nicht verletzen.", betonte Amira.

Rash lächelte. "Du küsst mich nicht nass und du
fasst mir nicht zwischen die Beine.", fuhr
Rash fort. "Und schließlich gibt es noch ein Aus-Wort. Wenn
eine Person von uns 'Rot' sagt, hören wir mit allem auf."

Das war beruhigend.

"Das war es.", sagte Rash. "Möchtest du mir ein Messer an
den Hals legen, einfach, um herauszufinden, wie es sich anfühlt?"

"Ich weiß, wie sich das anfühlt.", sagte Amira, vielleicht
eine Spur zu energisch.

"Mit nichts anderem hätte ich gerechnet. Aber weißt du, wie
es sich anfühlt, wenn dir eigentlich keine Gefahr
droht und dir deswegen niemand böse ist?", fragte Rash.

Es störte Amira, dass Rash weiterhin so etwas wie die Oberhand
hatte. Es würde sich nicht unbedingt ändern, wenn sie Rash
das Messer an den Hals legte. Aber es war deshalb verlockend, es
zu tun. Rash hatte viel mehr Erfahrung und Wissen und sie
daher gefühlt ein wenig im Griff. Es wurde Zeit für mehr
Gleichgewicht. Vielleicht waren das unsinnige Gedanken. Aber
trotzdem trat Amira noch einen Schritt näher an Rash heran, sodass
sie Rashs Atem im Gesicht spüren konnte. Rash war ein wenig größer
als sie.

Sie führte die Klinge behutsam an Rashs Hals, beobachtete, wie
der Kehlkopf sich dabei bewegte, war sehr vorsichtig. Aber
auch nicht so vorsichtig, als wäre sie nicht überzeugt von dem, was sie tat. Mehr
eine erforschende Vorsicht. Rash atmete etwas schneller. Amira legte
Rash eine Hand auf die Schulter und drückte den Elben sehr sachte etwas
nach unten. Rash folgte der unausgesprochenen Aufforderung
ohne Zögern. Fügsam. Es war ein gutes Gefühl, nun endlich Rash ein wenig im
Griff zu haben. Nicht mehr das Gefühl zu haben, ausgeliefert
zu sein, und das -- wie Rash erklärt hatte --, ohne die Angst,
dass ihr das übel genommen würde.

Rashs Atem zitterte. Amira erinnerte sich daran, dass Rash
erzählt hatte, die Kapitänin würde Rash bedrohen, und dass
Rash darauf stünde. Amira tat also hier nicht nur etwas, was
ihr Sicherheit gab, sondern auch etwas, was Rash gefiel. Und
Befehle gefielen Rash auch, erinnerte sie sich. "Fass mich
zärtlich an.", befahl sie.

"Wo?", fragte Rash. Das war nicht mehr diese selbstsichere
Person von eben. Da war ein Zittern in der unsicheren
Stimme. Aber auch ein Genießen, das Amira beruhigte.

Die Frage war schwierig. Amira fielen alle möglichen
Stellen ein. Eigentlich wollte sie es nicht selbst
entscheiden. Ein weiteres Mal stritt es sich in ihr. Sie
hatte einen Drang etwas sehr Fieses zu sagen, aber Fiessein
war eigentlich etwas, was sie ablegen wollte. "Magst du, wenn
Leute fies zu dir sind?", fragte sie.

"Oh ja.", antwortete Rash atemlos. "Sehr." Und fügte
ein halb geflehtes "Bitte." hinzu.

Amira strich sanft und kontrolliert mit der Klinge
über Rashs Hals. Sie wusste, was sie tat. Und sie genoss
die Furcht, die dadurch in Rashs Gesicht trat. Sie
hatte so etwas noch nie genossen. Aber jetzt, jetzt gehörte
es hier hin. "An den richtigen Stellen.", sagte sie leise. "Sofort."

"Welches sind die richtigen Stellen?", flüsterte Rash mit
einer Verunsicherung, die Amira genoss.

Amira verzog das Gesicht zu einem Ausdruck, der sich für sie streng
anfühlte. "Das musst du wissen. Du weißt doch sonst einfach
alles."

Und dann spürte sie Rashs Hände sanft an ihrer Hüfte. An ihrem
muskulösen, durchtrainierten Körper. Der das erste Mal für
zärtliche Hände durchtrainiert war, und nicht, weil es ihr
Job war. Der das erste Mal genossen und lieb gehabt wurde. So
fühlte sie sich, als Rashs Hände langsam um ihre Taille
streichelten.

"Darf ich unter deiner Kleidung streicheln?", fragte Rash.

Amira war die Stimme wieder zu selbstsicher. Sie presste Rash noch
etwas mehr mit dem Körper gegen das Pult und drückte Rash an den Schultern noch ein
Stück herab, sodass sie von oben in Rashs Gesicht blickte. Die
Unterwürfigkeit flackerte wieder darin auf. Gut so. Und doch nickte
sie einmal. "Tu es.", sagte sie. "Und mach es richtig!"

Rash atmete noch etwas schneller, als Rashs Finger nach den komplizierten
Verschlüssen ihrer Kleidung tasteten und sich unter den Stoff
drängten. Amira wollte die Augen schließen, nur die zarten, liebevollen
Berührungen fühlen, aber das ging nicht. Wie konnte außerdem eine
Person, die sie so wenig kannte, ihr so sehr das Gefühl geben, geliebt
zu werden? Vielleicht liebte Rash nur ihren Körper. Und vielleicht
war das "nur" in dem Satz auch einfach schlecht und konnte
weg. Rash liebte ihren Körper. Und vielleicht war dieses Lieben nichteinmal
mit einer Emotion bei Rash verbunden sondern eine reine Handlung. Auch
das war genug. Da gab es nichts Schlechtes
dran. Die Finger rannen erforschend, aber
nicht drängend über Amiras Rücken und Seite, versuchten vorsichtig
höher unter die dafür zu enge Kleidung zu gelangen.

Amira hatte immer geliebt, den Rücken gestreichelt zu bekommen. Irgendwo
weit aus der Tiefe drangen diese Erinnerungen wieder hoch.

Sie konnte die Augen nicht schließen, während sie ein Messer an einen
zarten Hals hielt. Sie nahm es weg und Rash hielt in der Streichelbewegung
inne, der Blick auf einmal besorgt. "Ist etwas schlecht?", fragte
Rash.

"Kannst du dich darauf verlassen, dass ich dich ermorden könnte und
würde, wenn du nicht tust, was ich will, wenn ich mein Messer
nicht an deiner Kehle habe?", fragte Amira.

Rash schluckte. "Und würde?"

"Das war Teil des Spiels." Eine Welle von Selbstabscheu durchrann
Amira. Sie hätte das nicht sagen sollen. Das war schlimm. Sie befreite
sich von Rash und lehnte sich an die Wand zurück, an der sie vorhin
gestanden hatte.

Rash stand sofort ihr gegenüber. "Es ist in Ordnung.", versicherte
Rash.

Aber das war es nicht. Ganz und gar nicht.

"Ich habe dich in die Ecke gedrängt.", schob Rash nach.

"Du mich?", fragte Amira. Es machte sie fast wütend.

"Diese Spiele sind nicht ohne.", erklärte Rash. "Ich bin in einen
Sog geraten. Es fühlte sich so an, als ob ich wisse, was
du wolltest. Und ich wollte es. Aber wir haben nicht ausgiebig
vorher darüber gesprochen. Wenn wir so ein gefährliches Spiel spielen, ohne
es ausgiebig vorzubereiten, gut genug absprechen, was zum
Spiel gehört und was nicht, dann gehört so etwas noch zu den
harmloseren Dingen, die passieren können."

"Mich nervt, dass du so viel weißt. Ich wollte Kontrolle haben.", sagte
Amira. "Irgendeine."

Rash nickte. "Und das ist auch ein wichtiger Punkt, der gefährlich
ist. Denn eigentlich weiß ich nichts über dich." Rash seufzte. "Ich hätte
nicht sagen sollen, dass du kuscheln willst. Ich
habe manchmal das Bedürfnis, dich in den Arm zu
nehmen. Weil du sehr so wirkst, als würde es dir nicht gut gehen. Und
dann reiße ich mich sehr zusammen, es nicht zu tun. Daraus ist vielleicht
die Idee entsprungen, du könntest einfach nur kuscheln wollen."

Amira blickte Rash lange an. Vielleicht wollte sie das. In
den Arm genommen werden von dieser Person. "Vielleicht sollte
ich meine Skepsis fallen lassen, weil du recht hast und ich
es will." Aber es fühlte sich nicht
richtig an.

"Es gibt diese Gefahr, dass, was ich darlege, quasi die Funktion
einer Regie-Anweisung bekommen kann.", wandt Rash ein. "Besonders
wenn ich so rüberkomme, als wüsste ich vieles, und dann noch
einmal besonders, weil ich erfahren bin und du in diesem Bereich
kaum. Das stimmt doch, oder?"

Amira nickte stirnrunzelnd.

"In solchen
Konstellationen gibt es häufiger das Problem, dass geäußerte Ideen,
was der weniger erfahrenen Person gefallen könnte, auslösen, dass sie
damit den eigenen Willen oder wenigstens eigentlich gesunde Hemmungen
überschreiben. Du hast gefragt, was deine Gefühle bedeuten, was
eine Unsicherheit suggeriert, als wüsstest du eben selbst nicht genau,
was du willst. Und wenn ich dir dann sage, was du willst... Ich
habe Angst, dass du dann deshalb daran glaubst, dass ich
recht hätte, weil du noch keine bessere Antwort hast, obwohl
eine andere Antwort richtiger wäre." Rash
verhedderte sich in ein paar Füllwörtern. "Ich bekomme
das nicht unkompliziert ausgedrückt und weiß auch gar nicht, ob es
zwischen uns ein Problem ist. Aber allein die Möglichkeit macht
die Situation gefährlich und erzeugt ein Machtgefälle zwischen uns."

"Ich weiß, was du meinst." Es fühlte sich erleichternd für Amira
an, dass Rash das aussprach. Auch hier hatte Rash Recht. Obwohl: nicht
ganz. Es erklärte einen Mechanismus, den Amira in sich anspringen
gefühlt hatte, aber es war nicht so, dass Amiras Wille sich einfach
Rashs Vorschlägen gefügt hätte. "Ich
denke, du schätzt eigentlich richtig ein, was ich möchte. Aber
ich entscheide mich schneller dazu, als ich mich eigentlich
darauf einstellen kann. Bevor ich mich sicher fühle."

"Das ist nicht gut, aber gut, dass du das sagst.", stellte Rash
fest.

Sie schwiegen eine Weile und es war nicht das behaglichste Schweigen. Amira
fühlte sich unruhig. Die Situation war unaufgelöst. Ein Teil von
ihr wollte ungefähr dahin, wo sie eben noch gewesen waren, aber
anders, sicherer.

"Warum wolltest du, dass ich Gefahr wahrnehme, ohne dass du
ein Messer an mich legst?", fragte Rash schließlich.

"Weil du eben recht hast!" Die Wut überwältigte Amira wie aus
dem Nichts. "Weil ich kuscheln will. Einfach bloß kuscheln. Mich
dabei entspannen. Was ich nicht kann, wenn ich mit einem Messer
in der Hand aufpassen muss. Aber ich hasse es, -- immer noch! --, dass
du so viel über mich und alles weißt,
so viel Kontrolle hast. Ich mag es nicht, wenn das keinen Gegenpol
bekommt, der sich für mich nach Kontrolle und Ausgleich anfühlt." Amira holte
Luft und beruhigte sich etwas. Wut empfand sie sehr selten so
unkontrolliert und stark. "Es tut mir leid. Ich wollte dich nicht
anschreien. Das ist ungerecht. Weil ich tatsächlich genossen habe. Kurz."

Rashs Körper hatte am Anfang des Ausbruchs zwar kurz
gezuckt, aber wirkte nun wieder ganz weich. "Ich
bin so mutig, dich zu fragen, ob ich dir ein weiteres Spiel
vorschlagen darf."

Amira nickte ohne Zögern. Einen Moment fragte sie sich, ob sie hätte
zögern sollen, aber sie war sich recht schnell sicher, dass sie wenn,
erst später zögern wollte, wenn sie wusste, worum es ging.

"Ich kann mir vorstellen, dass es uns gefallen könnte, an einem ruhigen Ort zu
liegen, wenn dein Messer bereit für dich neben dir liegt
und du mich, wie du das gemeint hast, jederzeit damit bedrohen oder angreifen
könntest. Und wo du vielleicht in Ruhe herausfinden kannst, was du willst."

"Ich weiß keinen ruhigeren Ort, als diesen.", sagte Amira.

Rash lächelte. "Wir bekommen bestimmt die Kapitänskajüte, wenn
wir lieb fragen."

Amira schluckte. Damit hatte sie beim besten Willen nicht
gerechnet. Aber wenn Rash das sagte, stimmte das
wohl.

"Wenn es dich sicherer fühlen lässt, darfst du mir die Beine fesseln.", fügte
Rash hinzu. "Und deine Dominanz ist wunderschön. Ich mag die Momente, in
denen du mich tatsächlich klein kriegst."

Bei den letzten Worten lächelte Amira.

---

Nicht allzuviel später waren sie tatsächlich in der Kapitänskajüte für
sich. Amira war Rashs Vorschlag nachgekommen, Rashs Beine zu fesseln. Seil
gab es an Bord genug. Es war anderes Seil, als Amira es gewohnt war, aber
das machte ja nichts. Sie strich noch einmal sachte mit dem Messer
über Rashs Hals, nur um die Reaktion zu sehen, sich sicher zu sein, dass
Rash verstand, dass Amira das Sagen hatte, bevor sie es griffbereit an die
Seite legte. "Zieh mir den Oberkörper aus!", befahl sie. Dann hielt sie
Rash doch noch einmal auf -- indem sie Rash einfach mit der Hand auf Rashs
Brustbein wieder zu Boden drückte. Zunächst entfernte Amira die vielen
Messer aus der Kleidung, die Rash ihr gleich ausziehen würde.

Rash schmunzelte. "Weißt du, dass du unbeschreiblich schön bist?", hauchte
Rash.

Amira warf Rash einen bösen Blick zu und das reichte auch schon, um Rash
wieder unterwürfig zu stimmen. Warum mochte sie das so?

"Jetzt.", sagte sie, das letzte Messer aus der Oberbekleidung noch in der Hand. "Auch
das Kopftuch." Sie mochte es tragen.
Sie fühlte sich damit mehr bei sich und sicherer,
selbstbewusster. Aber der Gedanke von Rashs Händen in ihrem
Haar fühlte sich gut an. Es ging gerade um etwas anderes. Um
etwas, wo dieses Selbstbewusstsein anderswoher kam, wenn
überhaupt.

Sie legte doch wieder das
letzte Messer, das sie noch immer in der Hand hielt, sachte an
Rashs Kehle. Wieso genoss sie auf einmal so sehr, das zu tun? Vielleicht, weil
Rash es mochte. Vielleicht, weil es nun nichts Böses bedeutete. Vielleicht
erinnerte es sie an Trainingszeiten, wo es auch noch keine
so schlimme Bedeutung gehabt hatte, sondern lediglich
bewertet worden war, ob sie es gut machte.

Rash machte sich unsicher an ihrer Kleidung zu schaffen. Amira
mochte die Unsicherheit. Und dann, als sie halb nackt war, legte
sie sich neben Rash, legte das Messer hinter sich und sich
selbst in Rashs Arme. "Streichel mir den Rücken.", befahl sie
flüsternd. "Mach es richtig."

Rash tat es richtig. Nicht einen Moment fühlte Amira sich dabei
falsch oder ausgenutzt. Rash streichelte ihr liebevoll über
den Rücken. Sachte. Als würden sie sich schon lange kennen. Langsam
und einfühlsam, sodass Amira jede Berührung auskosten konnte. Warum
musste sie eine Person dafür bedrohen, sich diese Zuwendung
zugestehen zu dürfen? War das richtig interpretiert? Gestand
sie sich das sonst nicht zu? Oder war es, dass sie Rash
eben eigentlich nicht vertraute?

Sie vergaß die Messer hinter sich, ließ
los, atmete. Und dann drängte ein riesiger Kloß durch ihren Hals
nach oben. Ihr Körper begann zu zittern, als sie versuchte, das
Weinen mit Gewalt zu unterdrücken. Rash wurde noch zartfühlender. Übernahm
nicht Kontrolle, wie zuvor. Flüsterte in ihr Ohr: "Du darfst jederzeit
'rot' sagen."

Aber Amira sagte nicht 'rot'. Sie versuchte, dagegen anzukämpfen, aber
irgendwann drängte sich ihr Körper von selber fester in Rashs Arme. Ihr
Kopf legte sich unter Rashs Kinn. Rashs Halsbeuge war nass geheult, von den
kleinen Tränen, die sich an ihrem Willen, nicht zu heulen, vorbeigedrängt
hatten. Sie fühlte ihre eigenen Tränen auf ihrem Gesicht, als
sie es an den Hals anlehnte, an dem vorhin noch ihr Messer
gelegen hatte. Zarte, verletzliche, nasse Haut. Und schließlich ließ sie
los, heulte. Sie verließ sich darauf, dass
niemand sie verurteilen würde, obwohl das laute Schluchzen sicher draußen hörbar
war.

Die Tür öffnete sich einen Spaltbreit und sie hörte die
gelassene Stimme der Kapitänin: "Braucht ihr Hilfe?"

Sie spürte Rashs eine zärtliche Hand an ihrer Schläfe und den
Atem in ihrem Ohr, als Rash raunte: "Hast du das
Bedürfnis, dass wir etwas anders machen als gerade?"

Es war eine gute Frage, weil Amira nur den Kopf zu schütteln brauchte.

Auch die Kapitänin musste es gesehen haben. "Ich schaue nachher noch
einmal vorbei.", sagte sie und schloss die Tür.

Die Störung hatte dafür gesorgt, dass Amira sich etwas
zusammenriss. Aber mit Rashs zarten Fingern, die über ihr
Haar und ihren Rücken wanderten, überrollte sie eine weitere Welle. Welle
um Welle, wie das Meer, bis sie nicht mehr heulen konnte und erschöpft
in Rashs Armen lag.

Rash fragte nichts. Und Amira sagte nichts. Aber sie vermutete, dass
sie beide nicht mehr daran glaubten, dass es ein halbes Jahr brauchen
würde, bis sie sich Rash anvertrauen würde. Nur, würde es irgendwann
auch umgekehrt passieren?
